<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.4">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="no" active="yes"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="no" active="yes"/>
<layer number="118" name="BACKMAAT2" color="7" fill="1" visible="no" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="no" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="no" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="no" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="14" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="14" fill="2" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="14" fill="4" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="SparkFun">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
We've spent an enormous amount of time creating and checking these footprints and parts. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; CC v3.0 Share-Alike You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="5V">
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<text x="-1.016" y="3.556" size="1.778" layer="96">&gt;VALUE</text>
<pin name="5V" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="5V">
<gates>
<gate name="G$1" symbol="5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="akizuki">
<packages>
<package name="MUSB-5BF01AS">
<wire x1="-3.85" y1="0" x2="3.85" y2="0" width="0.127" layer="51"/>
<wire x1="-3.85" y1="0" x2="-3.85" y2="9.5" width="0.127" layer="51"/>
<wire x1="-3.85" y1="9.5" x2="3.85" y2="9.5" width="0.127" layer="51"/>
<wire x1="3.85" y1="9.5" x2="3.85" y2="0" width="0.127" layer="51"/>
<pad name="3" x="0" y="9.5" drill="0.5"/>
<pad name="5" x="1.6" y="9.5" drill="0.5"/>
<pad name="1" x="-1.6" y="9.5" drill="0.5"/>
<pad name="2" x="-0.8" y="8.3" drill="0.5"/>
<pad name="4" x="0.8" y="8.3" drill="0.5"/>
<pad name="S2" x="-3.7" y="3.85" drill="0.8" diameter="1.4224" shape="long" rot="R90"/>
<wire x1="-4.1" y1="3.2" x2="-4.1" y2="4.5" width="0" layer="46"/>
<wire x1="-4.1" y1="4.5" x2="-3.3" y2="4.5" width="0" layer="46" curve="-180"/>
<wire x1="-3.3" y1="4.5" x2="-3.3" y2="3.2" width="0" layer="46"/>
<wire x1="-3.3" y1="3.2" x2="-4.1" y2="3.2" width="0" layer="46" curve="-180"/>
<pad name="S3" x="3.7" y="3.85" drill="0.8" diameter="1.4224" shape="long" rot="R90"/>
<wire x1="3.3" y1="3.2" x2="3.3" y2="4.5" width="0" layer="46"/>
<wire x1="3.3" y1="4.5" x2="4.1" y2="4.5" width="0" layer="46" curve="-180"/>
<wire x1="4.1" y1="4.5" x2="4.1" y2="3.2" width="0" layer="46"/>
<wire x1="4.1" y1="3.2" x2="3.3" y2="3.2" width="0" layer="46" curve="-180"/>
<pad name="S1" x="-3.7" y="8.6" drill="0.8" shape="long" rot="R90"/>
<wire x1="-4.1" y1="7.95" x2="-4.1" y2="9.25" width="0" layer="46"/>
<wire x1="-4.1" y1="9.25" x2="-3.3" y2="9.25" width="0" layer="46" curve="-180"/>
<wire x1="-3.3" y1="9.25" x2="-3.3" y2="7.95" width="0" layer="46"/>
<wire x1="-3.3" y1="7.95" x2="-4.1" y2="7.95" width="0" layer="46" curve="-180"/>
<pad name="S4" x="3.7" y="8.6" drill="0.8" shape="long" rot="R90"/>
<wire x1="3.3" y1="7.95" x2="3.3" y2="9.25" width="0" layer="46"/>
<wire x1="3.3" y1="9.25" x2="4.1" y2="9.25" width="0" layer="46" curve="-180"/>
<wire x1="4.1" y1="9.25" x2="4.1" y2="7.95" width="0" layer="46"/>
<wire x1="4.1" y1="7.95" x2="3.3" y2="7.95" width="0" layer="46" curve="-180"/>
<wire x1="-3.85" y1="1.7" x2="-3.85" y2="0" width="0.254" layer="21"/>
<wire x1="-3.85" y1="0" x2="3.85" y2="0" width="0.254" layer="21"/>
<wire x1="3.85" y1="0" x2="3.85" y2="1.7" width="0.254" layer="21"/>
<wire x1="-3.85" y1="6.6" x2="-3.85" y2="5.85" width="0.254" layer="21"/>
<wire x1="3.85" y1="6.6" x2="3.85" y2="5.85" width="0.254" layer="21"/>
<text x="-2.2225" y="2.54" size="1.016" layer="21" ratio="15">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="USB-MINI-B-TH">
<wire x1="-2.54" y1="15.24" x2="10.16" y2="15.24" width="0.254" layer="94"/>
<wire x1="10.16" y1="15.24" x2="10.16" y2="-12.7" width="0.254" layer="94"/>
<wire x1="10.16" y1="-12.7" x2="-2.54" y2="-12.7" width="0.254" layer="94"/>
<text x="-1.778" y="-1.524" size="2.54" layer="94" rot="MR90">USB</text>
<pin name="D+" x="12.7" y="7.62" visible="pin" length="short" rot="R180"/>
<pin name="D-" x="12.7" y="10.16" visible="pin" length="short" rot="R180"/>
<pin name="VBUS" x="12.7" y="12.7" visible="pin" length="short" direction="pwr" rot="R180"/>
<pin name="GND" x="12.7" y="2.54" visible="pin" length="short" direction="pwr" rot="R180"/>
<pin name="ID" x="12.7" y="5.08" visible="pin" length="short" rot="R180"/>
<pin name="MTN1" x="12.7" y="-2.54" visible="pin" length="short" direction="pas" rot="R180"/>
<pin name="MTN2" x="12.7" y="-5.08" visible="pin" length="short" direction="pas" rot="R180"/>
<pin name="MTN3" x="12.7" y="-7.62" visible="pin" length="short" direction="pas" rot="R180"/>
<pin name="MTN4" x="12.7" y="-10.16" visible="pin" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="USB-MINI-B-TH" prefix="USB">
<gates>
<gate name="G$1" symbol="USB-MINI-B-TH" x="-2.54" y="-2.54"/>
</gates>
<devices>
<device name="" package="MUSB-5BF01AS">
<connects>
<connect gate="G$1" pin="D+" pad="3"/>
<connect gate="G$1" pin="D-" pad="2"/>
<connect gate="G$1" pin="GND" pad="5"/>
<connect gate="G$1" pin="ID" pad="4"/>
<connect gate="G$1" pin="MTN1" pad="S1"/>
<connect gate="G$1" pin="MTN2" pad="S2"/>
<connect gate="G$1" pin="MTN3" pad="S3"/>
<connect gate="G$1" pin="MTN4" pad="S4"/>
<connect gate="G$1" pin="VBUS" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="rc-master">
<description>&lt;b&gt;R/C MASTER! - v1.07 (07/03/2007)&lt;/b&gt;&lt;p&gt;
&lt;p&gt;This library is a collection of SMD and through hole resistors and capacitors by various manufacturers. Some of these include types from Panasonic, Kemet, BC Components and others. Many of the packages were adapted from &lt;b&gt;rcl.lbr&lt;/b&gt; and &lt;b&gt;cap-pan40.lbr&lt;/b&gt; and have been renamed and grouped in a more logical form.&lt;/p&gt;&lt;p&gt;Silkscreen elements are a minimum of 8 mils in width with larger components using 10 mil widths. Most of the SMD components have text sizes of 0 .04" and thru-hole components are 0.05" in size.  A text ratio of 14 is used in both cases. Where practical, the elements are aligned on a 12.5 mil or 6.25 mil grid depending on the pad size and spacing.&lt;/p&gt;
&lt;p&gt;&lt;h4&gt;All capacitors are grouped by the following naming conventions:&lt;/h4&gt;&lt;/p&gt;
&lt;table width="380" border="1" bordercolor="#000000"&gt;
  &lt;tr&gt; 
    &lt;td width="81" bgcolor="#33CCFF"&gt;&lt;div align="center"&gt;&lt;strong&gt;Prefix&lt;/strong&gt;&lt;/div&gt;&lt;/td&gt;
    &lt;td width="289" bgcolor="#33CCFF"&gt;&lt;div align="center"&gt;&lt;strong&gt;Description&lt;/strong&gt;&lt;/div&gt;&lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt; 
    &lt;td&gt;&lt;div align="center"&gt;CBP_&lt;/div&gt;&lt;/td&gt;
    &lt;td&gt;&lt;div align="center"&gt;Bipolar Electrolytic Types&lt;/div&gt;&lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt; 
    &lt;td&gt;&lt;div align="center"&gt;CCA_&lt;/div&gt;&lt;/td&gt;
    &lt;td&gt;&lt;div align="center"&gt;Chip Cap Array Types&lt;/div&gt;&lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt; 
    &lt;td&gt;&lt;div align="center"&gt;CP_&lt;/div&gt;&lt;/td&gt;
    &lt;td&gt;&lt;div align="center"&gt;Polarized Electrolytic/Tantalum Types&lt;/div&gt;&lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt; 
    &lt;td&gt;&lt;div align="center"&gt;C_&lt;/div&gt;&lt;/td&gt;
    &lt;td&gt;&lt;div align="center"&gt;Non-Polarized Film / Chip Types&lt;/div&gt;&lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt; 
    &lt;td&gt;&lt;div align="center"&gt;FB_&lt;/div&gt;&lt;/td&gt;
    &lt;td&gt;&lt;div align="center"&gt;Simple Ferrite Bead Types&lt;/div&gt;&lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt; 
    &lt;td&gt;&lt;div align="center"&gt;L_&lt;/div&gt;&lt;/td&gt;
    &lt;td&gt;&lt;div align="center"&gt;Simple Chip Inductors&lt;/div&gt;&lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt; 
    &lt;td&gt;&lt;div align="center"&gt;R_&lt;/div&gt;&lt;/td&gt;
    &lt;td&gt;&lt;div align="center"&gt;Resistor Types&lt;/div&gt;&lt;/td&gt;
  &lt;/tr&gt;
&lt;/table&gt;
&lt;p&gt;
&lt;p&gt;&lt;author&gt;THIS LIBRARY IS PROVIDED AS IS AND WITHOUT WARRANTY OF ANY KIND, EXPRESSED OR IMPLIED.&lt;br&gt;Copyright (C) 2007, Bob Starr&lt;br&gt; http://www.bobstarr.net&lt;br&gt;
&lt;/author&gt;</description>
<packages>
<package name="C1812">
<description>&lt;b&gt;SMD CHIP CAP&lt;/b&gt;&lt;p&gt;
1812, grid 0.0125 inch</description>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="-3.0605" y1="1.7768" x2="3.0606" y2="1.7769" width="0.0508" layer="39"/>
<wire x1="3.0606" y1="1.7769" x2="3.0606" y2="-1.7766" width="0.0508" layer="39"/>
<wire x1="3.0606" y1="-1.7766" x2="-3.0605" y2="-1.7767" width="0.0508" layer="39"/>
<wire x1="-3.0605" y1="-1.7767" x2="-3.0605" y2="1.7768" width="0.0508" layer="39"/>
<wire x1="-3.175" y1="1.9844" x2="3.175" y2="1.9844" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.9844" x2="3.175" y2="-1.9844" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.9844" x2="-3.175" y2="-1.9844" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="-1.9844" x2="-3.175" y2="1.9844" width="0.2032" layer="21"/>
<smd name="1" x="-1.905" y="0" dx="1.905" dy="3.4036" layer="1"/>
<smd name="2" x="1.905" y="0" dx="1.905" dy="3.4036" layer="1"/>
<text x="-2.8575" y="2.3813" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.8575" y="-3.3338" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.3175" y1="-0.7" x2="0.3175" y2="0.7" layer="35"/>
</package>
<package name="C1812K">
<description>&lt;b&gt;SMD CHIP CAP&lt;/b&gt; - KEMET 1812 Reflow solder&lt;p&gt;
Metric Code Size 4532</description>
<wire x1="-2.175" y1="1.525" x2="2.175" y2="1.525" width="0.1016" layer="51"/>
<wire x1="2.175" y1="-1.525" x2="-2.175" y2="-1.525" width="0.1016" layer="51"/>
<wire x1="-3.2192" y1="1.9355" x2="3.2193" y2="1.9355" width="0.0508" layer="39"/>
<wire x1="3.2193" y1="1.9355" x2="3.2193" y2="-1.9355" width="0.0508" layer="39"/>
<wire x1="3.2193" y1="-1.9355" x2="-3.2192" y2="-1.9355" width="0.0508" layer="39"/>
<wire x1="-3.2192" y1="-1.9355" x2="-3.2192" y2="1.9355" width="0.0508" layer="39"/>
<wire x1="-3.4131" y1="2.1431" x2="3.4131" y2="2.1431" width="0.2032" layer="21"/>
<wire x1="3.4131" y1="2.1431" x2="3.4131" y2="-2.1431" width="0.2032" layer="21"/>
<wire x1="3.4131" y1="-2.1431" x2="-3.4131" y2="-2.1431" width="0.2032" layer="21"/>
<wire x1="-3.4131" y1="-2.1431" x2="-3.4131" y2="2.1431" width="0.2032" layer="21"/>
<smd name="1" x="-2.2225" y="0" dx="1.8" dy="3.7" layer="1"/>
<smd name="2" x="2.2225" y="0" dx="1.8" dy="3.7" layer="1"/>
<text x="-2.8575" y="2.54" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.8575" y="-3.175" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.25" y1="-1.6" x2="-1.65" y2="1.6" layer="51"/>
<rectangle x1="1.65" y1="-1.6" x2="2.25" y2="1.6" layer="51"/>
<rectangle x1="-0.3175" y1="-0.7" x2="0.3175" y2="0.7" layer="35"/>
</package>
<package name="C1825">
<description>&lt;b&gt;SMD CHIP CAP&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="-3.0605" y1="3.3643" x2="3.0606" y2="3.3643" width="0.0508" layer="39"/>
<wire x1="3.0606" y1="3.3643" x2="3.0606" y2="-3.523" width="0.0508" layer="39"/>
<wire x1="3.0606" y1="-3.523" x2="-3.0605" y2="-3.523" width="0.0508" layer="39"/>
<wire x1="-3.0605" y1="-3.523" x2="-3.0605" y2="3.3643" width="0.0508" layer="39"/>
<wire x1="-3.175" y1="3.7307" x2="3.175" y2="3.7307" width="0.2032" layer="21"/>
<wire x1="3.175" y1="3.7307" x2="3.175" y2="-3.7306" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-3.7306" x2="-3.175" y2="-3.7306" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="-3.7306" x2="-3.175" y2="3.7307" width="0.2032" layer="21"/>
<smd name="1" x="-1.905" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.905" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-2.8575" y="4.1275" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.8575" y="-4.7625" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
<package name="C1825K">
<description>&lt;b&gt;SMD CHIP CAP&lt;/b&gt; - KEMET 1825 Reflow solder&lt;p&gt;
Metric Code Size 4564</description>
<wire x1="-1.525" y1="3.125" x2="1.525" y2="3.125" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-3.125" x2="-1.525" y2="-3.125" width="0.1016" layer="51"/>
<wire x1="-2.5842" y1="3.523" x2="2.5843" y2="3.523" width="0.0508" layer="39"/>
<wire x1="2.5843" y1="3.523" x2="2.5843" y2="-3.5231" width="0.0508" layer="39"/>
<wire x1="2.5843" y1="-3.5231" x2="-2.5842" y2="-3.5231" width="0.0508" layer="39"/>
<wire x1="-2.5842" y1="-3.5231" x2="-2.5842" y2="3.523" width="0.0508" layer="39"/>
<wire x1="-2.8575" y1="3.81" x2="2.8575" y2="3.81" width="0.2032" layer="21"/>
<wire x1="2.8575" y1="3.81" x2="2.8575" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="2.8575" y1="-3.81" x2="-2.8575" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="-2.8575" y1="-3.81" x2="-2.8575" y2="3.81" width="0.2032" layer="21"/>
<smd name="1" x="-1.5875" y="0" dx="1.8" dy="6.9" layer="1"/>
<smd name="2" x="1.5875" y="0" dx="1.8" dy="6.9" layer="1"/>
<text x="-2.2225" y="4.1275" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.2225" y="-5.08" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-3.2" x2="-1.1" y2="3.2" layer="51"/>
<rectangle x1="1.1" y1="-3.2" x2="1.6" y2="3.2" layer="51"/>
<rectangle x1="-0.3175" y1="-0.635" x2="0.3175" y2="0.635" layer="35"/>
</package>
<package name="C2012">
<description>&lt;b&gt;SMD CHIP CAP&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<wire x1="-1.6317" y1="0.8242" x2="1.6318" y2="0.8242" width="0.0508" layer="39"/>
<wire x1="1.6318" y1="0.8242" x2="1.6318" y2="-0.8243" width="0.0508" layer="39"/>
<wire x1="1.6318" y1="-0.8243" x2="-1.6317" y2="-0.8243" width="0.0508" layer="39"/>
<wire x1="-1.6317" y1="-0.8243" x2="-1.6317" y2="0.8242" width="0.0508" layer="39"/>
<wire x1="-1.905" y1="1.0319" x2="1.905" y2="1.0319" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.0319" x2="1.905" y2="-1.0319" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.0319" x2="-1.905" y2="-1.0319" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="-1.0319" x2="-1.905" y2="1.0319" width="0.2032" layer="21"/>
<smd name="1" x="-0.9525" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.9525" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.0917" y1="-0.7239" x2="-0.3416" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C2220K">
<description>&lt;b&gt;SMD CHIP CAP&lt;/b&gt; - KEMET 2220 Reflow solder&lt;p&gt;
Metric Code Size 5650</description>
<wire x1="-2.725" y1="2.425" x2="2.725" y2="2.425" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-2.425" x2="-2.725" y2="-2.425" width="0.1016" layer="51"/>
<wire x1="-3.6955" y1="2.888" x2="4.0131" y2="2.888" width="0.0508" layer="39"/>
<wire x1="4.0131" y1="2.888" x2="4.0131" y2="-2.888" width="0.0508" layer="39"/>
<wire x1="4.0131" y1="-2.888" x2="-3.6955" y2="-2.888" width="0.0508" layer="39"/>
<wire x1="-3.6955" y1="-2.888" x2="-3.6955" y2="2.888" width="0.0508" layer="39"/>
<wire x1="-3.81" y1="3.0956" x2="4.1275" y2="3.0956" width="0.2032" layer="21"/>
<wire x1="4.1275" y1="3.0956" x2="4.1275" y2="-3.0956" width="0.2032" layer="21"/>
<wire x1="4.1275" y1="-3.0956" x2="-3.81" y2="-3.0956" width="0.2032" layer="21"/>
<wire x1="-3.81" y1="-3.0956" x2="-3.81" y2="3.0956" width="0.2032" layer="21"/>
<smd name="1" x="-2.54" y="0" dx="1.85" dy="5.5" layer="1"/>
<smd name="2" x="2.8575" y="0" dx="1.85" dy="5.5" layer="1"/>
<text x="-3.4925" y="3.4925" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.4925" y="-4.445" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-2.5" x2="-2.2" y2="2.5" layer="51"/>
<rectangle x1="2.2" y1="-2.5" x2="2.8" y2="2.5" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
<package name="C2225K">
<description>&lt;b&gt;SMD CHIP CAP&lt;/b&gt; - KEMET 2225 Reflow solder&lt;p&gt;
Metric Code Size 5664</description>
<wire x1="-2.725" y1="3.075" x2="2.725" y2="3.075" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-3.075" x2="-2.725" y2="-3.075" width="0.1016" layer="51"/>
<wire x1="-3.6955" y1="3.523" x2="3.6956" y2="3.523" width="0.0508" layer="39"/>
<wire x1="3.6956" y1="3.523" x2="3.6956" y2="-3.5231" width="0.0508" layer="39"/>
<wire x1="3.6956" y1="-3.5231" x2="-3.6955" y2="-3.5231" width="0.0508" layer="39"/>
<wire x1="-3.6955" y1="-3.5231" x2="-3.6955" y2="3.523" width="0.0508" layer="39"/>
<wire x1="-3.81" y1="3.7306" x2="3.81" y2="3.7306" width="0.2032" layer="21"/>
<wire x1="3.81" y1="3.7306" x2="3.81" y2="-3.7307" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-3.7307" x2="-3.81" y2="-3.7307" width="0.2032" layer="21"/>
<wire x1="-3.81" y1="-3.7307" x2="-3.81" y2="3.7306" width="0.2032" layer="21"/>
<smd name="1" x="-2.54" y="0" dx="1.85" dy="6.8" layer="1"/>
<smd name="2" x="2.54" y="0" dx="1.85" dy="6.8" layer="1"/>
<text x="-3.4925" y="4.1275" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.4925" y="-5.08" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-3.15" x2="-2.2" y2="3.15" layer="51"/>
<rectangle x1="2.2" y1="-3.15" x2="2.8" y2="3.15" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
<package name="C3216">
<description>&lt;b&gt;SMD CHIP CAP&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<wire x1="-2.5842" y1="0.983" x2="2.5843" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.5843" y1="0.983" x2="2.5843" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.5843" y1="-0.983" x2="-2.5842" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.5842" y1="-0.983" x2="-2.5842" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-2.6988" y1="1.1907" x2="2.6988" y2="1.1907" width="0.2032" layer="21"/>
<wire x1="2.6988" y1="1.1907" x2="2.6988" y2="-1.1906" width="0.2032" layer="21"/>
<wire x1="2.6988" y1="-1.1906" x2="-2.6988" y2="-1.1906" width="0.2032" layer="21"/>
<wire x1="-2.6988" y1="-1.1906" x2="-2.6988" y2="1.1907" width="0.2032" layer="21"/>
<smd name="1" x="-1.5875" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.5875" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-2.2225" y="1.5875" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.2225" y="-2.54" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.7013" y1="-0.8509" x2="-0.9512" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.3" y1="-0.5001" x2="0.3" y2="0.5001" layer="35"/>
</package>
<package name="C3225">
<description>&lt;b&gt;SMD CHIP CAP&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="-2.4255" y1="1.4593" x2="2.4255" y2="1.4593" width="0.0508" layer="39"/>
<wire x1="2.4255" y1="1.4593" x2="2.4255" y2="-1.4593" width="0.0508" layer="39"/>
<wire x1="2.4255" y1="-1.4593" x2="-2.4255" y2="-1.4593" width="0.0508" layer="39"/>
<wire x1="-2.4255" y1="-1.4593" x2="-2.4255" y2="1.4593" width="0.0508" layer="39"/>
<wire x1="-2.4606" y1="1.5875" x2="2.4606" y2="1.5875" width="0.2032" layer="21"/>
<wire x1="2.4606" y1="1.5875" x2="2.4606" y2="-1.5875" width="0.2032" layer="21"/>
<wire x1="2.4606" y1="-1.5875" x2="-2.4606" y2="-1.5875" width="0.2032" layer="21"/>
<wire x1="-2.4606" y1="-1.5875" x2="-2.4606" y2="1.5875" width="0.2032" layer="21"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.2225" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.2225" y="-2.6988" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.7013" y1="-1.2954" x2="-0.9512" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.304" x2="1.7018" y2="1.2959" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="C4532">
<description>&lt;b&gt;SMD CHIP CAP&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="-3.0605" y1="1.7768" x2="3.0605" y2="1.7768" width="0.0508" layer="39"/>
<wire x1="3.0605" y1="1.7768" x2="3.0605" y2="-1.7767" width="0.0508" layer="39"/>
<wire x1="3.0605" y1="-1.7767" x2="-3.0605" y2="-1.7767" width="0.0508" layer="39"/>
<wire x1="-3.0605" y1="-1.7767" x2="-3.0605" y2="1.7768" width="0.0508" layer="39"/>
<wire x1="-3.175" y1="2.0637" x2="3.175" y2="2.0637" width="0.2032" layer="21"/>
<wire x1="3.175" y1="2.0637" x2="3.175" y2="-2.0638" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-2.0638" x2="-3.175" y2="-2.0638" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="-2.0638" x2="-3.175" y2="2.0637" width="0.2032" layer="21"/>
<smd name="1" x="-1.905" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.905" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-2.8575" y="2.54" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.8575" y="-3.175" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="C4564">
<description>&lt;b&gt;SMD CHIP CAP&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="-3.0605" y1="3.523" x2="3.0606" y2="3.523" width="0.0508" layer="39"/>
<wire x1="3.0606" y1="3.523" x2="3.0606" y2="-3.5231" width="0.0508" layer="39"/>
<wire x1="3.0606" y1="-3.5231" x2="-3.0605" y2="-3.5231" width="0.0508" layer="39"/>
<wire x1="-3.0605" y1="-3.5231" x2="-3.0605" y2="3.523" width="0.0508" layer="39"/>
<wire x1="-3.175" y1="3.81" x2="3.175" y2="3.81" width="0.2032" layer="21"/>
<wire x1="3.175" y1="3.81" x2="3.175" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-3.81" x2="-3.175" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="-3.81" x2="-3.175" y2="3.81" width="0.2032" layer="21"/>
<smd name="1" x="-1.905" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.905" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-2.8575" y="4.1275" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.8575" y="-5.08" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="C100-140X060">
<description>&lt;b&gt;NP FILM CAP&lt;/b&gt;&lt;p&gt;
10 mm lead spacing&lt;br&gt; outline 14 x 6 mm&lt;br&gt; grid 0.0125 inch</description>
<wire x1="-0.635" y1="0.9525" x2="-0.635" y2="0" width="0.254" layer="51"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.9525" width="0.254" layer="51"/>
<wire x1="0.635" y1="0.9525" x2="0.635" y2="0" width="0.254" layer="51"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-0.9525" width="0.254" layer="51"/>
<wire x1="-0.635" y1="0" x2="-5.08" y2="0" width="0.2032" layer="51"/>
<wire x1="0.635" y1="0" x2="5.08" y2="0" width="0.2032" layer="51"/>
<wire x1="7" y1="2" x2="6" y2="3" width="0.2032" layer="21" curve="90"/>
<wire x1="6" y1="3" x2="-6" y2="3" width="0.2032" layer="21"/>
<wire x1="-6" y1="3" x2="-7" y2="2" width="0.2032" layer="21" curve="90"/>
<wire x1="-7" y1="2" x2="-7" y2="-2" width="0.2032" layer="21"/>
<wire x1="-7" y1="-2" x2="-6" y2="-3" width="0.2032" layer="21" curve="90"/>
<wire x1="-6" y1="-3" x2="6" y2="-3" width="0.2032" layer="21"/>
<wire x1="6" y1="-3" x2="7" y2="-2" width="0.2032" layer="21" curve="90"/>
<wire x1="7" y1="-2" x2="7" y2="2" width="0.2032" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="7.62" y="-1.905" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="7.62" y="0" size="1.016" layer="25" ratio="18">&gt;NAME</text>
</package>
<package name="C120-150X060">
<description>&lt;b&gt;NP FILM CAP&lt;/b&gt;&lt;p&gt;
12 mm lead spacing&lt;br&gt; outline 15 x 6 mm&lt;br&gt; grid 0.0125 inch</description>
<wire x1="-0.635" y1="1.27" x2="-0.635" y2="0" width="0.254" layer="51"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-1.27" width="0.254" layer="51"/>
<wire x1="0.635" y1="1.27" x2="0.635" y2="0" width="0.254" layer="51"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-1.27" width="0.254" layer="51"/>
<wire x1="0.635" y1="0" x2="6.0325" y2="0" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="0" x2="-6.0325" y2="0" width="0.2032" layer="51"/>
<wire x1="-6.5" y1="3" x2="-7.5" y2="2" width="0.2032" layer="21" curve="90"/>
<wire x1="-7.5" y1="2" x2="-7.5" y2="-2" width="0.2032" layer="21"/>
<wire x1="-7.5" y1="-2" x2="-6.5" y2="-3" width="0.2032" layer="21" curve="90"/>
<wire x1="-6.5" y1="-3" x2="6.5" y2="-3" width="0.2032" layer="21"/>
<wire x1="6.5" y1="-3" x2="7.5" y2="-2" width="0.2032" layer="21" curve="90"/>
<wire x1="7.5" y1="-2" x2="7.5" y2="2" width="0.2032" layer="21"/>
<wire x1="7.5" y1="2" x2="6.5" y2="3" width="0.2032" layer="21" curve="90"/>
<wire x1="6.5" y1="3" x2="-6.5" y2="3" width="0.2032" layer="21"/>
<pad name="1" x="-6.0325" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<pad name="2" x="6.0325" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="8.255" y="-1.905" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="8.255" y="0" size="1.016" layer="25" ratio="18">&gt;NAME</text>
</package>
<package name="C145-180X070">
<description>&lt;b&gt;NP FILM CAP&lt;/b&gt;&lt;p&gt;
14.5 mm lead spacing&lt;br&gt; outline 18 x 7 mm&lt;br&gt; grid 0.0125 inch</description>
<wire x1="-0.635" y1="1.27" x2="-0.635" y2="0" width="0.254" layer="51"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-1.27" width="0.254" layer="51"/>
<wire x1="0.635" y1="1.27" x2="0.635" y2="0" width="0.254" layer="51"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-1.27" width="0.254" layer="51"/>
<wire x1="-0.635" y1="0" x2="-7.3025" y2="0" width="0.2032" layer="51"/>
<wire x1="0.635" y1="0" x2="7.3025" y2="0" width="0.2032" layer="51"/>
<wire x1="-8" y1="3.5" x2="-9" y2="2.5" width="0.2032" layer="21" curve="90"/>
<wire x1="-9" y1="2.5" x2="-9" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="-9" y1="-2.5" x2="-8" y2="-3.5" width="0.2032" layer="21" curve="90"/>
<wire x1="-8" y1="-3.5" x2="8" y2="-3.5" width="0.2032" layer="21"/>
<wire x1="8" y1="-3.5" x2="9" y2="-2.5" width="0.2032" layer="21" curve="90"/>
<wire x1="9" y1="-2.5" x2="9" y2="2.5" width="0.2032" layer="21"/>
<wire x1="9" y1="2.5" x2="8" y2="3.5" width="0.2032" layer="21" curve="90"/>
<wire x1="8" y1="3.5" x2="-8" y2="3.5" width="0.2032" layer="21"/>
<pad name="1" x="-7.3025" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<pad name="2" x="7.3025" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="9.8425" y="0" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="9.8425" y="-1.905" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-190X070">
<description>&lt;b&gt;NP FILM CAP&lt;/b&gt;&lt;p&gt;
15 mm lead spacing&lt;br&gt; outline 19 x 7 mm&lt;br&gt; grid 0.0125 inch</description>
<wire x1="-0.635" y1="1.27" x2="-0.635" y2="0" width="0.254" layer="51"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-1.27" width="0.254" layer="51"/>
<wire x1="0.635" y1="1.27" x2="0.635" y2="0" width="0.254" layer="51"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-1.27" width="0.254" layer="51"/>
<wire x1="-0.635" y1="0" x2="-7.62" y2="0" width="0.2032" layer="51"/>
<wire x1="0.635" y1="0" x2="7.62" y2="0" width="0.2032" layer="51"/>
<wire x1="-8.5" y1="3.5" x2="-9.5" y2="2.5" width="0.2032" layer="21" curve="90"/>
<wire x1="-9.5" y1="2.5" x2="-9.5" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="-9.5" y1="-2.5" x2="-8.5" y2="-3.5" width="0.2032" layer="21" curve="90"/>
<wire x1="-8.5" y1="-3.5" x2="8.5" y2="-3.5" width="0.2032" layer="21"/>
<wire x1="8.5" y1="-3.5" x2="9.5" y2="-2.5" width="0.2032" layer="21" curve="90"/>
<wire x1="9.5" y1="-2.5" x2="9.5" y2="2.5" width="0.2032" layer="21"/>
<wire x1="9.5" y1="2.5" x2="8.5" y2="3.5" width="0.2032" layer="21" curve="90"/>
<wire x1="8.5" y1="3.5" x2="-8.5" y2="3.5" width="0.2032" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="10.16" y="0" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="10.16" y="-1.905" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C170-210X070">
<description>&lt;b&gt;NP FILM CAP&lt;/b&gt;&lt;p&gt;
17 mm lead spacing&lt;br&gt; outline 21 x 7 mm &lt;br&gt; grid 0.0125 inch</description>
<wire x1="-0.635" y1="1.27" x2="-0.635" y2="0" width="0.254" layer="51"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-1.27" width="0.254" layer="51"/>
<wire x1="0.635" y1="1.27" x2="0.635" y2="0" width="0.254" layer="51"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-1.27" width="0.254" layer="51"/>
<wire x1="0.635" y1="0" x2="8.5725" y2="0" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="0" x2="-8.5725" y2="0" width="0.2032" layer="51"/>
<wire x1="-9.5" y1="3.5" x2="-10.5" y2="2.5" width="0.2032" layer="21" curve="90"/>
<wire x1="-10.5" y1="2.5" x2="-10.5" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="-10.5" y1="-2.5" x2="-9.5" y2="-3.5" width="0.2032" layer="21" curve="90"/>
<wire x1="-9.5" y1="-3.5" x2="9.5" y2="-3.5" width="0.2032" layer="21"/>
<wire x1="9.5" y1="-3.5" x2="10.5" y2="-2.5" width="0.2032" layer="21" curve="90"/>
<wire x1="10.5" y1="-2.5" x2="10.5" y2="2.5" width="0.2032" layer="21"/>
<wire x1="10.5" y1="2.5" x2="9.5" y2="3.5" width="0.2032" layer="21" curve="90"/>
<wire x1="9.5" y1="3.5" x2="-9.5" y2="3.5" width="0.2032" layer="21"/>
<pad name="1" x="-8.5725" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<pad name="2" x="8.5725" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="11.1125" y="0" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="11.1125" y="-1.905" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C200-230X080">
<description>&lt;b&gt;NP FILM CAP&lt;/b&gt;&lt;p&gt;
20 mm lead spacing&lt;br&gt; outline 23 x 8 mm&lt;br&gt; grid 0.0125 inch</description>
<wire x1="-0.635" y1="0.9525" x2="-0.635" y2="0" width="0.254" layer="51"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.9525" width="0.254" layer="51"/>
<wire x1="0.635" y1="0.9525" x2="0.635" y2="0" width="0.254" layer="51"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-0.9525" width="0.254" layer="51"/>
<wire x1="0.635" y1="0" x2="9.8425" y2="0" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="0" x2="-9.8425" y2="0" width="0.2032" layer="51"/>
<wire x1="11.5" y1="-3" x2="10.5" y2="-4" width="0.2032" layer="21" curve="-90"/>
<wire x1="10.5" y1="-4" x2="-10.5" y2="-4" width="0.2032" layer="21"/>
<wire x1="-10.5" y1="-4" x2="-11.5" y2="-3" width="0.2032" layer="21" curve="-90"/>
<wire x1="-11.5" y1="-3" x2="-11.5" y2="3" width="0.2032" layer="21"/>
<wire x1="-11.5" y1="3" x2="-10.5" y2="4" width="0.2032" layer="21" curve="-90"/>
<wire x1="-10.5" y1="4" x2="10.5" y2="4" width="0.2032" layer="21"/>
<wire x1="10.5" y1="4" x2="11.5" y2="3" width="0.2032" layer="21" curve="-90"/>
<wire x1="11.5" y1="3" x2="11.5" y2="-3" width="0.2032" layer="21"/>
<pad name="1" x="-9.8425" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<pad name="2" x="9.8425" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="12.065" y="-1.905" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="12.065" y="0" size="1.016" layer="25" ratio="18">&gt;NAME</text>
</package>
<package name="C225-260X090">
<description>&lt;b&gt;NP FILM CAP&lt;/b&gt;&lt;p&gt;
22.5 mm lead spacing&lt;br&gt; outline 26 x 9 mm&lt;br&gt; grid 0.0125 inch</description>
<wire x1="-0.635" y1="1.5875" x2="-0.635" y2="0" width="0.254" layer="51"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-1.5875" width="0.254" layer="51"/>
<wire x1="0.635" y1="1.5875" x2="0.635" y2="0" width="0.254" layer="51"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-1.5875" width="0.254" layer="51"/>
<wire x1="-0.635" y1="0" x2="-11.1125" y2="0" width="0.2032" layer="51"/>
<wire x1="0.635" y1="0" x2="11.1125" y2="0" width="0.2032" layer="51"/>
<wire x1="12" y1="-4.5" x2="13" y2="-3.5" width="0.2032" layer="21" curve="90"/>
<wire x1="13" y1="-3.5" x2="13" y2="3.5" width="0.2032" layer="21"/>
<wire x1="13" y1="3.5" x2="12" y2="4.5" width="0.2032" layer="21" curve="90"/>
<wire x1="12" y1="4.5" x2="-12" y2="4.5" width="0.2032" layer="21"/>
<wire x1="-12" y1="4.5" x2="-13" y2="3.5" width="0.2032" layer="21" curve="90"/>
<wire x1="-13" y1="3.5" x2="-13" y2="-3.5" width="0.2032" layer="21"/>
<wire x1="-13" y1="-3.5" x2="-12" y2="-4.5" width="0.2032" layer="21" curve="90"/>
<wire x1="-12" y1="-4.5" x2="12" y2="-4.5" width="0.2032" layer="21"/>
<pad name="1" x="-11.1125" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<pad name="2" x="11.1125" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="13.6525" y="0" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="13.6525" y="-1.905" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-320X100">
<description>&lt;b&gt;NP FILM CAP&lt;/b&gt;&lt;p&gt;
27.5 mm lead spacing&lt;br&gt; outline 32 x 10 mm&lt;br&gt; grid 0.0125 inch</description>
<wire x1="-0.635" y1="1.27" x2="-0.635" y2="0" width="0.254" layer="51"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-1.27" width="0.254" layer="51"/>
<wire x1="0.635" y1="1.27" x2="0.635" y2="0" width="0.254" layer="51"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-1.27" width="0.254" layer="51"/>
<wire x1="0.635" y1="0" x2="13.6525" y2="0" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="0" x2="-13.6525" y2="0" width="0.2032" layer="51"/>
<wire x1="-15" y1="-5" x2="-16" y2="-4" width="0.2032" layer="21" curve="-90"/>
<wire x1="-16" y1="-4" x2="-16" y2="4" width="0.2032" layer="21"/>
<wire x1="-16" y1="4" x2="-15" y2="5" width="0.2032" layer="21" curve="-90"/>
<wire x1="-15" y1="5" x2="15" y2="5" width="0.2032" layer="21"/>
<wire x1="15" y1="5" x2="16" y2="4" width="0.2032" layer="21" curve="-90"/>
<wire x1="16" y1="4" x2="16" y2="-4" width="0.2032" layer="21"/>
<wire x1="16" y1="-4" x2="15" y2="-5" width="0.2032" layer="21" curve="-90"/>
<wire x1="15" y1="-5" x2="-15" y2="-5" width="0.2032" layer="21"/>
<pad name="1" x="-13.6525" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<pad name="2" x="13.6525" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="16.8275" y="0" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="16.8275" y="-1.905" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C035-055X025">
<description>&lt;b&gt;NP FILM CAP&lt;/b&gt;&lt;p&gt; 
3.5 mm lead spacing&lt;br&gt; outline 5.5 x 2.5 mm&lt;br&gt; grid 0.0125 inch</description>
<wire x1="-0.3175" y1="0.635" x2="-0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-0.3175" y2="-0.635" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0.635" x2="0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0" x2="0.3175" y2="-0.635" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-1.5875" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="1.905" y2="0" width="0.2032" layer="51"/>
<wire x1="-2.0988" y1="1.17" x2="-2.5988" y2="0.67" width="0.2032" layer="21" curve="90"/>
<wire x1="-2.5988" y1="0.67" x2="-2.5988" y2="-0.67" width="0.2032" layer="21"/>
<wire x1="-2.5988" y1="-0.67" x2="-2.0988" y2="-1.17" width="0.2032" layer="21" curve="90"/>
<wire x1="-2.0988" y1="-1.17" x2="2.4163" y2="-1.17" width="0.2032" layer="21"/>
<wire x1="2.4163" y1="-1.17" x2="2.9163" y2="-0.67" width="0.2032" layer="21" curve="90"/>
<wire x1="2.9163" y1="-0.67" x2="2.9163" y2="0.67" width="0.2032" layer="21"/>
<wire x1="2.9163" y1="0.67" x2="2.4163" y2="1.17" width="0.2032" layer="21" curve="90"/>
<wire x1="2.4163" y1="1.17" x2="-2.0988" y2="1.17" width="0.2032" layer="21"/>
<pad name="1" x="-1.5875" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<pad name="2" x="1.905" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="3.81" y="-1.5875" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="3.81" y="0" size="1.016" layer="25" ratio="18">&gt;NAME</text>
</package>
<package name="C050-075X030">
<description>&lt;b&gt;NP FILM CAP&lt;/b&gt;&lt;p&gt;
5 mm lead spacing&lt;br&gt; outline 7.5 x 3.0 mm&lt;br&gt; grid 0.0125 inch</description>
<wire x1="-0.3175" y1="0.635" x2="-0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-0.3175" y2="-0.635" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0.635" x2="0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0" x2="0.3175" y2="-0.635" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-2.54" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="2.54" y2="0" width="0.2032" layer="51"/>
<wire x1="-3.75" y1="1" x2="-3.25" y2="1.5" width="0.2032" layer="21" curve="-90"/>
<wire x1="-3.25" y1="1.5" x2="3.25" y2="1.5" width="0.2032" layer="21"/>
<wire x1="3.25" y1="1.5" x2="3.75" y2="1" width="0.2032" layer="21" curve="-90"/>
<wire x1="3.75" y1="1" x2="3.75" y2="-1" width="0.2032" layer="21"/>
<wire x1="3.75" y1="-1" x2="3.25" y2="-1.5" width="0.2032" layer="21" curve="-90"/>
<wire x1="3.25" y1="-1.5" x2="-3.25" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-3.25" y1="-1.5" x2="-3.75" y2="-1" width="0.2032" layer="21" curve="-90"/>
<wire x1="-3.75" y1="-1" x2="-3.75" y2="1" width="0.2032" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" diameter="1.778" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" diameter="1.778" shape="octagon"/>
<text x="4.445" y="0" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="4.445" y="-1.5875" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-110X050">
<description>&lt;b&gt;NP FILM CAP&lt;/b&gt;&lt;p&gt;
7.5 mm lead spacing&lt;br&gt; outline 11 x 5 mm&lt;br&gt; grid 0.0125 inch</description>
<wire x1="-0.3175" y1="0.9525" x2="-0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-0.3175" y2="-0.9525" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0.9525" x2="0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0" x2="0.3175" y2="-0.9525" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-3.81" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="3.81" y2="0" width="0.2032" layer="51"/>
<wire x1="-5.5" y1="2" x2="-5" y2="2.5" width="0.2032" layer="21" curve="-90"/>
<wire x1="-5" y1="2.5" x2="5" y2="2.5" width="0.2032" layer="21"/>
<wire x1="5" y1="2.5" x2="5.5" y2="2" width="0.2032" layer="21" curve="-90"/>
<wire x1="5.5" y1="2" x2="5.5" y2="-2" width="0.2032" layer="21"/>
<wire x1="5.5" y1="-2" x2="5" y2="-2.5" width="0.2032" layer="21" curve="-90"/>
<wire x1="5" y1="-2.5" x2="-5" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="-5" y1="-2.5" x2="-5.5" y2="-2" width="0.2032" layer="21" curve="-90"/>
<wire x1="-5.5" y1="-2" x2="-5.5" y2="2" width="0.2032" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<text x="6.0325" y="-1.905" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="6.0325" y="0" size="1.016" layer="25" ratio="18">&gt;NAME</text>
</package>
<package name="C085-110X050">
<description>&lt;b&gt;NP FILM CAP&lt;/b&gt;&lt;p&gt;
8.5 mm lead spacing&lt;br&gt; outline 11 x 5 mm&lt;br&gt; grid 0.0125 inch</description>
<wire x1="-0.3175" y1="0.9525" x2="-0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-0.3175" y2="-0.9525" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0.9525" x2="0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0" x2="0.3175" y2="-0.9525" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-3.81" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="3.81" y2="0" width="0.2032" layer="51"/>
<wire x1="-5.5" y1="2" x2="-5" y2="2.5" width="0.2032" layer="21" curve="-90"/>
<wire x1="-5" y1="2.5" x2="5" y2="2.5" width="0.2032" layer="21"/>
<wire x1="5" y1="2.5" x2="5.5" y2="2" width="0.2032" layer="21" curve="-90"/>
<wire x1="5.5" y1="2" x2="5.5" y2="-2" width="0.2032" layer="21"/>
<wire x1="5.5" y1="-2" x2="5" y2="-2.5" width="0.2032" layer="21" curve="-90"/>
<wire x1="5" y1="-2.5" x2="-5" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="-5" y1="-2.5" x2="-5.5" y2="-2" width="0.2032" layer="21" curve="-90"/>
<wire x1="-5.5" y1="-2" x2="-5.5" y2="2" width="0.2032" layer="21"/>
<pad name="1" x="-4.1275" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<pad name="2" x="4.1275" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<text x="6.0325" y="0" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="6.0325" y="-1.905" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-310X150">
<description>&lt;b&gt;NP FILM CAP&lt;/b&gt;&lt;p&gt;
27.5 mm lead spacing&lt;br&gt; outline 31 x 15 mm&lt;br&gt; grid 0.0125 inch</description>
<wire x1="-0.635" y1="1.27" x2="-0.635" y2="0" width="0.254" layer="51"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-1.27" width="0.254" layer="51"/>
<wire x1="0.635" y1="1.27" x2="0.635" y2="0" width="0.254" layer="51"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-1.27" width="0.254" layer="51"/>
<wire x1="0.635" y1="0" x2="13.6525" y2="0" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="0" x2="-13.6525" y2="0" width="0.2032" layer="51"/>
<wire x1="14.5" y1="-7.5" x2="15.5" y2="-6.5" width="0.2032" layer="21" curve="90"/>
<wire x1="15.5" y1="-6.5" x2="15.5" y2="6.5" width="0.2032" layer="21"/>
<wire x1="15.5" y1="6.5" x2="14.5" y2="7.5" width="0.2032" layer="21" curve="90"/>
<wire x1="14.5" y1="7.5" x2="-14.5" y2="7.5" width="0.2032" layer="21"/>
<wire x1="-14.5" y1="7.5" x2="-15.5" y2="6.5" width="0.2032" layer="21" curve="90"/>
<wire x1="-15.5" y1="6.5" x2="-15.5" y2="-6.5" width="0.2032" layer="21"/>
<wire x1="-15.5" y1="-6.5" x2="-14.5" y2="-7.5" width="0.2032" layer="21" curve="90"/>
<wire x1="-14.5" y1="-7.5" x2="14.5" y2="-7.5" width="0.2032" layer="21"/>
<pad name="1" x="-13.6525" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<pad name="2" x="13.6525" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="16.1925" y="0" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="16.1925" y="-1.905" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-075X040">
<description>&lt;b&gt;NP FILM CAP&lt;/b&gt;&lt;p&gt;
5 mm lead spacing&lt;br&gt; outline 7.5 x 4.0 mm&lt;br&gt; grid 0.0125 inch</description>
<wire x1="-0.3175" y1="0.635" x2="-0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-0.3175" y2="-0.635" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0.635" x2="0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0" x2="0.3175" y2="-0.635" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-2.54" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="2.54" y2="0" width="0.2032" layer="51"/>
<wire x1="-3.75" y1="-1.5" x2="-3.25" y2="-2" width="0.2032" layer="21" curve="90"/>
<wire x1="-3.25" y1="-2" x2="3.25" y2="-2" width="0.2032" layer="21"/>
<wire x1="3.25" y1="-2" x2="3.75" y2="-1.5" width="0.2032" layer="21" curve="90"/>
<wire x1="3.75" y1="-1.5" x2="3.75" y2="1.5" width="0.2032" layer="21"/>
<wire x1="3.75" y1="1.5" x2="3.25" y2="2" width="0.2032" layer="21" curve="90"/>
<wire x1="3.25" y1="2" x2="-3.25" y2="2" width="0.2032" layer="21"/>
<wire x1="-3.25" y1="2" x2="-3.75" y2="1.5" width="0.2032" layer="21" curve="90"/>
<wire x1="-3.75" y1="1.5" x2="-3.75" y2="-1.5" width="0.2032" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" diameter="1.778" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" diameter="1.778" shape="octagon"/>
<text x="4.445" y="0" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="4.445" y="-1.5875" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C060-090X030">
<description>&lt;b&gt;NP FILM CAP&lt;/b&gt;&lt;p&gt;
6 mm lead spacing&lt;br&gt; outline 9.00 x 3.0 mm&lt;br&gt; grid 0.00625 inch</description>
<wire x1="-0.3175" y1="0.635" x2="-0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-0.3175" y2="-0.635" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0.635" x2="0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0" x2="0.3175" y2="-0.635" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-2.8575" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="2.54" y2="0" width="0.2032" layer="51"/>
<wire x1="-4.375" y1="1" x2="-3.875" y2="1.5" width="0.2032" layer="21" curve="-90"/>
<wire x1="-3.875" y1="1.5" x2="3.875" y2="1.5" width="0.2032" layer="21"/>
<wire x1="3.875" y1="1.5" x2="4.375" y2="1" width="0.2032" layer="21" curve="-90"/>
<wire x1="4.375" y1="1" x2="4.375" y2="-1" width="0.2032" layer="21"/>
<wire x1="4.375" y1="-1" x2="3.875" y2="-1.5" width="0.2032" layer="21" curve="-90"/>
<wire x1="3.875" y1="-1.5" x2="-3.875" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-3.875" y1="-1.5" x2="-4.375" y2="-1" width="0.2032" layer="21" curve="-90"/>
<wire x1="-4.375" y1="-1" x2="-4.375" y2="1" width="0.2032" layer="21"/>
<pad name="1" x="-3" y="0" drill="0.8128" diameter="1.778" shape="octagon"/>
<pad name="2" x="3" y="0" drill="0.8128" diameter="1.778" shape="octagon"/>
<text x="4.7626" y="0" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="4.7626" y="-1.5875" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-080X045">
<description>&lt;b&gt;NP FILM CAP&lt;/b&gt;&lt;p&gt;
5 mm lead spacing&lt;br&gt; outline 8.0 x 4.5 mm&lt;br&gt; grid 0.0125 inch</description>
<wire x1="-0.3175" y1="0.635" x2="-0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-0.3175" y2="-0.635" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0.635" x2="0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0" x2="0.3175" y2="-0.635" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-2.54" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="2.54" y2="0" width="0.2032" layer="51"/>
<wire x1="-4" y1="-1.75" x2="-3.5" y2="-2.25" width="0.2032" layer="21" curve="90"/>
<wire x1="-3.5" y1="-2.25" x2="3.5" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="3.5" y1="-2.25" x2="4" y2="-1.75" width="0.2032" layer="21" curve="90"/>
<wire x1="4" y1="-1.75" x2="4" y2="1.75" width="0.2032" layer="21"/>
<wire x1="4" y1="1.75" x2="3.5" y2="2.25" width="0.2032" layer="21" curve="90"/>
<wire x1="3.5" y1="2.25" x2="-3.5" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="2.25" x2="-4" y2="1.75" width="0.2032" layer="21" curve="90"/>
<wire x1="-4" y1="1.75" x2="-4" y2="-1.75" width="0.2032" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" diameter="1.778" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" diameter="1.778" shape="octagon"/>
<text x="4.445" y="0" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="4.445" y="-1.905" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-110X060">
<description>&lt;b&gt;NP FILM CAP&lt;/b&gt;&lt;p&gt;
7.5 mm lead spacing&lt;br&gt; outline 11 x 6 mm&lt;br&gt; grid 0.0125 inch</description>
<wire x1="-0.3175" y1="0.9525" x2="-0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-0.3175" y2="-0.9525" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0.9525" x2="0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0" x2="0.3175" y2="-0.9525" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-3.81" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="3.81" y2="0" width="0.2032" layer="51"/>
<wire x1="-5.5" y1="2.5" x2="-5" y2="3" width="0.2032" layer="21" curve="-90"/>
<wire x1="-5" y1="3" x2="5" y2="3" width="0.2032" layer="21"/>
<wire x1="5" y1="3" x2="5.5" y2="2.5" width="0.2032" layer="21" curve="-90"/>
<wire x1="5.5" y1="2.5" x2="5.5" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="5.5" y1="-2.5" x2="5" y2="-3" width="0.2032" layer="21" curve="-90"/>
<wire x1="5" y1="-3" x2="-5" y2="-3" width="0.2032" layer="21"/>
<wire x1="-5" y1="-3" x2="-5.5" y2="-2.5" width="0.2032" layer="21" curve="-90"/>
<wire x1="-5.5" y1="-2.5" x2="-5.5" y2="2.5" width="0.2032" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<text x="6.0325" y="-1.905" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="6.0325" y="0" size="1.016" layer="25" ratio="18">&gt;NAME</text>
</package>
<package name="C075-110X070">
<description>&lt;b&gt;NP FILM CAP&lt;/b&gt;&lt;p&gt;
7.5 mm lead spacing&lt;br&gt; outline 11 x 7 mm&lt;br&gt; grid 0.0125 inch</description>
<wire x1="-0.3175" y1="0.9525" x2="-0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-0.3175" y2="-0.9525" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0.9525" x2="0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0" x2="0.3175" y2="-0.9525" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-3.81" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="3.81" y2="0" width="0.2032" layer="51"/>
<wire x1="-5.5" y1="3" x2="-5" y2="3.5" width="0.2032" layer="21" curve="-90"/>
<wire x1="-5" y1="3.5" x2="5" y2="3.5" width="0.2032" layer="21"/>
<wire x1="5" y1="3.5" x2="5.5" y2="3" width="0.2032" layer="21" curve="-90"/>
<wire x1="5.5" y1="3" x2="5.5" y2="-3" width="0.2032" layer="21"/>
<wire x1="5.5" y1="-3" x2="5" y2="-3.5" width="0.2032" layer="21" curve="-90"/>
<wire x1="5" y1="-3.5" x2="-5" y2="-3.5" width="0.2032" layer="21"/>
<wire x1="-5" y1="-3.5" x2="-5.5" y2="-3" width="0.2032" layer="21" curve="-90"/>
<wire x1="-5.5" y1="-3" x2="-5.5" y2="3" width="0.2032" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<text x="6.0325" y="-1.905" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="6.0325" y="0" size="1.016" layer="25" ratio="18">&gt;NAME</text>
</package>
<package name="C050-110X055">
<description>&lt;b&gt;NP FILM CAP&lt;/b&gt;&lt;p&gt;
5 mm lead spacing&lt;br&gt; outline 11.0 x 5.5 mm&lt;br&gt; grid 0.0125 inch</description>
<wire x1="-0.3175" y1="0.635" x2="-0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-0.3175" y2="-0.635" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0.635" x2="0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0" x2="0.3175" y2="-0.635" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-2.54" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="2.54" y2="0" width="0.2032" layer="51"/>
<wire x1="-5.25" y1="-2.25" x2="-4.75" y2="-2.75" width="0.2032" layer="21" curve="90"/>
<wire x1="-4.75" y1="-2.75" x2="4.75" y2="-2.75" width="0.2032" layer="21"/>
<wire x1="4.75" y1="-2.75" x2="5.25" y2="-2.25" width="0.2032" layer="21" curve="90"/>
<wire x1="5.25" y1="-2.25" x2="5.25" y2="2.25" width="0.2032" layer="21"/>
<wire x1="5.25" y1="2.25" x2="4.75" y2="2.75" width="0.2032" layer="21" curve="90"/>
<wire x1="4.75" y1="2.75" x2="-4.75" y2="2.75" width="0.2032" layer="21"/>
<wire x1="-4.75" y1="2.75" x2="-5.25" y2="2.25" width="0.2032" layer="21" curve="90"/>
<wire x1="-5.25" y1="2.25" x2="-5.25" y2="-2.25" width="0.2032" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" diameter="1.778" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" diameter="1.778" shape="octagon"/>
<text x="6.0125" y="0" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="6.0125" y="-1.905" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-105X035">
<description>&lt;b&gt;NP FILM CAP&lt;/b&gt;&lt;p&gt;
7.5 mm lead spacing&lt;br&gt; outline 10.5 x 3.5 mm&lt;br&gt; grid 0.0125 inch</description>
<wire x1="-0.3175" y1="0.9525" x2="-0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-0.3175" y2="-0.9525" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0.9525" x2="0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0" x2="0.3175" y2="-0.9525" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-3.81" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="3.81" y2="0" width="0.2032" layer="51"/>
<wire x1="-5.25" y1="1.25" x2="-4.75" y2="1.75" width="0.2032" layer="21" curve="-90"/>
<wire x1="-4.75" y1="1.75" x2="4.75" y2="1.75" width="0.2032" layer="21"/>
<wire x1="4.75" y1="1.75" x2="5.25" y2="1.25" width="0.2032" layer="21" curve="-90"/>
<wire x1="5.25" y1="1.25" x2="5.25" y2="-1.25" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-1.25" x2="4.75" y2="-1.75" width="0.2032" layer="21" curve="-90"/>
<wire x1="4.75" y1="-1.75" x2="-4.75" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="-4.75" y1="-1.75" x2="-5.25" y2="-1.25" width="0.2032" layer="21" curve="-90"/>
<wire x1="-5.25" y1="-1.25" x2="-5.25" y2="1.25" width="0.2032" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<text x="6.0325" y="-1.905" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="6.0325" y="0" size="1.016" layer="25" ratio="18">&gt;NAME</text>
</package>
<package name="C050-075X035">
<description>&lt;b&gt;NP FILM CAP&lt;/b&gt;&lt;p&gt;
5 mm lead spacing&lt;br&gt; outline 7.5 x 3.5 mm&lt;br&gt; grid 0.0125 inch</description>
<wire x1="-0.3175" y1="0.635" x2="-0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-0.3175" y2="-0.635" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0.635" x2="0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0" x2="0.3175" y2="-0.635" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-2.54" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="2.54" y2="0" width="0.2032" layer="51"/>
<wire x1="-3.75" y1="-1.25" x2="-3.25" y2="-1.75" width="0.2032" layer="21" curve="90"/>
<wire x1="-3.25" y1="-1.75" x2="3.25" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="3.25" y1="-1.75" x2="3.75" y2="-1.25" width="0.2032" layer="21" curve="90"/>
<wire x1="3.75" y1="-1.25" x2="3.75" y2="1.25" width="0.2032" layer="21"/>
<wire x1="3.75" y1="1.25" x2="3.25" y2="1.75" width="0.2032" layer="21" curve="90"/>
<wire x1="3.25" y1="1.75" x2="-3.25" y2="1.75" width="0.2032" layer="21"/>
<wire x1="-3.25" y1="1.75" x2="-3.75" y2="1.25" width="0.2032" layer="21" curve="90"/>
<wire x1="-3.75" y1="1.25" x2="-3.75" y2="-1.25" width="0.2032" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" diameter="1.778" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" diameter="1.778" shape="octagon"/>
<text x="4.445" y="0" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="4.445" y="-1.5875" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-075X080">
<description>&lt;b&gt;NP FILM CAP&lt;/b&gt;&lt;p&gt;
5 mm lead spacing&lt;br&gt; outline 7.5 x 8.0 mm&lt;br&gt; grid 0.0125 inch</description>
<wire x1="-0.3175" y1="0.635" x2="-0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-0.3175" y2="-0.635" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0.635" x2="0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0" x2="0.3175" y2="-0.635" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-2.54" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="2.54" y2="0" width="0.2032" layer="51"/>
<wire x1="-3.75" y1="-3.5" x2="-3.25" y2="-4" width="0.2032" layer="21" curve="90"/>
<wire x1="-3.25" y1="-4" x2="3.25" y2="-4" width="0.2032" layer="21"/>
<wire x1="3.25" y1="-4" x2="3.75" y2="-3.5" width="0.2032" layer="21" curve="90"/>
<wire x1="3.75" y1="-3.5" x2="3.75" y2="3.5" width="0.2032" layer="21"/>
<wire x1="3.75" y1="3.5" x2="3.25" y2="4" width="0.2032" layer="21" curve="90"/>
<wire x1="3.25" y1="4" x2="-3.25" y2="4" width="0.2032" layer="21"/>
<wire x1="-3.25" y1="4" x2="-3.75" y2="3.5" width="0.2032" layer="21" curve="90"/>
<wire x1="-3.75" y1="3.5" x2="-3.75" y2="-3.5" width="0.2032" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" diameter="1.778" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" diameter="1.778" shape="octagon"/>
<text x="4.445" y="0" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="4.445" y="-1.905" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C072-095X030">
<description>&lt;b&gt;NP FILM CAP&lt;/b&gt;&lt;p&gt;
7.2 mm lead spacing&lt;br&gt; outline 9.5 x 3.0 mm&lt;br&gt; grid 0.0125 inch</description>
<wire x1="-0.3175" y1="0.9525" x2="-0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-0.3175" y2="-0.9525" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0.9525" x2="0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0" x2="0.3175" y2="-0.9525" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-3.56" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="3.56" y2="0" width="0.2032" layer="51"/>
<wire x1="-4.75" y1="1" x2="-4.25" y2="1.5" width="0.2032" layer="21" curve="-90"/>
<wire x1="-4.25" y1="1.5" x2="4.25" y2="1.5" width="0.2032" layer="21"/>
<wire x1="4.25" y1="1.5" x2="4.75" y2="1" width="0.2032" layer="21" curve="-90"/>
<wire x1="4.75" y1="1" x2="4.75" y2="-1" width="0.2032" layer="21"/>
<wire x1="4.75" y1="-1" x2="4.25" y2="-1.5" width="0.2032" layer="21" curve="-90"/>
<wire x1="4.25" y1="-1.5" x2="-4.25" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-4.25" y1="-1.5" x2="-4.75" y2="-1" width="0.2032" layer="21" curve="-90"/>
<wire x1="-4.75" y1="-1" x2="-4.75" y2="1" width="0.2032" layer="21"/>
<pad name="1" x="-3.6" y="0" drill="0.8128" diameter="1.778" shape="octagon"/>
<pad name="2" x="3.6" y="0" drill="0.8128" diameter="1.778" shape="octagon"/>
<text x="5.5325" y="-1.405" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="5.5325" y="0.5" size="1.016" layer="25" ratio="18">&gt;NAME</text>
</package>
<package name="C072-095X035">
<description>&lt;b&gt;NP FILM CAP&lt;/b&gt;&lt;p&gt;
7.2 mm lead spacing&lt;br&gt; outline 9.5 x 3.5 mm&lt;br&gt; grid 0.0125 inch</description>
<wire x1="-0.3175" y1="0.9525" x2="-0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-0.3175" y2="-0.9525" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0.9525" x2="0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0" x2="0.3175" y2="-0.9525" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-3.56" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="3.56" y2="0" width="0.2032" layer="51"/>
<wire x1="-4.75" y1="1.25" x2="-4.25" y2="1.75" width="0.2032" layer="21" curve="-90"/>
<wire x1="-4.25" y1="1.75" x2="4.25" y2="1.75" width="0.2032" layer="21"/>
<wire x1="4.25" y1="1.75" x2="4.75" y2="1.25" width="0.2032" layer="21" curve="-90"/>
<wire x1="4.75" y1="1.25" x2="4.75" y2="-1.25" width="0.2032" layer="21"/>
<wire x1="4.75" y1="-1.25" x2="4.25" y2="-1.75" width="0.2032" layer="21" curve="-90"/>
<wire x1="4.25" y1="-1.75" x2="-4.25" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="-4.25" y1="-1.75" x2="-4.75" y2="-1.25" width="0.2032" layer="21" curve="-90"/>
<wire x1="-4.75" y1="-1.25" x2="-4.75" y2="1.25" width="0.2032" layer="21"/>
<pad name="1" x="-3.6" y="0" drill="0.8128" diameter="1.778" shape="octagon"/>
<pad name="2" x="3.6" y="0" drill="0.8128" diameter="1.778" shape="octagon"/>
<text x="5.5325" y="-1.405" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="5.5325" y="0.5" size="1.016" layer="25" ratio="18">&gt;NAME</text>
</package>
<package name="C080-110X045">
<description>&lt;b&gt;NP FILM CAP&lt;/b&gt;&lt;p&gt;
8.0 mm lead spacing&lt;br&gt; outline 11 x 4.5 mm&lt;br&gt; grid 0.00625 inch</description>
<wire x1="-0.3175" y1="0.9525" x2="-0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-0.3175" y2="-0.9525" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0.9525" x2="0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0" x2="0.3175" y2="-0.9525" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-3.81" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="3.81" y2="0" width="0.2032" layer="51"/>
<wire x1="-5.5" y1="1.75" x2="-5" y2="2.25" width="0.2032" layer="21" curve="-90"/>
<wire x1="-5" y1="2.25" x2="5" y2="2.25" width="0.2032" layer="21"/>
<wire x1="5" y1="2.25" x2="5.5" y2="1.75" width="0.2032" layer="21" curve="-90"/>
<wire x1="5.5" y1="1.75" x2="5.5" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="5.5" y1="-1.75" x2="5" y2="-2.25" width="0.2032" layer="21" curve="-90"/>
<wire x1="5" y1="-2.25" x2="-5" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-5" y1="-2.25" x2="-5.5" y2="-1.75" width="0.2032" layer="21" curve="-90"/>
<wire x1="-5.5" y1="-1.75" x2="-5.5" y2="1.75" width="0.2032" layer="21"/>
<pad name="1" x="-3.9688" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<pad name="2" x="3.9688" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<text x="6.0325" y="0" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="6.0325" y="-1.905" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C072-095X060">
<description>&lt;b&gt;NP FILM CAP&lt;/b&gt;&lt;p&gt;
7.2 mm lead spacing&lt;br&gt; outline 9.5 x 6.0 mm&lt;br&gt; grid 0.0125 inch</description>
<wire x1="-0.3175" y1="0.9525" x2="-0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-0.3175" y2="-0.9525" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0.9525" x2="0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0" x2="0.3175" y2="-0.9525" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-3.56" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="3.56" y2="0" width="0.2032" layer="51"/>
<wire x1="-4.75" y1="2.5" x2="-4.25" y2="3" width="0.2032" layer="21" curve="-90"/>
<wire x1="-4.25" y1="3" x2="4.25" y2="3" width="0.2032" layer="21"/>
<wire x1="4.25" y1="3" x2="4.75" y2="2.5" width="0.2032" layer="21" curve="-90"/>
<wire x1="4.75" y1="2.5" x2="4.75" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="4.75" y1="-2.5" x2="4.25" y2="-3" width="0.2032" layer="21" curve="-90"/>
<wire x1="4.25" y1="-3" x2="-4.25" y2="-3" width="0.2032" layer="21"/>
<wire x1="-4.25" y1="-3" x2="-4.75" y2="-2.5" width="0.2032" layer="21" curve="-90"/>
<wire x1="-4.75" y1="-2.5" x2="-4.75" y2="2.5" width="0.2032" layer="21"/>
<pad name="1" x="-3.6" y="0" drill="0.8128" diameter="1.778" shape="octagon"/>
<pad name="2" x="3.6" y="0" drill="0.8128" diameter="1.778" shape="octagon"/>
<text x="5.5325" y="-1.405" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="5.5325" y="0.5" size="1.016" layer="25" ratio="18">&gt;NAME</text>
</package>
<package name="C072-095X065">
<description>&lt;b&gt;NP FILM CAP&lt;/b&gt;&lt;p&gt;
7.2 mm lead spacing&lt;br&gt; outline 9.5 x 6.5 mm&lt;br&gt; grid 0.0125 inch</description>
<wire x1="-0.3175" y1="0.9525" x2="-0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-0.3175" y2="-0.9525" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0.9525" x2="0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0" x2="0.3175" y2="-0.9525" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-3.56" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="3.56" y2="0" width="0.2032" layer="51"/>
<wire x1="-4.75" y1="2.75" x2="-4.25" y2="3.25" width="0.2032" layer="21" curve="-90"/>
<wire x1="-4.25" y1="3.25" x2="4.25" y2="3.25" width="0.2032" layer="21"/>
<wire x1="4.25" y1="3.25" x2="4.75" y2="2.75" width="0.2032" layer="21" curve="-90"/>
<wire x1="4.75" y1="2.75" x2="4.75" y2="-2.75" width="0.2032" layer="21"/>
<wire x1="4.75" y1="-2.75" x2="4.25" y2="-3.25" width="0.2032" layer="21" curve="-90"/>
<wire x1="4.25" y1="-3.25" x2="-4.25" y2="-3.25" width="0.2032" layer="21"/>
<wire x1="-4.25" y1="-3.25" x2="-4.75" y2="-2.75" width="0.2032" layer="21" curve="-90"/>
<wire x1="-4.75" y1="-2.75" x2="-4.75" y2="2.75" width="0.2032" layer="21"/>
<pad name="1" x="-3.6" y="0" drill="0.8128" diameter="1.778" shape="octagon"/>
<pad name="2" x="3.6" y="0" drill="0.8128" diameter="1.778" shape="octagon"/>
<text x="5.5325" y="-1.405" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="5.5325" y="0.5" size="1.016" layer="25" ratio="18">&gt;NAME</text>
</package>
<package name="C072-095X070">
<description>&lt;b&gt;NP FILM CAP&lt;/b&gt;&lt;p&gt;
7.2 mm lead spacing&lt;br&gt; outline 9.5 x 7.0 mm&lt;br&gt; grid 0.0125 inch</description>
<wire x1="-0.3175" y1="0.9525" x2="-0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-0.3175" y2="-0.9525" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0.9525" x2="0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0" x2="0.3175" y2="-0.9525" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-3.56" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="3.56" y2="0" width="0.2032" layer="51"/>
<wire x1="-4.75" y1="3" x2="-4.25" y2="3.5" width="0.2032" layer="21" curve="-90"/>
<wire x1="-4.25" y1="3.5" x2="4.25" y2="3.5" width="0.2032" layer="21"/>
<wire x1="4.25" y1="3.5" x2="4.75" y2="3" width="0.2032" layer="21" curve="-90"/>
<wire x1="4.75" y1="3" x2="4.75" y2="-3" width="0.2032" layer="21"/>
<wire x1="4.75" y1="-3" x2="4.25" y2="-3.5" width="0.2032" layer="21" curve="-90"/>
<wire x1="4.25" y1="-3.5" x2="-4.25" y2="-3.5" width="0.2032" layer="21"/>
<wire x1="-4.25" y1="-3.5" x2="-4.75" y2="-3" width="0.2032" layer="21" curve="-90"/>
<wire x1="-4.75" y1="-3" x2="-4.75" y2="3" width="0.2032" layer="21"/>
<pad name="1" x="-3.6" y="0" drill="0.8128" diameter="1.778" shape="octagon"/>
<pad name="2" x="3.6" y="0" drill="0.8128" diameter="1.778" shape="octagon"/>
<text x="5.5325" y="-1.405" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="5.5325" y="0.5" size="1.016" layer="25" ratio="18">&gt;NAME</text>
</package>
<package name="C025-040X018">
<description>&lt;b&gt;NP FILM CAP&lt;/b&gt;&lt;p&gt; 
2.5 mm lead spacing&lt;br&gt; outline 4.0 x 1.8 mm&lt;br&gt;grid 0.0125 inch</description>
<wire x1="0.3175" y1="0" x2="1.27" y2="0" width="0.2032" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-1.27" y2="0" width="0.2032" layer="51"/>
<wire x1="-0.3175" y1="0.635" x2="-0.3175" y2="-0.635" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0.635" x2="0.3175" y2="-0.635" width="0.254" layer="51"/>
<wire x1="-1.7762" y1="1.0587" x2="-2.2762" y2="0.5587" width="0.2032" layer="21" curve="90"/>
<wire x1="-2.2762" y1="0.5587" x2="-2.2762" y2="-0.5587" width="0.2032" layer="21"/>
<wire x1="-2.2762" y1="-0.5587" x2="-1.7762" y2="-1.0587" width="0.2032" layer="21" curve="90"/>
<wire x1="-1.7762" y1="-1.0587" x2="1.7762" y2="-1.0587" width="0.2032" layer="21"/>
<wire x1="1.7762" y1="-1.0587" x2="2.2762" y2="-0.5587" width="0.2032" layer="21" curve="90"/>
<wire x1="2.2762" y1="-0.5587" x2="2.2762" y2="0.5587" width="0.2032" layer="21"/>
<wire x1="2.2762" y1="0.5587" x2="1.7762" y2="1.0587" width="0.2032" layer="21" curve="90"/>
<wire x1="1.7762" y1="1.0587" x2="-1.7762" y2="1.0587" width="0.2032" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" diameter="1.4224" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" diameter="1.4224" shape="octagon"/>
<text x="3.175" y="0" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="3.175" y="-1.5875" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C020-035X018">
<description>&lt;b&gt;NP FILM CAP&lt;/b&gt;&lt;p&gt; 
2 mm lead spacing&lt;br&gt; outline 3.5 x 1.8 mm&lt;br&gt;grid 0.0125 inch</description>
<wire x1="-0.3175" y1="0.535" x2="-0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-0.3175" y2="-0.535" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0.535" x2="0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0" x2="0.3175" y2="-0.535" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-0.9525" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="0.9525" y2="0" width="0.2032" layer="51"/>
<wire x1="-1.3" y1="0.9" x2="-1.8" y2="0.4" width="0.2032" layer="21" curve="90"/>
<wire x1="-1.8" y1="0.4" x2="-1.8" y2="-0.4" width="0.2032" layer="21"/>
<wire x1="-1.8" y1="-0.4" x2="-1.3" y2="-0.9" width="0.2032" layer="21" curve="90"/>
<wire x1="-1.3" y1="-0.9" x2="1.3" y2="-0.9" width="0.2032" layer="21"/>
<wire x1="1.3" y1="-0.9" x2="1.8" y2="-0.4" width="0.2032" layer="21" curve="90"/>
<wire x1="1.8" y1="-0.4" x2="1.8" y2="0.4" width="0.2032" layer="21"/>
<wire x1="1.8" y1="0.4" x2="1.3" y2="0.9" width="0.2032" layer="21" curve="90"/>
<wire x1="1.3" y1="0.9" x2="-1.3" y2="0.9" width="0.2032" layer="21"/>
<pad name="1" x="-0.9525" y="0" drill="0.6096" diameter="1.27" shape="octagon"/>
<pad name="2" x="0.9525" y="0" drill="0.6096" diameter="1.27" shape="octagon"/>
<text x="2.54" y="-0.635" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="2.54" y="-2.2225" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-050X025">
<description>&lt;b&gt;NP FILM CAP&lt;/b&gt;&lt;p&gt;
2.5 mm lead spacing&lt;br&gt; outline 5 x 2.5 mm&lt;br&gt; grid 0.0125 inch</description>
<wire x1="0.3175" y1="0" x2="1.27" y2="0" width="0.2032" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-1.27" y2="0" width="0.2032" layer="51"/>
<wire x1="-0.3175" y1="0.635" x2="-0.3175" y2="-0.635" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0.635" x2="0.3175" y2="-0.635" width="0.254" layer="51"/>
<wire x1="2" y1="-1.3" x2="2.5" y2="-0.8" width="0.2032" layer="21" curve="90"/>
<wire x1="2.5" y1="-0.8" x2="2.5" y2="0.8" width="0.2032" layer="21"/>
<wire x1="2.5" y1="0.8" x2="2" y2="1.3" width="0.2032" layer="21" curve="90"/>
<wire x1="2" y1="1.3" x2="-2" y2="1.3" width="0.2032" layer="21"/>
<wire x1="-2" y1="1.3" x2="-2.5" y2="0.8" width="0.2032" layer="21" curve="90"/>
<wire x1="-2.5" y1="0.8" x2="-2.5" y2="-0.8" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-0.8" x2="-2" y2="-1.3" width="0.2032" layer="21" curve="90"/>
<wire x1="-2" y1="-1.3" x2="2" y2="-1.3" width="0.2032" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="3.175" y="0" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="3.175" y="-1.5875" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="CA-100-024X044">
<description>&lt;b&gt;AXIAL FILM CAP&lt;/b&gt;&lt;p&gt;
grid 10 mm, outline 2.4 x 4.4 mm</description>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.5875" y1="-1.1113" x2="-2.2225" y2="-0.4763" width="0.2032" layer="21" curve="-90"/>
<wire x1="-2.2225" y1="-0.4763" x2="-2.2225" y2="0.4763" width="0.2032" layer="21"/>
<wire x1="-2.2225" y1="0.4763" x2="-1.5875" y2="1.1113" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1.5875" y1="1.1113" x2="1.5875" y2="1.1113" width="0.2032" layer="21"/>
<wire x1="1.5875" y1="1.1113" x2="2.2225" y2="0.4763" width="0.2032" layer="21" curve="-90"/>
<wire x1="2.2225" y1="0.4763" x2="2.2225" y2="-0.4763" width="0.2032" layer="21"/>
<wire x1="2.2225" y1="-0.4763" x2="1.5875" y2="-1.1113" width="0.2032" layer="21" curve="-90"/>
<wire x1="1.5875" y1="-1.1113" x2="-1.5875" y2="-1.1113" width="0.2032" layer="21"/>
<wire x1="-4.1275" y1="0" x2="-5.08" y2="0" width="0.3048" layer="51"/>
<wire x1="5.08" y1="0" x2="4.1275" y2="0" width="0.3048" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<text x="-2.2225" y="1.5875" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.2225" y="-2.54" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.1275" y1="-0.1588" x2="-2.2225" y2="0.1588" layer="21"/>
<rectangle x1="2.2225" y1="-0.1588" x2="4.1275" y2="0.1588" layer="21"/>
</package>
<package name="CA-050-024X044">
<description>&lt;b&gt;AXIAL FILM CAP&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.4 x 4.4 mm</description>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.5875" y1="-1.1113" x2="-2.2225" y2="-0.4763" width="0.2032" layer="21" curve="-90"/>
<wire x1="-2.2225" y1="-0.4763" x2="-2.2225" y2="0.4763" width="0.2032" layer="51"/>
<wire x1="-2.2225" y1="0.4763" x2="-1.5875" y2="1.1113" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1.5875" y1="1.1113" x2="1.5875" y2="1.1113" width="0.2032" layer="21"/>
<wire x1="1.5875" y1="1.1113" x2="2.2225" y2="0.4763" width="0.2032" layer="21" curve="-90"/>
<wire x1="2.2225" y1="0.4763" x2="2.2225" y2="-0.4763" width="0.2032" layer="51"/>
<wire x1="2.2225" y1="-0.4763" x2="1.5875" y2="-1.1113" width="0.2032" layer="21" curve="-90"/>
<wire x1="1.5875" y1="-1.1113" x2="-1.5875" y2="-1.1113" width="0.2032" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" diameter="1.778" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" diameter="1.778" shape="octagon"/>
<text x="-2.2225" y="1.5875" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.2225" y="-2.54" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.5395" y1="-0.1588" x2="-2.222" y2="0.1588" layer="51"/>
<rectangle x1="2.2225" y1="-0.1588" x2="2.54" y2="0.1588" layer="51"/>
</package>
<package name="C0402">
<description>&lt;b&gt;SMD CHIP CAP&lt;/b&gt;&lt;p&gt;
0402, grid 0.0125 inch</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1016" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1016" layer="51"/>
<wire x1="-1.1555" y1="0.483" x2="1.1555" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.1555" y1="0.483" x2="1.1555" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.1555" y1="-0.483" x2="-1.1555" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.1555" y1="-0.483" x2="-1.1555" y2="0.483" width="0.0508" layer="39"/>
<wire x1="-1.0319" y1="0.5557" x2="1.0319" y2="0.5557" width="0.2032" layer="21"/>
<wire x1="1.0319" y1="0.5557" x2="1.0319" y2="-0.5556" width="0.2032" layer="21"/>
<wire x1="1.0319" y1="-0.5556" x2="-1.0319" y2="-0.5556" width="0.2032" layer="21"/>
<wire x1="-1.0319" y1="-0.5556" x2="-1.0319" y2="0.5557" width="0.2032" layer="21"/>
<smd name="1" x="-0.4762" y="0" dx="0.6" dy="0.7" layer="1"/>
<smd name="2" x="0.4762" y="0" dx="0.6" dy="0.7" layer="1"/>
<text x="-0.9525" y="0.7939" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-0.9525" y="-1.5876" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C0603">
<description>&lt;b&gt;SMD CHIP CAP&lt;/b&gt;&lt;p&gt;
0603, grid 0.0125 inch</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1016" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1016" layer="51"/>
<wire x1="-1.473" y1="0.6655" x2="1.473" y2="0.6655" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.6655" x2="1.473" y2="-0.6655" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.6655" x2="-1.473" y2="-0.6655" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.6655" x2="-1.473" y2="0.6655" width="0.0508" layer="39"/>
<wire x1="-1.4875" y1="0.7938" x2="1.4875" y2="0.7938" width="0.2032" layer="21"/>
<wire x1="1.4875" y1="0.7938" x2="1.4875" y2="-0.7938" width="0.2032" layer="21"/>
<wire x1="1.4875" y1="-0.7938" x2="-1.4875" y2="-0.7938" width="0.2032" layer="21"/>
<wire x1="-1.4875" y1="-0.7938" x2="-1.4875" y2="0.7938" width="0.2032" layer="21"/>
<smd name="1" x="-0.7938" y="0" dx="0.9" dy="1.1" layer="1"/>
<smd name="2" x="0.7938" y="0" dx="0.9" dy="1.1" layer="1"/>
<text x="-1.27" y="1.1113" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.27" y="-1.905" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C0805">
<description>&lt;b&gt;SMD CHIP CAP&lt;/b&gt;&lt;p&gt;
0805, grid 0.0125 inch</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1016" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1016" layer="51"/>
<wire x1="-1.8143" y1="0.8243" x2="1.8143" y2="0.8243" width="0.0508" layer="39"/>
<wire x1="1.8143" y1="0.8243" x2="1.8143" y2="-0.8243" width="0.0508" layer="39"/>
<wire x1="1.8143" y1="-0.8243" x2="-1.8143" y2="-0.8243" width="0.0508" layer="39"/>
<wire x1="-1.8143" y1="-0.8243" x2="-1.8143" y2="0.8243" width="0.0508" layer="39"/>
<wire x1="-1.8256" y1="1.0319" x2="1.8256" y2="1.0319" width="0.2032" layer="21"/>
<wire x1="1.8256" y1="1.0319" x2="1.8256" y2="-1.0319" width="0.2032" layer="21"/>
<wire x1="1.8256" y1="-1.0319" x2="-1.8256" y2="-1.0319" width="0.2032" layer="21"/>
<wire x1="-1.8256" y1="-1.0319" x2="-1.8256" y2="1.0319" width="0.2032" layer="21"/>
<smd name="1" x="-0.9525" y="0" dx="1.2" dy="1.5" layer="1"/>
<smd name="2" x="0.9525" y="0" dx="1.2" dy="1.5" layer="1"/>
<text x="-1.5875" y="1.4288" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.5874" y="-2.2225" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="C1206">
<description>&lt;b&gt;SMD CHIP CAP&lt;/b&gt;&lt;p&gt;
1206, grid 0.0125 inch</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1016" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1016" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.4731" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.4731" y1="0.983" x2="2.4731" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.4731" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-2.34" y1="1.1907" x2="2.3401" y2="1.1907" width="0.2032" layer="21"/>
<wire x1="2.3401" y1="1.1907" x2="2.3401" y2="-1.1907" width="0.2032" layer="21"/>
<wire x1="2.3401" y1="-1.1907" x2="-2.34" y2="-1.1907" width="0.2032" layer="21"/>
<wire x1="-2.34" y1="-1.1907" x2="-2.34" y2="1.1907" width="0.2032" layer="21"/>
<smd name="2" x="1.3288" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="1" x="-1.3287" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-2.2225" y="1.5875" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.2225" y="-2.2225" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="C1210">
<description>&lt;b&gt;SMD CHIP CAP&lt;/b&gt;&lt;p&gt;
1210, grid 0.0125 inch</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1016" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1016" layer="51"/>
<wire x1="-2.6317" y1="1.483" x2="2.6318" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.6318" y1="1.483" x2="2.6318" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.6318" y1="-1.483" x2="-2.6317" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.6317" y1="-1.483" x2="-2.6317" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-2.6987" y1="1.6669" x2="2.6988" y2="1.6669" width="0.2032" layer="21"/>
<wire x1="2.6988" y1="1.6669" x2="2.6988" y2="-1.6669" width="0.2032" layer="21"/>
<wire x1="2.6988" y1="-1.6669" x2="-2.6987" y2="-1.6669" width="0.2032" layer="21"/>
<wire x1="-2.6987" y1="-1.6669" x2="-2.6987" y2="1.6669" width="0.2032" layer="21"/>
<smd name="1" x="-1.5875" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.5875" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.54" y="-2.8575" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8999" x2="0.3" y2="0.8999" layer="35"/>
</package>
<package name="C070-095X035">
<description>&lt;b&gt;NP FILM CAP&lt;/b&gt;&lt;p&gt;
7.0 mm lead spacing&lt;br&gt; outline 9.5 x 3.5 mm&lt;br&gt; grid 0.0125 inch</description>
<wire x1="-0.3175" y1="0.9525" x2="-0.3175" y2="0" width="0.2032" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-0.3175" y2="-0.9525" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0.9525" x2="0.3175" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="0.3175" y2="-0.9525" width="0.2032" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-3.4925" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="3.4925" y2="0" width="0.2032" layer="51"/>
<wire x1="-4.75" y1="1.25" x2="-4.25" y2="1.75" width="0.2032" layer="21" curve="-90"/>
<wire x1="-4.25" y1="1.75" x2="4.25" y2="1.75" width="0.2032" layer="21"/>
<wire x1="4.25" y1="1.75" x2="4.75" y2="1.25" width="0.2032" layer="21" curve="-90"/>
<wire x1="4.75" y1="1.25" x2="4.75" y2="-1.25" width="0.2032" layer="21"/>
<wire x1="4.75" y1="-1.25" x2="4.25" y2="-1.75" width="0.2032" layer="21" curve="-90"/>
<wire x1="4.25" y1="-1.75" x2="-4.25" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="-4.25" y1="-1.75" x2="-4.75" y2="-1.25" width="0.2032" layer="21" curve="-90"/>
<wire x1="-4.75" y1="-1.25" x2="-4.75" y2="1.25" width="0.2032" layer="21"/>
<pad name="1" x="-3.4925" y="0" drill="0.8128" diameter="1.778" shape="octagon"/>
<pad name="2" x="3.4925" y="0" drill="0.8128" diameter="1.778" shape="octagon"/>
<text x="5.5325" y="-1.405" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="5.5325" y="0.5" size="1.016" layer="25" ratio="18">&gt;NAME</text>
</package>
<package name="C050-120X055">
<description>&lt;b&gt;NP FILM CAP&lt;/b&gt;&lt;p&gt;
5 mm lead spacing&lt;br&gt; outline 12.0 x 5.5 mm&lt;br&gt; grid 0.0125 inch</description>
<wire x1="-6" y1="-2.25" x2="-5.5" y2="-2.75" width="0.2032" layer="21" curve="90"/>
<wire x1="-5.5" y1="-2.75" x2="5.5" y2="-2.75" width="0.2032" layer="21"/>
<wire x1="5.5" y1="-2.75" x2="6" y2="-2.25" width="0.2032" layer="21" curve="90"/>
<wire x1="6" y1="-2.25" x2="6" y2="2.25" width="0.2032" layer="21"/>
<wire x1="6" y1="2.25" x2="5.5" y2="2.75" width="0.2032" layer="21" curve="90"/>
<wire x1="5.5" y1="2.75" x2="-5.5" y2="2.75" width="0.2032" layer="21"/>
<wire x1="-5.5" y1="2.75" x2="-6" y2="2.25" width="0.2032" layer="21" curve="90"/>
<wire x1="-6" y1="2.25" x2="-6" y2="-2.25" width="0.2032" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" diameter="1.778" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" diameter="1.778" shape="octagon"/>
<text x="6.7625" y="0" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="6.7625" y="-1.905" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-120X055">
<description>&lt;b&gt;NP FILM CAP&lt;/b&gt;&lt;p&gt;
5 mm lead spacing&lt;br&gt; outline 12.0 x 5.5 mm&lt;br&gt; grid 0.0125 inch</description>
<wire x1="-0.3175" y1="0.9525" x2="-0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-0.3175" y2="-0.9525" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0.9525" x2="0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0" x2="0.3175" y2="-0.9525" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-3.81" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="3.81" y2="0" width="0.2032" layer="51"/>
<wire x1="-5.75" y1="2.25" x2="-5.25" y2="2.75" width="0.2032" layer="21" curve="-90"/>
<wire x1="-5.25" y1="2.75" x2="5.25" y2="2.75" width="0.2032" layer="21"/>
<wire x1="5.25" y1="2.75" x2="5.75" y2="2.25" width="0.2032" layer="21" curve="-90"/>
<wire x1="5.75" y1="2.25" x2="5.75" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="5.75" y1="-2.25" x2="5.25" y2="-2.75" width="0.2032" layer="21" curve="-90"/>
<wire x1="5.25" y1="-2.75" x2="-5.25" y2="-2.75" width="0.2032" layer="21"/>
<wire x1="-5.25" y1="-2.75" x2="-5.75" y2="-2.25" width="0.2032" layer="21" curve="-90"/>
<wire x1="-5.75" y1="-2.25" x2="-5.75" y2="2.25" width="0.2032" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<text x="6.5325" y="-1.905" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="6.5325" y="0" size="1.016" layer="25" ratio="18">&gt;NAME</text>
</package>
<package name="C150-200X095">
<description>&lt;b&gt;NP FILM CAP&lt;/b&gt;&lt;p&gt;
15 mm lead spacing&lt;br&gt; outline 20.0 x 9.5 mm&lt;br&gt; grid 0.0125 inch</description>
<wire x1="-0.635" y1="1.27" x2="-0.635" y2="0" width="0.254" layer="51"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-1.27" width="0.254" layer="51"/>
<wire x1="0.635" y1="1.27" x2="0.635" y2="0" width="0.254" layer="51"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-1.27" width="0.254" layer="51"/>
<wire x1="-0.635" y1="0" x2="-7.62" y2="0" width="0.2032" layer="51"/>
<wire x1="0.635" y1="0" x2="7.62" y2="0" width="0.2032" layer="51"/>
<wire x1="-9" y1="4.75" x2="-10" y2="3.75" width="0.2032" layer="21" curve="90"/>
<wire x1="-10" y1="3.75" x2="-10" y2="-3.75" width="0.2032" layer="21"/>
<wire x1="-10" y1="-3.75" x2="-9" y2="-4.75" width="0.2032" layer="21" curve="90"/>
<wire x1="-9" y1="-4.75" x2="9" y2="-4.75" width="0.2032" layer="21"/>
<wire x1="9" y1="-4.75" x2="10" y2="-3.75" width="0.2032" layer="21" curve="90"/>
<wire x1="10" y1="-3.75" x2="10" y2="3.75" width="0.2032" layer="21"/>
<wire x1="10" y1="3.75" x2="9" y2="4.75" width="0.2032" layer="21" curve="90"/>
<wire x1="9" y1="4.75" x2="-9" y2="4.75" width="0.2032" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="10.795" y="0.635" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="10.795" y="-1.905" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-085X045">
<description>&lt;b&gt;NP FILM CAP&lt;/b&gt;&lt;p&gt;
5 mm lead spacing&lt;br&gt; outline 8.5 x 4.5 mm&lt;br&gt; grid 0.0125 inch</description>
<wire x1="-0.3175" y1="0.635" x2="-0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-0.3175" y2="-0.635" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0.635" x2="0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0" x2="0.3175" y2="-0.635" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-2.54" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="2.54" y2="0" width="0.2032" layer="51"/>
<wire x1="-4.25" y1="-1.75" x2="-3.75" y2="-2.25" width="0.2032" layer="21" curve="90"/>
<wire x1="-3.75" y1="-2.25" x2="3.75" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="3.75" y1="-2.25" x2="4.25" y2="-1.75" width="0.2032" layer="21" curve="90"/>
<wire x1="4.25" y1="-1.75" x2="4.25" y2="1.75" width="0.2032" layer="21"/>
<wire x1="4.25" y1="1.75" x2="3.75" y2="2.25" width="0.2032" layer="21" curve="90"/>
<wire x1="3.75" y1="2.25" x2="-3.75" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-3.75" y1="2.25" x2="-4.25" y2="1.75" width="0.2032" layer="21" curve="90"/>
<wire x1="-4.25" y1="1.75" x2="-4.25" y2="-1.75" width="0.2032" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" diameter="1.778" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" diameter="1.778" shape="octagon"/>
<text x="5.08" y="0" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="5.08" y="-1.905" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C100-200X100">
<description>&lt;b&gt;NP FILM CAP&lt;/b&gt;&lt;p&gt;
10 mm lead spacing&lt;br&gt; outline 20 x 10 mm&lt;br&gt; grid 0.0125 inch</description>
<wire x1="-0.635" y1="0.9525" x2="-0.635" y2="0" width="0.254" layer="51"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.9525" width="0.254" layer="51"/>
<wire x1="0.635" y1="0.9525" x2="0.635" y2="0" width="0.254" layer="51"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-0.9525" width="0.254" layer="51"/>
<wire x1="-0.635" y1="0" x2="-5.08" y2="0" width="0.2032" layer="51"/>
<wire x1="0.635" y1="0" x2="5.08" y2="0" width="0.2032" layer="51"/>
<wire x1="10" y1="4" x2="9" y2="5" width="0.2032" layer="21" curve="90"/>
<wire x1="9" y1="5" x2="-9" y2="5" width="0.2032" layer="21"/>
<wire x1="-9" y1="5" x2="-10" y2="4" width="0.2032" layer="21" curve="90"/>
<wire x1="-10" y1="4" x2="-10" y2="-4" width="0.2032" layer="21"/>
<wire x1="-10" y1="-4" x2="-9" y2="-5" width="0.2032" layer="21" curve="90"/>
<wire x1="-9" y1="-5" x2="9" y2="-5" width="0.2032" layer="21"/>
<wire x1="9" y1="-5" x2="10" y2="-4" width="0.2032" layer="21" curve="90"/>
<wire x1="10" y1="-4" x2="10" y2="4" width="0.2032" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="10.62" y="-1.905" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="10.62" y="0" size="1.016" layer="25" ratio="18">&gt;NAME</text>
</package>
<package name="CX23PW-050">
<description>&lt;b&gt;POLYSTYRENE CAP&lt;/b&gt;&lt;p&gt; 
 Xicon 23P Series, 2.0 mm lead spacing, 5 mm diameter, grid 0.0125 inch</description>
<circle x="0" y="0" radius="2.7127" width="0.2032" layer="21"/>
<pad name="1" x="-0.9525" y="0" drill="0.8128" diameter="1.27" shape="long" rot="R90"/>
<pad name="2" x="0.9525" y="0" drill="0.8128" diameter="1.27" shape="long" rot="R90"/>
<text x="3.175" y="-2.54" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="3.175" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
</package>
<package name="CX23PW-060">
<description>&lt;b&gt;POLYSTYRENE CAP&lt;/b&gt;&lt;p&gt; 
 Xicon 23P Series, 2.0 mm lead spacing, 6 mm diameter, grid 0.0125 inch</description>
<circle x="0" y="0" radius="3.1622" width="0.2032" layer="21"/>
<pad name="1" x="-0.9525" y="0" drill="0.8128" diameter="1.27" shape="long" rot="R90"/>
<pad name="2" x="0.9525" y="0" drill="0.8128" diameter="1.27" shape="long" rot="R90"/>
<text x="3.175" y="-3.175" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="3.175" y="2.54" size="1.016" layer="25" ratio="18">&gt;NAME</text>
</package>
<package name="CX23PW-070">
<description>&lt;b&gt;POLYSTYRENE CAP&lt;/b&gt;&lt;p&gt; 
 Xicon 23P Series, 2.0 mm lead spacing, 7 mm diameter, grid 0.0125 inch</description>
<circle x="0" y="0" radius="3.64" width="0.2032" layer="21"/>
<pad name="1" x="-0.9525" y="0" drill="0.8128" diameter="1.27" shape="long" rot="R90"/>
<pad name="2" x="0.9525" y="0" drill="0.8128" diameter="1.27" shape="long" rot="R90"/>
<text x="3.81" y="-3.175" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="3.81" y="2.54" size="1.016" layer="25" ratio="18">&gt;NAME</text>
</package>
<package name="CX23PW-080">
<description>&lt;b&gt;POLYSTYRENE CAP&lt;/b&gt;&lt;p&gt; 
 Xicon 23P Series, 2.0 mm lead spacing, 8 mm diameter, grid 0.0125 inch</description>
<circle x="0" y="0" radius="4.1231" width="0.2032" layer="21"/>
<pad name="1" x="-0.9525" y="0" drill="0.8128" diameter="1.27" shape="long" rot="R90"/>
<pad name="2" x="0.9525" y="0" drill="0.8128" diameter="1.27" shape="long" rot="R90"/>
<text x="3.81" y="-3.81" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="3.81" y="3.175" size="1.016" layer="25" ratio="18">&gt;NAME</text>
</package>
<package name="CX23PW-090">
<description>&lt;b&gt;POLYSTYRENE CAP&lt;/b&gt;&lt;p&gt; 
 Xicon 23P Series, 2.0 mm lead spacing, 9 mm diameter, grid 0.0125 inch</description>
<circle x="0" y="0" radius="4.6097" width="0.2032" layer="21"/>
<pad name="1" x="-0.9525" y="0" drill="0.8128" diameter="1.27" shape="long" rot="R90"/>
<pad name="2" x="0.9525" y="0" drill="0.8128" diameter="1.27" shape="long" rot="R90"/>
<text x="3.81" y="-4.445" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="3.81" y="3.81" size="1.016" layer="25" ratio="18">&gt;NAME</text>
</package>
<package name="C0201">
<description>&lt;b&gt;SMD CHIP CAP&lt;/b&gt;&lt;p&gt;
0201, grid 0.00625 inch</description>
<wire x1="-0.195" y1="0.124" x2="0.195" y2="0.124" width="0.1016" layer="51"/>
<wire x1="0.195" y1="-0.124" x2="-0.195" y2="-0.124" width="0.1016" layer="51"/>
<wire x1="-0.7938" y1="0.4763" x2="0.7938" y2="0.4763" width="0.2032" layer="21"/>
<wire x1="0.7938" y1="0.4763" x2="0.7938" y2="-0.4763" width="0.2032" layer="21"/>
<wire x1="0.7938" y1="-0.4763" x2="-0.7938" y2="-0.4763" width="0.2032" layer="21"/>
<wire x1="-0.7938" y1="-0.4763" x2="-0.7938" y2="0.4763" width="0.2032" layer="21"/>
<smd name="1" x="-0.3175" y="0" dx="0.5" dy="0.4" layer="1" rot="R90"/>
<smd name="2" x="0.3175" y="0" dx="0.5" dy="0.4" layer="1" rot="R90"/>
<text x="-0.7938" y="0.7939" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-0.7938" y="-1.5876" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.304" y1="-0.2" x2="-0.15" y2="0.2" layer="51"/>
<rectangle x1="0.15" y1="-0.2" x2="0.3088" y2="0.2" layer="51"/>
</package>
<package name="C1608">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.473" y1="0.6655" x2="1.473" y2="0.6655" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.6655" x2="1.473" y2="-0.6655" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.6655" x2="-1.473" y2="-0.6655" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.6655" x2="-1.473" y2="0.6655" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<wire x1="1.5875" y1="0.7938" x2="1.5875" y2="-0.7938" width="0.2032" layer="21"/>
<wire x1="1.5875" y1="-0.7938" x2="-1.5875" y2="-0.7938" width="0.2032" layer="21"/>
<wire x1="-1.5875" y1="-0.7938" x2="-1.5875" y2="0.7938" width="0.2032" layer="21"/>
<wire x1="-1.5875" y1="0.7938" x2="1.5875" y2="0.7938" width="0.2032" layer="21"/>
<smd name="1" x="-0.7938" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.7938" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-1.27" y="1.27" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.27" y="-1.905" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C1808">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.4732" y1="1.0002" x2="1.4732" y2="1.0002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.0002" x2="1.4732" y2="-1.0002" width="0.1016" layer="51"/>
<wire x1="-3.3338" y1="1.7463" x2="3.3338" y2="1.7463" width="0.2032" layer="21"/>
<wire x1="3.3338" y1="1.7463" x2="3.3338" y2="-1.7463" width="0.2032" layer="21"/>
<wire x1="3.3338" y1="-1.7463" x2="-3.3338" y2="-1.7463" width="0.2032" layer="21"/>
<wire x1="-3.3338" y1="-1.7463" x2="-3.3338" y2="1.7463" width="0.2032" layer="21"/>
<smd name="1" x="-2.1225" y="0" dx="1.8" dy="2.9" layer="1"/>
<smd name="2" x="2.1225" y="0" dx="1.8" dy="2.9" layer="1"/>
<text x="-3.175" y="2.2225" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.175" y="-2.8575" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.1" x2="-1.4376" y2="1.1" layer="51"/>
<rectangle x1="1.4478" y1="-1.1" x2="2.3978" y2="1.1" layer="51"/>
</package>
<package name="CX23PS-055X120">
<description>&lt;b&gt;POLYSTYRENE CAP&lt;/b&gt;&lt;p&gt; 
 Xicon 23P Series, 5.5 x 12.0 mm, grid 0.0125 inch</description>
<wire x1="-4.5" y1="2.5" x2="4.5" y2="2.5" width="0.2032" layer="21"/>
<wire x1="4.5" y1="2.5" x2="4.5" y2="-2.5" width="0.2032" layer="51"/>
<wire x1="4.5" y1="-2.5" x2="-4.5" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-2.5" x2="-4.5" y2="2.5" width="0.2032" layer="51"/>
<wire x1="-5.5" y1="0.5" x2="-5.5" y2="-0.5" width="0.2032" layer="21"/>
<wire x1="-5.5" y1="0.5" x2="-4.5" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-5.5" y1="-0.5" x2="-4.5" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="4.5" y1="2.5" x2="5.5" y2="0.5" width="0.2032" layer="21"/>
<wire x1="5.5" y1="0.5" x2="5.5" y2="-0.5" width="0.2032" layer="21"/>
<wire x1="5.5" y1="-0.5" x2="4.5" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="-5" y1="-1.5" x2="-7" y2="-1.5" width="0.2032" layer="51"/>
<wire x1="5" y1="1.5" x2="7" y2="1.5" width="0.2032" layer="51"/>
<pad name="1" x="-7" y="-1.5" drill="0.8128" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="2" x="7" y="1.5" drill="0.8128" diameter="1.778" shape="octagon" rot="R90"/>
<text x="-1.325" y="-1.54" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="-4.445" y="3.175" size="1.016" layer="25" ratio="18">&gt;NAME</text>
</package>
<package name="C050-075X57">
<wire x1="-0.3175" y1="0.635" x2="-0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-0.3175" y2="-0.635" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0.635" x2="0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0" x2="0.3175" y2="-0.635" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-2.54" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="2.54" y2="0" width="0.2032" layer="51"/>
<wire x1="-3.75" y1="-2.25" x2="-3.25" y2="-2.75" width="0.2032" layer="21" curve="90"/>
<wire x1="-3.25" y1="-2.75" x2="3.25" y2="-2.75" width="0.2032" layer="21"/>
<wire x1="3.25" y1="-2.75" x2="3.75" y2="-2.25" width="0.2032" layer="21" curve="90"/>
<wire x1="3.75" y1="-2.25" x2="3.75" y2="2.25" width="0.2032" layer="21"/>
<wire x1="3.75" y1="2.25" x2="3.25" y2="2.75" width="0.2032" layer="21" curve="90"/>
<wire x1="3.25" y1="2.75" x2="-3.25" y2="2.75" width="0.2032" layer="21"/>
<wire x1="-3.25" y1="2.75" x2="-3.75" y2="2.25" width="0.2032" layer="21" curve="90"/>
<wire x1="-3.75" y1="2.25" x2="-3.75" y2="-2.25" width="0.2032" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" diameter="1.778" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" diameter="1.778" shape="octagon"/>
<text x="4.445" y="0" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="4.445" y="-1.5875" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-100X045">
<wire x1="-0.3175" y1="0.9525" x2="-0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-0.3175" y2="-0.9525" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0.9525" x2="0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0" x2="0.3175" y2="-0.9525" width="0.254" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-3.81" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="3.81" y2="0" width="0.2032" layer="51"/>
<wire x1="-5.25" y1="1.75" x2="-4.75" y2="2.25" width="0.2032" layer="21" curve="-90"/>
<wire x1="-4.75" y1="2.25" x2="4.75" y2="2.25" width="0.2032" layer="21"/>
<wire x1="4.75" y1="2.25" x2="5.25" y2="1.75" width="0.2032" layer="21" curve="-90"/>
<wire x1="5.25" y1="1.75" x2="5.25" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-1.75" x2="4.75" y2="-2.25" width="0.2032" layer="21" curve="-90"/>
<wire x1="4.75" y1="-2.25" x2="-4.75" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-4.75" y1="-2.25" x2="-5.25" y2="-1.75" width="0.2032" layer="21" curve="-90"/>
<wire x1="-5.25" y1="-1.75" x2="-5.25" y2="1.75" width="0.2032" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<text x="6.0325" y="-1.905" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="6.0325" y="0" size="1.016" layer="25" ratio="18">&gt;NAME</text>
</package>
<package name="C050-075X018">
<wire x1="0.3175" y1="0" x2="1.27" y2="0" width="0.2032" layer="51"/>
<wire x1="-0.3175" y1="0" x2="-1.27" y2="0" width="0.2032" layer="51"/>
<wire x1="-0.3175" y1="0.635" x2="-0.3175" y2="-0.635" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0.635" x2="0.3175" y2="0" width="0.254" layer="51"/>
<wire x1="0.3175" y1="0" x2="0.3175" y2="-0.635" width="0.254" layer="51"/>
<wire x1="-3.0262" y1="1.0587" x2="-3.5262" y2="0.5587" width="0.2032" layer="21" curve="90"/>
<wire x1="-3.5262" y1="0.5587" x2="-3.5262" y2="-0.5587" width="0.2032" layer="21"/>
<wire x1="-3.5262" y1="-0.5587" x2="-3.0262" y2="-1.0587" width="0.2032" layer="21" curve="90"/>
<wire x1="-3.0262" y1="-1.0587" x2="3.0262" y2="-1.0587" width="0.2032" layer="21"/>
<wire x1="3.0262" y1="-1.0587" x2="3.5262" y2="-0.5587" width="0.2032" layer="21" curve="90"/>
<wire x1="3.5262" y1="-0.5587" x2="3.5262" y2="0.5587" width="0.2032" layer="21"/>
<wire x1="3.5262" y1="0.5587" x2="3.0262" y2="1.0587" width="0.2032" layer="21" curve="90"/>
<wire x1="3.0262" y1="1.0587" x2="-3.0262" y2="1.0587" width="0.2032" layer="21"/>
<pad name="1" x="-2.52" y="0" drill="0.8128" diameter="1.4224" shape="octagon"/>
<pad name="2" x="2.52" y="0" drill="0.8128" diameter="1.4224" shape="octagon"/>
<text x="4.425" y="0" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="4.425" y="-1.5875" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="R0402">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt;&lt;p&gt;
0402, grid 0.00625 inch</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1016" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1016" layer="51"/>
<wire x1="-1.1555" y1="0.483" x2="1.1555" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.1555" y1="0.483" x2="1.1555" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.1555" y1="-0.483" x2="-1.1555" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.1555" y1="-0.483" x2="-1.1555" y2="0.483" width="0.0508" layer="39"/>
<wire x1="-1.0318" y1="0.5557" x2="1.0319" y2="0.5557" width="0.2032" layer="21"/>
<wire x1="1.0319" y1="0.5557" x2="1.0319" y2="-0.5556" width="0.2032" layer="21"/>
<wire x1="1.0319" y1="-0.5556" x2="-1.0318" y2="-0.5556" width="0.2032" layer="21"/>
<wire x1="-1.0318" y1="-0.5556" x2="-1.0318" y2="0.5557" width="0.2032" layer="21"/>
<smd name="1" x="-0.4762" y="0" dx="0.6" dy="0.7" layer="1"/>
<smd name="2" x="0.4762" y="0" dx="0.6" dy="0.7" layer="1"/>
<text x="-0.9525" y="0.7938" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-0.9525" y="-1.5875" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0603">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt;&lt;p&gt;
0603, grid 0.00625 inch</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1016" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1016" layer="51"/>
<wire x1="-1.473" y1="0.6655" x2="1.473" y2="0.6655" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.6655" x2="1.473" y2="-0.6655" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.6655" x2="-1.473" y2="-0.6655" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.6655" x2="-1.473" y2="0.6655" width="0.0508" layer="39"/>
<wire x1="-1.4875" y1="0.7937" x2="1.4875" y2="0.7937" width="0.2032" layer="21"/>
<wire x1="1.4875" y1="0.7937" x2="1.4875" y2="-0.7938" width="0.2032" layer="21"/>
<wire x1="1.4875" y1="-0.7938" x2="-1.4875" y2="-0.7938" width="0.2032" layer="21"/>
<wire x1="-1.4875" y1="-0.7938" x2="-1.4875" y2="0.7937" width="0.2032" layer="21"/>
<smd name="1" x="-0.7938" y="0" dx="0.9" dy="1.1" layer="1"/>
<smd name="2" x="0.7938" y="0" dx="0.9" dy="1.1" layer="1"/>
<text x="-1.27" y="1.1113" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.27" y="-1.905" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0805">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt;&lt;p&gt;
0805, grid 0.0125 inch</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1016" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1016" layer="51"/>
<wire x1="-1.8143" y1="0.8243" x2="1.8143" y2="0.8243" width="0.0508" layer="39"/>
<wire x1="1.8143" y1="0.8243" x2="1.8143" y2="-0.8243" width="0.0508" layer="39"/>
<wire x1="1.8143" y1="-0.8243" x2="-1.8143" y2="-0.8243" width="0.0508" layer="39"/>
<wire x1="-1.8143" y1="-0.8243" x2="-1.8143" y2="0.8243" width="0.0508" layer="39"/>
<wire x1="-1.8256" y1="1.0319" x2="1.8256" y2="1.0319" width="0.2032" layer="21"/>
<wire x1="1.8256" y1="1.0319" x2="1.8256" y2="-1.0319" width="0.2032" layer="21"/>
<wire x1="1.8256" y1="-1.0319" x2="-1.8256" y2="-1.0319" width="0.2032" layer="21"/>
<wire x1="-1.8256" y1="-1.0319" x2="-1.8256" y2="1.0319" width="0.2032" layer="21"/>
<smd name="1" x="-0.9525" y="0" dx="1.2" dy="1.5" layer="1"/>
<smd name="2" x="0.9525" y="0" dx="1.2" dy="1.5" layer="1"/>
<text x="-1.5875" y="1.4288" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.5874" y="-2.0638" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R0805W">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt;&lt;p&gt;
0805, wave solder, grid 0.0125 inch</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1016" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1016" layer="51"/>
<wire x1="-1.973" y1="0.8243" x2="1.973" y2="0.8243" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.8243" x2="1.973" y2="-0.8243" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.8243" x2="-1.973" y2="-0.8243" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.8243" x2="-1.973" y2="0.8243" width="0.0508" layer="39"/>
<wire x1="-1.9844" y1="0.8732" x2="1.9844" y2="0.8732" width="0.2032" layer="21"/>
<wire x1="1.9844" y1="0.8732" x2="1.9844" y2="-0.8732" width="0.2032" layer="21"/>
<wire x1="1.9844" y1="-0.8732" x2="-1.9844" y2="-0.8732" width="0.2032" layer="21"/>
<wire x1="-1.9844" y1="-0.8732" x2="-1.9844" y2="0.8732" width="0.2032" layer="21"/>
<smd name="1" x="-0.94" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="0.94" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-1.27" y="1.27" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R1005">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt;&lt;p&gt;
1005, grid 0.0125 inch</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1016" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1016" layer="51"/>
<wire x1="-0.9967" y1="0.483" x2="0.9968" y2="0.483" width="0.0508" layer="39"/>
<wire x1="0.9968" y1="0.483" x2="0.9968" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="0.9968" y1="-0.483" x2="-0.9967" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-0.9967" y1="-0.483" x2="-0.9967" y2="0.483" width="0.0508" layer="39"/>
<wire x1="-1.1113" y1="0.7144" x2="1.1113" y2="0.7144" width="0.2032" layer="21"/>
<wire x1="1.1113" y1="0.7144" x2="1.1113" y2="-0.7144" width="0.2032" layer="21"/>
<wire x1="1.1113" y1="-0.7144" x2="-1.1113" y2="-0.7144" width="0.2032" layer="21"/>
<wire x1="-1.1113" y1="-0.7144" x2="-1.1113" y2="0.7144" width="0.2032" layer="21"/>
<smd name="1" x="-0.4763" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.4763" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.9525" y="0.9525" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-0.9525" y="-1.905" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="R1206">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt;&lt;p&gt;
1206, grid 0.0125 inch</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1016" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1016" layer="51"/>
<wire x1="-2.4731" y1="0.983" x2="2.4731" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.4731" y1="0.983" x2="2.4731" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.4731" y1="-0.983" x2="-2.4731" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.4731" y1="-0.983" x2="-2.4731" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-2.3401" y1="1.1907" x2="2.3401" y2="1.1907" width="0.2032" layer="21"/>
<wire x1="2.3401" y1="1.1907" x2="2.3401" y2="-1.1907" width="0.2032" layer="21"/>
<wire x1="2.3401" y1="-1.1907" x2="-2.3401" y2="-1.1907" width="0.2032" layer="21"/>
<wire x1="-2.3401" y1="-1.1907" x2="-2.3401" y2="1.1907" width="0.2032" layer="21"/>
<smd name="2" x="1.3288" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="1" x="-1.3288" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-2.0638" y="1.5875" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.0638" y="-2.2225" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1206W">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt;&lt;p&gt;
0201, wave solder, grid 0.0125 inch</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1016" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1016" layer="51"/>
<wire x1="-2.3142" y1="0.983" x2="2.3142" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.3142" y1="0.983" x2="2.3142" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.3142" y1="-0.983" x2="-2.3142" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.3142" y1="-0.983" x2="-2.3142" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-2.2606" y1="1.1319" x2="2.2606" y2="1.1319" width="0.2032" layer="21"/>
<wire x1="2.2606" y1="1.1319" x2="2.2606" y2="-1.1319" width="0.2032" layer="21"/>
<wire x1="2.2606" y1="-1.1319" x2="-2.2606" y2="-1.1319" width="0.2032" layer="21"/>
<wire x1="-2.2606" y1="-1.1319" x2="-2.2606" y2="1.1319" width="0.2032" layer="21"/>
<smd name="1" x="-1.3402" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.3402" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-2.0412" y="1.4" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.0637" y="-2.2225" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1210">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt;&lt;p&gt;
1210, grid 0.0125 inch</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1016" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1016" layer="51"/>
<wire x1="-2.6317" y1="1.483" x2="2.6318" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.6318" y1="1.483" x2="2.6318" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.6318" y1="-1.483" x2="-2.6317" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.6317" y1="-1.483" x2="-2.6317" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-2.6988" y1="1.6669" x2="2.6988" y2="1.6669" width="0.2032" layer="21"/>
<wire x1="2.6988" y1="1.6669" x2="2.6988" y2="-1.6669" width="0.2032" layer="21"/>
<wire x1="2.6988" y1="-1.6669" x2="-2.6988" y2="-1.6669" width="0.2032" layer="21"/>
<wire x1="-2.6988" y1="-1.6669" x2="-2.6988" y2="1.6669" width="0.2032" layer="21"/>
<smd name="1" x="-1.5875" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.5875" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.2225" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.2225" y="-2.8575" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8999" x2="0.3" y2="0.8999" layer="35"/>
</package>
<package name="R1210W">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt;&lt;p&gt;
0201, wave solder, grid 0.00625 inch</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.2032" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.2032" layer="51"/>
<wire x1="-2.6318" y1="1.3243" x2="2.6318" y2="1.3243" width="0.0508" layer="39"/>
<wire x1="2.6318" y1="1.3243" x2="2.6318" y2="-1.3243" width="0.0508" layer="39"/>
<wire x1="2.6318" y1="-1.3243" x2="-2.6318" y2="-1.3243" width="0.0508" layer="39"/>
<wire x1="-2.6318" y1="-1.3243" x2="-2.6318" y2="1.3243" width="0.0508" layer="39"/>
<wire x1="-2.7782" y1="1.5081" x2="2.7782" y2="1.5081" width="0.2032" layer="21"/>
<wire x1="2.7782" y1="1.5081" x2="2.7782" y2="-1.5081" width="0.2032" layer="21"/>
<wire x1="2.7782" y1="-1.5081" x2="-2.7782" y2="-1.5081" width="0.2032" layer="21"/>
<wire x1="-2.7782" y1="-1.5081" x2="-2.7782" y2="1.5081" width="0.2032" layer="21"/>
<smd name="1" x="-1.5875" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.5875" y="0" dx="1.8" dy="1.8" layer="1" rot="R90"/>
<text x="-2.2225" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.2225" y="-2.8575" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="R2010">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt;&lt;p&gt;
2010, grid 0.00625 inch</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.2032" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.2032" layer="51"/>
<wire x1="-3.4731" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.4731" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.4731" y1="-1.483" x2="-3.4731" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.027" y1="1.245" x2="1.027" y2="1.245" width="0.2032" layer="51"/>
<wire x1="-1.002" y1="-1.245" x2="1.016" y2="-1.245" width="0.2032" layer="51"/>
<wire x1="-3.4132" y1="1.6669" x2="3.4132" y2="1.6669" width="0.2032" layer="21"/>
<wire x1="3.4132" y1="1.6669" x2="3.4132" y2="-1.6669" width="0.2032" layer="21"/>
<wire x1="3.4132" y1="-1.6669" x2="-3.4132" y2="-1.6669" width="0.2032" layer="21"/>
<wire x1="-3.4132" y1="-1.6669" x2="-3.4132" y2="1.6669" width="0.2032" layer="21"/>
<smd name="1" x="-2.2225" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2225" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-2.8575" y="2.0638" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.8575" y="-2.8575" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2010W">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt;&lt;p&gt;
2010, wave solder, grid 0.00625 inch</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.2032" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.2032" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-3.4925" y1="1.5081" x2="3.4925" y2="1.5081" width="0.2032" layer="21"/>
<wire x1="3.4925" y1="1.5081" x2="3.4925" y2="-1.5081" width="0.2032" layer="21"/>
<wire x1="3.4925" y1="-1.5081" x2="-3.4925" y2="-1.5081" width="0.2032" layer="21"/>
<wire x1="-3.4925" y1="-1.5081" x2="-3.4925" y2="1.5081" width="0.2032" layer="21"/>
<smd name="1" x="-2.2225" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.2225" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-3.175" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.175" y="-2.54" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2012">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt;&lt;p&gt;
2012, grid 0.0125 inch</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1016" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1016" layer="51"/>
<wire x1="-1.8143" y1="0.9831" x2="1.8142" y2="0.9831" width="0.0508" layer="39"/>
<wire x1="1.8142" y1="0.9831" x2="1.8142" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.8142" y1="-0.983" x2="-1.8143" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.8143" y1="-0.983" x2="-1.8143" y2="0.9831" width="0.0508" layer="39"/>
<wire x1="-1.905" y1="1.0319" x2="1.905" y2="1.0319" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.0319" x2="1.905" y2="-1.0319" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.0319" x2="-1.905" y2="-1.0319" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="-1.0319" x2="-1.905" y2="1.0319" width="0.2032" layer="21"/>
<smd name="1" x="-0.9525" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.9525" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.4288" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2012W">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt;&lt;p&gt;
2012, wave solder, grid 0.0125 inch</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1016" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1016" layer="51"/>
<wire x1="-1.973" y1="0.8243" x2="1.973" y2="0.8243" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.8243" x2="1.973" y2="-0.8243" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.8243" x2="-1.973" y2="-0.8243" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.8243" x2="-1.973" y2="0.8243" width="0.0508" layer="39"/>
<wire x1="-1.9844" y1="0.8731" x2="1.9844" y2="0.8731" width="0.2032" layer="21"/>
<wire x1="1.9844" y1="0.8731" x2="1.9844" y2="-0.8732" width="0.2032" layer="21"/>
<wire x1="1.9844" y1="-0.8732" x2="-1.9844" y2="-0.8732" width="0.2032" layer="21"/>
<wire x1="-1.9844" y1="-0.8732" x2="-1.9844" y2="0.8731" width="0.2032" layer="21"/>
<smd name="1" x="-0.94" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="0.94" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-1.5875" y="1.27" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.5875" y="-2.2225" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2512">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt;&lt;p&gt;
2512, grid 0.0125 inch</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.2032" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.2032" layer="51"/>
<wire x1="-3.973" y1="1.8243" x2="3.973" y2="1.8243" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.8243" x2="3.973" y2="-1.8242" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.8242" x2="-3.973" y2="-1.8242" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.8242" x2="-3.973" y2="1.8243" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="1.473" x2="1.498" y2="1.473" width="0.2032" layer="51"/>
<wire x1="-1.473" y1="-1.473" x2="1.498" y2="-1.473" width="0.2032" layer="51"/>
<wire x1="-4.0482" y1="1.905" x2="4.0481" y2="1.905" width="0.2032" layer="21"/>
<wire x1="4.0481" y1="1.905" x2="4.0481" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="4.0481" y1="-1.905" x2="-4.0482" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="-4.0482" y1="-1.905" x2="-4.0482" y2="1.905" width="0.2032" layer="21"/>
<smd name="1" x="-2.8575" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8575" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-3.4925" y="2.2225" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.4925" y="-3.175" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R2512W">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt;&lt;p&gt;
2512, wave solder, grid 0.0125 inch</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.2032" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.2032" layer="51"/>
<wire x1="-3.973" y1="1.8242" x2="3.973" y2="1.8242" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.8242" x2="3.973" y2="-1.8243" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.8243" x2="-3.973" y2="-1.8243" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.8243" x2="-3.973" y2="1.8242" width="0.0508" layer="39"/>
<wire x1="-4.1275" y1="1.7462" x2="4.1275" y2="1.7462" width="0.2032" layer="21"/>
<wire x1="4.1275" y1="1.7462" x2="4.1275" y2="-1.7463" width="0.2032" layer="21"/>
<wire x1="4.1275" y1="-1.7463" x2="-4.1275" y2="-1.7463" width="0.2032" layer="21"/>
<wire x1="-4.1275" y1="-1.7463" x2="-4.1275" y2="1.7462" width="0.2032" layer="21"/>
<smd name="1" x="-2.8575" y="0" dx="2" dy="2.1" layer="1"/>
<smd name="2" x="2.8575" y="0" dx="2" dy="2.1" layer="1"/>
<text x="-3.81" y="2.2225" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.81" y="-2.8575" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R3216">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt;&lt;p&gt;
3216, grid 0.0125 inch</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1016" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1016" layer="51"/>
<wire x1="-2.6318" y1="0.983" x2="2.6318" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.6318" y1="0.983" x2="2.6318" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.6318" y1="-0.983" x2="-2.6318" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.6318" y1="-0.983" x2="-2.6318" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-2.6988" y1="1.1906" x2="2.6988" y2="1.1906" width="0.2032" layer="21"/>
<wire x1="2.6988" y1="1.1906" x2="2.6988" y2="-1.1906" width="0.2032" layer="21"/>
<wire x1="2.6988" y1="-1.1906" x2="-2.6988" y2="-1.1906" width="0.2032" layer="21"/>
<wire x1="-2.6988" y1="-1.1906" x2="-2.6988" y2="1.1906" width="0.2032" layer="21"/>
<smd name="1" x="-1.5875" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.5875" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-2.2225" y="1.5875" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.2225" y="-2.2225" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3216W">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt;&lt;p&gt;
2512, wave solder, grid 0.0125 inch</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.2032" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.2032" layer="51"/>
<wire x1="-2.6317" y1="0.983" x2="2.6318" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.6318" y1="0.983" x2="2.6318" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.6318" y1="-0.983" x2="-2.6317" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.6317" y1="-0.983" x2="-2.6317" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-2.7781" y1="1.0319" x2="2.7781" y2="1.0319" width="0.2032" layer="21"/>
<wire x1="2.7781" y1="1.0319" x2="2.7781" y2="-1.0319" width="0.2032" layer="21"/>
<wire x1="2.7781" y1="-1.0319" x2="-2.7781" y2="-1.0319" width="0.2032" layer="21"/>
<wire x1="-2.7781" y1="-1.0319" x2="-2.7781" y2="1.0319" width="0.2032" layer="21"/>
<smd name="1" x="-1.5875" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.5875" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-2.2225" y="1.27" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.2225" y="-2.2225" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3225">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt;&lt;p&gt;
3225, grid 0.0125 inch</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1016" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1016" layer="51"/>
<wire x1="-2.6318" y1="1.483" x2="2.6318" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.6318" y1="1.483" x2="2.6318" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.6318" y1="-1.483" x2="-2.6318" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.6318" y1="-1.483" x2="-2.6318" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-2.6988" y1="1.6669" x2="2.6988" y2="1.6669" width="0.2032" layer="21"/>
<wire x1="2.6988" y1="1.6669" x2="2.6988" y2="-1.6669" width="0.2032" layer="21"/>
<wire x1="2.6988" y1="-1.6669" x2="-2.6988" y2="-1.6669" width="0.2032" layer="21"/>
<wire x1="-2.6988" y1="-1.6669" x2="-2.6988" y2="1.6669" width="0.2032" layer="21"/>
<smd name="1" x="-1.5875" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.5875" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.2225" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.2225" y="-2.8575" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R3225W">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt;&lt;p&gt;
3225, wave solder, grid 0.0125 inch</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.2032" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.2032" layer="51"/>
<wire x1="-2.6318" y1="1.483" x2="2.6317" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.6317" y1="1.483" x2="2.6317" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.6317" y1="-1.483" x2="-2.6318" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.6318" y1="-1.483" x2="-2.6318" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-2.7782" y1="1.5082" x2="2.7782" y2="1.5082" width="0.2032" layer="21"/>
<wire x1="2.7782" y1="1.5082" x2="2.7782" y2="-1.5082" width="0.2032" layer="21"/>
<wire x1="2.7782" y1="-1.5082" x2="-2.7782" y2="-1.5082" width="0.2032" layer="21"/>
<wire x1="-2.7782" y1="-1.5082" x2="-2.7782" y2="1.5082" width="0.2032" layer="21"/>
<smd name="1" x="-1.5875" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.5875" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-2.2225" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.2225" y="-2.54" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R5025">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt;&lt;p&gt;
5025, grid 0.0125 inch</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.2032" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.2032" layer="51"/>
<wire x1="-3.3143" y1="1.483" x2="3.3143" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.3143" y1="1.483" x2="3.3143" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.3143" y1="-1.483" x2="-3.3143" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.3143" y1="-1.483" x2="-3.3143" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9" y1="1.245" x2="0.9" y2="1.245" width="0.2032" layer="51"/>
<wire x1="-0.875" y1="-1.245" x2="0.925" y2="-1.245" width="0.2032" layer="51"/>
<wire x1="-3.4132" y1="1.6669" x2="3.4131" y2="1.6669" width="0.2032" layer="21"/>
<wire x1="3.4131" y1="1.6669" x2="3.4131" y2="-1.6669" width="0.2032" layer="21"/>
<wire x1="3.4131" y1="-1.6669" x2="-3.4132" y2="-1.6669" width="0.2032" layer="21"/>
<wire x1="-3.4132" y1="-1.6669" x2="-3.4132" y2="1.6669" width="0.2032" layer="21"/>
<smd name="1" x="-2.2225" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2225" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-2.8575" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.8575" y="-2.8575" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R5025W">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt;&lt;p&gt;
5025, wave solder, grid 0.0125 inch</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.2032" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.2032" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-3.4925" y1="1.5082" x2="3.4925" y2="1.5082" width="0.2032" layer="21"/>
<wire x1="3.4925" y1="1.5082" x2="3.4925" y2="-1.5082" width="0.2032" layer="21"/>
<wire x1="3.4925" y1="-1.5082" x2="-3.4925" y2="-1.5082" width="0.2032" layer="21"/>
<wire x1="-3.4925" y1="-1.5082" x2="-3.4925" y2="1.5082" width="0.2032" layer="21"/>
<smd name="1" x="-2.2225" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.2225" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-3.175" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.175" y="-2.6988" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt;&lt;p&gt;
6332, grid 0.0125 inch</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.2032" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.2032" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="1.473" x2="1.498" y2="1.473" width="0.2032" layer="51"/>
<wire x1="-1.473" y1="-1.473" x2="1.498" y2="-1.473" width="0.2032" layer="51"/>
<wire x1="-4.0481" y1="1.905" x2="4.0482" y2="1.905" width="0.2032" layer="21"/>
<wire x1="4.0482" y1="1.905" x2="4.0482" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="4.0482" y1="-1.905" x2="-4.0481" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="-4.0481" y1="-1.905" x2="-4.0481" y2="1.905" width="0.2032" layer="21"/>
<smd name="1" x="-2.8575" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8575" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-3.4925" y="2.2225" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.4925" y="-3.0163" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332W">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt;&lt;p&gt;
6332, wave solder, grid 0.0125 inch</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.2032" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.2032" layer="51"/>
<wire x1="-3.973" y1="1.6655" x2="3.973" y2="1.6655" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.6655" x2="3.973" y2="-1.6655" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.6655" x2="-3.973" y2="-1.6655" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.6655" x2="-3.973" y2="1.6655" width="0.0508" layer="39"/>
<wire x1="-4.1275" y1="1.7462" x2="4.1275" y2="1.7462" width="0.2032" layer="21"/>
<wire x1="4.1275" y1="1.7462" x2="4.1275" y2="-1.8257" width="0.2032" layer="21"/>
<wire x1="4.1275" y1="-1.8257" x2="-4.1275" y2="-1.8257" width="0.2032" layer="21"/>
<wire x1="-4.1275" y1="-1.8257" x2="-4.1275" y2="1.7462" width="0.2032" layer="21"/>
<smd name="1" x="-2.8575" y="0" dx="2" dy="2.1" layer="1"/>
<smd name="2" x="2.8575" y="0" dx="2" dy="2.1" layer="1"/>
<text x="-3.81" y="2.2225" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.81" y="-2.8575" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="RM0805">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.8142" y1="0.983" x2="1.8143" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.8143" y1="-0.983" x2="-1.8142" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.8142" y1="-0.983" x2="-1.8142" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.8143" y1="0.983" x2="1.8143" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1016" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1016" layer="51"/>
<wire x1="-1.905" y1="1.1113" x2="1.905" y2="1.1113" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.1113" x2="1.905" y2="-1.1113" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.1113" x2="-1.905" y2="-1.1113" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="-1.1113" x2="-1.905" y2="1.1113" width="0.2032" layer="21"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.27" y="1.4288" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="RM1206">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.3242" x2="2.473" y2="1.3242" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.3242" x2="-2.473" y2="-1.3242" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.3242" x2="-2.473" y2="1.3242" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.3242" x2="2.473" y2="-1.3242" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1016" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1016" layer="51"/>
<wire x1="-2.4606" y1="1.27" x2="2.4607" y2="1.27" width="0.2032" layer="21"/>
<wire x1="2.4607" y1="1.27" x2="2.4607" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="2.4607" y1="-1.27" x2="-2.4606" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-2.4606" y1="-1.27" x2="-2.4606" y2="1.27" width="0.2032" layer="21"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.905" y="1.5875" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.905" y="-2.3813" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="RM1406">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.6554" y1="0.983" x2="2.6555" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.6555" y1="-0.983" x2="-2.6554" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.6554" y1="-0.983" x2="-2.6554" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.6555" y1="0.983" x2="2.6555" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.2032" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.2032" layer="51"/>
<wire x1="0.6858" y1="0.762" x2="-0.6858" y2="0.762" width="0.2032" layer="51"/>
<wire x1="0.6858" y1="-0.762" x2="-0.6858" y2="-0.762" width="0.2032" layer="51"/>
<wire x1="-2.6988" y1="1.1906" x2="2.6988" y2="1.1906" width="0.2032" layer="21"/>
<wire x1="2.6988" y1="1.1906" x2="2.6988" y2="-1.1906" width="0.2032" layer="21"/>
<wire x1="2.6988" y1="-1.1906" x2="-2.6988" y2="-1.1906" width="0.2032" layer="21"/>
<wire x1="-2.6988" y1="-1.1906" x2="-2.6988" y2="1.1906" width="0.2032" layer="21"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-2.2225" y="1.5875" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.2225" y="-2.2225" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="RM2012">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.8143" y1="0.983" x2="1.8143" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.8143" y1="-0.983" x2="-1.8143" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.8143" y1="-0.983" x2="-1.8143" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.8143" y1="0.983" x2="1.8143" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1016" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1016" layer="51"/>
<wire x1="-1.905" y1="1.1113" x2="1.905" y2="1.1113" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.1113" x2="1.905" y2="-1.1113" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.1113" x2="-1.905" y2="-1.1113" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="-1.1113" x2="-1.905" y2="1.1113" width="0.2032" layer="21"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.27" y="-2.0638" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="RM2309">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-3.838" y1="1.483" x2="3.838" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.838" y1="-1.483" x2="-3.838" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.838" y1="-1.483" x2="-3.838" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.838" y1="1.483" x2="3.838" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.2032" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.2032" layer="51"/>
<wire x1="1.651" y1="1.1684" x2="-1.6764" y2="1.1684" width="0.2032" layer="51"/>
<wire x1="1.651" y1="-1.1684" x2="-1.651" y2="-1.1684" width="0.2032" layer="51"/>
<wire x1="-3.8893" y1="1.5875" x2="3.8894" y2="1.5875" width="0.2032" layer="21"/>
<wire x1="3.8894" y1="1.5875" x2="3.8894" y2="-1.5875" width="0.2032" layer="21"/>
<wire x1="3.8894" y1="-1.5875" x2="-3.8893" y2="-1.5875" width="0.2032" layer="21"/>
<wire x1="-3.8893" y1="-1.5875" x2="-3.8893" y2="1.5875" width="0.2032" layer="21"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-3.175" y="1.5875" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.175" y="-2.54" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="RM3216">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.3243" x2="2.473" y2="1.3243" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.3242" x2="-2.473" y2="-1.3242" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.3242" x2="-2.473" y2="1.3243" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.3243" x2="2.473" y2="-1.3242" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1016" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1016" layer="51"/>
<wire x1="-2.4606" y1="1.27" x2="2.4606" y2="1.27" width="0.2032" layer="21"/>
<wire x1="2.4606" y1="1.27" x2="2.4606" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="2.4606" y1="-1.27" x2="-2.4606" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-2.4606" y1="-1.27" x2="-2.4606" y2="1.27" width="0.2032" layer="21"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.905" y="1.5875" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="RM3516">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.6555" y1="0.983" x2="2.6555" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.6555" y1="-0.983" x2="-2.6555" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.6555" y1="-0.983" x2="-2.6555" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.6555" y1="0.983" x2="2.6555" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.2032" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.2032" layer="51"/>
<wire x1="0.6858" y1="0.762" x2="-0.6858" y2="0.762" width="0.2032" layer="51"/>
<wire x1="0.6858" y1="-0.762" x2="-0.6858" y2="-0.762" width="0.2032" layer="51"/>
<wire x1="-2.6988" y1="1.1907" x2="2.6988" y2="1.1907" width="0.2032" layer="21"/>
<wire x1="2.6988" y1="1.1907" x2="2.6988" y2="-1.1907" width="0.2032" layer="21"/>
<wire x1="2.6988" y1="-1.1907" x2="-2.6988" y2="-1.1907" width="0.2032" layer="21"/>
<wire x1="-2.6988" y1="-1.1907" x2="-2.6988" y2="1.1907" width="0.2032" layer="21"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-2.2225" y="1.5875" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.2225" y="-2.2225" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="RM5923">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-3.838" y1="1.483" x2="3.838" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.838" y1="-1.483" x2="-3.838" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.838" y1="-1.483" x2="-3.838" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.838" y1="1.483" x2="3.838" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.2032" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.2032" layer="51"/>
<wire x1="1.651" y1="1.1684" x2="-1.6764" y2="1.1684" width="0.2032" layer="51"/>
<wire x1="1.651" y1="-1.1684" x2="-1.651" y2="-1.1684" width="0.2032" layer="51"/>
<wire x1="-3.8894" y1="1.5875" x2="3.8894" y2="1.5875" width="0.2032" layer="21"/>
<wire x1="3.8894" y1="1.5875" x2="3.8894" y2="-1.5875" width="0.2032" layer="21"/>
<wire x1="3.8894" y1="-1.5875" x2="-3.8894" y2="-1.5875" width="0.2032" layer="21"/>
<wire x1="-3.8894" y1="-1.5875" x2="-3.8894" y2="1.5875" width="0.2032" layer="21"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-3.175" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.175" y="-2.6988" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="0204/5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0" x2="-2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-1.7" y1="0.8" x2="1.7" y2="0.8" width="0.2032" layer="21"/>
<wire x1="1.7" y1="-0.8" x2="-1.7" y2="-0.8" width="0.2032" layer="21"/>
<wire x1="-1.7" y1="-0.8" x2="-1.7" y2="-0.5" width="0.2032" layer="21"/>
<wire x1="-1.7" y1="-0.5" x2="-1.7" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.5" x2="-1.7" y2="0.8" width="0.2032" layer="21"/>
<wire x1="1.7" y1="0.5" x2="1.7" y2="0.8" width="0.2032" layer="21"/>
<wire x1="1.7" y1="-0.8" x2="1.7" y2="-0.5" width="0.2032" layer="21"/>
<wire x1="1.7" y1="-0.5" x2="1.7" y2="0.5" width="0.2032" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" diameter="1.4224" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" diameter="1.4224" shape="octagon"/>
<text x="-1.905" y="1.27" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-0.254" x2="-1.778" y2="0.254" layer="51"/>
<rectangle x1="1.778" y1="-0.254" x2="2.032" y2="0.254" layer="51"/>
</package>
<package name="0204/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 7.5 mm</description>
<wire x1="3.81" y1="0" x2="2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-3.81" y1="0" x2="-2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-1.7" y1="0.8" x2="1.7" y2="0.8" width="0.2032" layer="21"/>
<wire x1="1.7" y1="0.8" x2="1.7" y2="-0.8" width="0.2032" layer="21"/>
<wire x1="1.7" y1="-0.8" x2="-1.7" y2="-0.8" width="0.2032" layer="21"/>
<wire x1="-1.7" y1="0.8" x2="-1.7" y2="-0.8" width="0.2032" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" diameter="1.4224" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" diameter="1.4224" shape="octagon"/>
<text x="-1.905" y="1.27" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.4288" y="-0.4763" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.8575" y1="-0.254" x2="-1.778" y2="0.254" layer="21"/>
<rectangle x1="1.778" y1="-0.254" x2="2.8575" y2="0.254" layer="21"/>
<rectangle x1="1.678" y1="-0.254" x2="1.932" y2="0.254" layer="51"/>
</package>
<package name="0207/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 10 mm</description>
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.2" y1="1.1" x2="3.2" y2="1.1" width="0.2032" layer="21"/>
<wire x1="3.2" y1="1.1" x2="3.2" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-1.1" x2="-3.2" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-1.1" x2="-3.2" y2="1.1" width="0.2032" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" diameter="1.524" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" diameter="1.524" shape="octagon"/>
<text x="-3.175" y="1.5875" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.54" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
</package>
<package name="0207/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 12 mm</description>
<wire x1="6.35" y1="0" x2="5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="4.445" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-4.445" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-3.2" y1="1.1" x2="3.2" y2="1.1" width="0.2032" layer="21"/>
<wire x1="3.2" y1="1.1" x2="3.2" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-1.1" x2="-3.2" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-1.1" x2="-3.2" y2="1.1" width="0.2032" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" diameter="1.524" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" diameter="1.524" shape="octagon"/>
<text x="-3.175" y="1.5875" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.54" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.3086" y2="0.3048" layer="21"/>
<rectangle x1="-5.3086" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
</package>
<package name="0207/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 15mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="5.715" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-5.715" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-3.2" y1="1.1" x2="3.2" y2="1.1" width="0.2032" layer="21"/>
<wire x1="3.2" y1="1.1" x2="3.2" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-1.1" x2="-3.2" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-1.1" x2="-3.2" y2="1.1" width="0.2032" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="0.8128" diameter="1.524" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.8128" diameter="1.524" shape="octagon"/>
<text x="-3.175" y="1.5875" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.54" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="5.715" y1="-0.3048" x2="6.5786" y2="0.3048" layer="21"/>
<rectangle x1="-6.5786" y1="-0.3048" x2="-5.715" y2="0.3048" layer="21"/>
</package>
<package name="0207/2V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="1.219" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.27" width="0.1778" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" diameter="1.524" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" diameter="1.524" shape="octagon"/>
<text x="0" y="1.27" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="0" y="-2.2225" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.3" x2="0.3" y2="0.3" layer="21"/>
</package>
<package name="0207/5V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-0.889" y2="0" width="0.6096" layer="51"/>
<wire x1="0.889" y1="0" x2="2.54" y2="0" width="0.6096" layer="51"/>
<circle x="-2.54" y="0" radius="1.27" width="0.1778" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" diameter="1.524" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" diameter="1.524" shape="octagon"/>
<text x="-0.9525" y="0.9525" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-0.9525" y="-2.2225" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-0.3" x2="1.6" y2="0.3" layer="21"/>
</package>
<package name="0207/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 7.5 mm</description>
<wire x1="-3.81" y1="0" x2="-3.429" y2="0" width="0.6096" layer="51"/>
<wire x1="3.429" y1="0" x2="3.81" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.2" y1="1.1" x2="3.2" y2="1.1" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-1.1" x2="-3.2" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-1.1" x2="-3.2" y2="-0.8" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-0.8" x2="-3.2" y2="0.8" width="0.2032" layer="51"/>
<wire x1="-3.2" y1="0.8" x2="-3.2" y2="1.1" width="0.2032" layer="21"/>
<wire x1="3.2" y1="0.8" x2="3.2" y2="1.1" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-1.1" x2="3.2" y2="-0.8" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-0.8" x2="3.2" y2="0.8" width="0.2032" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" diameter="1.524" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" diameter="1.524" shape="octagon"/>
<text x="-2.54" y="1.5875" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.54" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.429" y1="-0.3048" x2="-3.175" y2="0.3048" layer="51"/>
<rectangle x1="3.175" y1="-0.3048" x2="3.429" y2="0.3048" layer="51"/>
</package>
<package name="0309/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 10mm</description>
<wire x1="-4.699" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="5.08" y1="0" x2="4.699" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.3" y1="1.5" x2="-4.3" y2="0.9" width="0.2032" layer="21"/>
<wire x1="-4.3" y1="0.9" x2="-4.3" y2="-0.9" width="0.2032" layer="51"/>
<wire x1="-4.3" y1="-0.9" x2="-4.3" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-4.3" y1="-1.5" x2="4.3" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="4.3" y1="1.5" x2="-4.3" y2="1.5" width="0.2032" layer="21"/>
<wire x1="4.3" y1="1.5" x2="4.3" y2="0.9" width="0.2032" layer="21"/>
<wire x1="4.3" y1="-0.9" x2="4.3" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="4.3" y1="0.8" x2="4.3" y2="-1" width="0.2032" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<text x="-4.1275" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.4925" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.6228" y1="-0.3048" x2="-4.318" y2="0.3048" layer="51"/>
<rectangle x1="4.318" y1="-0.3048" x2="4.6228" y2="0.3048" layer="51"/>
</package>
<package name="0309/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.3" y1="1.5" x2="-4.3" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-4.3" y1="-1.5" x2="4.3" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="4.3" y1="-1.5" x2="4.3" y2="1.5" width="0.2032" layer="21"/>
<wire x1="4.3" y1="1.5" x2="-4.3" y2="1.5" width="0.2032" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<text x="-4.1275" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.318" y1="-0.3048" x2="5.1816" y2="0.3048" layer="21"/>
<rectangle x1="-5.1816" y1="-0.3048" x2="-4.318" y2="0.3048" layer="21"/>
</package>
<package name="0411/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.261" y2="0" width="0.762" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.261" y2="0" width="0.762" layer="51"/>
<wire x1="-4.9" y1="2" x2="4.9" y2="2" width="0.2032" layer="21"/>
<wire x1="4.9" y1="2" x2="4.9" y2="-2" width="0.2032" layer="21"/>
<wire x1="4.9" y1="-2" x2="-4.9" y2="-2" width="0.2032" layer="21"/>
<wire x1="-4.9" y1="-2" x2="-4.9" y2="2" width="0.2032" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.9144" diameter="2.1844" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.9144" diameter="2.1844" shape="octagon"/>
<text x="-5.08" y="2.54" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.4925" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.1594" y1="-0.381" x2="-4.88" y2="0.381" layer="21"/>
<rectangle x1="4.88" y1="-0.381" x2="5.1594" y2="0.381" layer="21"/>
</package>
<package name="0411/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 15 mm</description>
<wire x1="-7.62" y1="0" x2="-6.35" y2="0" width="0.762" layer="51"/>
<wire x1="6.35" y1="0" x2="7.62" y2="0" width="0.762" layer="51"/>
<wire x1="-4.9" y1="2" x2="4.9" y2="2" width="0.2032" layer="21"/>
<wire x1="4.9" y1="2" x2="4.9" y2="-2" width="0.2032" layer="21"/>
<wire x1="4.9" y1="-2" x2="-4.9" y2="-2" width="0.2032" layer="21"/>
<wire x1="-4.9" y1="-2" x2="-4.9" y2="2" width="0.2032" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="0.9144" diameter="2.1844" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.9144" diameter="2.1844" shape="octagon"/>
<text x="-5.08" y="2.54" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.4925" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.3" y1="-0.381" x2="-4.9" y2="0.381" layer="21"/>
<rectangle x1="4.9" y1="-0.381" x2="6.3" y2="0.381" layer="21"/>
</package>
<package name="0411V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 3.81 mm</description>
<wire x1="1.27" y1="0" x2="-2.54" y2="0" width="0.762" layer="51"/>
<circle x="-2.54" y="0" radius="2.032" width="0.2032" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.9144" diameter="2.1844" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.9144" diameter="2.1844" shape="octagon"/>
<text x="-0.3175" y="1.27" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-0.3175" y="-2.54" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.3" y1="-0.381" x2="0" y2="0.381" layer="21"/>
</package>
<package name="0414/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.1" y1="2.1" x2="6.1" y2="2.1" width="0.2032" layer="21"/>
<wire x1="6.1" y1="2.1" x2="6.1" y2="-2.1" width="0.2032" layer="21"/>
<wire x1="6.1" y1="-2.1" x2="-6.1" y2="-2.1" width="0.2032" layer="21"/>
<wire x1="-6.1" y1="-2.1" x2="-6.1" y2="2.1" width="0.2032" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="-6.0325" y="2.54" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-4.445" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="6.096" y1="-0.4064" x2="6.5024" y2="0.4064" layer="21"/>
<rectangle x1="-6.5024" y1="-0.4064" x2="-6.096" y2="0.4064" layer="21"/>
</package>
<package name="0414V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.159" width="0.2032" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="0" y="1.27" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="0" y="-2.54" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.1" y1="-0.4064" x2="1.1" y2="0.4064" layer="21"/>
</package>
<package name="0617/17">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 17.5 mm</description>
<wire x1="-8.89" y1="0" x2="-8.636" y2="0" width="0.8128" layer="51"/>
<wire x1="8.636" y1="0" x2="8.89" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.2" y1="-3" x2="-8.2" y2="-1.4" width="0.2032" layer="21"/>
<wire x1="-8.2" y1="-1.4" x2="-8.2" y2="1.4" width="0.2032" layer="51"/>
<wire x1="-8.2" y1="1.4" x2="-8.2" y2="3" width="0.2032" layer="21"/>
<wire x1="-8.2" y1="3" x2="8.3" y2="3" width="0.2032" layer="21"/>
<wire x1="8.3" y1="-3" x2="-8.2" y2="-3" width="0.2032" layer="21"/>
<wire x1="8.3" y1="1.4" x2="8.3" y2="3" width="0.2032" layer="21"/>
<wire x1="8.3" y1="-3" x2="8.3" y2="-1.4" width="0.2032" layer="21"/>
<wire x1="8.3" y1="-1.4" x2="8.3" y2="1.4" width="0.2032" layer="51"/>
<pad name="1" x="-8.89" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="-7.9375" y="3.81" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-6.0325" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-8.5344" y1="-0.4064" x2="-8.2296" y2="0.4064" layer="51"/>
<rectangle x1="8.2296" y1="-0.4064" x2="8.5344" y2="0.4064" layer="51"/>
</package>
<package name="0617/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 22.5 mm</description>
<wire x1="-10.087" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="11.413" y1="0" x2="10.07" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.2" y1="3" x2="8.2" y2="3" width="0.2032" layer="21"/>
<wire x1="8.2" y1="3" x2="8.2" y2="-3" width="0.2032" layer="21"/>
<wire x1="8.2" y1="-3" x2="-8.2" y2="-3" width="0.2032" layer="21"/>
<wire x1="-8.2" y1="-3" x2="-8.2" y2="3" width="0.2032" layer="21"/>
<pad name="1" x="-11.43" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="-8.255" y="3.81" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-6.35" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10" y1="-0.4" x2="-8.25" y2="0.4" layer="21"/>
<rectangle x1="8.3" y1="-0.4" x2="10.05" y2="0.4" layer="21"/>
</package>
<package name="0617V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="3.048" width="0.2032" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="0.9525" y="1.5875" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="0.9525" y="-2.8575" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.1" y1="-0.4" x2="1.1" y2="0.4064" layer="21"/>
</package>
<package name="0922/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 22.5 mm</description>
<wire x1="11.73" y1="0" x2="10.46" y2="0" width="0.8128" layer="51"/>
<wire x1="-11.73" y1="0" x2="-10.46" y2="0" width="0.8128" layer="51"/>
<wire x1="-10.1" y1="4.5" x2="10.1" y2="4.5" width="0.2032" layer="21"/>
<wire x1="10.1" y1="4.5" x2="10.1" y2="-4.6" width="0.2032" layer="21"/>
<wire x1="10.1" y1="-4.6" x2="-10.1" y2="-4.6" width="0.2032" layer="21"/>
<wire x1="-10.1" y1="-4.6" x2="-10.1" y2="4.5" width="0.2032" layer="21"/>
<pad name="1" x="-11.7475" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<pad name="2" x="11.7475" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="-9.8425" y="5.3975" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-6.35" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="10.1425" y1="-0.4064" x2="10.2949" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-10.16" y2="0.4064" layer="21"/>
</package>
<package name="P0613V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.286" width="0.2032" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.2032" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" diameter="2.1844" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="2.1844" shape="octagon"/>
<text x="-0.254" y="1.143" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-0.254" y="-2.413" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="P0613/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="0.635" width="0.2032" layer="51"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="6.477" y1="-2.3" x2="-6.477" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-6.477" y1="-2.3" x2="-6.477" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="-0.635" width="0.2032" layer="51"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="2.3" width="0.2032" layer="21"/>
<wire x1="-6.477" y1="2.3" x2="6.477" y2="2.3" width="0.2032" layer="21"/>
<wire x1="6.477" y1="2.3" x2="6.477" y2="0.635" width="0.2032" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" diameter="2.1844" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" diameter="2.1844" shape="octagon"/>
<text x="-6.477" y="2.6924" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-4.318" y="-0.7112" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-7.0358" y1="-0.4064" x2="-6.477" y2="0.4064" layer="51"/>
<rectangle x1="6.477" y1="-0.4064" x2="7.0358" y2="0.4064" layer="51"/>
</package>
<package name="P0817/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 22.5 mm</description>
<wire x1="-10.414" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="11.43" y1="0" x2="10.414" y2="0" width="0.8128" layer="51"/>
<wire x1="8.5" y1="3.75" x2="8.5" y2="-3.75" width="0.2032" layer="21"/>
<wire x1="8.5" y1="-3.75" x2="-8.5" y2="-3.75" width="0.2032" layer="21"/>
<wire x1="-8.5" y1="-3.75" x2="-8.5" y2="3.75" width="0.2032" layer="21"/>
<wire x1="-8.5" y1="3.75" x2="8.5" y2="3.75" width="0.2032" layer="21"/>
<pad name="1" x="-11.43" y="0" drill="1.016" diameter="2.1844" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" diameter="2.1844" shape="octagon"/>
<text x="-8.382" y="4.2164" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-6.223" y="-0.5842" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="6.604" y="-2.2606" size="1.27" layer="51" ratio="10" rot="R90">0817</text>
<rectangle x1="8.509" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-8.509" y2="0.4064" layer="21"/>
</package>
<package name="P0817V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 6.35 mm</description>
<wire x1="-3.81" y1="0" x2="-5.08" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="3.81" width="0.2032" layer="21"/>
<circle x="-5.08" y="0" radius="1.27" width="0.2032" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.016" diameter="2.1844" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="1.016" diameter="2.1844" shape="octagon"/>
<text x="-1.016" y="1.27" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.016" y="-2.54" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.032" size="1.016" layer="21" ratio="18">0817</text>
<rectangle x1="-3.81" y1="-0.4064" x2="0" y2="0.4064" layer="21"/>
</package>
<package name="RMINI_MELF-0102AX">
<description>&lt;b&gt;SMD Mini MELF 0102 Axial&lt;/b&gt;</description>
<circle x="0" y="0" radius="0.6" width="0" layer="51"/>
<circle x="0" y="0" radius="0.6" width="0" layer="52"/>
<smd name="1" x="0" y="0" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="2" x="0" y="0" dx="1.9" dy="1.9" layer="16" roundness="100"/>
<text x="-1.27" y="1.27" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.27" y="-1.905" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<hole x="0" y="0" drill="1.3"/>
</package>
<package name="0922V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 7.5 mm</description>
<wire x1="-5.08" y1="0" x2="2.54" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="4.572" width="0.2032" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="0" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="0" y="-2.8575" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.6" y1="-0.4064" x2="1.1" y2="0.4064" layer="21"/>
</package>
<package name="RMINI_MELF-0102R">
<description>&lt;b&gt;SMD CECC Size RC2211&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="-1.4288" y1="0.9525" x2="1.4288" y2="0.9525" width="0.2032" layer="21"/>
<wire x1="1.4288" y1="0.9525" x2="1.4288" y2="-0.9525" width="0.2032" layer="21"/>
<wire x1="1.4288" y1="-0.9525" x2="-1.4288" y2="-0.9525" width="0.2032" layer="21"/>
<wire x1="-1.4288" y1="-0.9525" x2="-1.4288" y2="0.9525" width="0.2032" layer="21"/>
<smd name="1" x="-0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<text x="-1.27" y="1.27" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.27" y="-2.0638" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="RMINI_MELF-0102W">
<description>&lt;b&gt;SMD CECC Size RC2211&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="-1.5081" y1="0.9525" x2="1.5082" y2="0.9525" width="0.2032" layer="21"/>
<wire x1="1.5082" y1="0.9525" x2="1.5082" y2="-0.9525" width="0.2032" layer="21"/>
<wire x1="1.5082" y1="-0.9525" x2="-1.5081" y2="-0.9525" width="0.2032" layer="21"/>
<wire x1="-1.5081" y1="-0.9525" x2="-1.5081" y2="0.9525" width="0.2032" layer="21"/>
<smd name="1" x="-0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<smd name="2" x="0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<text x="-1.27" y="1.2701" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.27" y="-1.905" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="RMINI_MELF-0204R">
<description>&lt;b&gt;SMD CECC Size RC3715&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="-2.2225" y1="1.1113" x2="2.2225" y2="1.1113" width="0.2032" layer="21"/>
<wire x1="2.2225" y1="1.1113" x2="2.2225" y2="-1.1113" width="0.2032" layer="21"/>
<wire x1="2.2225" y1="-1.1113" x2="-2.2225" y2="-1.1113" width="0.2032" layer="21"/>
<wire x1="-2.2225" y1="-1.1113" x2="-2.2225" y2="1.1113" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<text x="-1.5875" y="1.27" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.5875" y="-2.0638" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="RMINI_MELF-0204W">
<description>&lt;b&gt;SMD CECC Size RC3715&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="-2.3813" y1="1.1113" x2="2.3813" y2="1.1113" width="0.2032" layer="21"/>
<wire x1="2.3813" y1="1.1113" x2="2.3813" y2="-1.1113" width="0.2032" layer="21"/>
<wire x1="2.3813" y1="-1.1113" x2="-2.3813" y2="-1.1113" width="0.2032" layer="21"/>
<wire x1="-2.3813" y1="-1.1113" x2="-2.3813" y2="1.1113" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<text x="-1.905" y="1.4288" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.905" y="-2.2225" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="RMINI_MELF-0207R">
<description>&lt;b&gt;SMD CECC Size RC6123&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="-3.3338" y1="1.5082" x2="3.3338" y2="1.5082" width="0.2032" layer="21"/>
<wire x1="3.3338" y1="1.5082" x2="3.3338" y2="-1.5081" width="0.2032" layer="21"/>
<wire x1="3.3338" y1="-1.5081" x2="-3.3338" y2="-1.5081" width="0.2032" layer="21"/>
<wire x1="-3.3338" y1="-1.5081" x2="-3.3338" y2="1.5082" width="0.2032" layer="21"/>
<smd name="1" x="-2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<smd name="2" x="2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<text x="-2.8575" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.8575" y="-2.6988" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="RMINI_MELF-0207W">
<description>&lt;b&gt;SMD CECC Size RC6123&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="-4.1275" y1="1.5875" x2="4.1275" y2="1.5875" width="0.2032" layer="21"/>
<wire x1="4.1275" y1="1.5875" x2="4.1275" y2="-1.5875" width="0.2032" layer="21"/>
<wire x1="4.1275" y1="-1.5875" x2="-4.1275" y2="-1.5875" width="0.2032" layer="21"/>
<wire x1="-4.1275" y1="-1.5875" x2="-4.1275" y2="1.5875" width="0.2032" layer="21"/>
<smd name="1" x="-2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<smd name="2" x="2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<text x="-3.4925" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.4925" y="-2.8575" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0204V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.508" layer="51"/>
<circle x="-1.27" y="0" radius="1.0261" width="0.2032" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" diameter="1.4224" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" diameter="1.4224" shape="octagon"/>
<text x="-2.2225" y="1.27" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.2225" y="-2.2225" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.2" x2="0.4" y2="0.2" layer="21"/>
</package>
<package name="0309V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 2.5 mm</description>
<wire x1="1.27" y1="0" x2="0.635" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.524" width="0.1778" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<text x="0.635" y="1.27" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="0.635" y="-2.2225" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-0.3048" x2="0.5588" y2="0.3048" layer="51"/>
<rectangle x1="-0.635" y1="-0.3048" x2="-0.3302" y2="0.3048" layer="51"/>
<rectangle x1="-0.3302" y1="-0.3048" x2="0.254" y2="0.3048" layer="51"/>
</package>
<package name="0207/10B">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 10 mm</description>
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.2" y1="1.1" x2="3.2" y2="1.1" width="0.2032" layer="21"/>
<wire x1="3.2" y1="1.1" x2="3.2" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-1.1" x2="-3.2" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-1.1" x2="-3.2" y2="1.1" width="0.2032" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="-3.175" y="1.5875" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.54" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
</package>
<package name="MF50">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MF50, RCD Components</description>
<wire x1="2.25" y1="-0.615" x2="1.865" y2="-1" width="0.2032" layer="21" curve="-90"/>
<wire x1="1.865" y1="-1" x2="-1.865" y2="-1" width="0.2032" layer="21"/>
<wire x1="-1.865" y1="-1" x2="-2.25" y2="-0.615" width="0.2032" layer="21" curve="-90"/>
<wire x1="-2.25" y1="-0.615" x2="-2.25" y2="0.615" width="0.2032" layer="21"/>
<wire x1="-2.25" y1="0.615" x2="-1.865" y2="1" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1.865" y1="1" x2="1.865" y2="1" width="0.2032" layer="21"/>
<wire x1="1.865" y1="1" x2="2.25" y2="0.615" width="0.2032" layer="21" curve="-90"/>
<wire x1="2.25" y1="0.615" x2="2.25" y2="-0.615" width="0.2032" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" diameter="1.4224" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" diameter="1.4224" shape="octagon"/>
<text x="-1.905" y="1.5875" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.7463" y="-0.4763" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3" y1="-0.25" x2="-2.25" y2="0.25" layer="21"/>
<rectangle x1="2.25" y1="-0.25" x2="3" y2="0.25" layer="21"/>
</package>
<package name="MF60">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MF60, RCD Components</description>
<wire x1="5.17" y1="-1.385" x2="4.535" y2="-2.02" width="0.2032" layer="21" curve="-90"/>
<wire x1="4.535" y1="-2.02" x2="-4.535" y2="-2.02" width="0.2032" layer="21"/>
<wire x1="-4.535" y1="-2.02" x2="-5.17" y2="-1.385" width="0.2032" layer="21" curve="-90"/>
<wire x1="-5.17" y1="-1.385" x2="-5.17" y2="1.385" width="0.2032" layer="21"/>
<wire x1="-5.17" y1="1.385" x2="-4.535" y2="2.02" width="0.2032" layer="21" curve="-90"/>
<wire x1="-4.535" y1="2.02" x2="4.535" y2="2.02" width="0.2032" layer="21"/>
<wire x1="4.535" y1="2.02" x2="5.17" y2="1.385" width="0.2032" layer="21" curve="-90"/>
<wire x1="5.17" y1="1.385" x2="5.17" y2="-1.385" width="0.2032" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="-5.08" y="2.8575" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.81" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.5" y1="-0.25" x2="-5.17" y2="0.25" layer="21"/>
<rectangle x1="5.17" y1="-0.25" x2="6.5" y2="0.25" layer="21"/>
</package>
<package name="MF55">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MF55, RCD Components</description>
<wire x1="3.65" y1="-0.615" x2="3.015" y2="-1.25" width="0.2032" layer="21" curve="-90"/>
<wire x1="3.015" y1="-1.25" x2="-3.015" y2="-1.25" width="0.2032" layer="21"/>
<wire x1="-3.015" y1="-1.25" x2="-3.65" y2="-0.615" width="0.2032" layer="21" curve="-90"/>
<wire x1="-3.65" y1="-0.615" x2="-3.65" y2="0.615" width="0.2032" layer="21"/>
<wire x1="-3.65" y1="0.615" x2="-3.015" y2="1.25" width="0.2032" layer="21" curve="-90"/>
<wire x1="-3.015" y1="1.25" x2="3.015" y2="1.25" width="0.2032" layer="21"/>
<wire x1="3.015" y1="1.25" x2="3.65" y2="0.615" width="0.2032" layer="21" curve="-90"/>
<wire x1="3.65" y1="0.615" x2="3.65" y2="-0.615" width="0.2032" layer="21"/>
<pad name="1" x="-5.715" y="0" drill="0.8128" diameter="1.4224" shape="octagon"/>
<pad name="2" x="5.715" y="0" drill="0.8128" diameter="1.4224" shape="octagon"/>
<text x="-3.175" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.8963" y="-0.4763" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.75" y1="-0.25" x2="-3.65" y2="0.25" layer="21"/>
<rectangle x1="3.65" y1="-0.25" x2="4.75" y2="0.25" layer="21"/>
</package>
<package name="MF65">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MF65, RCD Components</description>
<wire x1="7.42" y1="-1.985" x2="6.785" y2="-2.62" width="0.2032" layer="21" curve="-90"/>
<wire x1="6.785" y1="-2.62" x2="-6.785" y2="-2.62" width="0.2032" layer="21"/>
<wire x1="-6.785" y1="-2.62" x2="-7.42" y2="-1.985" width="0.2032" layer="21" curve="-90"/>
<wire x1="-7.42" y1="-1.985" x2="-7.42" y2="1.985" width="0.2032" layer="21"/>
<wire x1="-7.42" y1="1.985" x2="-6.785" y2="2.62" width="0.2032" layer="21" curve="-90"/>
<wire x1="-6.785" y1="2.62" x2="6.785" y2="2.62" width="0.2032" layer="21"/>
<wire x1="6.785" y1="2.62" x2="7.42" y2="1.985" width="0.2032" layer="21" curve="-90"/>
<wire x1="7.42" y1="1.985" x2="7.42" y2="-1.985" width="0.2032" layer="21"/>
<pad name="1" x="-10.16" y="0" drill="1.016" diameter="2.1844" shape="octagon"/>
<pad name="2" x="10.16" y="0" drill="1.016" diameter="2.1844" shape="octagon"/>
<text x="-6.985" y="3.4925" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.81" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-8.75" y1="-0.25" x2="-7.42" y2="0.25" layer="21"/>
<rectangle x1="7.42" y1="-0.25" x2="8.75" y2="0.25" layer="21"/>
</package>
<package name="MF70">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MF70, RCD Components</description>
<wire x1="9.17" y1="-2.735" x2="8.535" y2="-3.37" width="0.2032" layer="21" curve="-90"/>
<wire x1="8.535" y1="-3.37" x2="-8.535" y2="-3.37" width="0.2032" layer="21"/>
<wire x1="-8.535" y1="-3.37" x2="-9.17" y2="-2.735" width="0.2032" layer="21" curve="-90"/>
<wire x1="-9.17" y1="-2.735" x2="-9.17" y2="2.735" width="0.2032" layer="21"/>
<wire x1="-9.17" y1="2.735" x2="-8.535" y2="3.37" width="0.2032" layer="21" curve="-90"/>
<wire x1="-8.535" y1="3.37" x2="8.535" y2="3.37" width="0.2032" layer="21"/>
<wire x1="8.535" y1="3.37" x2="9.17" y2="2.735" width="0.2032" layer="21" curve="-90"/>
<wire x1="9.17" y1="2.735" x2="9.17" y2="-2.735" width="0.2032" layer="21"/>
<pad name="1" x="-12.7" y="0" drill="1.016" diameter="2.1844" shape="octagon"/>
<pad name="2" x="12.7" y="0" drill="1.016" diameter="2.1844" shape="octagon"/>
<text x="-8.89" y="4.1275" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.81" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-11.25" y1="-0.25" x2="-9.17" y2="0.25" layer="21"/>
<rectangle x1="9.17" y1="-0.25" x2="11.25" y2="0.25" layer="21"/>
</package>
<package name="MF75">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MF75, RCD Components</description>
<wire x1="14.67" y1="-4.485" x2="14.035" y2="-5.12" width="0.2032" layer="21" curve="-90"/>
<wire x1="14.035" y1="-5.12" x2="-14.035" y2="-5.12" width="0.2032" layer="21"/>
<wire x1="-14.035" y1="-5.12" x2="-14.67" y2="-4.485" width="0.2032" layer="21" curve="-90"/>
<wire x1="-14.67" y1="-4.485" x2="-14.67" y2="4.985" width="0.2032" layer="21"/>
<wire x1="-14.67" y1="4.985" x2="-14.035" y2="5.62" width="0.2032" layer="21" curve="-90"/>
<wire x1="-14.035" y1="5.62" x2="14.035" y2="5.62" width="0.2032" layer="21"/>
<wire x1="14.035" y1="5.62" x2="14.67" y2="4.985" width="0.2032" layer="21" curve="-90"/>
<wire x1="14.67" y1="4.985" x2="14.67" y2="-4.485" width="0.2032" layer="21"/>
<pad name="1" x="-19.05" y="0" drill="1.27" diameter="2.54" shape="octagon"/>
<pad name="2" x="19.05" y="0" drill="1.27" diameter="2.54" shape="octagon"/>
<text x="-14.605" y="6.35" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="14.67" y1="-0.5" x2="17.5" y2="0.5" layer="21"/>
<rectangle x1="-17.58" y1="-0.5" x2="-14.75" y2="0.5" layer="21"/>
</package>
<package name="0207/10C">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 10 mm</description>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.2" y1="1.1" x2="3.2" y2="1.1" width="0.2032" layer="21"/>
<wire x1="3.2" y1="1.1" x2="3.2" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-1.1" x2="-3.2" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-1.1" x2="-3.2" y2="1.1" width="0.2032" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<text x="-3.175" y="1.5875" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.54" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
</package>
<package name="0309/12B">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.3" y1="1.5" x2="-4.3" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-4.3" y1="-1.5" x2="4.3" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="4.3" y1="-1.5" x2="4.3" y2="1.5" width="0.2032" layer="21"/>
<wire x1="4.3" y1="1.5" x2="-4.3" y2="1.5" width="0.2032" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.9144" diameter="2.1844" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.9144" diameter="2.1844" shape="octagon"/>
<text x="-4.1275" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.318" y1="-0.3048" x2="5.1816" y2="0.3048" layer="21"/>
<rectangle x1="-5.1816" y1="-0.3048" x2="-4.318" y2="0.3048" layer="21"/>
</package>
<package name="0309/14">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 12.5 mm</description>
<wire x1="-7.05" y1="0" x2="-5.7" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.3" y1="1.5" x2="-4.3" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-4.3" y1="-1.5" x2="4.3" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="4.3" y1="-1.5" x2="4.3" y2="1.5" width="0.2032" layer="21"/>
<wire x1="4.3" y1="1.5" x2="-4.3" y2="1.5" width="0.2032" layer="21"/>
<wire x1="7.05" y1="0" x2="5.7" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-6.985" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<pad name="2" x="6.985" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<text x="-4.1275" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.8" y1="-0.3048" x2="-4.318" y2="0.3" layer="21"/>
<rectangle x1="4.318" y1="-0.3" x2="5.8" y2="0.3048" layer="21" rot="R180"/>
</package>
<package name="0309/14B">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 12.5 mm</description>
<wire x1="6.95" y1="0" x2="5.68" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.95" y1="0" x2="-5.68" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.3" y1="1.5" x2="-4.3" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-4.3" y1="-1.5" x2="4.3" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="4.3" y1="-1.5" x2="4.3" y2="1.5" width="0.2032" layer="21"/>
<wire x1="4.3" y1="1.5" x2="-4.3" y2="1.5" width="0.2032" layer="21"/>
<pad name="1" x="-6.985" y="0" drill="0.9144" diameter="2.1844" shape="octagon"/>
<pad name="2" x="6.985" y="0" drill="0.9144" diameter="2.1844" shape="octagon"/>
<text x="-4.1275" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.2863" y1="-0.3175" x2="5.8166" y2="0.3048" layer="21"/>
<rectangle x1="-5.8166" y1="-0.3175" x2="-4.2863" y2="0.3048" layer="21"/>
</package>
<package name="0309/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 15mm</description>
<wire x1="-6.604" y1="0" x2="-7.62" y2="0" width="0.6096" layer="51"/>
<wire x1="7.62" y1="0" x2="6.477" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.3" y1="1.5" x2="-4.3" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-4.3" y1="-1.5" x2="4.3" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="4.3" y1="-1.5" x2="4.3" y2="1.5" width="0.2032" layer="21"/>
<wire x1="4.3" y1="1.5" x2="-4.3" y2="1.5" width="0.2032" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<text x="-4.1275" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.477" y1="-0.3175" x2="-4.318" y2="0.3175" layer="21"/>
<rectangle x1="4.318" y1="-0.3175" x2="6.477" y2="0.3175" layer="21"/>
</package>
<package name="0309/15B">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 15mm</description>
<wire x1="-4.3" y1="1.5" x2="-4.3" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-4.3" y1="-1.5" x2="4.3" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="4.3" y1="-1.5" x2="4.3" y2="1.5" width="0.2032" layer="21"/>
<wire x1="4.3" y1="1.5" x2="-4.3" y2="1.5" width="0.2032" layer="21"/>
<wire x1="-6.604" y1="0" x2="-7.62" y2="0" width="0.6096" layer="51"/>
<wire x1="7.596" y1="0" x2="6.58" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-7.62" y="0" drill="0.9144" diameter="2.1844" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.9144" diameter="2.1844" shape="octagon"/>
<text x="-4.1275" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.477" y1="-0.3175" x2="-4.318" y2="0.3175" layer="21"/>
<rectangle x1="4.318" y1="-0.3175" x2="6.477" y2="0.3175" layer="21"/>
</package>
<package name="0309/20">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 20mm</description>
<wire x1="-9.144" y1="0" x2="-10.16" y2="0" width="0.6096" layer="51"/>
<wire x1="10.16" y1="0" x2="9.017" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.3" y1="1.5" x2="-4.3" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-4.3" y1="-1.5" x2="4.3" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="4.3" y1="-1.5" x2="4.3" y2="1.5" width="0.2032" layer="21"/>
<wire x1="4.3" y1="1.5" x2="-4.3" y2="1.5" width="0.2032" layer="21"/>
<pad name="1" x="-10.16" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<pad name="2" x="10.16" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="-4.1275" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="-4.1275" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-9.017" y1="-0.3175" x2="-4.318" y2="0.3175" layer="21"/>
<rectangle x1="4.318" y1="-0.3175" x2="9.017" y2="0.3175" layer="21"/>
</package>
<package name="0207/2VB">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="1.219" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.27" width="0.1778" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="0" y="1.27" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="0" y="-2.2225" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.3" x2="0.3" y2="0.3" layer="21"/>
</package>
<package name="0207/2VC">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="1.219" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.27" width="0.1778" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<text x="0" y="1.27" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="0" y="-2.2225" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.3" x2="0.3" y2="0.3" layer="21"/>
</package>
<package name="R0201">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt;&lt;p&gt;
0201, grid 0.00625 inch</description>
<wire x1="-0.195" y1="0.124" x2="0.195" y2="0.124" width="0.1016" layer="51"/>
<wire x1="0.195" y1="-0.124" x2="-0.195" y2="-0.124" width="0.1016" layer="51"/>
<wire x1="-0.7438" y1="0.4763" x2="0.7438" y2="0.4763" width="0.2032" layer="21"/>
<wire x1="0.7438" y1="0.4763" x2="0.7438" y2="-0.4763" width="0.2032" layer="21"/>
<wire x1="0.7438" y1="-0.4763" x2="-0.7438" y2="-0.4763" width="0.2032" layer="21"/>
<wire x1="-0.7438" y1="-0.4763" x2="-0.7438" y2="0.4763" width="0.2032" layer="21"/>
<smd name="1" x="-0.3175" y="0" dx="0.5" dy="0.4" layer="1" rot="R90"/>
<smd name="2" x="0.3175" y="0" dx="0.5" dy="0.4" layer="1" rot="R90"/>
<text x="-0.7938" y="0.7939" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-0.7938" y="-1.5876" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.304" y1="-0.2" x2="-0.15" y2="0.2" layer="51"/>
<rectangle x1="0.15" y1="-0.2" x2="0.3088" y2="0.2" layer="51"/>
</package>
<package name="0414/17">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 15 mm</description>
<wire x1="8.5725" y1="0" x2="6.9215" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.5725" y1="0" x2="-6.9215" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.1" y1="2.1" x2="6.1" y2="2.1" width="0.2032" layer="21"/>
<wire x1="6.1" y1="2.1" x2="6.1" y2="-2.1" width="0.2032" layer="21"/>
<wire x1="6.1" y1="-2.1" x2="-6.1" y2="-2.1" width="0.2032" layer="21"/>
<wire x1="-6.1" y1="-2.1" x2="-6.1" y2="2.1" width="0.2032" layer="21"/>
<pad name="1" x="-8.5725" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<pad name="2" x="8.5725" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="-6.0325" y="2.54" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-4.445" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="6.096" y1="-0.4064" x2="6.985" y2="0.4064" layer="21"/>
<rectangle x1="-6.985" y1="-0.4064" x2="-6.096" y2="0.4064" layer="21"/>
</package>
<package name="CE-010X030">
<description>&lt;b&gt;POLCAP RADIAL&lt;/b&gt;&lt;p&gt;
1.0 mm lead spacing, 3 mm diameter, grid 0.0125 inch</description>
<wire x1="-2.2225" y1="0.9525" x2="-1.5875" y2="0.9525" width="0.254" layer="21"/>
<wire x1="-1.905" y1="0.635" x2="-1.905" y2="1.27" width="0.254" layer="21"/>
<wire x1="-0.3587" y1="0" x2="-0.7" y2="0" width="0.2032" layer="51"/>
<wire x1="0.2764" y1="0" x2="0.7" y2="0" width="0.2032" layer="51"/>
<wire x1="0.2" y1="0" x2="0.4" y2="0.6" width="0.254" layer="51" curve="-37.877434" cap="flat"/>
<wire x1="0.4" y1="-0.6" x2="0.2" y2="0" width="0.254" layer="51" curve="-37.380842" cap="flat"/>
<wire x1="-0.2587" y1="-0.6349" x2="-0.2587" y2="0.635" width="0.254" layer="51"/>
<circle x="0" y="0" radius="1.506" width="0.2032" layer="21"/>
<pad name="+" x="-0.635" y="0" drill="0.6096" diameter="0.8128" shape="long" rot="R90"/>
<pad name="-" x="0.635" y="0" drill="0.6096" diameter="0.8128" shape="long" rot="R90"/>
<text x="1.5874" y="-1.905" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="1.5874" y="0.635" size="1.016" layer="25" ratio="18">&gt;NAME</text>
</package>
<package name="CE-020X050">
<description>&lt;b&gt;POLCAP RADIAL&lt;/b&gt;&lt;p&gt;
2.0 mm lead spacing, 5 mm diameter, grid 0.0125 inch</description>
<wire x1="-1.27" y1="1.905" x2="-0.635" y2="1.905" width="0.254" layer="21"/>
<wire x1="-0.9525" y1="1.5875" x2="-0.9525" y2="2.2225" width="0.254" layer="21"/>
<wire x1="-0.3175" y1="0" x2="-0.9525" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3176" y1="0" x2="0.9525" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3176" y1="0" x2="0.6351" y2="1.1112" width="0.3048" layer="51" curve="-37.874253" cap="flat"/>
<wire x1="0.6351" y1="-1.1113" x2="0.3176" y2="0" width="0.3048" layer="51" curve="-37.379812" cap="flat"/>
<wire x1="-0.3175" y1="-1.1112" x2="-0.3175" y2="1.1113" width="0.3048" layer="51"/>
<circle x="0" y="0" radius="2.7127" width="0.2032" layer="21"/>
<pad name="+" x="-0.9525" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="-" x="0.9525" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="2.8575" y="-2.54" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="2.8575" y="1.27" size="1.016" layer="25" ratio="18">&gt;NAME</text>
</package>
<package name="CE-025X060">
<description>&lt;b&gt;POLCAP RADIAL&lt;/b&gt;&lt;p&gt;
2.5 mm lead spacing, 6 mm diameter, grid 0.0125 inch</description>
<wire x1="-1.7462" y1="2.0638" x2="-0.7937" y2="2.0638" width="0.254" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="1.5875" width="0.254" layer="21"/>
<wire x1="-0.3175" y1="0" x2="-1.27" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3176" y1="0" x2="1.27" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3176" y1="0" x2="0.6351" y2="1.1112" width="0.3048" layer="51" curve="-37.874253" cap="flat"/>
<wire x1="0.6351" y1="-1.1113" x2="0.3176" y2="0" width="0.3048" layer="51" curve="-37.379812" cap="flat"/>
<wire x1="-0.3175" y1="-1.1112" x2="-0.3175" y2="1.1113" width="0.3048" layer="51"/>
<circle x="0" y="0" radius="3.4342" width="0.2032" layer="21"/>
<pad name="+" x="-1.27" y="0" drill="0.8128" diameter="1.778"/>
<pad name="-" x="1.27" y="0" drill="0.8128" diameter="1.778" shape="octagon"/>
<text x="3.81" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="3.81" y="-3.175" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="CE-025X065">
<description>&lt;b&gt;POLCAP RADIAL&lt;/b&gt;&lt;p&gt;
2.5 mm lead spacing, 6.5 mm diameter, grid 0.0125 inch</description>
<wire x1="-1.7462" y1="2.0638" x2="-0.7937" y2="2.0638" width="0.254" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="1.5875" width="0.254" layer="21"/>
<wire x1="-0.3175" y1="0" x2="-1.27" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3176" y1="0" x2="1.27" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3176" y1="0" x2="0.6351" y2="1.1112" width="0.3048" layer="51" curve="-37.874253" cap="flat"/>
<wire x1="0.6351" y1="-1.1113" x2="0.3176" y2="0" width="0.3048" layer="51" curve="-37.379812" cap="flat"/>
<wire x1="-0.3175" y1="-1.1112" x2="-0.3175" y2="1.1113" width="0.3048" layer="51"/>
<circle x="0" y="0" radius="3.62" width="0.2032" layer="21"/>
<pad name="+" x="-1.27" y="0" drill="0.8128" diameter="1.778"/>
<pad name="-" x="1.27" y="0" drill="0.8128" diameter="1.778" shape="octagon"/>
<text x="3.81" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="3.81" y="-3.175" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="CE-035X100">
<description>&lt;b&gt;POLCAP RADIAL&lt;/b&gt;&lt;p&gt;
3.5 mm lead spacing, 10 mm diameter, grid 0.0125 inch</description>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="21"/>
<wire x1="-1.905" y1="3.175" x2="-1.905" y2="1.905" width="0.254" layer="21"/>
<wire x1="-0.4762" y1="0" x2="-1.9049" y2="0" width="0.2032" layer="51"/>
<wire x1="0.6351" y1="0" x2="1.5875" y2="0" width="0.2032" layer="51"/>
<wire x1="0.6351" y1="0" x2="1.1113" y2="1.5875" width="0.4064" layer="51" curve="-37.872144" cap="flat"/>
<wire x1="1.1113" y1="-1.4288" x2="0.6351" y2="0" width="0.4064" layer="51" curve="-37.375347" cap="flat"/>
<wire x1="-0.4762" y1="-1.5875" x2="-0.4762" y2="1.5875" width="0.508" layer="51"/>
<circle x="0" y="0" radius="5.0899" width="0.2032" layer="21"/>
<pad name="+" x="-1.905" y="0" drill="1.016" diameter="2.1844"/>
<pad name="-" x="1.5875" y="0" drill="1.016" diameter="2.1844" shape="octagon"/>
<text x="-2.8575" y="-3.4925" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="4.445" y="3.4925" size="1.016" layer="25" ratio="18">&gt;NAME</text>
</package>
<package name="CE-035X080">
<description>&lt;b&gt;POLCAP RADIAL&lt;/b&gt;&lt;p&gt;
3.5 mm lead spacing, 8 mm diameter, grid 0.0125 inch</description>
<wire x1="-2.54" y1="2.2225" x2="-1.27" y2="2.2225" width="0.254" layer="21"/>
<wire x1="-1.905" y1="2.8575" x2="-1.905" y2="1.5875" width="0.254" layer="21"/>
<wire x1="-0.635" y1="0" x2="-1.905" y2="0" width="0.2032" layer="51"/>
<wire x1="0.4763" y1="0" x2="1.5875" y2="0" width="0.2032" layer="51"/>
<wire x1="0.4763" y1="0" x2="0.9525" y2="1.5875" width="0.4064" layer="51" curve="-37.872144" cap="flat"/>
<wire x1="0.9525" y1="-1.4288" x2="0.4763" y2="0" width="0.4064" layer="51" curve="-37.375347" cap="flat"/>
<wire x1="-0.635" y1="-1.5875" x2="-0.635" y2="1.5875" width="0.508" layer="51"/>
<circle x="0" y="0" radius="4.016" width="0.2032" layer="21"/>
<pad name="+" x="-1.905" y="0" drill="1.016" diameter="1.9304"/>
<pad name="-" x="1.5875" y="0" drill="1.016" diameter="1.9304" shape="octagon"/>
<text x="3.81" y="-3.4925" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="3.81" y="2.2225" size="1.016" layer="25" ratio="18">&gt;NAME</text>
</package>
<package name="CE-050X100">
<description>&lt;b&gt;POLCAP RADIAL&lt;/b&gt;&lt;p&gt;
5.0 mm lead spacing, 10 mm diameter, grid 0.0125 inch</description>
<wire x1="-3.175" y1="2.54" x2="-1.905" y2="2.54" width="0.254" layer="21"/>
<wire x1="-2.54" y1="3.175" x2="-2.54" y2="1.905" width="0.254" layer="21"/>
<wire x1="-0.635" y1="0" x2="-2.54" y2="0" width="0.2032" layer="51"/>
<wire x1="0.4763" y1="0" x2="2.54" y2="0" width="0.2032" layer="51"/>
<wire x1="0.4763" y1="0" x2="0.9525" y2="1.5875" width="0.4064" layer="51" curve="-37.875184" cap="flat"/>
<wire x1="0.9525" y1="-1.4288" x2="0.4763" y2="0" width="0.4064" layer="51" curve="-37.375347" cap="flat"/>
<wire x1="-0.635" y1="-1.5875" x2="-0.635" y2="1.5875" width="0.508" layer="51"/>
<circle x="0" y="0" radius="5.1293" width="0.2032" layer="21"/>
<pad name="+" x="-2.54" y="0" drill="1.016" diameter="2.1844"/>
<pad name="-" x="2.54" y="0" drill="1.016" diameter="2.1844" shape="octagon"/>
<text x="4.445" y="3.4925" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.175" y="-3.4925" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="CE-050X120">
<description>&lt;b&gt;POLCAP RADIAL&lt;/b&gt;&lt;p&gt;
5.0 mm lead spacing, 12 mm diameter, grid 0.0125 inch</description>
<wire x1="-3.175" y1="2.54" x2="-1.905" y2="2.54" width="0.254" layer="21"/>
<wire x1="-2.54" y1="3.175" x2="-2.54" y2="1.905" width="0.254" layer="21"/>
<wire x1="-0.635" y1="0" x2="-2.54" y2="0" width="0.2032" layer="51"/>
<wire x1="0.4763" y1="0" x2="2.54" y2="0" width="0.2032" layer="51"/>
<wire x1="0.4763" y1="0" x2="0.9525" y2="1.5875" width="0.4064" layer="51" curve="-37.872144" cap="flat"/>
<wire x1="0.9525" y1="-1.4288" x2="0.4763" y2="0" width="0.4064" layer="51" curve="-37.375347" cap="flat"/>
<wire x1="-0.635" y1="-1.5875" x2="-0.635" y2="1.5875" width="0.508" layer="51"/>
<circle x="0" y="0" radius="6.0241" width="0.2032" layer="21"/>
<pad name="+" x="-2.54" y="0" drill="1.016" diameter="2.1844"/>
<pad name="-" x="2.54" y="0" drill="1.016" diameter="2.1844" shape="octagon"/>
<text x="5.08" y="4.1275" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.175" y="-3.81" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="CE-075X160">
<description>&lt;b&gt;POLCAP RADIAL&lt;/b&gt;&lt;p&gt;
7.5 mm lead spacing, 16 mm diameter, grid 0.0125 inch</description>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.254" layer="25"/>
<wire x1="-3.81" y1="1.905" x2="-3.81" y2="3.175" width="0.254" layer="25"/>
<wire x1="-0.635" y1="0" x2="-3.81" y2="0" width="0.2032" layer="51"/>
<wire x1="0.4763" y1="0" x2="3.81" y2="0" width="0.2032" layer="51"/>
<wire x1="0.4763" y1="0" x2="0.9525" y2="1.5875" width="0.4064" layer="51" curve="-37.872144" cap="flat"/>
<wire x1="0.9525" y1="-1.4288" x2="0.4763" y2="0" width="0.4064" layer="51" curve="-37.375347" cap="flat"/>
<wire x1="-0.635" y1="-1.5875" x2="-0.635" y2="1.5875" width="0.508" layer="51"/>
<circle x="0" y="0" radius="8.255" width="0.2032" layer="21"/>
<pad name="+" x="-3.81" y="0" drill="1.27" diameter="2.54"/>
<pad name="-" x="3.81" y="0" drill="1.27" diameter="2.54" shape="octagon"/>
<text x="-3.175" y="-4.7625" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="6.0325" y="6.35" size="1.016" layer="25" ratio="18">&gt;NAME</text>
</package>
<package name="CE-075X180">
<description>&lt;b&gt;POLCAP RADIAL&lt;/b&gt;&lt;p&gt;
7.5 mm lead spacing, 18 mm diameter, grid 0.0125 inch</description>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.254" layer="25"/>
<wire x1="-3.81" y1="1.905" x2="-3.81" y2="3.175" width="0.254" layer="25"/>
<wire x1="-0.635" y1="0" x2="-3.81" y2="0" width="0.2032" layer="51"/>
<wire x1="0.4763" y1="0" x2="3.81" y2="0" width="0.2032" layer="51"/>
<wire x1="0.4763" y1="0" x2="0.9525" y2="1.5875" width="0.4064" layer="51" curve="-37.872144" cap="flat"/>
<wire x1="0.9525" y1="-1.4288" x2="0.4763" y2="0" width="0.4064" layer="51" curve="-37.375347" cap="flat"/>
<wire x1="-0.635" y1="-1.5875" x2="-0.635" y2="1.5875" width="0.508" layer="51"/>
<circle x="0" y="0" radius="9.1581" width="0.2032" layer="21"/>
<pad name="+" x="-3.81" y="0" drill="1.27" diameter="2.54"/>
<pad name="-" x="3.81" y="0" drill="1.27" diameter="2.54" shape="octagon"/>
<text x="-3.175" y="-5.3975" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="7.3025" y="6.6675" size="1.016" layer="25" ratio="18">&gt;NAME</text>
</package>
<package name="CEA-100X100">
<description>&lt;b&gt;POLCAP AXIAL&lt;/b&gt;&lt;p&gt;
10 mm lead spacing, 10 mm body, grid 0.0125 inch</description>
<wire x1="-6.985" y1="1.905" x2="-6.35" y2="1.905" width="0.254" layer="21"/>
<wire x1="-6.6675" y1="2.2225" x2="-6.6675" y2="1.5875" width="0.254" layer="21"/>
<wire x1="-5.715" y1="3.81" x2="-5.3975" y2="4.1275" width="0.2032" layer="21" curve="-90"/>
<wire x1="-5.715" y1="-3.81" x2="-5.3975" y2="-4.1275" width="0.2032" layer="21" curve="90"/>
<wire x1="4.7625" y1="4.1275" x2="5.08" y2="3.81" width="0.2032" layer="21" curve="-90"/>
<wire x1="4.7625" y1="-4.1275" x2="5.08" y2="-3.81" width="0.2032" layer="21" curve="90"/>
<wire x1="-5.715" y1="3.81" x2="-5.715" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="5.08" y1="3.81" x2="5.08" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="4.7625" y1="4.1275" x2="-4.445" y2="4.1275" width="0.2032" layer="21"/>
<wire x1="4.7625" y1="-4.1275" x2="-4.445" y2="-4.1275" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="-4.1275" x2="-4.7625" y2="-3.81" width="0.2032" layer="21" curve="-90"/>
<wire x1="-4.7625" y1="-3.81" x2="-4.445" y2="-4.1275" width="0.2032" layer="21" curve="-90"/>
<wire x1="-5.08" y1="4.1275" x2="-4.7625" y2="3.81" width="0.2032" layer="21" curve="90"/>
<wire x1="-4.7625" y1="3.81" x2="-4.445" y2="4.1275" width="0.2032" layer="21" curve="90"/>
<wire x1="-5.3975" y1="4.1275" x2="-5.08" y2="4.1275" width="0.2032" layer="21"/>
<wire x1="-5.3975" y1="-4.1275" x2="-5.08" y2="-4.1275" width="0.2032" layer="21"/>
<wire x1="-4.7625" y1="-3.81" x2="-4.7625" y2="3.81" width="0.2032" layer="21"/>
<pad name="+" x="-8.255" y="0" drill="0.8128" diameter="1.6764"/>
<pad name="-" x="7.62" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="-4.7625" y="4.7625" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-7.1438" y1="-0.1588" x2="-5.715" y2="0.1588" layer="21"/>
<rectangle x1="5.08" y1="-0.1588" x2="6.5088" y2="0.1588" layer="21"/>
</package>
<package name="CEA-100X250">
<description>&lt;b&gt;POLCAP AXIAL&lt;/b&gt;&lt;p&gt;
25 mm lead spacing, 10 mm body, grid 0.0125 inch</description>
<wire x1="-14.605" y1="2.54" x2="-13.97" y2="2.54" width="0.254" layer="21"/>
<wire x1="-14.2875" y1="2.8575" x2="-14.2875" y2="2.2225" width="0.254" layer="21"/>
<wire x1="-13.335" y1="5.08" x2="-13.0175" y2="5.3975" width="0.2032" layer="21" curve="-90"/>
<wire x1="-13.335" y1="-5.08" x2="-13.0175" y2="-5.3975" width="0.2032" layer="21" curve="90"/>
<wire x1="11.7475" y1="5.3975" x2="12.065" y2="5.08" width="0.2032" layer="21" curve="-90"/>
<wire x1="11.7475" y1="-5.3975" x2="12.065" y2="-5.08" width="0.2032" layer="21" curve="90"/>
<wire x1="-13.335" y1="5.08" x2="-13.335" y2="-5.08" width="0.2032" layer="21"/>
<wire x1="12.065" y1="5.08" x2="12.065" y2="-5.08" width="0.2032" layer="21"/>
<wire x1="11.7475" y1="5.3975" x2="-11.7475" y2="5.3975" width="0.2032" layer="21"/>
<wire x1="11.7475" y1="-5.3975" x2="-11.7475" y2="-5.3975" width="0.2032" layer="21"/>
<wire x1="-12.3825" y1="-5.3975" x2="-12.065" y2="-5.08" width="0.2032" layer="21" curve="-90"/>
<wire x1="-12.065" y1="-5.08" x2="-11.7475" y2="-5.3975" width="0.2032" layer="21" curve="-90"/>
<wire x1="-12.3825" y1="5.3975" x2="-12.065" y2="5.08" width="0.2032" layer="21" curve="90"/>
<wire x1="-12.065" y1="5.08" x2="-11.7475" y2="5.3975" width="0.2032" layer="21" curve="90"/>
<wire x1="-13.0175" y1="5.3975" x2="-12.3825" y2="5.3975" width="0.2032" layer="21"/>
<wire x1="-13.0175" y1="-5.3975" x2="-12.3825" y2="-5.3975" width="0.2032" layer="21"/>
<wire x1="-12.065" y1="-5.08" x2="-12.065" y2="5.08" width="0.2032" layer="21"/>
<pad name="+" x="-16.51" y="0" drill="0.8128" diameter="1.6764"/>
<pad name="-" x="15.24" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="-12.3825" y="5.715" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-15.3988" y1="-0.1588" x2="-13.335" y2="0.1588" layer="21"/>
<rectangle x1="12.065" y1="-0.1588" x2="14.1288" y2="0.1588" layer="21"/>
</package>
<package name="CEA-120X250">
<description>&lt;b&gt;POLCAP AXIAL&lt;/b&gt;&lt;p&gt;
25 mm lead spacing, 12 mm body, grid 0.0125 inch</description>
<wire x1="-13.6525" y1="2.54" x2="-13.0175" y2="2.54" width="0.254" layer="21"/>
<wire x1="-13.335" y1="2.8575" x2="-13.335" y2="2.2225" width="0.254" layer="21"/>
<wire x1="-12.3825" y1="5.715" x2="-12.065" y2="6.0325" width="0.2032" layer="21" curve="-90"/>
<wire x1="-12.3825" y1="-5.715" x2="-12.065" y2="-6.0325" width="0.2032" layer="21" curve="90"/>
<wire x1="11.43" y1="6.0325" x2="11.7475" y2="5.715" width="0.2032" layer="21" curve="-90"/>
<wire x1="11.43" y1="-6.0325" x2="11.7475" y2="-5.715" width="0.2032" layer="21" curve="90"/>
<wire x1="-12.3825" y1="5.715" x2="-12.3825" y2="-5.715" width="0.2032" layer="21"/>
<wire x1="11.7475" y1="5.715" x2="11.7475" y2="-5.715" width="0.2032" layer="21"/>
<wire x1="11.43" y1="6.0325" x2="-10.795" y2="6.0325" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-6.0325" x2="-10.795" y2="-6.0325" width="0.2032" layer="21"/>
<wire x1="-11.43" y1="-6.0325" x2="-11.1125" y2="-5.715" width="0.2032" layer="21" curve="-90"/>
<wire x1="-11.1125" y1="-5.715" x2="-10.795" y2="-6.0325" width="0.2032" layer="21" curve="-90"/>
<wire x1="-11.43" y1="6.0325" x2="-11.1125" y2="5.715" width="0.2032" layer="21" curve="90"/>
<wire x1="-11.1125" y1="5.715" x2="-10.795" y2="6.0325" width="0.2032" layer="21" curve="90"/>
<wire x1="-12.065" y1="6.0325" x2="-11.43" y2="6.0325" width="0.2032" layer="21"/>
<wire x1="-12.065" y1="-6.0325" x2="-11.43" y2="-6.0325" width="0.2032" layer="21"/>
<wire x1="-11.1125" y1="-5.715" x2="-11.1125" y2="5.715" width="0.2032" layer="21"/>
<pad name="+" x="-15.875" y="0" drill="1.016" diameter="2.1844"/>
<pad name="-" x="15.24" y="0" drill="1.016" diameter="2.1844" shape="octagon"/>
<text x="-11.43" y="6.35" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-14.4463" y1="-0.1588" x2="-12.3825" y2="0.1588" layer="21"/>
<rectangle x1="11.7475" y1="-0.1588" x2="13.8113" y2="0.1588" layer="21"/>
</package>
<package name="CEA-120X310">
<description>&lt;b&gt;POLCAP AXIAL&lt;/b&gt;&lt;p&gt;
31 mm lead spacing, 12 mm body, grid 0.0125 inch</description>
<wire x1="-17.4625" y1="2.54" x2="-16.8275" y2="2.54" width="0.254" layer="21"/>
<wire x1="-17.145" y1="2.8575" x2="-17.145" y2="2.2225" width="0.254" layer="21"/>
<wire x1="-16.1925" y1="5.715" x2="-15.875" y2="6.0325" width="0.2032" layer="21" curve="-90"/>
<wire x1="-16.1925" y1="-5.715" x2="-15.875" y2="-6.0325" width="0.2032" layer="21" curve="90"/>
<wire x1="13.97" y1="6.0325" x2="14.2875" y2="5.715" width="0.2032" layer="21" curve="-90"/>
<wire x1="13.97" y1="-6.0325" x2="14.2875" y2="-5.715" width="0.2032" layer="21" curve="90"/>
<wire x1="-16.1925" y1="5.715" x2="-16.1925" y2="-5.715" width="0.2032" layer="21"/>
<wire x1="14.2875" y1="5.715" x2="14.2875" y2="-5.715" width="0.2032" layer="21"/>
<wire x1="13.97" y1="6.0325" x2="-14.605" y2="6.0325" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-6.0325" x2="-14.605" y2="-6.0325" width="0.2032" layer="21"/>
<wire x1="-15.24" y1="-6.0325" x2="-14.9225" y2="-5.715" width="0.2032" layer="21" curve="-90"/>
<wire x1="-14.9225" y1="-5.715" x2="-14.605" y2="-6.0325" width="0.2032" layer="21" curve="-90"/>
<wire x1="-15.24" y1="6.0325" x2="-14.9225" y2="5.715" width="0.2032" layer="21" curve="90"/>
<wire x1="-14.9225" y1="5.715" x2="-14.605" y2="6.0325" width="0.2032" layer="21" curve="90"/>
<wire x1="-15.875" y1="6.0325" x2="-15.24" y2="6.0325" width="0.2032" layer="21"/>
<wire x1="-15.875" y1="-6.0325" x2="-15.24" y2="-6.0325" width="0.2032" layer="21"/>
<wire x1="-14.9225" y1="-5.715" x2="-14.9225" y2="5.715" width="0.2032" layer="21"/>
<pad name="+" x="-19.685" y="0" drill="1.016" diameter="2.1844"/>
<pad name="-" x="17.78" y="0" drill="1.016" diameter="2.1844" shape="octagon"/>
<text x="-15.24" y="6.35" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.81" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-18.2563" y1="-0.1588" x2="-16.1925" y2="0.1588" layer="21"/>
<rectangle x1="14.2875" y1="-0.1588" x2="16.3513" y2="0.1588" layer="21"/>
</package>
<package name="CEA-160X250">
<description>&lt;b&gt;POLCAP AXIAL&lt;/b&gt;&lt;p&gt;
25 mm lead spacing, 16 mm body, grid 0.0125 inch</description>
<wire x1="-13.6525" y1="3.175" x2="-13.0175" y2="3.175" width="0.254" layer="21"/>
<wire x1="-13.335" y1="3.4925" x2="-13.335" y2="2.8575" width="0.254" layer="21"/>
<wire x1="-12.3825" y1="7.62" x2="-12.065" y2="7.9375" width="0.2032" layer="21" curve="-90"/>
<wire x1="-12.3825" y1="-7.62" x2="-12.065" y2="-7.9375" width="0.2032" layer="21" curve="90"/>
<wire x1="11.43" y1="7.9375" x2="11.7475" y2="7.62" width="0.2032" layer="21" curve="-90"/>
<wire x1="11.43" y1="-7.9375" x2="11.7475" y2="-7.62" width="0.2032" layer="21" curve="90"/>
<wire x1="-12.3825" y1="7.62" x2="-12.3825" y2="-7.62" width="0.2032" layer="21"/>
<wire x1="11.7475" y1="7.62" x2="11.7475" y2="-7.62" width="0.2032" layer="21"/>
<wire x1="11.43" y1="7.9375" x2="-10.795" y2="7.9375" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-7.9375" x2="-10.795" y2="-7.9375" width="0.2032" layer="21"/>
<wire x1="-11.43" y1="-7.9375" x2="-11.1125" y2="-7.62" width="0.2032" layer="21" curve="-90"/>
<wire x1="-11.1125" y1="-7.62" x2="-10.795" y2="-7.9375" width="0.2032" layer="21" curve="-90"/>
<wire x1="-11.43" y1="7.9375" x2="-11.1125" y2="7.62" width="0.2032" layer="21" curve="90"/>
<wire x1="-11.1125" y1="7.62" x2="-10.795" y2="7.9375" width="0.2032" layer="21" curve="90"/>
<wire x1="-12.065" y1="7.9375" x2="-11.43" y2="7.9375" width="0.2032" layer="21"/>
<wire x1="-12.065" y1="-7.9375" x2="-11.43" y2="-7.9375" width="0.2032" layer="21"/>
<wire x1="-11.1125" y1="-7.62" x2="-11.1125" y2="7.62" width="0.2032" layer="21"/>
<pad name="+" x="-15.875" y="0" drill="1.016" diameter="2.1844"/>
<pad name="-" x="15.24" y="0" drill="1.016" diameter="2.1844" shape="octagon"/>
<text x="-11.43" y="8.255" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-14.4463" y1="-0.1588" x2="-12.3825" y2="0.1588" layer="21"/>
<rectangle x1="11.7475" y1="-0.1588" x2="13.8113" y2="0.1588" layer="21"/>
</package>
<package name="CEA-160X310">
<description>&lt;b&gt;POLCAP AXIAL&lt;/b&gt;&lt;p&gt;
31 mm lead spacing, 16 mm body, grid 0.0125 inch</description>
<wire x1="-17.4625" y1="3.175" x2="-16.8275" y2="3.175" width="0.254" layer="21"/>
<wire x1="-17.145" y1="3.4925" x2="-17.145" y2="2.8575" width="0.254" layer="21"/>
<wire x1="-16.1925" y1="7.62" x2="-15.875" y2="7.9375" width="0.2032" layer="21" curve="-90"/>
<wire x1="-16.1925" y1="-7.62" x2="-15.875" y2="-7.9375" width="0.2032" layer="21" curve="90"/>
<wire x1="13.97" y1="7.9375" x2="14.2875" y2="7.62" width="0.2032" layer="21" curve="-90"/>
<wire x1="13.97" y1="-7.9375" x2="14.2875" y2="-7.62" width="0.2032" layer="21" curve="90"/>
<wire x1="-16.1925" y1="7.62" x2="-16.1925" y2="-7.62" width="0.2032" layer="21"/>
<wire x1="14.2875" y1="7.62" x2="14.2875" y2="-7.62" width="0.2032" layer="21"/>
<wire x1="13.97" y1="7.9375" x2="-14.605" y2="7.9375" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-7.9375" x2="-14.605" y2="-7.9375" width="0.2032" layer="21"/>
<wire x1="-15.24" y1="-7.9375" x2="-14.9225" y2="-7.62" width="0.2032" layer="21" curve="-90"/>
<wire x1="-14.9225" y1="-7.62" x2="-14.605" y2="-7.9375" width="0.2032" layer="21" curve="-90"/>
<wire x1="-15.24" y1="7.9375" x2="-14.9225" y2="7.62" width="0.2032" layer="21" curve="90"/>
<wire x1="-14.9225" y1="7.62" x2="-14.605" y2="7.9375" width="0.2032" layer="21" curve="90"/>
<wire x1="-15.875" y1="7.9375" x2="-15.24" y2="7.9375" width="0.2032" layer="21"/>
<wire x1="-15.875" y1="-7.9375" x2="-15.24" y2="-7.9375" width="0.2032" layer="21"/>
<wire x1="-14.9225" y1="-7.62" x2="-14.9225" y2="7.62" width="0.2032" layer="21"/>
<pad name="+" x="-19.685" y="0" drill="1.016" diameter="2.1844"/>
<pad name="-" x="17.78" y="0" drill="1.016" diameter="2.1844" shape="octagon"/>
<text x="-15.24" y="8.255" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.81" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-18.2563" y1="-0.1588" x2="-16.1925" y2="0.1588" layer="21"/>
<rectangle x1="14.2875" y1="-0.1588" x2="16.3513" y2="0.1588" layer="21"/>
</package>
<package name="CEA-180X400">
<description>&lt;b&gt;POLCAP AXIAL&lt;/b&gt;&lt;p&gt;
40 mm lead spacing, 18 mm body, grid 0.0125 inch</description>
<wire x1="-21.2725" y1="3.175" x2="-20.6375" y2="3.175" width="0.254" layer="21"/>
<wire x1="-20.955" y1="3.4925" x2="-20.955" y2="2.8575" width="0.254" layer="21"/>
<wire x1="-20.0025" y1="8.89" x2="-19.685" y2="9.2075" width="0.2032" layer="21" curve="-90"/>
<wire x1="-20.0025" y1="-8.89" x2="-19.685" y2="-9.2075" width="0.2032" layer="21" curve="90"/>
<wire x1="19.05" y1="9.2075" x2="19.3675" y2="8.89" width="0.2032" layer="21" curve="-90"/>
<wire x1="19.05" y1="-9.2075" x2="19.3675" y2="-8.89" width="0.2032" layer="21" curve="90"/>
<wire x1="-20.0025" y1="8.89" x2="-20.0025" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="19.3675" y1="8.89" x2="19.3675" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="19.05" y1="9.2075" x2="-18.415" y2="9.2075" width="0.2032" layer="21"/>
<wire x1="19.05" y1="-9.2075" x2="-18.415" y2="-9.2075" width="0.2032" layer="21"/>
<wire x1="-19.05" y1="-9.2075" x2="-18.7325" y2="-8.89" width="0.2032" layer="21" curve="-90"/>
<wire x1="-18.7325" y1="-8.89" x2="-18.415" y2="-9.2075" width="0.2032" layer="21" curve="-90"/>
<wire x1="-19.05" y1="9.2075" x2="-18.7325" y2="8.89" width="0.2032" layer="21" curve="90"/>
<wire x1="-18.7325" y1="8.89" x2="-18.415" y2="9.2075" width="0.2032" layer="21" curve="90"/>
<wire x1="-19.685" y1="9.2075" x2="-19.05" y2="9.2075" width="0.2032" layer="21"/>
<wire x1="-19.685" y1="-9.2075" x2="-19.05" y2="-9.2075" width="0.2032" layer="21"/>
<wire x1="-18.7325" y1="-8.89" x2="-18.7325" y2="8.89" width="0.2032" layer="21"/>
<pad name="+" x="-23.495" y="0" drill="1.016" diameter="2.54"/>
<pad name="-" x="22.86" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="-19.05" y="9.525" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-7.62" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-22.0663" y1="-0.1588" x2="-20.0025" y2="0.1588" layer="21"/>
<rectangle x1="19.3675" y1="-0.1588" x2="21.4313" y2="0.1588" layer="21"/>
</package>
<package name="CEA-220X400">
<description>&lt;b&gt;POLCAP AXIAL&lt;/b&gt;&lt;p&gt;
40 mm lead spacing, 22 mm body, grid 0.0125 inch</description>
<wire x1="-21.2725" y1="3.175" x2="-20.6375" y2="3.175" width="0.254" layer="21"/>
<wire x1="-20.955" y1="3.4925" x2="-20.955" y2="2.8575" width="0.254" layer="21"/>
<wire x1="-20.0025" y1="10.795" x2="-19.685" y2="11.1125" width="0.2032" layer="21" curve="-90"/>
<wire x1="-20.0025" y1="-10.795" x2="-19.685" y2="-11.1125" width="0.2032" layer="21" curve="90"/>
<wire x1="19.05" y1="11.1125" x2="19.3675" y2="10.795" width="0.2032" layer="21" curve="-90"/>
<wire x1="19.05" y1="-11.1125" x2="19.3675" y2="-10.795" width="0.2032" layer="21" curve="90"/>
<wire x1="-20.0025" y1="10.795" x2="-20.0025" y2="-10.795" width="0.2032" layer="21"/>
<wire x1="19.3675" y1="10.795" x2="19.3675" y2="-10.795" width="0.2032" layer="21"/>
<wire x1="19.05" y1="11.1125" x2="-18.415" y2="11.1125" width="0.2032" layer="21"/>
<wire x1="19.05" y1="-11.1125" x2="-18.415" y2="-11.1125" width="0.2032" layer="21"/>
<wire x1="-19.05" y1="-11.1125" x2="-18.7325" y2="-10.795" width="0.2032" layer="21" curve="-90"/>
<wire x1="-18.7325" y1="-10.795" x2="-18.415" y2="-11.1125" width="0.2032" layer="21" curve="-90"/>
<wire x1="-19.05" y1="11.1125" x2="-18.7325" y2="10.795" width="0.2032" layer="21" curve="90"/>
<wire x1="-18.7325" y1="10.795" x2="-18.415" y2="11.1125" width="0.2032" layer="21" curve="90"/>
<wire x1="-19.685" y1="11.1125" x2="-19.05" y2="11.1125" width="0.2032" layer="21"/>
<wire x1="-19.685" y1="-11.1125" x2="-19.05" y2="-11.1125" width="0.2032" layer="21"/>
<wire x1="-18.7325" y1="-10.795" x2="-18.7325" y2="10.795" width="0.2032" layer="21"/>
<pad name="+" x="-23.495" y="0" drill="1.016" diameter="2.54"/>
<pad name="-" x="22.86" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="-19.05" y="11.43" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-7.62" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-22.0663" y1="-0.1588" x2="-20.0025" y2="0.1588" layer="21"/>
<rectangle x1="19.3675" y1="-0.1588" x2="21.4313" y2="0.1588" layer="21"/>
</package>
<package name="CEA-220X500">
<description>&lt;b&gt;POLCAP AXIAL&lt;/b&gt;&lt;p&gt;
50 mm lead spacing, 22 mm body, grid 0.0125 inch</description>
<wire x1="-25.4" y1="3.175" x2="-24.765" y2="3.175" width="0.254" layer="21"/>
<wire x1="-25.0825" y1="3.4925" x2="-25.0825" y2="2.8575" width="0.254" layer="21"/>
<wire x1="-24.13" y1="10.795" x2="-23.8125" y2="11.1125" width="0.2032" layer="21" curve="-90"/>
<wire x1="-24.13" y1="-10.795" x2="-23.8125" y2="-11.1125" width="0.2032" layer="21" curve="90"/>
<wire x1="24.4475" y1="11.1125" x2="24.765" y2="10.795" width="0.2032" layer="21" curve="-90"/>
<wire x1="24.4475" y1="-11.1125" x2="24.765" y2="-10.795" width="0.2032" layer="21" curve="90"/>
<wire x1="-24.13" y1="10.795" x2="-24.13" y2="-10.795" width="0.2032" layer="21"/>
<wire x1="24.765" y1="10.795" x2="24.765" y2="-10.795" width="0.2032" layer="21"/>
<wire x1="24.4475" y1="11.1125" x2="-22.5425" y2="11.1125" width="0.2032" layer="21"/>
<wire x1="24.4475" y1="-11.1125" x2="-22.5425" y2="-11.1125" width="0.2032" layer="21"/>
<wire x1="-23.1775" y1="-11.1125" x2="-22.86" y2="-10.795" width="0.2032" layer="21" curve="-90"/>
<wire x1="-22.86" y1="-10.795" x2="-22.5425" y2="-11.1125" width="0.2032" layer="21" curve="-90"/>
<wire x1="-23.1775" y1="11.1125" x2="-22.86" y2="10.795" width="0.2032" layer="21" curve="90"/>
<wire x1="-22.86" y1="10.795" x2="-22.5425" y2="11.1125" width="0.2032" layer="21" curve="90"/>
<wire x1="-23.8125" y1="11.1125" x2="-23.1775" y2="11.1125" width="0.2032" layer="21"/>
<wire x1="-23.8125" y1="-11.1125" x2="-23.1775" y2="-11.1125" width="0.2032" layer="21"/>
<wire x1="-22.86" y1="-10.795" x2="-22.86" y2="10.795" width="0.2032" layer="21"/>
<pad name="+" x="-27.94" y="0" drill="1.016" diameter="2.54"/>
<pad name="-" x="28.575" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="-23.1775" y="11.43" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-6.985" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-26.1938" y1="-0.1588" x2="-24.13" y2="0.1588" layer="21"/>
<rectangle x1="24.765" y1="-0.1588" x2="26.8288" y2="0.1588" layer="21"/>
</package>
<package name="CEA-035X070">
<description>&lt;b&gt;POLCAP AXIAL&lt;/b&gt;&lt;p&gt;
7 mm lead spacing, 3.5 mm body, grid 0.0125 inch</description>
<wire x1="-3.81" y1="1.4288" x2="-3.81" y2="-1.4288" width="0.2032" layer="21"/>
<wire x1="-3.6513" y1="-1.5875" x2="-3.3337" y2="-1.5875" width="0.2032" layer="21"/>
<wire x1="-2.8573" y1="-1.5875" x2="3.0163" y2="-1.5875" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.4288" x2="3.175" y2="1.4288" width="0.2032" layer="21"/>
<wire x1="3.0163" y1="1.5875" x2="-2.8574" y2="1.5875" width="0.2032" layer="21"/>
<wire x1="-3.3337" y1="1.5875" x2="-3.6513" y2="1.5875" width="0.2032" layer="21"/>
<wire x1="3.0163" y1="1.5876" x2="3.175" y2="1.4288" width="0.2032" layer="21" curve="-90.036092" cap="flat"/>
<wire x1="3.0163" y1="-1.5875" x2="3.1751" y2="-1.4288" width="0.2032" layer="21" curve="89.747993" cap="flat"/>
<wire x1="-3.8101" y1="1.4288" x2="-3.6513" y2="1.5875" width="0.2032" layer="21" curve="-90.036092" cap="flat"/>
<wire x1="-3.81" y1="-1.4288" x2="-3.6513" y2="-1.5876" width="0.2032" layer="21" curve="89.747993" cap="flat"/>
<wire x1="-3.3338" y1="1.5876" x2="-2.8576" y2="1.5876" width="0.2032" layer="21" curve="180" cap="flat"/>
<wire x1="-3.3337" y1="-1.5876" x2="-2.8575" y2="-1.5876" width="0.2032" layer="21" curve="-180" cap="flat"/>
<wire x1="-3.0956" y1="1.3494" x2="-3.0956" y2="-1.3494" width="0.2032" layer="21"/>
<wire x1="-4.7625" y1="0.9525" x2="-4.1275" y2="0.9525" width="0.254" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-4.445" y2="0.635" width="0.254" layer="21"/>
<pad name="+" x="-5.715" y="0" drill="0.8128" diameter="1.6764"/>
<pad name="-" x="5.08" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="-3.175" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.8575" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.762" y1="-0.1588" x2="-3.8095" y2="0.1588" layer="21"/>
<rectangle x1="3.175" y1="-0.1588" x2="4.1275" y2="0.1588" layer="21"/>
</package>
<package name="CEA-045X100">
<description>&lt;b&gt;POLCAP AXIAL&lt;/b&gt;&lt;p&gt;
10 mm lead spacing,  4.5 mm body, grid 0.0125 inch</description>
<wire x1="-6.985" y1="1.27" x2="-6.35" y2="1.27" width="0.254" layer="21"/>
<wire x1="-6.6675" y1="1.5875" x2="-6.6675" y2="0.9525" width="0.254" layer="21"/>
<wire x1="-5.715" y1="1.905" x2="-5.3975" y2="2.2225" width="0.2032" layer="21" curve="-90"/>
<wire x1="-5.715" y1="-1.905" x2="-5.3975" y2="-2.2225" width="0.2032" layer="21" curve="90"/>
<wire x1="4.7625" y1="2.2225" x2="5.08" y2="1.905" width="0.2032" layer="21" curve="-90"/>
<wire x1="4.7625" y1="-2.2225" x2="5.08" y2="-1.905" width="0.2032" layer="21" curve="90"/>
<wire x1="-5.715" y1="1.905" x2="-5.715" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.08" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="4.7625" y1="2.2225" x2="-4.445" y2="2.2225" width="0.2032" layer="21"/>
<wire x1="4.7625" y1="-2.2225" x2="-4.445" y2="-2.2225" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="-2.2225" x2="-4.7625" y2="-1.905" width="0.2032" layer="21" curve="-90"/>
<wire x1="-4.7625" y1="-1.905" x2="-4.445" y2="-2.2225" width="0.2032" layer="21" curve="-90"/>
<wire x1="-5.08" y1="2.2225" x2="-4.7625" y2="1.905" width="0.2032" layer="21" curve="90"/>
<wire x1="-4.7625" y1="1.905" x2="-4.445" y2="2.2225" width="0.2032" layer="21" curve="90"/>
<wire x1="-5.3975" y1="2.2225" x2="-5.08" y2="2.2225" width="0.2032" layer="21"/>
<wire x1="-5.3975" y1="-2.2225" x2="-5.08" y2="-2.2225" width="0.2032" layer="21"/>
<wire x1="-4.7625" y1="-1.905" x2="-4.7625" y2="1.905" width="0.2032" layer="21"/>
<pad name="+" x="-8.255" y="0" drill="0.8128" diameter="1.6764"/>
<pad name="-" x="7.62" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="-4.7625" y="2.54" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-7.1438" y1="-0.1588" x2="-5.715" y2="0.1588" layer="21"/>
<rectangle x1="5.08" y1="-0.1588" x2="6.5088" y2="0.1588" layer="21"/>
</package>
<package name="CEA-063X100">
<description>&lt;b&gt;POLCAP AXIAL&lt;/b&gt;&lt;p&gt;
10 mm lead spacing, 6.3 mm body, grid 0.0125 inch</description>
<wire x1="-6.985" y1="1.27" x2="-6.35" y2="1.27" width="0.254" layer="21"/>
<wire x1="-6.6675" y1="1.5875" x2="-6.6675" y2="0.9525" width="0.254" layer="21"/>
<wire x1="-5.715" y1="2.54" x2="-5.3975" y2="2.8575" width="0.2032" layer="21" curve="-90"/>
<wire x1="-5.715" y1="-2.54" x2="-5.3975" y2="-2.8575" width="0.2032" layer="21" curve="90"/>
<wire x1="4.7625" y1="2.8575" x2="5.08" y2="2.54" width="0.2032" layer="21" curve="-90"/>
<wire x1="4.7625" y1="-2.8575" x2="5.08" y2="-2.54" width="0.2032" layer="21" curve="90"/>
<wire x1="-5.715" y1="2.54" x2="-5.715" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="4.7625" y1="2.8575" x2="-4.445" y2="2.8575" width="0.2032" layer="21"/>
<wire x1="4.7625" y1="-2.8575" x2="-4.445" y2="-2.8575" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="-2.8575" x2="-4.7625" y2="-2.54" width="0.2032" layer="21" curve="-90"/>
<wire x1="-4.7625" y1="-2.54" x2="-4.445" y2="-2.8575" width="0.2032" layer="21" curve="-90"/>
<wire x1="-5.08" y1="2.8575" x2="-4.7625" y2="2.54" width="0.2032" layer="21" curve="90"/>
<wire x1="-4.7625" y1="2.54" x2="-4.445" y2="2.8575" width="0.2032" layer="21" curve="90"/>
<wire x1="-5.3975" y1="2.8575" x2="-5.08" y2="2.8575" width="0.2032" layer="21"/>
<wire x1="-5.3975" y1="-2.8575" x2="-5.08" y2="-2.8575" width="0.2032" layer="21"/>
<wire x1="-4.7625" y1="-2.54" x2="-4.7625" y2="2.54" width="0.2032" layer="21"/>
<pad name="+" x="-8.255" y="0" drill="0.8128" diameter="1.6764"/>
<pad name="-" x="7.62" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="-4.7625" y="3.175" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-7.1438" y1="-0.1588" x2="-5.715" y2="0.1588" layer="21"/>
<rectangle x1="5.08" y1="-0.1588" x2="6.5088" y2="0.1588" layer="21"/>
</package>
<package name="CEA-080X160">
<description>&lt;b&gt;POLCAP AXIAL&lt;/b&gt;&lt;p&gt;
16 mm lead spacing, 8 mm body, grid 0.0125 inch</description>
<wire x1="-9.525" y1="1.905" x2="-8.89" y2="1.905" width="0.254" layer="21"/>
<wire x1="-9.2075" y1="2.2225" x2="-9.2075" y2="1.5875" width="0.254" layer="21"/>
<wire x1="-8.255" y1="3.81" x2="-7.9375" y2="4.1275" width="0.2032" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-3.81" x2="-7.9375" y2="-4.1275" width="0.2032" layer="21" curve="90"/>
<wire x1="7.9375" y1="4.1275" x2="8.255" y2="3.81" width="0.2032" layer="21" curve="-90"/>
<wire x1="7.9375" y1="-4.1275" x2="8.255" y2="-3.81" width="0.2032" layer="21" curve="90"/>
<wire x1="-8.255" y1="3.81" x2="-8.255" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="8.255" y1="3.81" x2="8.255" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="7.9375" y1="4.1275" x2="-6.6675" y2="4.1275" width="0.2032" layer="21"/>
<wire x1="7.9375" y1="-4.1275" x2="-6.6675" y2="-4.1275" width="0.2032" layer="21"/>
<wire x1="-7.3025" y1="-4.1275" x2="-6.985" y2="-3.81" width="0.2032" layer="21" curve="-90"/>
<wire x1="-6.985" y1="-3.81" x2="-6.6675" y2="-4.1275" width="0.2032" layer="21" curve="-90"/>
<wire x1="-7.3025" y1="4.1275" x2="-6.985" y2="3.81" width="0.2032" layer="21" curve="90"/>
<wire x1="-6.985" y1="3.81" x2="-6.6675" y2="4.1275" width="0.2032" layer="21" curve="90"/>
<wire x1="-7.9375" y1="4.1275" x2="-7.3025" y2="4.1275" width="0.2032" layer="21"/>
<wire x1="-7.9375" y1="-4.1275" x2="-7.3025" y2="-4.1275" width="0.2032" layer="21"/>
<wire x1="-6.985" y1="-3.81" x2="-6.985" y2="3.81" width="0.2032" layer="21"/>
<pad name="+" x="-11.43" y="0" drill="0.8128" diameter="1.6764"/>
<pad name="-" x="11.43" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="-7.3025" y="4.445" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.3188" y1="-0.1588" x2="-8.255" y2="0.1588" layer="21"/>
<rectangle x1="8.255" y1="-0.1588" x2="10.3188" y2="0.1588" layer="21"/>
</package>
<package name="CEA-080X200">
<description>&lt;b&gt;POLCAP AXIAL&lt;/b&gt;&lt;p&gt;
20 mm lead spacing, 8 mm body, grid 0.0125 inch</description>
<wire x1="-10.16" y1="1.905" x2="-9.525" y2="1.905" width="0.254" layer="21"/>
<wire x1="-9.8425" y1="2.2225" x2="-9.8425" y2="1.5875" width="0.254" layer="21"/>
<wire x1="-8.89" y1="3.81" x2="-8.5725" y2="4.1275" width="0.2032" layer="21" curve="-90"/>
<wire x1="-8.89" y1="-3.81" x2="-8.5725" y2="-4.1275" width="0.2032" layer="21" curve="90"/>
<wire x1="11.1125" y1="4.1275" x2="11.43" y2="3.81" width="0.2032" layer="21" curve="-90"/>
<wire x1="11.1125" y1="-4.1275" x2="11.43" y2="-3.81" width="0.2032" layer="21" curve="90"/>
<wire x1="-8.89" y1="3.81" x2="-8.89" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="11.43" y1="3.81" x2="11.43" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="11.1125" y1="4.1275" x2="-7.3025" y2="4.1275" width="0.2032" layer="21"/>
<wire x1="11.1125" y1="-4.1275" x2="-7.3025" y2="-4.1275" width="0.2032" layer="21"/>
<wire x1="-7.9375" y1="-4.1275" x2="-7.62" y2="-3.81" width="0.2032" layer="21" curve="-90"/>
<wire x1="-7.62" y1="-3.81" x2="-7.3025" y2="-4.1275" width="0.2032" layer="21" curve="-90"/>
<wire x1="-7.9375" y1="4.1275" x2="-7.62" y2="3.81" width="0.2032" layer="21" curve="90"/>
<wire x1="-7.62" y1="3.81" x2="-7.3025" y2="4.1275" width="0.2032" layer="21" curve="90"/>
<wire x1="-8.5725" y1="4.1275" x2="-7.9375" y2="4.1275" width="0.2032" layer="21"/>
<wire x1="-8.5725" y1="-4.1275" x2="-7.9375" y2="-4.1275" width="0.2032" layer="21"/>
<wire x1="-7.62" y1="-3.81" x2="-7.62" y2="3.81" width="0.2032" layer="21"/>
<pad name="+" x="-12.065" y="0" drill="0.8128" diameter="1.6764"/>
<pad name="-" x="14.605" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="-7.9375" y="4.445" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.81" y="-0.635" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.9538" y1="-0.1588" x2="-8.89" y2="0.1588" layer="21"/>
<rectangle x1="11.43" y1="-0.1588" x2="13.4938" y2="0.1588" layer="21"/>
</package>
<package name="CETS-20">
<description>&lt;b&gt;POLCAP SNAP-IN RADIAL&lt;/b&gt; - Panasonic&lt;p&gt;
TS Snap-In Series 10 mm lead spacing, 20 mm diameter, grid 0.0125 inch</description>
<wire x1="-5.8737" y1="6.1913" x2="-3.9687" y2="6.1913" width="0.3048" layer="25"/>
<wire x1="-4.9212" y1="5.2388" x2="-4.9212" y2="7.1438" width="0.3048" layer="25"/>
<circle x="0" y="0" radius="10.2045" width="0.254" layer="21"/>
<pad name="+" x="-4.9213" y="1.905" drill="2.0066" diameter="3.81"/>
<pad name="-" x="4.9214" y="-1.905" drill="2.0066" diameter="3.81" shape="octagon"/>
<text x="-3.175" y="-6.6675" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="8.5725" y="7.9375" size="1.016" layer="25" ratio="18">&gt;NAME</text>
</package>
<package name="CETS-22">
<description>&lt;b&gt;POLCAP SNAP-IN RADIAL&lt;/b&gt; - Panasonic&lt;p&gt;
TS Snap-In Series 10 mm lead spacing, 22 mm diameter, grid 0.0125 inch</description>
<wire x1="-5.8737" y1="6.1913" x2="-3.9687" y2="6.1913" width="0.3048" layer="25"/>
<wire x1="-4.9212" y1="5.2388" x2="-4.9212" y2="7.1438" width="0.3048" layer="25"/>
<circle x="0" y="0" radius="11.2253" width="0.254" layer="21"/>
<pad name="+" x="-4.9213" y="1.905" drill="2.0066" diameter="3.81"/>
<pad name="-" x="4.9214" y="-1.905" drill="2.0066" diameter="3.81" shape="octagon"/>
<text x="-3.175" y="-7.3025" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="9.2075" y="7.9375" size="1.016" layer="25" ratio="18">&gt;NAME</text>
</package>
<package name="CETS-25">
<description>&lt;b&gt;POLCAP SNAP-IN RADIAL&lt;/b&gt; - Panasonic&lt;p&gt;
TS Snap-In Series 10 mm lead spacing, 25 mm diameter, grid 0.0125 inch</description>
<wire x1="-5.8737" y1="6.1913" x2="-3.9687" y2="6.1913" width="0.3048" layer="25"/>
<wire x1="-4.9212" y1="5.2388" x2="-4.9212" y2="7.1438" width="0.3048" layer="25"/>
<circle x="0" y="0" radius="12.8577" width="0.254" layer="21"/>
<pad name="+" x="-4.9213" y="1.905" drill="2.0066" diameter="3.81"/>
<pad name="-" x="4.9214" y="-1.905" drill="2.0066" diameter="3.81" shape="octagon"/>
<text x="-3.175" y="-7.9375" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="11.43" y="8.89" size="1.016" layer="25" ratio="18">&gt;NAME</text>
</package>
<package name="CETS-30">
<description>&lt;b&gt;POLCAP SNAP-IN RADIAL&lt;/b&gt; - Panasonic&lt;p&gt;
TS Snap-In Series 10 mm lead spacing, 30 mm diameter, grid 0.0125 inch</description>
<wire x1="-5.8737" y1="6.1913" x2="-3.9687" y2="6.1913" width="0.3048" layer="25"/>
<wire x1="-4.9212" y1="5.2388" x2="-4.9212" y2="7.1438" width="0.3048" layer="25"/>
<circle x="0" y="0" radius="17.0536" width="0.254" layer="21"/>
<pad name="+" x="-4.9213" y="1.905" drill="2.0066" diameter="3.81"/>
<pad name="-" x="4.9214" y="-1.905" drill="2.0066" diameter="3.81" shape="octagon"/>
<text x="-3.175" y="-9.8425" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="14.9225" y="11.43" size="1.016" layer="25" ratio="18">&gt;NAME</text>
</package>
<package name="CETS-35">
<description>&lt;b&gt;POLCAP SNAP-IN RADIAL&lt;/b&gt; - Panasonic&lt;p&gt;
TS Snap-In Series 10 mm lead spacing, 35 mm diameter, grid 0.0125 inch</description>
<wire x1="-5.8737" y1="6.1913" x2="-3.9687" y2="6.1913" width="0.3048" layer="25"/>
<wire x1="-4.9212" y1="5.2388" x2="-4.9212" y2="7.1438" width="0.3048" layer="25"/>
<circle x="0" y="0" radius="19.1818" width="0.254" layer="21"/>
<pad name="+" x="-4.9213" y="1.905" drill="2.0066" diameter="3.81"/>
<pad name="-" x="4.9214" y="-1.905" drill="2.0066" diameter="3.81" shape="octagon"/>
<text x="-3.175" y="-10.4775" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="16.8275" y="12.065" size="1.016" layer="25" ratio="18">&gt;NAME</text>
</package>
<package name="CETS-40">
<description>&lt;b&gt;POLCAP SNAP-IN RADIAL&lt;/b&gt; - Panasonic&lt;p&gt;
TS Snap-In Series 10 mm lead spacing, 40 mm diameter, grid 0.0125 inch</description>
<wire x1="-5.8737" y1="6.1913" x2="-3.9687" y2="6.1913" width="0.3048" layer="25"/>
<wire x1="-4.9212" y1="5.2388" x2="-4.9212" y2="7.1438" width="0.3048" layer="25"/>
<circle x="0" y="0" radius="20.9862" width="0.254" layer="21"/>
<pad name="+" x="-4.9213" y="1.905" drill="2.0066" diameter="3.81"/>
<pad name="-" x="4.9214" y="-1.905" drill="2.0066" diameter="3.81" shape="octagon"/>
<text x="-3.175" y="-10.4775" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="16.8275" y="14.9225" size="1.016" layer="25" ratio="18">&gt;NAME</text>
</package>
<package name="CE-050X125">
<description>&lt;b&gt;POLCAP RADIAL&lt;/b&gt;&lt;p&gt;
5.0 mm lead spacing, 12.5 mm diameter, grid 0.0125 inch</description>
<wire x1="-3.175" y1="2.54" x2="-1.905" y2="2.54" width="0.254" layer="21"/>
<wire x1="-2.54" y1="3.175" x2="-2.54" y2="1.905" width="0.254" layer="21"/>
<wire x1="-0.635" y1="0" x2="-2.54" y2="0" width="0.2032" layer="51"/>
<wire x1="0.4763" y1="0" x2="2.54" y2="0" width="0.2032" layer="51"/>
<wire x1="0.4763" y1="0" x2="0.9525" y2="1.5875" width="0.4064" layer="51" curve="-37.872144" cap="flat"/>
<wire x1="0.9525" y1="-1.4288" x2="0.4763" y2="0" width="0.4064" layer="51" curve="-37.375347" cap="flat"/>
<wire x1="-0.635" y1="-1.5875" x2="-0.635" y2="1.5875" width="0.508" layer="51"/>
<circle x="0" y="0" radius="6.35" width="0.2032" layer="21"/>
<pad name="+" x="-2.54" y="0" drill="1.016" diameter="2.1844"/>
<pad name="-" x="2.54" y="0" drill="1.016" diameter="2.1844" shape="octagon"/>
<text x="5.08" y="4.1275" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.175" y="-3.81" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C7343-TE-D">
<description>&lt;b&gt;SMD TANTALUM&lt;/b&gt; - Panasonic Size D&lt;p&gt; 
EIA Code 7343, Size D, 7.3 mm x 4.3 mm, grid 0.0125 inch</description>
<wire x1="-3.3925" y1="2.2225" x2="3.3925" y2="2.2225" width="0.2032" layer="21"/>
<wire x1="-3.3925" y1="-2.2225" x2="3.3925" y2="-2.2225" width="0.2032" layer="21"/>
<wire x1="-3.3925" y1="2.2225" x2="-3.3925" y2="-2.2225" width="0.2032" layer="51"/>
<wire x1="3.3925" y1="2.2225" x2="3.3925" y2="-2.2225" width="0.2032" layer="51"/>
<wire x1="-4.9655" y1="2.253" x2="4.9656" y2="2.253" width="0.0508" layer="39"/>
<wire x1="4.9656" y1="2.253" x2="4.9656" y2="-2.253" width="0.0508" layer="39"/>
<wire x1="4.9656" y1="-2.253" x2="-4.9655" y2="-2.253" width="0.0508" layer="39"/>
<wire x1="-4.9655" y1="-2.253" x2="-4.9655" y2="2.253" width="0.0508" layer="39"/>
<smd name="-" x="-3.175" y="0" dx="3.81" dy="3.175" layer="1" rot="R90"/>
<smd name="+" x="3.175" y="0" dx="3.81" dy="3.175" layer="1" rot="R90"/>
<text x="-3.4925" y="-3.4925" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="-3.4925" y="2.54" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<rectangle x1="2.7576" y1="0.9525" x2="4.3451" y2="1.27" layer="51" rot="R90"/>
<rectangle x1="-5.4563" y1="-0.1587" x2="-1.6463" y2="0.1588" layer="51" rot="R90"/>
<rectangle x1="1.2" y1="-2.2219" x2="3.4" y2="2.2" layer="51"/>
<rectangle x1="2.7575" y1="-1.2695" x2="4.345" y2="-0.952" layer="51" rot="R90"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
<rectangle x1="0.8" y1="-2.2225" x2="1.3288" y2="2.2" layer="21"/>
</package>
<package name="CEH-015-040X110">
<description>&lt;b&gt;POLCAP RADIAL&lt;/b&gt; - Horizontal&lt;p&gt;
1.5 mm lead spacing, 4 mm diameter, 11 mm length, grid 0.0125 inch</description>
<wire x1="-1.635" y1="1.3175" x2="1.6351" y2="1.3175" width="0.2032" layer="21"/>
<wire x1="1.7938" y1="1.4762" x2="1.7938" y2="1.7938" width="0.2032" layer="21"/>
<wire x1="1.7938" y1="2.2702" x2="1.7938" y2="12.0638" width="0.2032" layer="21"/>
<wire x1="1.6351" y1="12.2225" x2="-1.635" y2="12.2225" width="0.2032" layer="21"/>
<wire x1="-1.7937" y1="12.0638" x2="-1.7937" y2="2.2701" width="0.2032" layer="21"/>
<wire x1="-1.7937" y1="1.7938" x2="-1.7937" y2="1.4762" width="0.2032" layer="21"/>
<wire x1="-1.7938" y1="12.0638" x2="-1.635" y2="12.2225" width="0.2032" layer="21" curve="-90.108344" cap="flat"/>
<wire x1="1.7938" y1="12.0638" x2="1.6351" y2="12.2226" width="0.2032" layer="21" curve="89.747993" cap="flat"/>
<wire x1="-1.635" y1="1.3174" x2="-1.7937" y2="1.4762" width="0.2032" layer="21" curve="-89.963931" cap="flat"/>
<wire x1="1.6351" y1="1.3175" x2="1.7939" y2="1.4762" width="0.2032" layer="21" curve="90" cap="flat"/>
<wire x1="-1.7938" y1="1.7937" x2="-1.7938" y2="2.2699" width="0.2032" layer="21" curve="180" cap="flat"/>
<wire x1="1.7939" y1="1.7938" x2="1.7939" y2="2.27" width="0.2032" layer="21" curve="-180" cap="flat"/>
<wire x1="-1.5556" y1="2.0319" x2="1.5557" y2="2.0319" width="0.2032" layer="21"/>
<wire x1="-2.2225" y1="0" x2="-1.5875" y2="0" width="0.254" layer="21"/>
<wire x1="-1.905" y1="-0.3175" x2="-1.905" y2="0.3175" width="0.254" layer="21"/>
<wire x1="-0.3175" y1="7.905" x2="-0.3175" y2="5.365" width="0.2032" layer="51"/>
<wire x1="-0.3175" y1="5.365" x2="-0.635" y2="5.365" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="5.365" x2="-0.635" y2="6.635" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="6.635" x2="-0.635" y2="7.905" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="7.905" x2="-0.3175" y2="7.905" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="6.635" x2="-1.27" y2="6.635" width="0.2032" layer="51"/>
<wire x1="0.635" y1="6.635" x2="1.27" y2="6.635" width="0.2032" layer="51"/>
<pad name="+" x="-0.7938" y="0" drill="0.6096" diameter="0.8128" shape="long" rot="R90"/>
<pad name="-" x="0.7937" y="0" drill="0.6096" diameter="0.8128" shape="long" rot="R90"/>
<text x="3.81" y="1.27" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<text x="-2.54" y="1.27" size="1.016" layer="25" ratio="18" rot="R90">&gt;NAME</text>
<rectangle x1="0" y1="5.365" x2="0.635" y2="7.905" layer="51"/>
</package>
<package name="CEH-015-040X070">
<description>&lt;b&gt;POLCAP RADIAL&lt;/b&gt; - Horizontal&lt;p&gt;
1.5 mm lead spacing, 4 mm diameter, 7 mm length, grid 0.00625 inch</description>
<wire x1="-1.635" y1="1.3175" x2="1.6351" y2="1.3175" width="0.2032" layer="21"/>
<wire x1="1.7938" y1="1.4762" x2="1.7938" y2="1.7938" width="0.2032" layer="21"/>
<wire x1="1.7938" y1="2.2702" x2="1.7938" y2="8.0638" width="0.2032" layer="21"/>
<wire x1="1.6351" y1="8.2225" x2="-1.635" y2="8.2225" width="0.2032" layer="21"/>
<wire x1="-1.7937" y1="8.0638" x2="-1.7937" y2="2.2701" width="0.2032" layer="21"/>
<wire x1="-1.7937" y1="1.7938" x2="-1.7937" y2="1.4762" width="0.2032" layer="21"/>
<wire x1="-1.7938" y1="8.0638" x2="-1.635" y2="8.2225" width="0.2032" layer="21" curve="-90.108344" cap="flat"/>
<wire x1="1.7938" y1="8.0638" x2="1.6351" y2="8.2226" width="0.2032" layer="21" curve="89.747993" cap="flat"/>
<wire x1="-1.635" y1="1.3174" x2="-1.7937" y2="1.4762" width="0.2032" layer="21" curve="-89.963931" cap="flat"/>
<wire x1="1.6351" y1="1.3175" x2="1.7939" y2="1.4762" width="0.2032" layer="21" curve="90" cap="flat"/>
<wire x1="-1.7938" y1="1.7937" x2="-1.7938" y2="2.2699" width="0.2032" layer="21" curve="180" cap="flat"/>
<wire x1="1.7939" y1="1.7938" x2="1.7939" y2="2.27" width="0.2032" layer="21" curve="-180" cap="flat"/>
<wire x1="-1.5556" y1="2.0319" x2="1.5557" y2="2.0319" width="0.2032" layer="21"/>
<wire x1="-2.2225" y1="0" x2="-1.5875" y2="0" width="0.254" layer="21"/>
<wire x1="-1.905" y1="-0.3175" x2="-1.905" y2="0.3175" width="0.254" layer="21"/>
<wire x1="-0.3175" y1="6.27" x2="-0.3175" y2="3.73" width="0.2032" layer="51"/>
<wire x1="-0.3175" y1="3.73" x2="-0.635" y2="3.73" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="3.73" x2="-0.635" y2="5" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="5" x2="-0.635" y2="6.27" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="6.27" x2="-0.3175" y2="6.27" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="5" x2="-1.27" y2="5" width="0.2032" layer="51"/>
<wire x1="0.635" y1="5" x2="1.27" y2="5" width="0.2032" layer="51"/>
<pad name="+" x="-0.7938" y="0" drill="0.6096" diameter="0.8128" shape="long" rot="R90"/>
<pad name="-" x="0.7937" y="0" drill="0.6096" diameter="0.8128" shape="long" rot="R90"/>
<text x="3.81" y="1.27" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<text x="-2.54" y="1.27" size="1.016" layer="25" ratio="18" rot="R90">&gt;NAME</text>
<rectangle x1="0" y1="3.73" x2="0.635" y2="6.27" layer="51"/>
</package>
<package name="CEH-020-050X110">
<description>&lt;b&gt;POLCAP RADIAL&lt;/b&gt; - Horizontal&lt;p&gt;
2.0 mm lead spacing, 5 mm diameter, 11 mm length, grid 0.0125 inch</description>
<wire x1="-2.27" y1="1.9525" x2="2.2701" y2="1.9525" width="0.2032" layer="21"/>
<wire x1="2.4288" y1="2.1112" x2="2.4288" y2="2.4288" width="0.2032" layer="21"/>
<wire x1="2.4288" y1="2.9052" x2="2.4288" y2="13.4613" width="0.2032" layer="21"/>
<wire x1="2.2701" y1="13.62" x2="-2.27" y2="13.62" width="0.2032" layer="21"/>
<wire x1="-2.4287" y1="13.4613" x2="-2.4287" y2="2.9051" width="0.2032" layer="21"/>
<wire x1="-2.4287" y1="2.4288" x2="-2.4287" y2="2.1112" width="0.2032" layer="21"/>
<wire x1="-2.4288" y1="13.4613" x2="-2.27" y2="13.62" width="0.2032" layer="21" curve="-90.108344" cap="flat"/>
<wire x1="2.4288" y1="13.4613" x2="2.2701" y2="13.6201" width="0.2032" layer="21" curve="89.747993" cap="flat"/>
<wire x1="-2.27" y1="1.9524" x2="-2.4287" y2="2.1112" width="0.2032" layer="21" curve="-89.963931" cap="flat"/>
<wire x1="2.2701" y1="1.9525" x2="2.4289" y2="2.1112" width="0.2032" layer="21" curve="90" cap="flat"/>
<wire x1="-2.4288" y1="2.4287" x2="-2.4288" y2="2.9049" width="0.2032" layer="21" curve="180" cap="flat"/>
<wire x1="2.4289" y1="2.4288" x2="2.4289" y2="2.905" width="0.2032" layer="21" curve="-180" cap="flat"/>
<wire x1="-2.1906" y1="2.6669" x2="2.1907" y2="2.6669" width="0.2032" layer="21"/>
<wire x1="-0.3175" y1="8.905" x2="-0.3175" y2="6.365" width="0.2032" layer="51"/>
<wire x1="-0.3175" y1="6.365" x2="-0.635" y2="6.365" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="6.365" x2="-0.635" y2="7.635" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="7.635" x2="-0.635" y2="8.905" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="8.905" x2="-0.3175" y2="8.905" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="7.635" x2="-1.27" y2="7.635" width="0.2032" layer="51"/>
<wire x1="0.635" y1="7.635" x2="1.27" y2="7.635" width="0.2032" layer="51"/>
<wire x1="-3.0162" y1="0" x2="-2.0636" y2="0" width="0.254" layer="21"/>
<wire x1="-2.5399" y1="0.4763" x2="-2.5399" y2="-0.4763" width="0.254" layer="21"/>
<pad name="+" x="-0.9525" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="-" x="0.9525" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="4.445" y="1.905" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<text x="-3.175" y="1.905" size="1.016" layer="25" ratio="18" rot="R90">&gt;NAME</text>
<rectangle x1="0" y1="6.365" x2="0.635" y2="8.905" layer="51"/>
</package>
<package name="CEH-020-050X150">
<description>&lt;b&gt;POLCAP RADIAL&lt;/b&gt; - Horizontal&lt;p&gt;
2.0 mm lead spacing, 5 mm diameter, 15 mm length, grid 0.0125 inch</description>
<wire x1="-2.27" y1="1.9525" x2="2.2701" y2="1.9525" width="0.2032" layer="21"/>
<wire x1="2.4288" y1="2.1112" x2="2.4288" y2="2.4288" width="0.2032" layer="21"/>
<wire x1="2.4288" y1="2.9052" x2="2.4288" y2="16.4613" width="0.2032" layer="21"/>
<wire x1="2.2701" y1="16.62" x2="-2.27" y2="16.62" width="0.2032" layer="21"/>
<wire x1="-2.4287" y1="16.4613" x2="-2.4287" y2="2.9051" width="0.2032" layer="21"/>
<wire x1="-2.4287" y1="2.4288" x2="-2.4287" y2="2.1112" width="0.2032" layer="21"/>
<wire x1="-2.4288" y1="16.4613" x2="-2.27" y2="16.62" width="0.2032" layer="21" curve="-90.108344" cap="flat"/>
<wire x1="2.4288" y1="16.4613" x2="2.2701" y2="16.6201" width="0.2032" layer="21" curve="89.747993" cap="flat"/>
<wire x1="-2.27" y1="1.9524" x2="-2.4287" y2="2.1112" width="0.2032" layer="21" curve="-89.963931" cap="flat"/>
<wire x1="2.2701" y1="1.9525" x2="2.4289" y2="2.1112" width="0.2032" layer="21" curve="90" cap="flat"/>
<wire x1="-2.4288" y1="2.4287" x2="-2.4288" y2="2.9049" width="0.2032" layer="21" curve="180" cap="flat"/>
<wire x1="2.4289" y1="2.4288" x2="2.4289" y2="2.905" width="0.2032" layer="21" curve="-180" cap="flat"/>
<wire x1="-2.1906" y1="2.6669" x2="2.1907" y2="2.6669" width="0.2032" layer="21"/>
<wire x1="-0.3175" y1="10.27" x2="-0.3175" y2="7.73" width="0.2032" layer="51"/>
<wire x1="-0.3175" y1="7.73" x2="-0.635" y2="7.73" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="7.73" x2="-0.635" y2="9" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="9" x2="-0.635" y2="10.27" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="10.27" x2="-0.3175" y2="10.27" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="9" x2="-1.27" y2="9" width="0.2032" layer="51"/>
<wire x1="0.635" y1="9" x2="1.27" y2="9" width="0.2032" layer="51"/>
<wire x1="-3.0162" y1="0" x2="-2.0636" y2="0" width="0.254" layer="21"/>
<wire x1="-2.5399" y1="0.4763" x2="-2.5399" y2="-0.4763" width="0.254" layer="21"/>
<pad name="+" x="-0.9525" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="-" x="0.9525" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="4.445" y="1.905" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<text x="-3.175" y="1.905" size="1.016" layer="25" ratio="18" rot="R90">&gt;NAME</text>
<rectangle x1="0" y1="7.73" x2="0.635" y2="10.27" layer="51"/>
</package>
<package name="CEH-025-060X110">
<description>&lt;b&gt;POLCAP RADIAL&lt;/b&gt; - Horizontal&lt;p&gt;
2.5 mm lead spacing, 6 mm diameter, 11 mm length, grid 0.0125 inch</description>
<wire x1="-2.905" y1="1.9525" x2="2.9051" y2="1.9525" width="0.2032" layer="21"/>
<wire x1="3.0638" y1="2.1112" x2="3.0638" y2="2.4288" width="0.2032" layer="21"/>
<wire x1="3.0638" y1="2.9052" x2="3.0638" y2="13.4613" width="0.2032" layer="21"/>
<wire x1="2.9051" y1="13.62" x2="-2.905" y2="13.62" width="0.2032" layer="21"/>
<wire x1="-3.0637" y1="13.4613" x2="-3.0637" y2="2.9051" width="0.2032" layer="21"/>
<wire x1="-3.0637" y1="2.4288" x2="-3.0637" y2="2.1112" width="0.2032" layer="21"/>
<wire x1="-3.0638" y1="13.4613" x2="-2.905" y2="13.62" width="0.2032" layer="21" curve="-90.108344" cap="flat"/>
<wire x1="3.0638" y1="13.4613" x2="2.9051" y2="13.6201" width="0.2032" layer="21" curve="89.747993" cap="flat"/>
<wire x1="-2.905" y1="1.9524" x2="-3.0637" y2="2.1112" width="0.2032" layer="21" curve="-89.963931" cap="flat"/>
<wire x1="2.9051" y1="1.9525" x2="3.0639" y2="2.1112" width="0.2032" layer="21" curve="90" cap="flat"/>
<wire x1="-3.0638" y1="2.4287" x2="-3.0638" y2="2.9049" width="0.2032" layer="21" curve="180" cap="flat"/>
<wire x1="3.0639" y1="2.4288" x2="3.0639" y2="2.905" width="0.2032" layer="21" curve="-180" cap="flat"/>
<wire x1="-2.8256" y1="2.6669" x2="2.8257" y2="2.6669" width="0.2032" layer="21"/>
<wire x1="-0.3175" y1="9.27" x2="-0.3175" y2="6.73" width="0.2032" layer="51"/>
<wire x1="-0.3175" y1="6.73" x2="-0.635" y2="6.73" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="6.73" x2="-0.635" y2="8" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="8" x2="-0.635" y2="9.27" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="9.27" x2="-0.3175" y2="9.27" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="8" x2="-1.27" y2="8" width="0.2032" layer="51"/>
<wire x1="0.635" y1="8" x2="1.27" y2="8" width="0.2032" layer="51"/>
<wire x1="-3.3337" y1="0" x2="-2.3811" y2="0" width="0.254" layer="21"/>
<wire x1="-2.8574" y1="0.4763" x2="-2.8574" y2="-0.4763" width="0.254" layer="21"/>
<pad name="+" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="-" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="5.08" y="1.905" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<text x="-3.81" y="1.905" size="1.016" layer="25" ratio="18" rot="R90">&gt;NAME</text>
<rectangle x1="0" y1="6.73" x2="0.635" y2="9.27" layer="51"/>
</package>
<package name="CEH-025-060X150">
<description>&lt;b&gt;POLCAP RADIAL&lt;/b&gt; - Horizontal&lt;p&gt;
2.5 mm lead spacing, 6 mm diameter, 15 mm length, grid 0.0125 inch</description>
<wire x1="-2.905" y1="1.9525" x2="2.9051" y2="1.9525" width="0.2032" layer="21"/>
<wire x1="3.0638" y1="2.1112" x2="3.0638" y2="2.4288" width="0.2032" layer="21"/>
<wire x1="3.0638" y1="2.9052" x2="3.0638" y2="15.7313" width="0.2032" layer="21"/>
<wire x1="2.9051" y1="15.89" x2="-2.905" y2="15.89" width="0.2032" layer="21"/>
<wire x1="-3.0637" y1="15.7313" x2="-3.0637" y2="2.9051" width="0.2032" layer="21"/>
<wire x1="-3.0637" y1="2.4288" x2="-3.0637" y2="2.1112" width="0.2032" layer="21"/>
<wire x1="-3.0638" y1="15.7313" x2="-2.905" y2="15.89" width="0.2032" layer="21" curve="-90.108344" cap="flat"/>
<wire x1="3.0638" y1="15.7313" x2="2.9051" y2="15.8901" width="0.2032" layer="21" curve="89.747993" cap="flat"/>
<wire x1="-2.905" y1="1.9524" x2="-3.0637" y2="2.1112" width="0.2032" layer="21" curve="-89.963931" cap="flat"/>
<wire x1="2.9051" y1="1.9525" x2="3.0639" y2="2.1112" width="0.2032" layer="21" curve="90" cap="flat"/>
<wire x1="-3.0638" y1="2.4287" x2="-3.0638" y2="2.9049" width="0.2032" layer="21" curve="180" cap="flat"/>
<wire x1="3.0639" y1="2.4288" x2="3.0639" y2="2.905" width="0.2032" layer="21" curve="-180" cap="flat"/>
<wire x1="-2.8256" y1="2.6669" x2="2.8257" y2="2.6669" width="0.2032" layer="21"/>
<wire x1="-0.3175" y1="10.27" x2="-0.3175" y2="7.73" width="0.2032" layer="51"/>
<wire x1="-0.3175" y1="7.73" x2="-0.635" y2="7.73" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="7.73" x2="-0.635" y2="9" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="9" x2="-0.635" y2="10.27" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="10.27" x2="-0.3175" y2="10.27" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="9" x2="-1.27" y2="9" width="0.2032" layer="51"/>
<wire x1="0.635" y1="9" x2="1.27" y2="9" width="0.2032" layer="51"/>
<wire x1="-3.3337" y1="0" x2="-2.3811" y2="0" width="0.254" layer="21"/>
<wire x1="-2.8574" y1="0.4763" x2="-2.8574" y2="-0.4763" width="0.254" layer="21"/>
<pad name="+" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="-" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="5.08" y="1.905" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<text x="-3.81" y="1.905" size="1.016" layer="25" ratio="18" rot="R90">&gt;NAME</text>
<rectangle x1="0" y1="7.73" x2="0.635" y2="10.27" layer="51"/>
</package>
<package name="CEH-050-100X120">
<description>&lt;b&gt;POLCAP RADIAL&lt;/b&gt; - Horizontal&lt;p&gt;
5.0 mm lead spacing, 10 mm diameter, 12 mm length, grid 0.0125 inch</description>
<wire x1="-4.81" y1="1.9525" x2="4.8101" y2="1.9525" width="0.2032" layer="21"/>
<wire x1="4.9688" y1="2.1112" x2="4.9688" y2="2.4288" width="0.2032" layer="21"/>
<wire x1="4.9688" y1="2.9052" x2="4.9688" y2="13.9613" width="0.2032" layer="21"/>
<wire x1="4.8101" y1="14.12" x2="-4.81" y2="14.12" width="0.2032" layer="21"/>
<wire x1="-4.9687" y1="13.9613" x2="-4.9687" y2="2.9051" width="0.2032" layer="21"/>
<wire x1="-4.9687" y1="2.4288" x2="-4.9687" y2="2.1112" width="0.2032" layer="21"/>
<wire x1="-4.9688" y1="13.9613" x2="-4.81" y2="14.12" width="0.2032" layer="21" curve="-90.108344" cap="flat"/>
<wire x1="4.9688" y1="13.9613" x2="4.8101" y2="14.1201" width="0.2032" layer="21" curve="89.747993" cap="flat"/>
<wire x1="-4.81" y1="1.9524" x2="-4.9687" y2="2.1112" width="0.2032" layer="21" curve="-89.963931" cap="flat"/>
<wire x1="4.8101" y1="1.9525" x2="4.9689" y2="2.1112" width="0.2032" layer="21" curve="90" cap="flat"/>
<wire x1="-4.9688" y1="2.4287" x2="-4.9688" y2="2.9049" width="0.2032" layer="21" curve="180" cap="flat"/>
<wire x1="4.9689" y1="2.4288" x2="4.9689" y2="2.905" width="0.2032" layer="21" curve="-180" cap="flat"/>
<wire x1="-4.7306" y1="2.6669" x2="4.7307" y2="2.6669" width="0.2032" layer="21"/>
<wire x1="-4.7625" y1="0.9525" x2="-3.8099" y2="0.9525" width="0.254" layer="21"/>
<wire x1="-4.2862" y1="1.4288" x2="-4.2862" y2="0.4762" width="0.254" layer="21"/>
<wire x1="-0.3175" y1="9.27" x2="-0.3175" y2="6.73" width="0.2032" layer="51"/>
<wire x1="-0.3175" y1="6.73" x2="-0.635" y2="6.73" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="6.73" x2="-0.635" y2="8" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="8" x2="-0.635" y2="9.27" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="9.27" x2="-0.3175" y2="9.27" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="8" x2="-1.27" y2="8" width="0.2032" layer="51"/>
<wire x1="0.635" y1="8" x2="1.27" y2="8" width="0.2032" layer="51"/>
<pad name="+" x="-2.54" y="0" drill="1.016" diameter="2.1844"/>
<pad name="-" x="2.54" y="0" drill="1.016" diameter="2.1844" shape="octagon"/>
<text x="6.985" y="1.905" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<text x="-5.715" y="1.905" size="1.016" layer="25" ratio="18" rot="R90">&gt;NAME</text>
<rectangle x1="0" y1="6.73" x2="0.635" y2="9.27" layer="51"/>
</package>
<package name="CEH-050-100X200">
<description>&lt;b&gt;POLCAP RADIAL&lt;/b&gt; - Horizontal&lt;p&gt;
5.0 mm lead spacing, 10 mm diameter, 20 mm length, grid 0.0125 inch</description>
<wire x1="-4.81" y1="1.9525" x2="4.8101" y2="1.9525" width="0.2032" layer="21"/>
<wire x1="4.9688" y1="2.1112" x2="4.9688" y2="2.4288" width="0.2032" layer="21"/>
<wire x1="4.9688" y1="2.9052" x2="4.9688" y2="21.9613" width="0.2032" layer="21"/>
<wire x1="4.8101" y1="22.12" x2="-4.81" y2="22.12" width="0.2032" layer="21"/>
<wire x1="-4.9687" y1="21.9613" x2="-4.9687" y2="2.9051" width="0.2032" layer="21"/>
<wire x1="-4.9687" y1="2.4288" x2="-4.9687" y2="2.1112" width="0.2032" layer="21"/>
<wire x1="-4.9688" y1="21.9613" x2="-4.81" y2="22.12" width="0.2032" layer="21" curve="-90.108344" cap="flat"/>
<wire x1="4.9688" y1="21.9613" x2="4.8101" y2="22.1201" width="0.2032" layer="21" curve="89.747993" cap="flat"/>
<wire x1="-4.81" y1="1.9524" x2="-4.9687" y2="2.1112" width="0.2032" layer="21" curve="-89.963931" cap="flat"/>
<wire x1="4.8101" y1="1.9525" x2="4.9689" y2="2.1112" width="0.2032" layer="21" curve="90" cap="flat"/>
<wire x1="-4.9688" y1="2.4287" x2="-4.9688" y2="2.9049" width="0.2032" layer="21" curve="180" cap="flat"/>
<wire x1="4.9689" y1="2.4288" x2="4.9689" y2="2.905" width="0.2032" layer="21" curve="-180" cap="flat"/>
<wire x1="-4.7306" y1="2.6669" x2="4.7307" y2="2.6669" width="0.2032" layer="21"/>
<wire x1="-0.3175" y1="14.27" x2="-0.3175" y2="11.73" width="0.2032" layer="51"/>
<wire x1="-0.3175" y1="11.73" x2="-0.635" y2="11.73" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="11.73" x2="-0.635" y2="13" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="13" x2="-0.635" y2="14.27" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="14.27" x2="-0.3175" y2="14.27" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="13" x2="-1.27" y2="13" width="0.2032" layer="51"/>
<wire x1="0.635" y1="13" x2="1.27" y2="13" width="0.2032" layer="51"/>
<wire x1="-4.7625" y1="0.9525" x2="-3.8099" y2="0.9525" width="0.254" layer="21"/>
<wire x1="-4.2862" y1="1.4288" x2="-4.2862" y2="0.4762" width="0.254" layer="21"/>
<pad name="+" x="-2.54" y="0" drill="1.016" diameter="2.1844"/>
<pad name="-" x="2.54" y="0" drill="1.016" diameter="2.1844" shape="octagon"/>
<text x="6.985" y="1.905" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<text x="-5.715" y="1.905" size="1.016" layer="25" ratio="18" rot="R90">&gt;NAME</text>
<rectangle x1="0" y1="11.73" x2="0.635" y2="14.27" layer="51"/>
</package>
<package name="CEH-050-100X120/R">
<description>&lt;b&gt;POLCAP RADIAL&lt;/b&gt; - Horizontal Rotated&lt;p&gt;
5.0 mm lead spacing, 10 mm diameter, 12 mm length, grid 0.0125 inch</description>
<wire x1="-4.81" y1="1.9525" x2="4.8101" y2="1.9525" width="0.2032" layer="21"/>
<wire x1="4.9688" y1="2.1112" x2="4.9688" y2="2.4288" width="0.2032" layer="21"/>
<wire x1="4.9688" y1="2.9052" x2="4.9688" y2="13.9613" width="0.2032" layer="21"/>
<wire x1="4.8101" y1="14.12" x2="-4.81" y2="14.12" width="0.2032" layer="21"/>
<wire x1="-4.9687" y1="13.9613" x2="-4.9687" y2="2.9051" width="0.2032" layer="21"/>
<wire x1="-4.9687" y1="2.4288" x2="-4.9687" y2="2.1112" width="0.2032" layer="21"/>
<wire x1="-4.9688" y1="13.9613" x2="-4.81" y2="14.12" width="0.2032" layer="21" curve="-90.108344" cap="flat"/>
<wire x1="4.9688" y1="13.9613" x2="4.8101" y2="14.1201" width="0.2032" layer="21" curve="89.747993" cap="flat"/>
<wire x1="-4.81" y1="1.9524" x2="-4.9687" y2="2.1112" width="0.2032" layer="21" curve="-89.963931" cap="flat"/>
<wire x1="4.8101" y1="1.9525" x2="4.9689" y2="2.1112" width="0.2032" layer="21" curve="90" cap="flat"/>
<wire x1="-4.9688" y1="2.4287" x2="-4.9688" y2="2.9049" width="0.2032" layer="21" curve="180" cap="flat"/>
<wire x1="4.9689" y1="2.4288" x2="4.9689" y2="2.905" width="0.2032" layer="21" curve="-180" cap="flat"/>
<wire x1="-4.7306" y1="2.6669" x2="4.7307" y2="2.6669" width="0.2032" layer="21"/>
<wire x1="4.7625" y1="0.9525" x2="3.8099" y2="0.9525" width="0.254" layer="21"/>
<wire x1="4.2862" y1="0.4762" x2="4.2862" y2="1.4288" width="0.254" layer="21"/>
<wire x1="-0.3175" y1="9.27" x2="-0.3175" y2="6.73" width="0.2032" layer="51"/>
<wire x1="-0.3175" y1="6.73" x2="-0.635" y2="6.73" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="6.73" x2="-0.635" y2="8" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="8" x2="-0.635" y2="9.27" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="9.27" x2="-0.3175" y2="9.27" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="8" x2="-1.27" y2="8" width="0.2032" layer="51"/>
<wire x1="0.635" y1="8" x2="1.27" y2="8" width="0.2032" layer="51"/>
<pad name="+" x="2.54" y="0" drill="1.016" diameter="2.1844" rot="R180"/>
<pad name="-" x="-2.54" y="0" drill="1.016" diameter="2.1844" shape="octagon" rot="R180"/>
<text x="6.985" y="1.905" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<text x="-5.715" y="1.905" size="1.016" layer="25" ratio="18" rot="R90">&gt;NAME</text>
<rectangle x1="0" y1="6.73" x2="0.635" y2="9.27" layer="51"/>
</package>
<package name="CEH-015-040X110/R">
<description>&lt;b&gt;POLCAP RADIAL&lt;/b&gt; - Horizontal Rotated&lt;p&gt;
1.5 mm lead spacing, 4 mm diameter, 11 mm length, grid 0.0125 inch</description>
<wire x1="-1.635" y1="1.3175" x2="1.6351" y2="1.3175" width="0.2032" layer="21"/>
<wire x1="1.7938" y1="1.4762" x2="1.7938" y2="1.7938" width="0.2032" layer="21"/>
<wire x1="1.7938" y1="2.2702" x2="1.7938" y2="12.0638" width="0.2032" layer="21"/>
<wire x1="1.6351" y1="12.2225" x2="-1.635" y2="12.2225" width="0.2032" layer="21"/>
<wire x1="-1.7937" y1="12.0638" x2="-1.7937" y2="2.2701" width="0.2032" layer="21"/>
<wire x1="-1.7937" y1="1.7938" x2="-1.7937" y2="1.4762" width="0.2032" layer="21"/>
<wire x1="-1.7938" y1="12.0638" x2="-1.635" y2="12.2225" width="0.2032" layer="21" curve="-90.108344" cap="flat"/>
<wire x1="1.7938" y1="12.0638" x2="1.6351" y2="12.2226" width="0.2032" layer="21" curve="89.747993" cap="flat"/>
<wire x1="-1.635" y1="1.3174" x2="-1.7937" y2="1.4762" width="0.2032" layer="21" curve="-89.963931" cap="flat"/>
<wire x1="1.6351" y1="1.3175" x2="1.7939" y2="1.4762" width="0.2032" layer="21" curve="90" cap="flat"/>
<wire x1="-1.7938" y1="1.7937" x2="-1.7938" y2="2.2699" width="0.2032" layer="21" curve="180" cap="flat"/>
<wire x1="1.7939" y1="1.7938" x2="1.7939" y2="2.27" width="0.2032" layer="21" curve="-180" cap="flat"/>
<wire x1="-1.5556" y1="2.0319" x2="1.5557" y2="2.0319" width="0.2032" layer="21"/>
<wire x1="2.2225" y1="0" x2="1.5875" y2="0" width="0.254" layer="21"/>
<wire x1="1.905" y1="0.3175" x2="1.905" y2="-0.3175" width="0.254" layer="21"/>
<wire x1="-0.3175" y1="7.905" x2="-0.3175" y2="5.365" width="0.2032" layer="51"/>
<wire x1="-0.3175" y1="5.365" x2="-0.635" y2="5.365" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="5.365" x2="-0.635" y2="6.635" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="6.635" x2="-0.635" y2="7.905" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="7.905" x2="-0.3175" y2="7.905" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="6.635" x2="-1.27" y2="6.635" width="0.2032" layer="51"/>
<wire x1="0.635" y1="6.635" x2="1.27" y2="6.635" width="0.2032" layer="51"/>
<pad name="+" x="0.7938" y="0" drill="0.6096" diameter="0.8128" shape="long" rot="R270"/>
<pad name="-" x="-0.7937" y="0" drill="0.6096" diameter="0.8128" shape="long" rot="R270"/>
<text x="3.81" y="1.27" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<text x="-2.54" y="1.27" size="1.016" layer="25" ratio="18" rot="R90">&gt;NAME</text>
<rectangle x1="0" y1="5.365" x2="0.635" y2="7.905" layer="51"/>
</package>
<package name="CEH-015-040X070/R">
<description>&lt;b&gt;POLCAP RADIAL&lt;/b&gt; - Horizontal Rotated&lt;p&gt;
1.5 mm lead spacing, 4 mm diameter, 7 mm length, grid 0.00625 inch</description>
<wire x1="-1.635" y1="1.3175" x2="1.6351" y2="1.3175" width="0.2032" layer="21"/>
<wire x1="1.7938" y1="1.4762" x2="1.7938" y2="1.7938" width="0.2032" layer="21"/>
<wire x1="1.7938" y1="2.2702" x2="1.7938" y2="8.0638" width="0.2032" layer="21"/>
<wire x1="1.6351" y1="8.2225" x2="-1.635" y2="8.2225" width="0.2032" layer="21"/>
<wire x1="-1.7937" y1="8.0638" x2="-1.7937" y2="2.2701" width="0.2032" layer="21"/>
<wire x1="-1.7937" y1="1.7938" x2="-1.7937" y2="1.4762" width="0.2032" layer="21"/>
<wire x1="-1.7938" y1="8.0638" x2="-1.635" y2="8.2225" width="0.2032" layer="21" curve="-90.108344" cap="flat"/>
<wire x1="1.7938" y1="8.0638" x2="1.6351" y2="8.2226" width="0.2032" layer="21" curve="89.747993" cap="flat"/>
<wire x1="-1.635" y1="1.3174" x2="-1.7937" y2="1.4762" width="0.2032" layer="21" curve="-89.963931" cap="flat"/>
<wire x1="1.6351" y1="1.3175" x2="1.7939" y2="1.4762" width="0.2032" layer="21" curve="90" cap="flat"/>
<wire x1="-1.7938" y1="1.7937" x2="-1.7938" y2="2.2699" width="0.2032" layer="21" curve="180" cap="flat"/>
<wire x1="1.7939" y1="1.7938" x2="1.7939" y2="2.27" width="0.2032" layer="21" curve="-180" cap="flat"/>
<wire x1="-1.5556" y1="2.0319" x2="1.5557" y2="2.0319" width="0.2032" layer="21"/>
<wire x1="2.2225" y1="0" x2="1.5875" y2="0" width="0.254" layer="21"/>
<wire x1="1.905" y1="0.3175" x2="1.905" y2="-0.3175" width="0.254" layer="21"/>
<wire x1="-0.3175" y1="6.27" x2="-0.3175" y2="3.73" width="0.2032" layer="51"/>
<wire x1="-0.3175" y1="3.73" x2="-0.635" y2="3.73" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="3.73" x2="-0.635" y2="5" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="5" x2="-0.635" y2="6.27" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="6.27" x2="-0.3175" y2="6.27" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="5" x2="-1.27" y2="5" width="0.2032" layer="51"/>
<wire x1="0.635" y1="5" x2="1.27" y2="5" width="0.2032" layer="51"/>
<pad name="+" x="0.7938" y="0" drill="0.6096" diameter="0.8128" shape="long" rot="R270"/>
<pad name="-" x="-0.7937" y="0" drill="0.6096" diameter="0.8128" shape="long" rot="R270"/>
<text x="3.81" y="1.27" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<text x="-2.54" y="1.27" size="1.016" layer="25" ratio="18" rot="R90">&gt;NAME</text>
<rectangle x1="0" y1="3.73" x2="0.635" y2="6.27" layer="51"/>
</package>
<package name="CEH-020-050X110/R">
<description>&lt;b&gt;POLCAP RADIAL&lt;/b&gt; - Horizontal Rotated&lt;p&gt;
2.0 mm lead spacing, 5 mm diameter, 11 mm length, grid 0.0125 inch</description>
<wire x1="-2.27" y1="1.9525" x2="2.2701" y2="1.9525" width="0.2032" layer="21"/>
<wire x1="2.4288" y1="2.1112" x2="2.4288" y2="2.4288" width="0.2032" layer="21"/>
<wire x1="2.4288" y1="2.9052" x2="2.4288" y2="13.4613" width="0.2032" layer="21"/>
<wire x1="2.2701" y1="13.62" x2="-2.27" y2="13.62" width="0.2032" layer="21"/>
<wire x1="-2.4287" y1="13.4613" x2="-2.4287" y2="2.9051" width="0.2032" layer="21"/>
<wire x1="-2.4287" y1="2.4288" x2="-2.4287" y2="2.1112" width="0.2032" layer="21"/>
<wire x1="-2.4288" y1="13.4613" x2="-2.27" y2="13.62" width="0.2032" layer="21" curve="-90.108344" cap="flat"/>
<wire x1="2.4288" y1="13.4613" x2="2.2701" y2="13.6201" width="0.2032" layer="21" curve="89.747993" cap="flat"/>
<wire x1="-2.27" y1="1.9524" x2="-2.4287" y2="2.1112" width="0.2032" layer="21" curve="-89.963931" cap="flat"/>
<wire x1="2.2701" y1="1.9525" x2="2.4289" y2="2.1112" width="0.2032" layer="21" curve="90" cap="flat"/>
<wire x1="-2.4288" y1="2.4287" x2="-2.4288" y2="2.9049" width="0.2032" layer="21" curve="180" cap="flat"/>
<wire x1="2.4289" y1="2.4288" x2="2.4289" y2="2.905" width="0.2032" layer="21" curve="-180" cap="flat"/>
<wire x1="-2.1906" y1="2.6669" x2="2.1907" y2="2.6669" width="0.2032" layer="21"/>
<wire x1="-0.3175" y1="8.905" x2="-0.3175" y2="6.365" width="0.2032" layer="51"/>
<wire x1="-0.3175" y1="6.365" x2="-0.635" y2="6.365" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="6.365" x2="-0.635" y2="7.635" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="7.635" x2="-0.635" y2="8.905" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="8.905" x2="-0.3175" y2="8.905" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="7.635" x2="-1.27" y2="7.635" width="0.2032" layer="51"/>
<wire x1="0.635" y1="7.635" x2="1.27" y2="7.635" width="0.2032" layer="51"/>
<wire x1="3.0162" y1="0" x2="2.0636" y2="0" width="0.254" layer="21"/>
<wire x1="2.5399" y1="-0.4763" x2="2.5399" y2="0.4763" width="0.254" layer="21"/>
<pad name="+" x="0.9525" y="0" drill="0.8128" shape="long" rot="R270"/>
<pad name="-" x="-0.9525" y="0" drill="0.8128" shape="long" rot="R270"/>
<text x="4.445" y="1.905" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<text x="-3.175" y="1.905" size="1.016" layer="25" ratio="18" rot="R90">&gt;NAME</text>
<rectangle x1="0" y1="6.365" x2="0.635" y2="8.905" layer="51"/>
</package>
<package name="CEH-020-050X150/R">
<description>&lt;b&gt;POLCAP RADIAL&lt;/b&gt; - Horizontal Rotated&lt;p&gt;
2.0 mm lead spacing, 5 mm diameter, 15 mm length, grid 0.0125 inch</description>
<wire x1="-2.27" y1="1.9525" x2="2.2701" y2="1.9525" width="0.2032" layer="21"/>
<wire x1="2.4288" y1="2.1112" x2="2.4288" y2="2.4288" width="0.2032" layer="21"/>
<wire x1="2.4288" y1="2.9052" x2="2.4288" y2="16.4613" width="0.2032" layer="21"/>
<wire x1="2.2701" y1="16.62" x2="-2.27" y2="16.62" width="0.2032" layer="21"/>
<wire x1="-2.4287" y1="16.4613" x2="-2.4287" y2="2.9051" width="0.2032" layer="21"/>
<wire x1="-2.4287" y1="2.4288" x2="-2.4287" y2="2.1112" width="0.2032" layer="21"/>
<wire x1="-2.4288" y1="16.4613" x2="-2.27" y2="16.62" width="0.2032" layer="21" curve="-90.108344" cap="flat"/>
<wire x1="2.4288" y1="16.4613" x2="2.2701" y2="16.6201" width="0.2032" layer="21" curve="89.747993" cap="flat"/>
<wire x1="-2.27" y1="1.9524" x2="-2.4287" y2="2.1112" width="0.2032" layer="21" curve="-89.963931" cap="flat"/>
<wire x1="2.2701" y1="1.9525" x2="2.4289" y2="2.1112" width="0.2032" layer="21" curve="90" cap="flat"/>
<wire x1="-2.4288" y1="2.4287" x2="-2.4288" y2="2.9049" width="0.2032" layer="21" curve="180" cap="flat"/>
<wire x1="2.4289" y1="2.4288" x2="2.4289" y2="2.905" width="0.2032" layer="21" curve="-180" cap="flat"/>
<wire x1="-2.1906" y1="2.6669" x2="2.1907" y2="2.6669" width="0.2032" layer="21"/>
<wire x1="-0.3175" y1="10.27" x2="-0.3175" y2="7.73" width="0.2032" layer="51"/>
<wire x1="-0.3175" y1="7.73" x2="-0.635" y2="7.73" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="7.73" x2="-0.635" y2="9" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="9" x2="-0.635" y2="10.27" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="10.27" x2="-0.3175" y2="10.27" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="9" x2="-1.27" y2="9" width="0.2032" layer="51"/>
<wire x1="0.635" y1="9" x2="1.27" y2="9" width="0.2032" layer="51"/>
<wire x1="3.0162" y1="0" x2="2.0636" y2="0" width="0.254" layer="21"/>
<wire x1="2.5399" y1="-0.4763" x2="2.5399" y2="0.4763" width="0.254" layer="21"/>
<pad name="+" x="0.9525" y="0" drill="0.8128" shape="long" rot="R270"/>
<pad name="-" x="-0.9525" y="0" drill="0.8128" shape="long" rot="R270"/>
<text x="4.445" y="1.905" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<text x="-3.175" y="1.905" size="1.016" layer="25" ratio="18" rot="R90">&gt;NAME</text>
<rectangle x1="0" y1="7.73" x2="0.635" y2="10.27" layer="51"/>
</package>
<package name="CEH-025-060X110/R">
<description>&lt;b&gt;POLCAP RADIAL&lt;/b&gt; - Horizontal Rotated&lt;p&gt;
2.5 mm lead spacing, 6 mm diameter, 11 mm length, grid 0.0125 inch</description>
<wire x1="-2.905" y1="1.9525" x2="2.9051" y2="1.9525" width="0.2032" layer="21"/>
<wire x1="3.0638" y1="2.1112" x2="3.0638" y2="2.4288" width="0.2032" layer="21"/>
<wire x1="3.0638" y1="2.9052" x2="3.0638" y2="13.4613" width="0.2032" layer="21"/>
<wire x1="2.9051" y1="13.62" x2="-2.905" y2="13.62" width="0.2032" layer="21"/>
<wire x1="-3.0637" y1="13.4613" x2="-3.0637" y2="2.9051" width="0.2032" layer="21"/>
<wire x1="-3.0637" y1="2.4288" x2="-3.0637" y2="2.1112" width="0.2032" layer="21"/>
<wire x1="-3.0638" y1="13.4613" x2="-2.905" y2="13.62" width="0.2032" layer="21" curve="-90.108344" cap="flat"/>
<wire x1="3.0638" y1="13.4613" x2="2.9051" y2="13.6201" width="0.2032" layer="21" curve="89.747993" cap="flat"/>
<wire x1="-2.905" y1="1.9524" x2="-3.0637" y2="2.1112" width="0.2032" layer="21" curve="-89.963931" cap="flat"/>
<wire x1="2.9051" y1="1.9525" x2="3.0639" y2="2.1112" width="0.2032" layer="21" curve="90" cap="flat"/>
<wire x1="-3.0638" y1="2.4287" x2="-3.0638" y2="2.9049" width="0.2032" layer="21" curve="180" cap="flat"/>
<wire x1="3.0639" y1="2.4288" x2="3.0639" y2="2.905" width="0.2032" layer="21" curve="-180" cap="flat"/>
<wire x1="-2.8256" y1="2.6669" x2="2.8257" y2="2.6669" width="0.2032" layer="21"/>
<wire x1="-0.3175" y1="9.27" x2="-0.3175" y2="6.73" width="0.2032" layer="51"/>
<wire x1="-0.3175" y1="6.73" x2="-0.635" y2="6.73" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="6.73" x2="-0.635" y2="8" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="8" x2="-0.635" y2="9.27" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="9.27" x2="-0.3175" y2="9.27" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="8" x2="-1.27" y2="8" width="0.2032" layer="51"/>
<wire x1="0.635" y1="8" x2="1.27" y2="8" width="0.2032" layer="51"/>
<wire x1="3.3337" y1="0" x2="2.3811" y2="0" width="0.254" layer="21"/>
<wire x1="2.8574" y1="-0.4763" x2="2.8574" y2="0.4763" width="0.254" layer="21"/>
<pad name="+" x="1.27" y="0" drill="0.8128" shape="long" rot="R270"/>
<pad name="-" x="-1.27" y="0" drill="0.8128" shape="long" rot="R270"/>
<text x="5.08" y="1.905" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<text x="-3.81" y="1.905" size="1.016" layer="25" ratio="18" rot="R90">&gt;NAME</text>
<rectangle x1="0" y1="6.73" x2="0.635" y2="9.27" layer="51"/>
</package>
<package name="CEH-025-060X150/R">
<description>&lt;b&gt;POLCAP RADIAL&lt;/b&gt; - Horizontal Rotated&lt;p&gt;
2.5 mm lead spacing, 6 mm diameter, 15 mm length, grid 0.0125 inch</description>
<wire x1="-2.905" y1="1.9525" x2="2.9051" y2="1.9525" width="0.2032" layer="21"/>
<wire x1="3.0638" y1="2.1112" x2="3.0638" y2="2.4288" width="0.2032" layer="21"/>
<wire x1="3.0638" y1="2.9052" x2="3.0638" y2="15.7313" width="0.2032" layer="21"/>
<wire x1="2.9051" y1="15.89" x2="-2.905" y2="15.89" width="0.2032" layer="21"/>
<wire x1="-3.0637" y1="15.7313" x2="-3.0637" y2="2.9051" width="0.2032" layer="21"/>
<wire x1="-3.0637" y1="2.4288" x2="-3.0637" y2="2.1112" width="0.2032" layer="21"/>
<wire x1="-3.0638" y1="15.7313" x2="-2.905" y2="15.89" width="0.2032" layer="21" curve="-90.108344" cap="flat"/>
<wire x1="3.0638" y1="15.7313" x2="2.9051" y2="15.8901" width="0.2032" layer="21" curve="89.747993" cap="flat"/>
<wire x1="-2.905" y1="1.9524" x2="-3.0637" y2="2.1112" width="0.2032" layer="21" curve="-89.963931" cap="flat"/>
<wire x1="2.9051" y1="1.9525" x2="3.0639" y2="2.1112" width="0.2032" layer="21" curve="90" cap="flat"/>
<wire x1="-3.0638" y1="2.4287" x2="-3.0638" y2="2.9049" width="0.2032" layer="21" curve="180" cap="flat"/>
<wire x1="3.0639" y1="2.4288" x2="3.0639" y2="2.905" width="0.2032" layer="21" curve="-180" cap="flat"/>
<wire x1="-2.8256" y1="2.6669" x2="2.8257" y2="2.6669" width="0.2032" layer="21"/>
<wire x1="-0.3175" y1="10.27" x2="-0.3175" y2="7.73" width="0.2032" layer="51"/>
<wire x1="-0.3175" y1="7.73" x2="-0.635" y2="7.73" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="7.73" x2="-0.635" y2="9" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="9" x2="-0.635" y2="10.27" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="10.27" x2="-0.3175" y2="10.27" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="9" x2="-1.27" y2="9" width="0.2032" layer="51"/>
<wire x1="0.635" y1="9" x2="1.27" y2="9" width="0.2032" layer="51"/>
<wire x1="3.3337" y1="0" x2="2.3811" y2="0" width="0.254" layer="21"/>
<wire x1="2.8574" y1="-0.4763" x2="2.8574" y2="0.4763" width="0.254" layer="21"/>
<pad name="+" x="1.27" y="0" drill="0.8128" shape="long" rot="R270"/>
<pad name="-" x="-1.27" y="0" drill="0.8128" shape="long" rot="R270"/>
<text x="5.08" y="1.905" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<text x="-3.81" y="1.905" size="1.016" layer="25" ratio="18" rot="R90">&gt;NAME</text>
<rectangle x1="0" y1="7.73" x2="0.635" y2="10.27" layer="51"/>
</package>
<package name="CEAA127X350">
<description>&lt;b&gt;POLCAP SCREW MOUNT&lt;/b&gt;&lt;p&gt;
AA-Series Computer Grade, 12.7 mm lead spacing, 35 mm diameter, grid 0.0125 inch</description>
<wire x1="-7.7787" y1="8.0963" x2="-4.6037" y2="8.0963" width="0.4064" layer="25"/>
<wire x1="-6.1912" y1="6.5088" x2="-6.1912" y2="9.6838" width="0.4064" layer="25"/>
<circle x="0" y="0" radius="17.7913" width="0.3048" layer="21"/>
<circle x="0" y="0" radius="15.6833" width="0.2032" layer="21"/>
<pad name="+" x="-6.35" y="0" drill="5.08" diameter="9.525"/>
<pad name="-" x="6.35" y="0" drill="5.08" diameter="9.525"/>
<text x="-3.175" y="-7.3025" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="13.0175" y="13.6525" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-5.08" y="-10.795" size="1.27" layer="51" ratio="10">AA-Series</text>
</package>
<package name="CEAA222X510">
<description>&lt;b&gt;POLCAP SCREW MOUNT&lt;/b&gt;&lt;p&gt;
AA-Series Computer Grade, 22.7 mm lead spacing, 51 mm diameter, grid 0.0125 inch</description>
<wire x1="-12.8587" y1="8.0963" x2="-9.6837" y2="8.0963" width="0.4064" layer="25"/>
<wire x1="-11.2712" y1="6.5088" x2="-11.2712" y2="9.6838" width="0.4064" layer="25"/>
<circle x="0" y="0" radius="26.9482" width="0.3048" layer="21"/>
<circle x="0" y="0" radius="23.4176" width="0.2032" layer="21"/>
<pad name="+" x="-11.1125" y="0" drill="5.08" diameter="9.525"/>
<pad name="-" x="11.1125" y="0" drill="5.08" diameter="9.525"/>
<text x="-3.175" y="-7.3025" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="21.9075" y="20.0025" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-5.08" y="-10.795" size="1.27" layer="51" ratio="10">AA-Series</text>
</package>
<package name="CEAA286X640">
<description>&lt;b&gt;POLCAP SCREW MOUNT&lt;/b&gt;&lt;p&gt;
AA-Series Computer Grade, 28.6 mm lead spacing, 64 mm diameter, grid 0.0125 inch</description>
<wire x1="-15.3987" y1="8.0963" x2="-12.2237" y2="8.0963" width="0.4064" layer="25"/>
<wire x1="-13.8112" y1="6.5088" x2="-13.8112" y2="9.6838" width="0.4064" layer="25"/>
<circle x="0" y="0" radius="31.75" width="0.3048" layer="21"/>
<circle x="0" y="0" radius="27.7372" width="0.2032" layer="21"/>
<pad name="+" x="-14.2875" y="0" drill="5.08" diameter="9.525"/>
<pad name="-" x="14.2875" y="0" drill="5.08" diameter="9.525"/>
<text x="-3.175" y="-7.3025" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="25.7175" y="23.8125" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-5.08" y="-10.795" size="1.27" layer="51" ratio="10">AA-Series</text>
</package>
<package name="CEAA318X790">
<description>&lt;b&gt;POLCAP SCREW MOUNT&lt;/b&gt;&lt;p&gt;
AA-Series Computer Grade, 31.8 mm lead spacing, 79 mm diameter, grid 0.0125 inch</description>
<wire x1="-17.3037" y1="8.7313" x2="-14.1287" y2="8.7313" width="0.4064" layer="25"/>
<wire x1="-15.7162" y1="7.1438" x2="-15.7162" y2="10.3188" width="0.4064" layer="25"/>
<circle x="0" y="0" radius="38.1" width="0.3048" layer="21"/>
<circle x="0" y="0" radius="32.6515" width="0.2032" layer="21"/>
<pad name="+" x="-16.1925" y="0" drill="5.08" diameter="9.525"/>
<pad name="-" x="14.9225" y="0" drill="5.08" diameter="9.525"/>
<text x="-3.175" y="-7.3025" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="29.5275" y="26.3525" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-5.08" y="-10.795" size="1.27" layer="51" ratio="10">AA-Series</text>
</package>
<package name="CTT-025-100">
<description>&lt;b&gt;POLCAP TANTALUM&lt;/b&gt;&lt;p&gt;
2.5 mm lead spacing, 10 mm diameter, grid 0.0125 inch</description>
<wire x1="-2.0637" y1="2.54" x2="-2.54" y2="2.54" width="0.254" layer="21"/>
<wire x1="-2.54" y1="2.54" x2="-3.0162" y2="2.54" width="0.254" layer="21"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="3.0163" width="0.254" layer="21"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="2.0638" width="0.254" layer="21"/>
<wire x1="-0.3175" y1="1.5875" x2="-0.3175" y2="-1.5875" width="0.2032" layer="51"/>
<wire x1="-0.3175" y1="-1.5875" x2="-0.9525" y2="-1.5875" width="0.2032" layer="51"/>
<wire x1="-0.9525" y1="-1.5875" x2="-0.9525" y2="0" width="0.2032" layer="51"/>
<wire x1="-0.9525" y1="0" x2="-0.9525" y2="1.5875" width="0.2032" layer="51"/>
<wire x1="-0.9525" y1="1.5875" x2="-0.3175" y2="1.5875" width="0.2032" layer="51"/>
<wire x1="-0.9525" y1="0" x2="-1.27" y2="0" width="0.2032" layer="51"/>
<wire x1="0.9525" y1="0" x2="1.27" y2="0" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="5.1293" width="0.2032" layer="21"/>
<pad name="+" x="-1.27" y="0" drill="0.8128" diameter="1.6764"/>
<pad name="-" x="1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="4.7625" y="3.175" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="4.7625" y="-4.7625" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.3175" y1="-1.5875" x2="0.9525" y2="1.5875" layer="51"/>
</package>
<package name="CTT-025-030">
<description>&lt;b&gt;POLCAP TANTALUM&lt;/b&gt;&lt;p&gt;
2.5 mm lead spacing, 3 mm diameter, grid 0.0125 inch</description>
<wire x1="-0.3175" y1="0.9525" x2="-0.3175" y2="-0.9525" width="0.2032" layer="51"/>
<wire x1="-0.3175" y1="-0.9525" x2="-0.635" y2="-0.9525" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="-0.9525" x2="-0.635" y2="0" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="0.9525" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="0.9525" x2="-0.3175" y2="0.9525" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.2032" layer="51"/>
<wire x1="0.635" y1="0" x2="1.27" y2="0" width="0.2032" layer="51"/>
<wire x1="-1.7462" y1="1.27" x2="-2.2225" y2="1.27" width="0.254" layer="21"/>
<wire x1="-2.2225" y1="1.27" x2="-2.6987" y2="1.27" width="0.254" layer="21"/>
<wire x1="-2.2225" y1="1.27" x2="-2.2225" y2="1.7463" width="0.254" layer="21"/>
<wire x1="-2.2225" y1="1.27" x2="-2.2225" y2="0.7938" width="0.254" layer="21"/>
<wire x1="-1.27" y1="0.9525" x2="1.27" y2="0.9525" width="0.2032" layer="21" curve="-106.260205" cap="flat"/>
<wire x1="-1.27" y1="-0.9525" x2="1.27" y2="-0.9525" width="0.2032" layer="21" curve="106.260205" cap="flat"/>
<pad name="+" x="-1.27" y="0" drill="0.8128" diameter="1.6764"/>
<pad name="-" x="1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="1.905" y="1.27" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="1.905" y="-2.2225" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0" y1="-0.9525" x2="0.635" y2="0.9525" layer="51"/>
</package>
<package name="CTT-025-040">
<description>&lt;b&gt;POLCAP TANTALUM&lt;/b&gt;&lt;p&gt;
2.5 mm lead spacing, 4 mm diameter, grid 0.0125 inch</description>
<wire x1="-1.7462" y1="1.27" x2="-2.2225" y2="1.27" width="0.254" layer="21"/>
<wire x1="-2.2225" y1="1.27" x2="-2.6987" y2="1.27" width="0.254" layer="21"/>
<wire x1="-2.2225" y1="1.27" x2="-2.2225" y2="1.7463" width="0.254" layer="21"/>
<wire x1="-2.2225" y1="1.27" x2="-2.2225" y2="0.7938" width="0.254" layer="21"/>
<wire x1="-1.5875" y1="0.9525" x2="1.5875" y2="0.9525" width="0.2032" layer="21" curve="-118.072487" cap="flat"/>
<wire x1="-1.5875" y1="-0.9525" x2="1.5875" y2="-0.9525" width="0.2032" layer="21" curve="118.072487" cap="flat"/>
<wire x1="0.635" y1="0" x2="1.27" y2="0" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="0.9525" x2="-0.3175" y2="0.9525" width="0.2032" layer="51"/>
<wire x1="-0.3175" y1="0.9525" x2="-0.3175" y2="-0.9525" width="0.2032" layer="51"/>
<wire x1="-0.3175" y1="-0.9525" x2="-0.635" y2="-0.9525" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="-0.9525" x2="-0.635" y2="0" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="0.9525" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.2032" layer="51"/>
<pad name="+" x="-1.27" y="0" drill="0.8128" diameter="1.6764"/>
<pad name="-" x="1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="1.905" y="1.27" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="1.905" y="-2.2225" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0" y1="-0.9525" x2="0.635" y2="0.9525" layer="51"/>
</package>
<package name="CTT-025-050">
<description>&lt;b&gt;POLCAP TANTALUM&lt;/b&gt;&lt;p&gt;
2.5 mm lead spacing, 5 mm diameter, grid 0.0125 inch</description>
<wire x1="-0.4762" y1="1.5875" x2="-0.9525" y2="1.5875" width="0.254" layer="21"/>
<wire x1="-0.9525" y1="1.5875" x2="-1.4287" y2="1.5875" width="0.254" layer="21"/>
<wire x1="-0.9525" y1="1.5875" x2="-0.9525" y2="2.0638" width="0.254" layer="21"/>
<wire x1="-0.9525" y1="1.5875" x2="-0.9525" y2="1.1113" width="0.254" layer="21"/>
<wire x1="0.635" y1="0" x2="1.27" y2="0" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="0.9525" x2="-0.3175" y2="0.9525" width="0.2032" layer="51"/>
<wire x1="-0.3175" y1="0.9525" x2="-0.3175" y2="-0.9525" width="0.2032" layer="51"/>
<wire x1="-0.3175" y1="-0.9525" x2="-0.635" y2="-0.9525" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="-0.9525" x2="-0.635" y2="0" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="0.9525" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="2.6181" width="0.2032" layer="21"/>
<pad name="+" x="-1.27" y="0" drill="0.8128" diameter="1.6764"/>
<pad name="-" x="1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="2.54" y="1.5875" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="2.54" y="-2.8575" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0" y1="-0.9525" x2="0.635" y2="0.9525" layer="51"/>
</package>
<package name="CTT-025-070">
<description>&lt;b&gt;POLCAP TANTALUM&lt;/b&gt;&lt;p&gt;
2.5 mm lead spacing, 7 mm diameter, grid 0.0125 inch</description>
<wire x1="-1.4287" y1="1.5875" x2="-1.905" y2="1.5875" width="0.254" layer="21"/>
<wire x1="-1.905" y1="1.5875" x2="-2.3812" y2="1.5875" width="0.254" layer="21"/>
<wire x1="-1.905" y1="1.5875" x2="-1.905" y2="2.0638" width="0.254" layer="21"/>
<wire x1="-1.905" y1="1.5875" x2="-1.905" y2="1.1113" width="0.254" layer="21"/>
<wire x1="-0.3175" y1="1.5875" x2="-0.3175" y2="-1.5875" width="0.2032" layer="51"/>
<wire x1="-0.3175" y1="-1.5875" x2="-0.9525" y2="-1.5875" width="0.2032" layer="51"/>
<wire x1="-0.9525" y1="-1.5875" x2="-0.9525" y2="0" width="0.2032" layer="51"/>
<wire x1="-0.9525" y1="0" x2="-0.9525" y2="1.5875" width="0.2032" layer="51"/>
<wire x1="-0.9525" y1="1.5875" x2="-0.3175" y2="1.5875" width="0.2032" layer="51"/>
<wire x1="-0.9525" y1="0" x2="-1.27" y2="0" width="0.2032" layer="51"/>
<wire x1="0.9525" y1="0" x2="1.27" y2="0" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="3.5497" width="0.2032" layer="21"/>
<pad name="+" x="-1.27" y="0" drill="0.8128" diameter="1.6764"/>
<pad name="-" x="1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="3.81" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="3.81" y="-3.175" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.3175" y1="-1.5875" x2="0.9525" y2="1.5875" layer="51"/>
</package>
<package name="CTT-050-100">
<description>&lt;b&gt;POLCAP TANTALUM&lt;/b&gt;&lt;p&gt;
5.0 mm lead spacing, 10 mm diameter, grid 0.0125 inch</description>
<wire x1="-2.3812" y1="2.8575" x2="-2.8575" y2="2.8575" width="0.254" layer="21"/>
<wire x1="-2.8575" y1="2.8575" x2="-3.3337" y2="2.8575" width="0.254" layer="21"/>
<wire x1="-2.8575" y1="2.8575" x2="-2.8575" y2="3.3338" width="0.254" layer="21"/>
<wire x1="-2.8575" y1="2.8575" x2="-2.8575" y2="2.3813" width="0.254" layer="21"/>
<wire x1="-0.3175" y1="1.5875" x2="-0.3175" y2="-1.5875" width="0.2032" layer="51"/>
<wire x1="-0.3175" y1="-1.5875" x2="-0.9525" y2="-1.5875" width="0.2032" layer="51"/>
<wire x1="-0.9525" y1="-1.5875" x2="-0.9525" y2="0" width="0.2032" layer="51"/>
<wire x1="-0.9525" y1="0" x2="-0.9525" y2="1.5875" width="0.2032" layer="51"/>
<wire x1="-0.9525" y1="1.5875" x2="-0.3175" y2="1.5875" width="0.2032" layer="51"/>
<wire x1="-0.9525" y1="0" x2="-2.54" y2="0" width="0.2032" layer="51"/>
<wire x1="0.9525" y1="0" x2="2.54" y2="0" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="5.2842" width="0.2032" layer="21"/>
<pad name="+" x="-2.54" y="0" drill="1.016" diameter="2.1844"/>
<pad name="-" x="2.54" y="0" drill="1.016" diameter="2.1844" shape="octagon"/>
<text x="5.08" y="2.54" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="5.08" y="-3.81" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.3175" y1="-1.5875" x2="0.9525" y2="1.5875" layer="51"/>
</package>
<package name="CTT-050-080">
<description>&lt;b&gt;POLCAP TANTALUM&lt;/b&gt;&lt;p&gt;
5.0 mm lead spacing, 8 mm diameter, grid 0.0125 inch</description>
<wire x1="-2.0637" y1="2.2225" x2="-2.54" y2="2.2225" width="0.254" layer="21"/>
<wire x1="-2.54" y1="2.2225" x2="-3.0162" y2="2.2225" width="0.254" layer="21"/>
<wire x1="-2.54" y1="2.2225" x2="-2.54" y2="2.6988" width="0.254" layer="21"/>
<wire x1="-2.54" y1="2.2225" x2="-2.54" y2="1.7463" width="0.254" layer="21"/>
<wire x1="-0.3175" y1="1.5875" x2="-0.3175" y2="-1.5875" width="0.2032" layer="51"/>
<wire x1="-0.3175" y1="-1.5875" x2="-0.9525" y2="-1.5875" width="0.2032" layer="51"/>
<wire x1="-0.9525" y1="-1.5875" x2="-0.9525" y2="0" width="0.2032" layer="51"/>
<wire x1="-0.9525" y1="0" x2="-0.9525" y2="1.5875" width="0.2032" layer="51"/>
<wire x1="-0.9525" y1="1.5875" x2="-0.3175" y2="1.5875" width="0.2032" layer="51"/>
<wire x1="-0.9525" y1="0" x2="-2.54" y2="0" width="0.2032" layer="51"/>
<wire x1="0.9525" y1="0" x2="2.54" y2="0" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="4.1275" width="0.2032" layer="21"/>
<pad name="+" x="-2.54" y="0" drill="1.016" diameter="2.1844"/>
<pad name="-" x="2.54" y="0" drill="1.016" diameter="2.1844" shape="octagon"/>
<text x="4.1275" y="2.2225" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="4.1275" y="-3.4925" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.3175" y1="-1.5875" x2="0.9525" y2="1.5875" layer="51"/>
</package>
<package name="C3216-TE-A">
<description>&lt;b&gt;SMD CHIP TANT&lt;/b&gt; - Panasonic Size A &lt;p&gt; 
EIA Code 3216, 3.2 mm x 1.6 mm, grid 0.0125 inch</description>
<wire x1="-1.57" y1="0.9113" x2="1.5494" y2="0.9113" width="0.2032" layer="21"/>
<wire x1="-1.57" y1="-0.9113" x2="1.5494" y2="-0.9113" width="0.2032" layer="21"/>
<wire x1="-1.57" y1="0.9113" x2="-1.57" y2="-0.9113" width="0.2032" layer="51"/>
<wire x1="1.5494" y1="0.9113" x2="1.5494" y2="-0.9113" width="0.2032" layer="51"/>
<wire x1="-2.5842" y1="0.983" x2="2.5843" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.5843" y1="0.983" x2="2.5843" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.5843" y1="-0.983" x2="-2.5842" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.5842" y1="-0.983" x2="-2.5842" y2="0.983" width="0.0508" layer="39"/>
<smd name="-" x="-1.5875" y="0" dx="1.27" dy="1.651" layer="1" rot="R90"/>
<smd name="+" x="1.5875" y="0" dx="1.27" dy="1.651" layer="1" rot="R90"/>
<text x="-1.8875" y="-2.0225" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="-1.8875" y="1.3875" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<rectangle x1="-2.45" y1="-0.15" x2="-1.05" y2="0.15" layer="51" rot="R90"/>
<rectangle x1="0.5" y1="-0.9" x2="1.45" y2="0.9" layer="51"/>
<rectangle x1="1.3963" y1="0.1963" x2="1.8963" y2="0.5037" layer="51" rot="R90"/>
<rectangle x1="1.3963" y1="-0.5037" x2="1.8963" y2="-0.1963" layer="51" rot="R90"/>
<rectangle x1="-0.3175" y1="-0.635" x2="0.3175" y2="0.635" layer="35"/>
<rectangle x1="0.3175" y1="-0.9525" x2="0.635" y2="0.9525" layer="21"/>
</package>
<package name="C3528-TE-B">
<description>&lt;b&gt;SMD CHIP TANT&lt;/b&gt; - Panasonic Size B&lt;p&gt; 
EIA Code 3528, 3.5 mm x 2.8 mm, grid 0.0125 inch</description>
<wire x1="-1.7925" y1="1.5225" x2="1.7925" y2="1.5225" width="0.2032" layer="21"/>
<wire x1="-1.7925" y1="-1.5225" x2="1.7925" y2="-1.5225" width="0.2032" layer="21"/>
<wire x1="-1.7925" y1="1.5225" x2="-1.7925" y2="-1.5225" width="0.2032" layer="51"/>
<wire x1="1.7925" y1="1.5225" x2="1.7925" y2="-1.5225" width="0.2032" layer="51"/>
<wire x1="-2.9017" y1="1.4593" x2="2.9018" y2="1.4593" width="0.0508" layer="39"/>
<wire x1="2.9018" y1="1.4593" x2="2.9018" y2="-1.4593" width="0.0508" layer="39"/>
<wire x1="2.9018" y1="-1.4593" x2="-2.9017" y2="-1.4593" width="0.0508" layer="39"/>
<wire x1="-2.9017" y1="-1.4593" x2="-2.9017" y2="1.4593" width="0.0508" layer="39"/>
<smd name="-" x="-1.905" y="0" dx="2.54" dy="1.524" layer="1" rot="R90"/>
<smd name="+" x="1.905" y="0" dx="2.54" dy="1.524" layer="1" rot="R90"/>
<text x="-2.2225" y="-2.8575" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="-2.2225" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<rectangle x1="-3.28" y1="-0.18" x2="-0.68" y2="0.18" layer="51" rot="R90"/>
<rectangle x1="0.8" y1="-1.5" x2="1.8" y2="1.45" layer="51"/>
<rectangle x1="1.4463" y1="0.5463" x2="2.4463" y2="0.8537" layer="51" rot="R90"/>
<rectangle x1="1.4463" y1="-0.9537" x2="2.4463" y2="-0.6463" layer="51" rot="R90"/>
<rectangle x1="-0.635" y1="-0.635" x2="0.635" y2="0.635" layer="35"/>
<rectangle x1="0.4762" y1="-1.5875" x2="0.9525" y2="1.5875" layer="21"/>
</package>
<package name="C6032-TE-C">
<description>&lt;b&gt;SMD CHIP TANT&lt;/b&gt; - Panasonic Size C&lt;p&gt; 
EIA Code 6032, 6.0 mm x 3.2 mm, grid 0.0125 inch</description>
<wire x1="-3.0163" y1="1.7463" x2="3.0163" y2="1.7463" width="0.2032" layer="21"/>
<wire x1="-3.0163" y1="-1.7463" x2="3.0163" y2="-1.7463" width="0.2032" layer="21"/>
<wire x1="-3.0163" y1="1.7463" x2="-3.0163" y2="-1.7463" width="0.2032" layer="51"/>
<wire x1="3.0163" y1="1.7463" x2="3.0163" y2="-1.7463" width="0.2032" layer="51"/>
<wire x1="-4.3304" y1="1.7768" x2="4.3306" y2="1.7768" width="0.0508" layer="39"/>
<wire x1="4.3306" y1="1.7768" x2="4.3306" y2="-1.7768" width="0.0508" layer="39"/>
<wire x1="4.3306" y1="-1.7768" x2="-4.3304" y2="-1.7768" width="0.0508" layer="39"/>
<wire x1="-4.3304" y1="-1.7768" x2="-4.3304" y2="1.7768" width="0.0508" layer="39"/>
<smd name="-" x="-2.8575" y="0" dx="2.794" dy="2.54" layer="1" rot="R90"/>
<smd name="+" x="2.8575" y="0" dx="2.794" dy="2.54" layer="1" rot="R90"/>
<text x="-2.9925" y="-2.9925" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="-2.9925" y="2.29" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<rectangle x1="2.4963" y1="0.7463" x2="3.7463" y2="1.0038" layer="51" rot="R90"/>
<rectangle x1="-4.655" y1="-0.155" x2="-1.655" y2="0.155" layer="51" rot="R90"/>
<rectangle x1="1.3" y1="-1.75" x2="3.0163" y2="1.7463" layer="51"/>
<rectangle x1="2.53" y1="-1.037" x2="3.7125" y2="-0.7795" layer="51" rot="R90"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
<rectangle x1="1" y1="-1.7463" x2="1.4288" y2="1.7" layer="21"/>
</package>
<package name="CSV-A">
<description>&lt;b&gt;SMD ELECTROLYTIC&lt;/b&gt; - Panasonic A&lt;p&gt; 
3 mm dia, grid 0.0125 inch</description>
<wire x1="1.8337" y1="1.675" x2="1.8337" y2="0.7025" width="0.2032" layer="21"/>
<wire x1="1.8337" y1="-0.7025" x2="1.8337" y2="-1.675" width="0.2032" layer="21"/>
<wire x1="1.8337" y1="-1.675" x2="-1.3137" y2="-1.675" width="0.2032" layer="21"/>
<wire x1="-1.3137" y1="-1.675" x2="-1.8337" y2="-1.155" width="0.2032" layer="21"/>
<wire x1="-1.8337" y1="-1.155" x2="-1.8337" y2="-0.7025" width="0.2032" layer="21"/>
<wire x1="1.8337" y1="1.675" x2="-1.3137" y2="1.675" width="0.2032" layer="21"/>
<wire x1="-1.3137" y1="1.675" x2="-1.8337" y2="1.155" width="0.2032" layer="21"/>
<wire x1="-1.8337" y1="1.155" x2="-1.8337" y2="0.7025" width="0.2032" layer="21"/>
<wire x1="0.905" y1="1.2" x2="0.905" y2="-1.2" width="0.1016" layer="51"/>
<circle x="0" y="0" radius="1.5811" width="0.1016" layer="51"/>
<smd name="+" x="-1.5875" y="0" dx="1.6764" dy="0.8128" layer="1"/>
<smd name="-" x="1.5875" y="0" dx="1.6764" dy="0.8128" layer="1"/>
<text x="2.54" y="0.635" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="2.54" y="-1.5875" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.0632" y1="-0.1588" x2="-1.5869" y2="0.1588" layer="51"/>
<rectangle x1="1.5875" y1="-0.1588" x2="2.0638" y2="0.1588" layer="51"/>
<rectangle x1="-0.3175" y1="-0.635" x2="0.3175" y2="0.635" layer="35"/>
</package>
<package name="CSV-B">
<description>&lt;b&gt;SMD ELECTROLYTIC&lt;/b&gt; - Panasonic B&lt;p&gt; 
4 mm dia, grid 0.0125 inch</description>
<wire x1="2.2862" y1="2.2862" x2="2.2862" y2="0.7938" width="0.2032" layer="21"/>
<wire x1="2.2862" y1="-0.7938" x2="2.2862" y2="-2.2863" width="0.2032" layer="21"/>
<wire x1="2.2862" y1="-2.2863" x2="-1.54" y2="-2.2863" width="0.2032" layer="21"/>
<wire x1="-1.54" y1="-2.2863" x2="-2.2863" y2="-1.5401" width="0.2032" layer="21"/>
<wire x1="-2.2863" y1="-1.5401" x2="-2.2863" y2="-0.7938" width="0.2032" layer="21"/>
<wire x1="2.2862" y1="2.2862" x2="-1.54" y2="2.2863" width="0.2032" layer="21"/>
<wire x1="-1.54" y1="2.2863" x2="-2.2863" y2="1.5401" width="0.2032" layer="21"/>
<wire x1="-2.2863" y1="1.5401" x2="-2.2862" y2="0.7938" width="0.2032" layer="21"/>
<wire x1="1.24" y1="1.7" x2="1.24" y2="-1.7" width="0.1016" layer="51"/>
<circle x="0" y="0" radius="2.1298" width="0.1016" layer="51"/>
<smd name="+" x="-1.905" y="0" dx="2.1844" dy="1.0668" layer="1"/>
<smd name="-" x="1.905" y="0" dx="2.1844" dy="1.0668" layer="1"/>
<text x="2.8575" y="0.9525" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="2.8575" y="-1.905" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.6988" y1="-0.3175" x2="-2.0638" y2="0.3175" layer="51"/>
<rectangle x1="2.0638" y1="-0.3175" x2="2.6988" y2="0.3175" layer="51"/>
<rectangle x1="-0.3175" y1="-0.635" x2="0.3175" y2="0.635" layer="35"/>
</package>
<package name="CSV-C">
<description>&lt;b&gt;SMD ELECTROLYTIC&lt;/b&gt; - Panasonic C&lt;p&gt; 
5 mm dia, grid 0.0125 inch</description>
<wire x1="2.6988" y1="2.6988" x2="2.6988" y2="0.9525" width="0.2032" layer="21"/>
<wire x1="2.6987" y1="-0.9525" x2="2.6987" y2="-2.6988" width="0.2032" layer="21"/>
<wire x1="2.6987" y1="-2.6988" x2="-1.7463" y2="-2.6988" width="0.2032" layer="21"/>
<wire x1="-1.7463" y1="-2.6988" x2="-2.6988" y2="-1.7463" width="0.2032" layer="21"/>
<wire x1="-2.6988" y1="-1.7463" x2="-2.6988" y2="-0.9525" width="0.2032" layer="21"/>
<wire x1="2.6988" y1="2.6988" x2="-1.7463" y2="2.6988" width="0.2032" layer="21"/>
<wire x1="-1.7463" y1="2.6988" x2="-2.6988" y2="1.7462" width="0.2032" layer="21"/>
<wire x1="-2.6988" y1="1.7462" x2="-2.6988" y2="0.9525" width="0.2032" layer="21"/>
<wire x1="1.5337" y1="1.9587" x2="1.5337" y2="-1.9588" width="0.1016" layer="51"/>
<circle x="0" y="0" radius="2.5597" width="0.1016" layer="51"/>
<smd name="+" x="-2.2225" y="0" dx="2.54" dy="1.27" layer="1"/>
<smd name="-" x="2.2225" y="0" dx="2.54" dy="1.27" layer="1"/>
<text x="3.175" y="1.27" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="3.175" y="-2.2225" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.175" y1="-0.3175" x2="-2.54" y2="0.3175" layer="51"/>
<rectangle x1="2.54" y1="-0.3175" x2="3.175" y2="0.3175" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
<package name="CSV-D">
<description>&lt;b&gt;SMD ELECTROLYTIC&lt;/b&gt; - Panasonic D&lt;p&gt; 
6.3 mm dia, grid 0.0125 inch</description>
<wire x1="3.3337" y1="3.3338" x2="3.3337" y2="0.9525" width="0.2032" layer="21"/>
<wire x1="3.3338" y1="-0.9525" x2="3.3338" y2="-3.3338" width="0.2032" layer="21"/>
<wire x1="3.3338" y1="-3.3338" x2="-2.0637" y2="-3.3337" width="0.2032" layer="21"/>
<wire x1="-2.0637" y1="-3.3337" x2="-3.3338" y2="-2.0637" width="0.2032" layer="21"/>
<wire x1="-3.3338" y1="-2.0637" x2="-3.3338" y2="-0.9525" width="0.2032" layer="21"/>
<wire x1="3.3337" y1="3.3338" x2="-2.0637" y2="3.3338" width="0.2032" layer="21"/>
<wire x1="-2.0637" y1="3.3338" x2="-3.3338" y2="2.0638" width="0.2032" layer="21"/>
<wire x1="-3.3338" y1="2.0638" x2="-3.3338" y2="0.9525" width="0.2032" layer="21"/>
<wire x1="1.9687" y1="2.4" x2="1.9687" y2="-2.4" width="0.1016" layer="51"/>
<circle x="0" y="0" radius="3.127" width="0.1016" layer="51"/>
<smd name="+" x="-2.54" y="0" dx="3.302" dy="1.27" layer="1"/>
<smd name="-" x="2.54" y="0" dx="3.302" dy="1.27" layer="1"/>
<text x="3.81" y="1.5875" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="3.81" y="-2.54" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3175" x2="3.81" y2="0.3175" layer="51"/>
<rectangle x1="-3.81" y1="-0.3175" x2="-3.175" y2="0.3175" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
<package name="CSV-E/F">
<description>&lt;b&gt;SMD ELECTROLYTIC&lt;/b&gt; - Panasonic E/F&lt;p&gt; 
8 mm dia, grid 0.0125 inch</description>
<wire x1="4.1275" y1="4.1275" x2="4.1275" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.1275" y1="-1.27" x2="4.1275" y2="-4.1275" width="0.2032" layer="21"/>
<wire x1="4.1275" y1="-4.1275" x2="-2.54" y2="-4.1275" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-4.1275" x2="-4.1275" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="-4.1275" y1="-2.54" x2="-4.1275" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.1275" y1="4.1275" x2="-2.54" y2="4.1275" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="4.1275" x2="-4.1275" y2="2.54" width="0.2032" layer="21"/>
<wire x1="-4.1275" y1="2.54" x2="-4.1275" y2="1.27" width="0.2032" layer="21"/>
<wire x1="2.48" y1="3" x2="2.48" y2="-3" width="0.1016" layer="51"/>
<circle x="0" y="0" radius="3.9782" width="0.1016" layer="51"/>
<smd name="+" x="-3.4925" y="0" dx="4.318" dy="1.9304" layer="1"/>
<smd name="-" x="3.4925" y="0" dx="4.318" dy="1.9304" layer="1"/>
<text x="4.7625" y="2.2225" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="4.7625" y="-3.175" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.9688" y1="-0.4763" x2="4.9213" y2="0.4763" layer="51"/>
<rectangle x1="-5.08" y1="-0.4763" x2="-3.9688" y2="0.4763" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
<package name="CSV-G">
<description>&lt;b&gt;SMD ELECTROLYTIC&lt;/b&gt; - Panasonic G&lt;p&gt; 
10 mm dia, grid 0.0125 inch</description>
<wire x1="5.08" y1="5.2387" x2="5.08" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="-5.2388" width="0.2032" layer="21"/>
<wire x1="5.08" y1="-5.2388" x2="-3.0162" y2="-5.2387" width="0.2032" layer="21"/>
<wire x1="-3.0162" y1="-5.2387" x2="-5.2387" y2="-3.0162" width="0.2032" layer="21"/>
<wire x1="-5.2387" y1="-3.0162" x2="-5.2388" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.08" y1="5.2387" x2="-3.3338" y2="5.2387" width="0.2032" layer="21"/>
<wire x1="-3.3338" y1="5.2387" x2="-5.2388" y2="3.3337" width="0.2032" layer="21"/>
<wire x1="-5.2388" y1="3.3337" x2="-5.2388" y2="1.27" width="0.2032" layer="21"/>
<wire x1="2.6976" y1="4.1587" x2="2.6976" y2="-4.1588" width="0.1016" layer="51"/>
<circle x="0" y="0" radius="5" width="0.1016" layer="51"/>
<smd name="+" x="-4.445" y="0" dx="4.318" dy="1.9304" layer="1"/>
<smd name="-" x="4.445" y="0" dx="4.318" dy="1.9304" layer="1"/>
<text x="5.715" y="2.54" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="5.715" y="-3.4925" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.032" y1="-0.4763" x2="-4.9207" y2="0.4763" layer="51"/>
<rectangle x1="4.9213" y1="-0.4763" x2="6.0326" y2="0.4763" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
<package name="C3216/T">
<description>&lt;b&gt;SMD CHIP TANT&lt;/b&gt;&lt;p&gt;
chip tantalum</description>
<wire x1="-2.743" y1="1.1417" x2="2.743" y2="1.1417" width="0.0508" layer="39"/>
<wire x1="2.743" y1="1.1417" x2="2.743" y2="-1.1418" width="0.0508" layer="39"/>
<wire x1="2.743" y1="-1.1418" x2="-2.743" y2="-1.1418" width="0.0508" layer="39"/>
<wire x1="-2.743" y1="-1.1418" x2="-2.743" y2="1.1417" width="0.0508" layer="39"/>
<wire x1="2.85" y1="0.9" x2="0.4" y2="0.9" width="0.2032" layer="21"/>
<wire x1="0.4" y1="0.9" x2="-2.45" y2="0.9" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="0.9" x2="-2.45" y2="-0.9" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="-0.9" x2="2.85" y2="-0.9" width="0.2032" layer="21"/>
<wire x1="2.85" y1="0.9" x2="2.85" y2="-0.9" width="0.2032" layer="21"/>
<wire x1="0.4" y1="0.9" x2="0.4" y2="-0.9" width="0.2032" layer="21"/>
<smd name="+" x="1.4289" y="0" dx="1.5" dy="1.27" layer="1"/>
<smd name="-" x="-1.4288" y="0" dx="1.5" dy="1.27" layer="1"/>
<text x="-2.2225" y="1.27" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.2225" y="-2.0637" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
<rectangle x1="2.35" y1="-0.95" x2="2.8" y2="0.95" layer="21"/>
</package>
<package name="C3216-18R/T">
<description>&lt;b&gt;SMD CHIP TANT&lt;/b&gt; - KEMET A / EIA 3216-18 Reflow solder&lt;p&gt;
KEMET S / EIA 3216-12</description>
<wire x1="-2.743" y1="0.8242" x2="2.743" y2="0.8242" width="0.0508" layer="39"/>
<wire x1="2.743" y1="0.8242" x2="2.743" y2="-0.8243" width="0.0508" layer="39"/>
<wire x1="2.743" y1="-0.8243" x2="-2.743" y2="-0.8243" width="0.0508" layer="39"/>
<wire x1="-2.743" y1="-0.8243" x2="-2.743" y2="0.8242" width="0.0508" layer="39"/>
<wire x1="1.651" y1="-0.559" x2="1.651" y2="0.559" width="0.2032" layer="51"/>
<wire x1="-1.651" y1="-0.559" x2="-1.651" y2="0.559" width="0.2032" layer="51"/>
<wire x1="1.5081" y1="-0.9731" x2="1.5081" y2="0.9731" width="0.2032" layer="51"/>
<wire x1="1.5081" y1="0.9731" x2="-1.5081" y2="0.9731" width="0.2032" layer="21"/>
<wire x1="-1.5081" y1="0.9731" x2="-1.5081" y2="-0.9731" width="0.2032" layer="51"/>
<wire x1="-1.5081" y1="-0.9731" x2="1.5081" y2="-0.9731" width="0.2032" layer="21"/>
<smd name="+" x="1.5875" y="0" dx="1.95" dy="1.5" layer="1"/>
<smd name="-" x="-1.5875" y="0" dx="1.95" dy="1.5" layer="1"/>
<text x="-1.905" y="1.27" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.905" y="-2.105" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.1588" y1="-0.4763" x2="0.1588" y2="0.4763" layer="35"/>
<rectangle x1="0.2" y1="-1" x2="0.5" y2="1" layer="21"/>
</package>
<package name="C3216-18W/T">
<description>&lt;b&gt;SMD CHIP TANT&lt;/b&gt; - KEMET A / EIA 3216-18 Wave solder&lt;p&gt;
KEMET S / EIA 3216-12</description>
<wire x1="-2.743" y1="0.983" x2="2.743" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.743" y1="0.983" x2="2.743" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.743" y1="-0.983" x2="-2.743" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.743" y1="-0.983" x2="-2.743" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.651" y1="-0.559" x2="1.651" y2="0.559" width="0.2032" layer="51"/>
<wire x1="-1.651" y1="-0.559" x2="-1.651" y2="0.559" width="0.2032" layer="51"/>
<wire x1="1.5081" y1="-0.9731" x2="1.5081" y2="0.9731" width="0.2032" layer="51"/>
<wire x1="1.5081" y1="0.9731" x2="-1.5081" y2="0.9731" width="0.2032" layer="21"/>
<wire x1="-1.5081" y1="0.9731" x2="-1.5081" y2="-0.9731" width="0.2032" layer="51"/>
<wire x1="-1.5081" y1="-0.9731" x2="1.5081" y2="-0.9731" width="0.2032" layer="21"/>
<smd name="+" x="1.5875" y="0" dx="2.15" dy="1.8" layer="1"/>
<smd name="-" x="-1.5875" y="0" dx="2.15" dy="1.8" layer="1"/>
<text x="-2.2225" y="1.27" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.905" y="-2.2225" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.1588" y1="-0.635" x2="0.1588" y2="0.635" layer="35"/>
<rectangle x1="0.2" y1="-1" x2="0.5" y2="1" layer="21"/>
</package>
<package name="C3528/T">
<description>&lt;b&gt;SMD CHIP TANT&lt;/b&gt;&lt;p&gt;
chip tantalum</description>
<wire x1="-2.743" y1="1.3005" x2="2.743" y2="1.3005" width="0.0508" layer="39"/>
<wire x1="2.743" y1="1.3005" x2="2.743" y2="-1.3005" width="0.0508" layer="39"/>
<wire x1="2.743" y1="-1.3005" x2="-2.743" y2="-1.3005" width="0.0508" layer="39"/>
<wire x1="-2.743" y1="-1.3005" x2="-2.743" y2="1.3005" width="0.0508" layer="39"/>
<wire x1="3.4" y1="1.5588" x2="-2.9" y2="1.5588" width="0.2032" layer="21"/>
<wire x1="-2.9" y1="1.5588" x2="-2.9" y2="-1.5587" width="0.2032" layer="21"/>
<wire x1="-2.9" y1="-1.5587" x2="0.3" y2="-1.5587" width="0.2032" layer="21"/>
<wire x1="0.3" y1="-1.5587" x2="3.4" y2="-1.5587" width="0.2032" layer="21"/>
<wire x1="3.4" y1="-1.5587" x2="3.4" y2="1.5588" width="0.2032" layer="21"/>
<wire x1="0.3" y1="-1.5587" x2="0.3" y2="1.5588" width="0.2032" layer="21"/>
<smd name="+" x="1.5875" y="0" dx="2" dy="2.2" layer="1"/>
<smd name="-" x="-1.5875" y="0" dx="2" dy="2.2" layer="1"/>
<text x="-2.6988" y="1.9051" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.6987" y="-2.6987" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
<rectangle x1="2.8575" y1="-1.5875" x2="3.3338" y2="1.5875" layer="21"/>
</package>
<package name="C3528-21R/T">
<description>&lt;b&gt;SMD CHIP TANT&lt;/b&gt; - KEMET B / EIA 3528-21 Reflow solder&lt;p&gt;
KEMET T / EIA 3528-12</description>
<wire x1="-1.5875" y1="1.3494" x2="1.5875" y2="1.3494" width="0.2032" layer="21"/>
<wire x1="1.5875" y1="1.3494" x2="1.5875" y2="-1.3494" width="0.2032" layer="51"/>
<wire x1="1.5875" y1="-1.3494" x2="-1.5875" y2="-1.3494" width="0.2032" layer="21"/>
<wire x1="-1.5875" y1="-1.3494" x2="-1.5875" y2="1.3494" width="0.2032" layer="51"/>
<wire x1="-2.5842" y1="1.4593" x2="2.5843" y2="1.4593" width="0.0508" layer="39"/>
<wire x1="2.5843" y1="1.4593" x2="2.5843" y2="-1.4593" width="0.0508" layer="39"/>
<wire x1="2.5843" y1="-1.4593" x2="-2.5842" y2="-1.4593" width="0.0508" layer="39"/>
<wire x1="-2.5842" y1="-1.4593" x2="-2.5842" y2="1.4593" width="0.0508" layer="39"/>
<smd name="+" x="1.525" y="0" dx="1.778" dy="2.286" layer="1"/>
<smd name="-" x="-1.525" y="0" dx="1.778" dy="2.286" layer="1"/>
<text x="-1.905" y="1.5875" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.3175" y1="-0.635" x2="0.3175" y2="0.635" layer="35"/>
<rectangle x1="-1.75" y1="-0.6" x2="-1.625" y2="0.6" layer="51"/>
<rectangle x1="1.625" y1="-0.6" x2="1.75" y2="0.6" layer="51"/>
<rectangle x1="0.1" y1="-1.3" x2="0.4763" y2="1.3" layer="21"/>
</package>
<package name="C3528-21W/T">
<description>&lt;b&gt;SMD CHIP TANT&lt;/b&gt; - KEMET B / EIA 3528-21 Wave solder&lt;p&gt;
KEMET T / EIA 3528-12</description>
<wire x1="-1.5875" y1="1.27" x2="1.5875" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.5875" y1="1.27" x2="1.5875" y2="0.9525" width="0.2032" layer="51"/>
<wire x1="1.5875" y1="0.9525" x2="1.5875" y2="-0.9525" width="0.2032" layer="51"/>
<wire x1="1.5875" y1="-0.9525" x2="1.5875" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="1.5875" y1="-1.27" x2="-1.5875" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.5875" y1="-1.27" x2="-1.5875" y2="-0.9525" width="0.2032" layer="51"/>
<wire x1="-1.5875" y1="-0.9525" x2="-1.5875" y2="0.9525" width="0.2032" layer="51"/>
<wire x1="-1.5875" y1="0.9525" x2="-1.5875" y2="1.27" width="0.2032" layer="51"/>
<wire x1="-2.9017" y1="1.1417" x2="2.9018" y2="1.1417" width="0.0508" layer="39"/>
<wire x1="2.9018" y1="1.1417" x2="2.9018" y2="-1.1418" width="0.0508" layer="39"/>
<wire x1="2.9018" y1="-1.1418" x2="-2.9017" y2="-1.1418" width="0.0508" layer="39"/>
<wire x1="-2.9017" y1="-1.1418" x2="-2.9017" y2="1.1417" width="0.0508" layer="39"/>
<smd name="+" x="1.625" y="0" dx="2.15" dy="1.8" layer="1"/>
<smd name="-" x="-1.625" y="0" dx="2.15" dy="1.8" layer="1"/>
<text x="-1.905" y="1.5875" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.75" y1="-0.6" x2="-1.625" y2="0.6" layer="51"/>
<rectangle x1="1.625" y1="-0.6" x2="1.75" y2="0.6" layer="51"/>
<rectangle x1="0.1588" y1="-1.27" x2="0.4763" y2="1.27" layer="21"/>
<rectangle x1="-0.3175" y1="-0.635" x2="0.3175" y2="0.635" layer="35"/>
</package>
<package name="C6032/T">
<description>&lt;b&gt;SMD CHIP TANT&lt;/b&gt;&lt;p&gt;
chip tantalum</description>
<wire x1="-4.013" y1="1.618" x2="4.013" y2="1.618" width="0.0508" layer="39"/>
<wire x1="4.013" y1="1.618" x2="4.013" y2="-1.618" width="0.0508" layer="39"/>
<wire x1="4.013" y1="-1.618" x2="-4.013" y2="-1.618" width="0.0508" layer="39"/>
<wire x1="-4.013" y1="-1.618" x2="-4.013" y2="1.618" width="0.0508" layer="39"/>
<wire x1="4.8" y1="1.7" x2="-4.15" y2="1.7" width="0.2032" layer="21"/>
<wire x1="-4.15" y1="1.7" x2="-4.15" y2="-1.7" width="0.2032" layer="21"/>
<wire x1="-4.15" y1="-1.7" x2="0.95" y2="-1.7" width="0.2032" layer="21"/>
<wire x1="0.95" y1="-1.7" x2="4.8" y2="-1.7" width="0.2032" layer="21"/>
<wire x1="4.8" y1="-1.7" x2="4.8" y2="1.7" width="0.2032" layer="21"/>
<wire x1="0.95" y1="-1.7" x2="0.95" y2="1.7" width="0.2032" layer="21"/>
<smd name="+" x="2.54" y="0" dx="2.6" dy="2.2" layer="1"/>
<smd name="-" x="-2.54" y="0" dx="2.6" dy="2.2" layer="1"/>
<text x="-3.9688" y="2.0638" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.9688" y="-2.8575" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
<rectangle x1="4.05" y1="-1.7" x2="4.75" y2="1.7" layer="21"/>
</package>
<package name="C6032-28R/T">
<description>&lt;b&gt;SMD CHIP TANT&lt;/b&gt; - KEMET C / EIA 6032-28 Reflow solder&lt;p&gt;
KEMET U / EIA 6032-15</description>
<wire x1="-2.8575" y1="1.5875" x2="2.8575" y2="1.5875" width="0.2032" layer="21"/>
<wire x1="2.8575" y1="1.57" x2="2.8575" y2="-1.57" width="0.2032" layer="51"/>
<wire x1="2.8575" y1="-1.5875" x2="-2.8575" y2="-1.5875" width="0.2032" layer="21"/>
<wire x1="-2.8575" y1="-1.57" x2="-2.8575" y2="1.57" width="0.2032" layer="51"/>
<wire x1="-4.013" y1="1.4592" x2="4.0131" y2="1.4592" width="0.0508" layer="39"/>
<wire x1="4.0131" y1="1.4592" x2="4.0131" y2="-1.4593" width="0.0508" layer="39"/>
<wire x1="4.0131" y1="-1.4593" x2="-4.013" y2="-1.4593" width="0.0508" layer="39"/>
<wire x1="-4.013" y1="-1.4593" x2="-4.013" y2="1.4592" width="0.0508" layer="39"/>
<smd name="+" x="2.54" y="0" dx="2.55" dy="2.5" layer="1"/>
<smd name="-" x="-2.54" y="0" dx="2.55" dy="2.5" layer="1"/>
<text x="-2.8575" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.8575" y="-2.8575" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.1" y1="-1.0995" x2="-2.95" y2="0.953" layer="51"/>
<rectangle x1="2.95" y1="-1.1" x2="3.1" y2="1.1" layer="51"/>
<rectangle x1="0.7" y1="-1.6" x2="1.1" y2="1.6" layer="21"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
<package name="C6032-28W/T">
<description>&lt;b&gt;SMD CHIP TANT&lt;/b&gt; - KEMET C / EIA 6032-28 Wave solder&lt;p&gt;
KEMET U / EIA 6032-15</description>
<wire x1="-2.8575" y1="1.5875" x2="2.8575" y2="1.5875" width="0.2032" layer="21"/>
<wire x1="2.8575" y1="-1.5875" x2="-2.8575" y2="-1.5875" width="0.2032" layer="21"/>
<wire x1="-2.8575" y1="-1.5875" x2="-2.8575" y2="1.5875" width="0.2032" layer="51"/>
<wire x1="2.8575" y1="1.5875" x2="2.8575" y2="-1.5875" width="0.2032" layer="51"/>
<wire x1="-4.013" y1="1.618" x2="4.0131" y2="1.618" width="0.0508" layer="39"/>
<wire x1="4.0131" y1="1.618" x2="4.0131" y2="-1.6179" width="0.0508" layer="39"/>
<wire x1="4.0131" y1="-1.6179" x2="-4.013" y2="-1.6179" width="0.0508" layer="39"/>
<wire x1="-4.013" y1="-1.6179" x2="-4.013" y2="1.618" width="0.0508" layer="39"/>
<smd name="+" x="2.54" y="0" dx="2.75" dy="1.8" layer="1"/>
<smd name="-" x="-2.54" y="0" dx="2.75" dy="1.8" layer="1"/>
<text x="-2.8575" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.8575" y="-2.8575" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.6" y1="-1.6" x2="1" y2="1.6" layer="21"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
<package name="C7343/T">
<description>&lt;b&gt;SMD CHIP TANT&lt;/b&gt;&lt;p&gt;
chip tantalum</description>
<wire x1="-4.648" y1="2.253" x2="4.6481" y2="2.253" width="0.0508" layer="39"/>
<wire x1="4.6481" y1="2.253" x2="4.6481" y2="-2.253" width="0.0508" layer="39"/>
<wire x1="4.6481" y1="-2.253" x2="-4.648" y2="-2.253" width="0.0508" layer="39"/>
<wire x1="-4.648" y1="-2.253" x2="-4.648" y2="2.253" width="0.0508" layer="39"/>
<wire x1="-4.8" y1="2.2" x2="1.6" y2="2.2" width="0.2032" layer="21"/>
<wire x1="1.6" y1="2.2" x2="5.3" y2="2.2" width="0.2032" layer="21"/>
<wire x1="5.3" y1="2.2" x2="5.3" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="5.3" y1="-2.2" x2="-4.8" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-4.8" y1="-2.2" x2="-4.8" y2="2.2" width="0.2032" layer="21"/>
<wire x1="1.6" y1="2.2" x2="1.6" y2="-2.2" width="0.2032" layer="21"/>
<smd name="+" x="3.175" y="0" dx="2.6" dy="2.4" layer="1"/>
<smd name="-" x="-3.175" y="0" dx="2.6" dy="2.4" layer="1"/>
<text x="-4.6038" y="2.54" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-4.6037" y="-3.3338" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
<rectangle x1="4.65" y1="-2.2" x2="5.35" y2="2.2" layer="21"/>
</package>
<package name="C139CLL-2R">
<description>&lt;b&gt;SMD POLCAP&lt;/b&gt; - reflow soldering&lt;p&gt; 
SMD (Chip) Long Life 139 CLL&lt;p&gt;
http://www.bccomponents.com/</description>
<wire x1="-8.1" y1="3.9" x2="-8.1" y2="-3.9" width="0.2032" layer="39"/>
<wire x1="-8.1" y1="-3.9" x2="8.1" y2="-3.9" width="0.2032" layer="39"/>
<wire x1="8.1" y1="-3.9" x2="8.1" y2="3.9" width="0.2032" layer="39"/>
<wire x1="8.1" y1="3.9" x2="-8.1" y2="3.9" width="0.2032" layer="39"/>
<wire x1="7.15" y1="-0.6" x2="6.9" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="6.9" y1="-0.85" x2="6.9" y2="0.85" width="0.2032" layer="51"/>
<wire x1="6.9" y1="0.85" x2="7.15" y2="0.6" width="0.2032" layer="51"/>
<wire x1="7.15" y1="0.6" x2="7.15" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="-7.15" y1="0.6" x2="-6.9" y2="0.85" width="0.2032" layer="51"/>
<wire x1="-6.9" y1="0.85" x2="-6.9" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="-6.9" y1="-0.85" x2="-7.15" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="-7.15" y1="-0.6" x2="-7.15" y2="0.6" width="0.2032" layer="51"/>
<wire x1="6.4" y1="-3.05" x2="-6.4" y2="-3.05" width="0.2032" layer="21"/>
<wire x1="-6.4" y1="-3.05" x2="-6.4" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-6.4" y1="-1.6" x2="-6.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-6.4" y1="1.6" x2="-6.4" y2="3.05" width="0.2032" layer="21"/>
<wire x1="-6.4" y1="3.05" x2="6.4" y2="3.05" width="0.2032" layer="21"/>
<wire x1="6.4" y1="3.05" x2="6.4" y2="1.6" width="0.2032" layer="21"/>
<wire x1="6.4" y1="1.6" x2="6.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="6.4" y1="-1.6" x2="6.4" y2="-3.05" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-2" x2="4.5" y2="-2" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-2" x2="4.25" y2="-2.95" width="0.2032" layer="21"/>
<wire x1="6.35" y1="2" x2="4.5" y2="2" width="0.2032" layer="21"/>
<wire x1="4.5" y1="2" x2="4.25" y2="2.95" width="0.2032" layer="21"/>
<smd name="+" x="6.0325" y="0" dx="3.5" dy="2.8" layer="1"/>
<smd name="-" x="-6.35" y="0" dx="3.5" dy="2.8" layer="1"/>
<text x="-6.35" y="3.81" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-6.35" y="-4.7625" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.5" y1="-1" x2="6.85" y2="1" layer="51"/>
<rectangle x1="-6.85" y1="-1" x2="-4.5" y2="1" layer="51"/>
</package>
<package name="C139CLL-2W">
<description>&lt;b&gt;SMD POLCAP&lt;/b&gt; - wave soldering&lt;p&gt; 
SMD (Chip) Long Life 139 CLL&lt;p&gt;
http://www.bccomponents.com/</description>
<wire x1="-10.15" y1="3.9" x2="-10.15" y2="-3.9" width="0.2032" layer="39"/>
<wire x1="-10.15" y1="-3.9" x2="10.15" y2="-3.9" width="0.2032" layer="39"/>
<wire x1="10.15" y1="-3.9" x2="10.15" y2="3.9" width="0.2032" layer="39"/>
<wire x1="10.15" y1="3.9" x2="-10.15" y2="3.9" width="0.2032" layer="39"/>
<wire x1="7.15" y1="-0.6" x2="6.9" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="6.9" y1="-0.85" x2="6.9" y2="0.85" width="0.2032" layer="51"/>
<wire x1="6.9" y1="0.85" x2="7.15" y2="0.6" width="0.2032" layer="51"/>
<wire x1="7.15" y1="0.6" x2="7.15" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="-7.15" y1="0.6" x2="-6.9" y2="0.85" width="0.2032" layer="51"/>
<wire x1="-6.9" y1="0.85" x2="-6.9" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="-6.9" y1="-0.85" x2="-7.15" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="-7.15" y1="-0.6" x2="-7.15" y2="0.6" width="0.2032" layer="51"/>
<wire x1="6.4" y1="-3.05" x2="-6.4" y2="-3.05" width="0.2032" layer="21"/>
<wire x1="-6.4" y1="-3.05" x2="-6.4" y2="3.05" width="0.2032" layer="51"/>
<wire x1="-6.4" y1="3.05" x2="6.4" y2="3.05" width="0.2032" layer="21"/>
<wire x1="6.4" y1="3.05" x2="6.4" y2="-3.05" width="0.2032" layer="51"/>
<wire x1="6.35" y1="-2" x2="4.5" y2="-2" width="0.2032" layer="51"/>
<wire x1="4.5" y1="-2" x2="4.25" y2="-2.95" width="0.2032" layer="21"/>
<wire x1="6.35" y1="2" x2="4.5" y2="2" width="0.2032" layer="51"/>
<wire x1="4.5" y1="2" x2="4.25" y2="2.95" width="0.2032" layer="21"/>
<smd name="+" x="7.3025" y="0" dx="4.3" dy="5" layer="1" roundness="50"/>
<smd name="-" x="-7.3025" y="0" dx="4.3" dy="5" layer="1" roundness="50"/>
<text x="-6.35" y="3.81" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-6.35" y="-4.7625" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.5" y1="-1" x2="6.85" y2="1" layer="51"/>
<rectangle x1="-6.85" y1="-1" x2="-4.5" y2="1" layer="51"/>
</package>
<package name="C139CLL-3W">
<description>&lt;b&gt;SMD POLCAP&lt;/b&gt; - wave soldering&lt;p&gt; 
SMD (Chip) Long Life 139 CLL&lt;p&gt;
http://www.bccomponents.com/</description>
<wire x1="-10.6" y1="6.4" x2="-10.6" y2="-6.4" width="0.2032" layer="39"/>
<wire x1="-10.6" y1="-6.4" x2="9.2" y2="-6.4" width="0.2032" layer="39"/>
<wire x1="9.2" y1="-6.4" x2="9.2" y2="6.4" width="0.2032" layer="39"/>
<wire x1="9.2" y1="6.4" x2="-10.6" y2="6.4" width="0.2032" layer="39"/>
<wire x1="7.15" y1="-0.6" x2="6.9" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="6.9" y1="-0.85" x2="6.9" y2="0.85" width="0.2032" layer="51"/>
<wire x1="6.9" y1="0.85" x2="7.15" y2="0.6" width="0.2032" layer="51"/>
<wire x1="7.15" y1="0.6" x2="7.15" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="-7.15" y1="0.6" x2="-6.9" y2="0.85" width="0.2032" layer="51"/>
<wire x1="-6.9" y1="0.85" x2="-6.9" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="-6.9" y1="-0.85" x2="-7.15" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="-7.15" y1="-0.6" x2="-7.15" y2="0.6" width="0.2032" layer="51"/>
<wire x1="6.4" y1="-3.75" x2="-6.4" y2="-3.75" width="0.2032" layer="21"/>
<wire x1="-6.4" y1="-3.75" x2="-6.4" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-6.4" y1="-1.6" x2="-6.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-6.4" y1="1.6" x2="-6.4" y2="3.75" width="0.2032" layer="21"/>
<wire x1="-6.4" y1="3.75" x2="6.4" y2="3.75" width="0.2032" layer="21"/>
<wire x1="6.4" y1="3.75" x2="6.4" y2="1.6" width="0.2032" layer="21"/>
<wire x1="6.4" y1="1.6" x2="6.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="6.4" y1="-1.6" x2="6.4" y2="-3.75" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-2.7" x2="4.5" y2="-2.7" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-2.7" x2="4.25" y2="-3.65" width="0.2032" layer="21"/>
<wire x1="6.35" y1="2.7" x2="4.5" y2="2.7" width="0.2032" layer="21"/>
<wire x1="4.5" y1="2.7" x2="4.25" y2="3.65" width="0.2032" layer="21"/>
<smd name="+" x="7.3025" y="0" dx="4.3" dy="6" layer="1" roundness="50"/>
<smd name="-" x="-7.3025" y="0" dx="4.3" dy="6" layer="1" roundness="50"/>
<text x="-6.35" y="4.445" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-6.35" y="-5.3975" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.5" y1="-1" x2="6.85" y2="1" layer="51"/>
<rectangle x1="-6.85" y1="-1" x2="-4.5" y2="1" layer="51"/>
</package>
<package name="C140CLH-0810">
<description>&lt;b&gt;SMD POLCAP&lt;/b&gt;&lt;p&gt;
Long life base plate, High temperature 140 CLH&lt;p&gt;
http://www.bccomponents.com/</description>
<wire x1="-4.85" y1="4.2" x2="3.4" y2="4.2" width="0.2032" layer="21"/>
<wire x1="4.85" y1="2.75" x2="4.85" y2="1.45" width="0.2032" layer="21"/>
<wire x1="4.85" y1="1.45" x2="4.85" y2="-1.45" width="0.2032" layer="51"/>
<wire x1="4.85" y1="-1.45" x2="4.85" y2="-2.75" width="0.2032" layer="21"/>
<wire x1="3.4" y1="-4.2" x2="-4.85" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="-4.85" y1="-4.2" x2="-4.85" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="-4.85" y1="-1.45" x2="-4.85" y2="1.45" width="0.2032" layer="51"/>
<wire x1="-4.85" y1="1.45" x2="-4.85" y2="4.2" width="0.2032" layer="21"/>
<wire x1="4.85" y1="2.75" x2="3.4" y2="4.2" width="0.2032" layer="21"/>
<wire x1="4.85" y1="-2.75" x2="3.4" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="-3.65" y1="1.4" x2="3.65" y2="1.4" width="0.2032" layer="21" curve="-138.030204" cap="flat"/>
<wire x1="3.65" y1="1.4" x2="3.65" y2="-1.4" width="0.2032" layer="51" curve="-41.969796" cap="flat"/>
<wire x1="-3.65" y1="-1.4" x2="3.65" y2="-1.4" width="0.2032" layer="21" curve="138.030204" cap="flat"/>
<wire x1="-3.65" y1="-1.4" x2="-3.65" y2="1.4" width="0.2032" layer="51" curve="-41.969796" cap="flat"/>
<smd name="+" x="3.4925" y="0" dx="3.5" dy="2.5" layer="1"/>
<smd name="-" x="-3.4925" y="0" dx="3.5" dy="2.5" layer="1"/>
<text x="-4.7625" y="5.08" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-4.7625" y="-5.715" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C140CLH-1010">
<description>&lt;b&gt;SMD POLCAP&lt;/b&gt;&lt;p&gt;
Long life base plate, High temperature 140 CLH&lt;p&gt;
http://www.bccomponents.com/</description>
<wire x1="-5.8" y1="5.15" x2="4.35" y2="5.15" width="0.2032" layer="21"/>
<wire x1="5.8" y1="3.7" x2="5.8" y2="1.45" width="0.2032" layer="21"/>
<wire x1="5.8" y1="1.45" x2="5.8" y2="-1.45" width="0.2032" layer="51"/>
<wire x1="5.8" y1="-1.45" x2="5.8" y2="-3.7" width="0.2032" layer="21"/>
<wire x1="4.35" y1="-5.15" x2="-5.8" y2="-5.15" width="0.2032" layer="21"/>
<wire x1="-5.8" y1="-5.15" x2="-5.8" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="-5.8" y1="-1.45" x2="-5.8" y2="1.45" width="0.2032" layer="51"/>
<wire x1="-5.8" y1="1.45" x2="-5.8" y2="5.15" width="0.2032" layer="21"/>
<wire x1="5.8" y1="3.7" x2="4.35" y2="5.15" width="0.2032" layer="21"/>
<wire x1="5.8" y1="-3.7" x2="4.35" y2="-5.15" width="0.2032" layer="21"/>
<wire x1="-4.7" y1="-1.4" x2="4.7" y2="-1.4" width="0.2032" layer="21" curve="146.825323" cap="flat"/>
<wire x1="4.7" y1="-1.4" x2="4.7" y2="1.4" width="0.2032" layer="51" curve="33.174677" cap="flat"/>
<wire x1="-4.7" y1="1.4" x2="4.7" y2="1.4" width="0.2032" layer="21" curve="-146.825323" cap="flat"/>
<wire x1="-4.7" y1="1.4" x2="-4.7" y2="-1.4" width="0.2032" layer="51" curve="33.174677" cap="flat"/>
<smd name="+" x="4.1275" y="0" dx="4.3" dy="2.5" layer="1"/>
<smd name="-" x="-4.1275" y="0" dx="4.3" dy="2.5" layer="1"/>
<text x="-5.715" y="6.0325" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-5.715" y="-6.6675" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C140CLH-1014">
<description>&lt;b&gt;SMD POLCAP&lt;/b&gt;&lt;p&gt;
Long life base plate, High temperature 140 CLH&lt;p&gt;
http://www.bccomponents.com/</description>
<wire x1="-5.8" y1="5.15" x2="4.35" y2="5.15" width="0.2032" layer="21"/>
<wire x1="5.8" y1="3.7" x2="5.8" y2="1.45" width="0.2032" layer="21"/>
<wire x1="5.8" y1="1.45" x2="5.8" y2="-1.45" width="0.2032" layer="51"/>
<wire x1="5.8" y1="-1.45" x2="5.8" y2="-3.7" width="0.2032" layer="21"/>
<wire x1="4.35" y1="-5.15" x2="-5.8" y2="-5.15" width="0.2032" layer="21"/>
<wire x1="-5.8" y1="-5.15" x2="-5.8" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="-5.8" y1="-1.45" x2="-5.8" y2="1.45" width="0.2032" layer="51"/>
<wire x1="-5.8" y1="1.45" x2="-5.8" y2="5.15" width="0.2032" layer="21"/>
<wire x1="5.8" y1="3.7" x2="4.35" y2="5.15" width="0.2032" layer="21"/>
<wire x1="5.8" y1="-3.7" x2="4.35" y2="-5.15" width="0.2032" layer="21"/>
<wire x1="-4.7" y1="-1.4" x2="4.7" y2="-1.4" width="0.2032" layer="21" curve="146.825323" cap="flat"/>
<wire x1="4.7" y1="-1.4" x2="4.7" y2="1.4" width="0.2032" layer="51" curve="33.174677" cap="flat"/>
<wire x1="-4.7" y1="1.4" x2="4.7" y2="1.4" width="0.2032" layer="21" curve="-146.825323" cap="flat"/>
<wire x1="-4.7" y1="1.4" x2="-4.7" y2="-1.4" width="0.2032" layer="51" curve="33.174677" cap="flat"/>
<smd name="+" x="4.1275" y="0" dx="4.3" dy="2.5" layer="1"/>
<smd name="-" x="-4.1275" y="0" dx="4.3" dy="2.5" layer="1"/>
<text x="-5.715" y="6.0325" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-5.715" y="-6.6675" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150CLZ-0810">
<description>&lt;b&gt;SMD POLCAP&lt;/b&gt;&lt;p&gt;
Long life base plate, very low impedance 150 CLZ&lt;p&gt;
http://www.bccomponents.com/</description>
<wire x1="-4.85" y1="4.2" x2="3.4" y2="4.2" width="0.2032" layer="21"/>
<wire x1="4.85" y1="2.75" x2="4.85" y2="1.45" width="0.2032" layer="21"/>
<wire x1="4.85" y1="1.45" x2="4.85" y2="-1.45" width="0.2032" layer="51"/>
<wire x1="4.85" y1="-1.45" x2="4.85" y2="-2.75" width="0.2032" layer="21"/>
<wire x1="3.4" y1="-4.2" x2="-4.85" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="-4.85" y1="-4.2" x2="-4.85" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="-4.85" y1="-1.45" x2="-4.85" y2="1.45" width="0.2032" layer="51"/>
<wire x1="-4.85" y1="1.45" x2="-4.85" y2="4.2" width="0.2032" layer="21"/>
<wire x1="4.85" y1="2.75" x2="3.4" y2="4.2" width="0.2032" layer="21"/>
<wire x1="4.85" y1="-2.75" x2="3.4" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="-3.65" y1="1.4" x2="3.65" y2="1.4" width="0.2032" layer="21" curve="-138.030204" cap="flat"/>
<wire x1="3.65" y1="1.4" x2="3.65" y2="-1.4" width="0.2032" layer="51" curve="-41.969796" cap="flat"/>
<wire x1="-3.65" y1="-1.4" x2="3.65" y2="-1.4" width="0.2032" layer="21" curve="138.030204" cap="flat"/>
<wire x1="-3.65" y1="-1.4" x2="-3.65" y2="1.4" width="0.2032" layer="51" curve="-41.969796" cap="flat"/>
<smd name="+" x="3.4925" y="0" dx="3.5" dy="2.5" layer="1"/>
<smd name="-" x="-3.4925" y="0" dx="3.5" dy="2.5" layer="1"/>
<text x="-4.7625" y="5.08" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-4.7625" y="-5.715" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150CLZ-1010">
<description>&lt;b&gt;SMD POLCAP&lt;/b&gt;&lt;p&gt;
Long life base plate, very low impedance 150 CLZ&lt;p&gt;
http://www.bccomponents.com/</description>
<wire x1="-5.8" y1="5.15" x2="4.35" y2="5.15" width="0.2032" layer="21"/>
<wire x1="5.8" y1="3.7" x2="5.8" y2="1.45" width="0.2032" layer="21"/>
<wire x1="5.8" y1="1.45" x2="5.8" y2="-1.45" width="0.2032" layer="51"/>
<wire x1="5.8" y1="-1.45" x2="5.8" y2="-3.7" width="0.2032" layer="21"/>
<wire x1="4.35" y1="-5.15" x2="-5.8" y2="-5.15" width="0.2032" layer="21"/>
<wire x1="-5.8" y1="-5.15" x2="-5.8" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="-5.8" y1="-1.45" x2="-5.8" y2="1.45" width="0.2032" layer="51"/>
<wire x1="-5.8" y1="1.45" x2="-5.8" y2="5.15" width="0.2032" layer="21"/>
<wire x1="5.8" y1="3.7" x2="4.35" y2="5.15" width="0.2032" layer="21"/>
<wire x1="5.8" y1="-3.7" x2="4.35" y2="-5.15" width="0.2032" layer="21"/>
<wire x1="-4.7" y1="-1.4" x2="4.7" y2="-1.4" width="0.2032" layer="21" curve="146.825323" cap="flat"/>
<wire x1="4.7" y1="-1.4" x2="4.7" y2="1.4" width="0.2032" layer="51" curve="33.174677" cap="flat"/>
<wire x1="-4.7" y1="1.4" x2="4.7" y2="1.4" width="0.2032" layer="21" curve="-146.825323" cap="flat"/>
<wire x1="-4.7" y1="1.4" x2="-4.7" y2="-1.4" width="0.2032" layer="51" curve="33.174677" cap="flat"/>
<smd name="+" x="4.1275" y="0" dx="4.3" dy="2.5" layer="1"/>
<smd name="-" x="-4.1275" y="0" dx="4.3" dy="2.5" layer="1"/>
<text x="-5.715" y="6.0325" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-5.715" y="-6.6675" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150CLZ-1014">
<description>&lt;b&gt;SMD POLCAP&lt;/b&gt;&lt;p&gt;
Long life base plate, very low impedance 150 CLZ&lt;p&gt;
http://www.bccomponents.com/</description>
<wire x1="-5.8" y1="5.15" x2="4.35" y2="5.15" width="0.2032" layer="21"/>
<wire x1="5.8" y1="3.7" x2="5.8" y2="1.45" width="0.2032" layer="21"/>
<wire x1="5.8" y1="1.45" x2="5.8" y2="-1.45" width="0.2032" layer="51"/>
<wire x1="5.8" y1="-1.45" x2="5.8" y2="-3.7" width="0.2032" layer="21"/>
<wire x1="4.35" y1="-5.15" x2="-5.8" y2="-5.15" width="0.2032" layer="21"/>
<wire x1="-5.8" y1="-5.15" x2="-5.8" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="-5.8" y1="-1.45" x2="-5.8" y2="1.45" width="0.2032" layer="51"/>
<wire x1="-5.8" y1="1.45" x2="-5.8" y2="5.15" width="0.2032" layer="21"/>
<wire x1="5.8" y1="3.7" x2="4.35" y2="5.15" width="0.2032" layer="21"/>
<wire x1="5.8" y1="-3.7" x2="4.35" y2="-5.15" width="0.2032" layer="21"/>
<wire x1="-4.7" y1="-1.4" x2="4.7" y2="-1.4" width="0.2032" layer="21" curve="146.825323" cap="flat"/>
<wire x1="4.7" y1="-1.4" x2="4.7" y2="1.4" width="0.2032" layer="51" curve="33.174677" cap="flat"/>
<wire x1="-4.7" y1="1.4" x2="4.7" y2="1.4" width="0.2032" layer="21" curve="-146.825323" cap="flat"/>
<wire x1="-4.7" y1="-1.4" x2="-4.7" y2="1.4" width="0.2032" layer="51" curve="-33.174677" cap="flat"/>
<smd name="+" x="4.1275" y="0" dx="4.3" dy="2.5" layer="1"/>
<smd name="-" x="-4.1275" y="0" dx="4.3" dy="2.5" layer="1"/>
<text x="-5.715" y="6.0325" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-5.715" y="-6.6675" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C153CLV-0405">
<description>&lt;b&gt;SMD POLCAP&lt;/b&gt;&lt;p&gt;
SMD (Chip) Long Life Vertical 153 CLV&lt;p&gt;
http://www.bccomponents.com/</description>
<wire x1="1.25" y1="-2.15" x2="-2.15" y2="-2.15" width="0.2032" layer="21"/>
<wire x1="-2.15" y1="-2.15" x2="-2.15" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-2.15" y1="-1.1" x2="-2.15" y2="1.1" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="1.1" x2="-2.15" y2="2.15" width="0.2032" layer="21"/>
<wire x1="-2.15" y1="2.15" x2="1.25" y2="2.15" width="0.2032" layer="21"/>
<wire x1="2.15" y1="1.25" x2="2.15" y2="1.1" width="0.2032" layer="21"/>
<wire x1="2.15" y1="1.1" x2="2.15" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="2.15" y1="-1.05" x2="2.15" y2="-1.25" width="0.2032" layer="21"/>
<wire x1="2.15" y1="-1.25" x2="1.25" y2="-2.15" width="0.2032" layer="21"/>
<wire x1="1.25" y1="2.15" x2="2.15" y2="1.25" width="0.2032" layer="21"/>
<wire x1="-1.65" y1="-0.95" x2="1.65" y2="-0.95" width="0.2032" layer="21" curve="120.136976" cap="flat"/>
<wire x1="-1.65" y1="-0.95" x2="-1.65" y2="0.95" width="0.2032" layer="51" curve="-59.863024" cap="flat"/>
<wire x1="-1.65" y1="0.95" x2="1.65" y2="0.95" width="0.2032" layer="21" curve="-120.136976" cap="flat"/>
<wire x1="1.65" y1="-0.95" x2="1.65" y2="0.95" width="0.2032" layer="51" curve="59.863024" cap="flat"/>
<smd name="+" x="1.905" y="0" dx="2.6" dy="1.6" layer="1"/>
<smd name="-" x="-1.905" y="0" dx="2.6" dy="1.6" layer="1"/>
<text x="-2.2225" y="2.8575" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.2225" y="-3.4925" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C153CLV-0505">
<description>&lt;b&gt;SMD POLCAP&lt;/b&gt;&lt;p&gt;
SMD (Chip) Long Life Vertical 153 CLV&lt;p&gt;
http://www.bccomponents.com/</description>
<wire x1="1.75" y1="-2.65" x2="-2.65" y2="-2.65" width="0.2032" layer="21"/>
<wire x1="-2.65" y1="-2.65" x2="-2.65" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-2.65" y1="-1.1" x2="-2.65" y2="1.1" width="0.2032" layer="51"/>
<wire x1="-2.65" y1="1.1" x2="-2.65" y2="2.65" width="0.2032" layer="21"/>
<wire x1="-2.65" y1="2.65" x2="1.75" y2="2.65" width="0.2032" layer="21"/>
<wire x1="2.65" y1="1.75" x2="2.65" y2="1.1" width="0.2032" layer="21"/>
<wire x1="2.65" y1="1.1" x2="2.65" y2="-1.1" width="0.2032" layer="51"/>
<wire x1="2.65" y1="-1.1" x2="2.65" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="2.65" y1="-1.75" x2="1.75" y2="-2.65" width="0.2032" layer="21"/>
<wire x1="1.75" y1="2.65" x2="2.65" y2="1.75" width="0.2032" layer="21"/>
<wire x1="-2.2" y1="-0.95" x2="2.2" y2="-0.95" width="0.2032" layer="21" curve="133.28887" cap="flat"/>
<wire x1="-2.2" y1="-0.95" x2="-2.2" y2="0.95" width="0.2032" layer="51" curve="-46.71113" cap="flat"/>
<wire x1="-2.2" y1="0.95" x2="2.2" y2="0.95" width="0.2032" layer="21" curve="-133.28887" cap="flat"/>
<wire x1="2.2" y1="-0.95" x2="2.2" y2="0.95" width="0.2032" layer="51" curve="46.71113" cap="flat"/>
<smd name="+" x="2.2225" y="0" dx="3" dy="1.6" layer="1"/>
<smd name="-" x="-2.2225" y="0" dx="3" dy="1.6" layer="1"/>
<text x="-2.54" y="3.175" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C153CLV-0605">
<description>&lt;b&gt;SMD POLCAP&lt;/b&gt;&lt;p&gt;
SMD (Chip) Long Life Vertical 153 CLV&lt;p&gt;
http://www.bccomponents.com/</description>
<wire x1="2.4" y1="-3.3" x2="-3.3" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="-3.3" x2="-3.3" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="-1.1" x2="-3.3" y2="1.1" width="0.2032" layer="51"/>
<wire x1="-3.3" y1="1.1" x2="-3.3" y2="3.3" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="3.3" x2="2.4" y2="3.3" width="0.2032" layer="21"/>
<wire x1="3.3" y1="2.4" x2="3.3" y2="1.1" width="0.2032" layer="21"/>
<wire x1="3.3" y1="1.1" x2="3.3" y2="-1.1" width="0.2032" layer="51"/>
<wire x1="3.3" y1="-1.1" x2="3.3" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="3.3" y1="-2.4" x2="2.4" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="2.4" y1="3.3" x2="3.3" y2="2.4" width="0.2032" layer="21"/>
<wire x1="-2.95" y1="-0.9" x2="2.95" y2="-0.95" width="0.2032" layer="21" curve="145.181395" cap="flat"/>
<wire x1="-2.95" y1="-0.9" x2="-2.95" y2="0.95" width="0.2032" layer="51" curve="-34.818605" cap="flat"/>
<wire x1="-2.95" y1="0.95" x2="2.95" y2="0.9" width="0.2032" layer="21" curve="-145.181395" cap="flat"/>
<wire x1="2.95" y1="-0.95" x2="2.95" y2="0.9" width="0.2032" layer="51" curve="34.818605" cap="flat"/>
<smd name="+" x="2.54" y="0" dx="3.5" dy="1.6" layer="1"/>
<smd name="-" x="-2.54" y="0" dx="3.5" dy="1.6" layer="1"/>
<text x="-3.175" y="3.81" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.175" y="-4.445" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C153CLV-0807">
<description>&lt;b&gt;SMD POLCAP&lt;/b&gt;&lt;p&gt;
SMD (Chip) Long Life Vertical 153 CLV&lt;p&gt;
http://www.bccomponents.com/</description>
<wire x1="3.3" y1="-4.2" x2="-4.2" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="-4.2" x2="-4.2" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="-1.1" x2="-4.2" y2="1.1" width="0.2032" layer="51"/>
<wire x1="-4.2" y1="1.1" x2="-4.2" y2="4.2" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="4.2" x2="3.3" y2="4.2" width="0.2032" layer="21"/>
<wire x1="4.2" y1="3.3" x2="4.2" y2="1.1" width="0.2032" layer="21"/>
<wire x1="4.2" y1="1.1" x2="4.2" y2="-1.1" width="0.2032" layer="51"/>
<wire x1="4.2" y1="-1.1" x2="4.2" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="4.2" y1="-3.3" x2="3.3" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="3.3" y1="4.2" x2="4.2" y2="3.3" width="0.2032" layer="21"/>
<wire x1="-3.75" y1="-1.05" x2="3.75" y2="-1.05" width="0.2032" layer="21" curve="148.008335" cap="flat"/>
<wire x1="-3.75" y1="-1.05" x2="-3.75" y2="1.05" width="0.2032" layer="51" curve="-31.284493" cap="flat"/>
<wire x1="-3.75" y1="1.05" x2="3.75" y2="1.05" width="0.2032" layer="21" curve="-148.008335" cap="flat"/>
<wire x1="3.75" y1="1.05" x2="3.75" y2="-1.05" width="0.2032" layer="51" curve="-31.284493" cap="flat"/>
<smd name="+" x="3.175" y="0" dx="4" dy="1.6" layer="1"/>
<smd name="-" x="-2.8575" y="0" dx="4" dy="1.6" layer="1"/>
<text x="-4.1275" y="4.7625" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-4.1275" y="-5.3975" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C153CLV-0810">
<description>&lt;b&gt;SMD POLCAP&lt;/b&gt;&lt;p&gt;
SMD (Chip) Long Life Vertical 153 CLV&lt;p&gt;
http://www.bccomponents.com/</description>
<wire x1="3.3" y1="-4.2" x2="-4.2" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="-4.2" x2="-4.2" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="-1.5" x2="-4.2" y2="1.5" width="0.2032" layer="51"/>
<wire x1="-4.2" y1="1.5" x2="-4.2" y2="4.2" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="4.2" x2="3.3" y2="4.2" width="0.2032" layer="21"/>
<wire x1="4.2" y1="3.3" x2="4.2" y2="1.5" width="0.2032" layer="21"/>
<wire x1="4.2" y1="1.5" x2="4.2" y2="-1.5" width="0.2032" layer="51"/>
<wire x1="4.2" y1="-1.5" x2="4.2" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="4.2" y1="-3.3" x2="3.3" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="3.3" y1="4.2" x2="4.2" y2="3.3" width="0.2032" layer="21"/>
<wire x1="-3.65" y1="-1.4" x2="3.65" y2="-1.4" width="0.2032" layer="21" curve="138.030204" cap="flat"/>
<wire x1="-3.65" y1="-1.4" x2="-3.65" y2="1.4" width="0.2032" layer="51" curve="-41.969796" cap="flat"/>
<wire x1="-3.65" y1="1.4" x2="3.65" y2="1.4" width="0.2032" layer="21" curve="-138.030204" cap="flat"/>
<wire x1="3.65" y1="-1.4" x2="3.65" y2="1.4" width="0.2032" layer="51" curve="41.969796" cap="flat"/>
<smd name="+" x="3.175" y="0" dx="3.5" dy="2.5" layer="1"/>
<smd name="-" x="-3.175" y="0" dx="3.5" dy="2.5" layer="1"/>
<text x="-4.1275" y="4.7625" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-4.1275" y="-5.715" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C153CLV-1010">
<description>&lt;b&gt;SMD POLCAP&lt;/b&gt;&lt;p&gt;
SMD (Chip) Long Life Vertical 153 CLV&lt;p&gt;
http://www.bccomponents.com/</description>
<wire x1="4.3" y1="-5.2" x2="-5.2" y2="-5.2" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="-5.2" x2="-5.2" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="-1.5" x2="-5.2" y2="1.5" width="0.2032" layer="51"/>
<wire x1="-5.2" y1="1.5" x2="-5.2" y2="5.2" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="5.2" x2="4.3" y2="5.2" width="0.2032" layer="21"/>
<wire x1="5.2" y1="4.3" x2="5.2" y2="1.5" width="0.2032" layer="21"/>
<wire x1="5.2" y1="1.5" x2="5.2" y2="-1.5" width="0.2032" layer="51"/>
<wire x1="5.2" y1="-1.5" x2="5.2" y2="-4.3" width="0.2032" layer="21"/>
<wire x1="5.2" y1="-4.3" x2="4.3" y2="-5.2" width="0.2032" layer="21"/>
<wire x1="4.3" y1="5.2" x2="5.2" y2="4.3" width="0.2032" layer="21"/>
<wire x1="4.65" y1="-1.55" x2="4.65" y2="1.55" width="0.2032" layer="51" curve="36.869898" cap="flat"/>
<wire x1="-4.65" y1="-1.55" x2="-4.65" y2="1.55" width="0.2032" layer="51" curve="-36.869898" cap="flat"/>
<wire x1="-4.65" y1="-1.55" x2="4.65" y2="-1.55" width="0.2032" layer="21" curve="143.130102" cap="flat"/>
<wire x1="-4.65" y1="1.55" x2="4.65" y2="1.55" width="0.2032" layer="21" curve="-143.130102" cap="flat"/>
<smd name="+" x="4.1275" y="0" dx="4" dy="2.5" layer="1"/>
<smd name="-" x="-4.1275" y="0" dx="4" dy="2.5" layer="1"/>
<text x="-5.08" y="6.0325" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-5.08" y="-6.985" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C153CLV-1012">
<description>&lt;b&gt;SMD POLCAP&lt;/b&gt;&lt;p&gt;
SMD (Chip) Long Life Vertical 153 CLV&lt;p&gt;
http://www.bccomponents.com/</description>
<wire x1="4.3" y1="-5.2" x2="-5.2" y2="-5.2" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="-5.2" x2="-5.2" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="-1.5" x2="-5.2" y2="1.5" width="0.2032" layer="51"/>
<wire x1="-5.2" y1="1.5" x2="-5.2" y2="5.2" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="5.2" x2="4.3" y2="5.2" width="0.2032" layer="21"/>
<wire x1="5.2" y1="4.3" x2="5.2" y2="1.5" width="0.2032" layer="21"/>
<wire x1="5.2" y1="1.5" x2="5.2" y2="-1.5" width="0.2032" layer="51"/>
<wire x1="5.2" y1="-1.5" x2="5.2" y2="-4.3" width="0.2032" layer="21"/>
<wire x1="5.2" y1="-4.3" x2="4.3" y2="-5.2" width="0.2032" layer="21"/>
<wire x1="4.3" y1="5.2" x2="5.2" y2="4.3" width="0.2032" layer="21"/>
<wire x1="4.65" y1="-1.55" x2="4.65" y2="1.55" width="0.2032" layer="51" curve="36.869898" cap="flat"/>
<wire x1="-4.65" y1="-1.55" x2="-4.65" y2="1.55" width="0.2032" layer="51" curve="-36.869898" cap="flat"/>
<wire x1="-4.65" y1="-1.55" x2="4.65" y2="-1.55" width="0.2032" layer="21" curve="143.130102" cap="flat"/>
<wire x1="-4.65" y1="1.55" x2="4.65" y2="1.55" width="0.2032" layer="21" curve="-143.130102" cap="flat"/>
<smd name="+" x="4.1275" y="0" dx="4.3" dy="2.5" layer="1"/>
<smd name="-" x="-4.1275" y="0" dx="4.3" dy="2.5" layer="1"/>
<text x="-5.08" y="6.0325" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-5.08" y="-6.985" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C153CLV-1014">
<description>&lt;b&gt;SMD POLCAP&lt;/b&gt;&lt;p&gt;
SMD (Chip) Long Life Vertical 153 CLV&lt;p&gt;
http://www.bccomponents.com/</description>
<wire x1="4.3" y1="-5.2" x2="-5.2" y2="-5.2" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="-5.2" x2="-5.2" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="-1.5" x2="-5.2" y2="1.5" width="0.2032" layer="51"/>
<wire x1="-5.2" y1="1.5" x2="-5.2" y2="5.2" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="5.2" x2="4.3" y2="5.2" width="0.2032" layer="21"/>
<wire x1="5.2" y1="4.3" x2="5.2" y2="1.5" width="0.2032" layer="21"/>
<wire x1="5.2" y1="1.5" x2="5.2" y2="-1.5" width="0.2032" layer="51"/>
<wire x1="5.2" y1="-1.5" x2="5.2" y2="-4.3" width="0.2032" layer="21"/>
<wire x1="5.2" y1="-4.3" x2="4.3" y2="-5.2" width="0.2032" layer="21"/>
<wire x1="4.3" y1="5.2" x2="5.2" y2="4.3" width="0.2032" layer="21"/>
<wire x1="4.65" y1="-1.55" x2="4.65" y2="1.55" width="0.2032" layer="51" curve="36.869898" cap="flat"/>
<wire x1="-4.65" y1="-1.55" x2="-4.65" y2="1.55" width="0.2032" layer="51" curve="-36.869898" cap="flat"/>
<wire x1="-4.65" y1="-1.55" x2="4.65" y2="-1.55" width="0.2032" layer="21" curve="143.130102" cap="flat"/>
<wire x1="-4.65" y1="1.55" x2="4.65" y2="1.55" width="0.2032" layer="21" curve="-143.130102" cap="flat"/>
<smd name="+" x="4.1275" y="0" dx="4.3" dy="2.5" layer="1"/>
<smd name="-" x="-4.1275" y="0" dx="4.3" dy="2.5" layer="1"/>
<text x="-5.08" y="6.0325" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-5.08" y="-6.985" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C175TMP-0808">
<description>&lt;b&gt;SMD POLCAP&lt;/b&gt;&lt;p&gt;
High Temperature solid electrolytic SMD 175 TMP&lt;p&gt;
http://www.bccomponents.com/</description>
<wire x1="3.2" y1="-4.1" x2="-4.1" y2="-4.1" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="-4.1" x2="-4.1" y2="-1.55" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="-1.55" x2="-4.1" y2="1.55" width="0.2032" layer="51"/>
<wire x1="-4.1" y1="1.55" x2="-4.1" y2="4.1" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="4.1" x2="3.2" y2="4.1" width="0.2032" layer="21"/>
<wire x1="4.1" y1="3.2" x2="4.1" y2="1.5" width="0.2032" layer="21"/>
<wire x1="4.1" y1="1.5" x2="4.1" y2="-1.55" width="0.2032" layer="51"/>
<wire x1="4.1" y1="-1.55" x2="4.1" y2="-3.2" width="0.2032" layer="21"/>
<wire x1="4.1" y1="-3.2" x2="3.2" y2="-4.1" width="0.2032" layer="21"/>
<wire x1="3.2" y1="4.1" x2="4.1" y2="3.2" width="0.2032" layer="21"/>
<wire x1="-3.6" y1="-1.45" x2="3.6" y2="-1.45" width="0.2032" layer="21" curve="136.123039" cap="flat"/>
<wire x1="-3.6" y1="-1.45" x2="-3.6" y2="1.45" width="0.2032" layer="51" curve="-43.876961" cap="flat"/>
<wire x1="-3.6" y1="1.45" x2="3.6" y2="1.45" width="0.2032" layer="21" curve="-136.123039" cap="flat"/>
<wire x1="3.6" y1="1.45" x2="3.6" y2="-1.45" width="0.2032" layer="51" curve="-43.876961" cap="flat"/>
<smd name="+" x="3.4925" y="0" dx="3" dy="2.5" layer="1"/>
<smd name="-" x="-3.4925" y="0" dx="3" dy="2.5" layer="1"/>
<text x="-4.1275" y="4.7625" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-4.1275" y="-5.3975" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C175TMP-0810">
<description>&lt;b&gt;SMD POLCAP&lt;/b&gt;&lt;p&gt;
High Temperature solid electrolytic SMD 175 TMP&lt;p&gt;
http://www.bccomponents.com/</description>
<wire x1="3.2" y1="-4.1" x2="-4.1" y2="-4.1" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="-4.1" x2="-4.1" y2="-1.55" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="-1.55" x2="-4.1" y2="1.55" width="0.2032" layer="51"/>
<wire x1="-4.1" y1="1.55" x2="-4.1" y2="4.1" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="4.1" x2="3.2" y2="4.1" width="0.2032" layer="21"/>
<wire x1="4.1" y1="3.2" x2="4.1" y2="1.5" width="0.2032" layer="21"/>
<wire x1="4.1" y1="1.5" x2="4.1" y2="-1.55" width="0.2032" layer="51"/>
<wire x1="4.1" y1="-1.55" x2="4.1" y2="-3.2" width="0.2032" layer="21"/>
<wire x1="4.1" y1="-3.2" x2="3.2" y2="-4.1" width="0.2032" layer="21"/>
<wire x1="3.2" y1="4.1" x2="4.1" y2="3.2" width="0.2032" layer="21"/>
<wire x1="-3.6" y1="-1.45" x2="3.6" y2="-1.45" width="0.2032" layer="21" curve="136.123039" cap="flat"/>
<wire x1="-3.6" y1="-1.45" x2="-3.6" y2="1.45" width="0.2032" layer="51" curve="-43.876961" cap="flat"/>
<wire x1="-3.6" y1="1.45" x2="3.6" y2="1.45" width="0.2032" layer="21" curve="-136.123039" cap="flat"/>
<wire x1="3.6" y1="-1.45" x2="3.6" y2="1.45" width="0.2032" layer="51" curve="43.876961" cap="flat"/>
<smd name="+" x="3.4925" y="0" dx="3" dy="2.5" layer="1"/>
<smd name="-" x="-3.4925" y="0" dx="3" dy="2.5" layer="1"/>
<text x="-4.1275" y="4.7625" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-4.1275" y="-5.3975" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="CSV-H">
<description>&lt;b&gt;SMD ELECTROLYTIC&lt;/b&gt; - Panasonic H&lt;p&gt; 
12.5 mm dia, grid 0.0125 inch</description>
<wire x1="6.35" y1="6.5088" x2="6.35" y2="1.27" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-1.27" x2="6.35" y2="-6.5088" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-6.5088" x2="-3.81" y2="-6.5087" width="0.2032" layer="21"/>
<wire x1="-3.81" y1="-6.5087" x2="-6.35" y2="-3.9687" width="0.2032" layer="21"/>
<wire x1="-6.35" y1="-3.9687" x2="-6.35" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.35" y1="6.5088" x2="-3.81" y2="6.5087" width="0.2032" layer="21"/>
<wire x1="-3.81" y1="6.5087" x2="-6.35" y2="3.9687" width="0.2032" layer="21"/>
<wire x1="-6.35" y1="3.9687" x2="-6.35" y2="1.27" width="0.2032" layer="21"/>
<wire x1="2.6976" y1="5.5" x2="2.6976" y2="-5.5" width="0.1016" layer="51"/>
<circle x="0" y="0" radius="6.1846" width="0.1016" layer="51"/>
<smd name="+" x="-5.3975" y="0" dx="4.318" dy="1.9304" layer="1"/>
<smd name="-" x="5.3975" y="0" dx="4.318" dy="1.9304" layer="1"/>
<text x="6.715" y="2.54" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="6.715" y="-3.4925" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-7.2144" y1="-0.4763" x2="-6.1031" y2="0.4763" layer="51"/>
<rectangle x1="6.08" y1="-0.4763" x2="7.1913" y2="0.4763" layer="51"/>
</package>
<package name="CSV-J">
<description>&lt;b&gt;SMD ELECTROLYTIC&lt;/b&gt; - Panasonic J&lt;p&gt; 
16 mm dia, grid 0.0125 inch</description>
<wire x1="8.5725" y1="8.4138" x2="8.5725" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.5725" y1="-1.27" x2="8.5725" y2="-8.4138" width="0.2032" layer="21"/>
<wire x1="8.5725" y1="-8.4138" x2="-6.0325" y2="-8.4137" width="0.2032" layer="21"/>
<wire x1="-6.0325" y1="-8.4137" x2="-8.5725" y2="-5.8737" width="0.2032" layer="21"/>
<wire x1="-8.5725" y1="-5.8737" x2="-8.5725" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.5725" y1="8.4138" x2="-6.0325" y2="8.4137" width="0.2032" layer="21"/>
<wire x1="-6.0325" y1="8.4137" x2="-8.5725" y2="5.8737" width="0.2032" layer="21"/>
<wire x1="-8.5725" y1="5.8737" x2="-8.5725" y2="1.27" width="0.2032" layer="21"/>
<wire x1="2.6976" y1="7.5638" x2="2.6976" y2="-7.5638" width="0.1016" layer="51"/>
<circle x="0" y="0" radius="8.1394" width="0.1016" layer="51"/>
<smd name="+" x="-7.62" y="0" dx="4.318" dy="1.9304" layer="1"/>
<smd name="-" x="7.62" y="0" dx="4.318" dy="1.9304" layer="1"/>
<text x="8.9375" y="2.54" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="8.9375" y="-2.8575" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-9.2782" y1="-0.4763" x2="-8.1669" y2="0.4763" layer="51"/>
<rectangle x1="8.1437" y1="-0.4763" x2="9.255" y2="0.4763" layer="51"/>
</package>
<package name="CSV-K">
<description>&lt;b&gt;SMD ELECTROLYTIC&lt;/b&gt; - Panasonic K&lt;p&gt; 
18 mm dia, grid 0.0125 inch</description>
<wire x1="9.525" y1="9.3663" x2="9.525" y2="1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="9.525" y2="-9.3663" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-9.3663" x2="-6.985" y2="-9.3662" width="0.2032" layer="21"/>
<wire x1="-6.985" y1="-9.3662" x2="-9.525" y2="-6.8262" width="0.2032" layer="21"/>
<wire x1="-9.525" y1="-6.8262" x2="-9.525" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="9.3663" x2="-6.985" y2="9.3662" width="0.2032" layer="21"/>
<wire x1="-6.985" y1="9.3662" x2="-9.525" y2="6.8262" width="0.2032" layer="21"/>
<wire x1="-9.525" y1="6.8262" x2="-9.525" y2="1.27" width="0.2032" layer="21"/>
<wire x1="2.6976" y1="8.5163" x2="2.6976" y2="-8.5163" width="0.1016" layer="51"/>
<circle x="0" y="0" radius="8.9442" width="0.1016" layer="51"/>
<smd name="+" x="-8.5725" y="0" dx="4.318" dy="1.9304" layer="1"/>
<smd name="-" x="8.5725" y="0" dx="4.318" dy="1.9304" layer="1"/>
<text x="9.89" y="2.54" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="9.89" y="-3.81" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.0719" y1="-0.4763" x2="-8.9606" y2="0.4763" layer="51"/>
<rectangle x1="8.9374" y1="-0.4763" x2="10.0487" y2="0.4763" layer="51"/>
</package>
<package name="CGOLD-SD-H">
<description>&lt;b&gt;GOLD CAP&lt;/b&gt; - Panasonic&lt;p&gt;
SD Series, Horizontal mount</description>
<wire x1="-3.548" y1="0" x2="-2.532" y2="0" width="0.2032" layer="21"/>
<wire x1="-3.04" y1="-0.508" x2="-3.04" y2="0.508" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-0.9525" x2="3.746" y2="-0.95" width="0.2032" layer="51"/>
<wire x1="3.746" y1="-0.95" x2="4" y2="-1.204" width="0.2032" layer="51" curve="-90"/>
<wire x1="4" y1="-1.204" x2="4" y2="-1.25" width="0.2032" layer="51"/>
<wire x1="4" y1="-1.25" x2="5.5" y2="-1.25" width="0.2032" layer="51"/>
<wire x1="5.5" y1="1.25" x2="4" y2="1.25" width="0.2032" layer="51"/>
<wire x1="4" y1="1.25" x2="4" y2="1.204" width="0.2032" layer="51"/>
<wire x1="4" y1="1.204" x2="3.746" y2="0.95" width="0.2032" layer="51" curve="-90"/>
<wire x1="3.746" y1="0.95" x2="0.635" y2="0.9525" width="0.2032" layer="51"/>
<wire x1="0.635" y1="0.9525" x2="-0.635" y2="0.9525" width="0.2032" layer="51"/>
<wire x1="-5.5" y1="1.25" x2="5.5" y2="1.25" width="0.2032" layer="21" curve="-154.391468"/>
<wire x1="5.5" y1="-1.25" x2="-5.5" y2="-1.25" width="0.2032" layer="21" curve="-154.391468"/>
<wire x1="5.5" y1="1.25" x2="6.75" y2="1.25" width="0.2032" layer="21"/>
<wire x1="6.75" y1="1.25" x2="6.75" y2="-1.25" width="0.2032" layer="21"/>
<wire x1="6.75" y1="-1.25" x2="5.5" y2="-1.25" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="1.25" x2="4.5" y2="1.25" width="0.2032" layer="51" curve="-148.951778"/>
<wire x1="-4.5" y1="-1.25" x2="4.5" y2="-1.25" width="0.2032" layer="51" curve="148.951778"/>
<wire x1="2.452" y1="0" x2="3.468" y2="0" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="0.9525" x2="-0.8731" y2="0.7144" width="0.2032" layer="51" curve="-90"/>
<wire x1="-0.8731" y1="0.7144" x2="-0.9525" y2="0.7144" width="0.2032" layer="51"/>
<wire x1="-0.9525" y1="0.7144" x2="-0.9525" y2="-0.7144" width="0.2032" layer="51"/>
<wire x1="-0.9525" y1="-0.7144" x2="-0.8731" y2="-0.7144" width="0.2032" layer="51"/>
<wire x1="-0.8731" y1="-0.7144" x2="-0.635" y2="-0.9525" width="0.2032" layer="51" curve="-90"/>
<circle x="1.25" y="0" radius="0.3905" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="0.3905" width="0.2032" layer="51"/>
<pad name="+" x="-5" y="0" drill="1.016" diameter="2.159" shape="octagon"/>
<pad name="-" x="5" y="0" drill="1.016" diameter="2.159" shape="octagon"/>
<text x="6.0325" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.2225" y="-3.175" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="CGOLD-SD-V">
<description>&lt;b&gt;GOLD CAP&lt;/b&gt; - Panasonic&lt;p&gt;
SD Series, Vertical mount</description>
<wire x1="-3.508" y1="-2.1275" x2="-2.492" y2="-2.1275" width="0.2032" layer="21"/>
<wire x1="-3" y1="-2.6355" x2="-3" y2="-1.6195" width="0.2032" layer="21"/>
<wire x1="2.492" y1="-2.1275" x2="3.508" y2="-2.1275" width="0.2032" layer="21"/>
<wire x1="-2" y1="1.25" x2="-2" y2="4.365" width="0.2032" layer="21"/>
<wire x1="-2" y1="4.365" x2="-1.365" y2="5" width="0.2032" layer="21" curve="-90"/>
<wire x1="1.25" y1="5.5" x2="2" y2="4.75" width="0.2032" layer="21" curve="-90.061148"/>
<wire x1="2" y1="4.75" x2="2" y2="1.25" width="0.2032" layer="21"/>
<wire x1="2" y1="-1.25" x2="2" y2="-4.75" width="0.2032" layer="21"/>
<wire x1="2" y1="-4.75" x2="1.25" y2="-5.5" width="0.2032" layer="21" curve="-90.015281"/>
<wire x1="-1.365" y1="-5" x2="-2" y2="-4.365" width="0.2032" layer="21" curve="-90"/>
<wire x1="-2" y1="-4.365" x2="-2" y2="-1.25" width="0.2032" layer="21"/>
<wire x1="1.25" y1="5.5" x2="-0.1448" y2="5.5" width="0.2032" layer="21"/>
<wire x1="-0.1448" y1="5.5" x2="-0.3244" y2="5.4256" width="0.2032" layer="21" curve="44.999323"/>
<wire x1="-0.3244" y1="5.4256" x2="-0.6756" y2="5.0744" width="0.2032" layer="21"/>
<wire x1="-0.6756" y1="5.0744" x2="-0.8552" y2="5" width="0.2032" layer="21" curve="-44.999323"/>
<wire x1="-0.8552" y1="5" x2="-1.25" y2="5" width="0.2032" layer="21"/>
<wire x1="1.25" y1="-5.5" x2="-0.1448" y2="-5.5" width="0.2032" layer="21"/>
<wire x1="-0.1448" y1="-5.5" x2="-0.3244" y2="-5.4256" width="0.2032" layer="21" curve="-45.018018"/>
<wire x1="-0.3244" y1="-5.4256" x2="-0.6756" y2="-5.0744" width="0.2032" layer="21"/>
<wire x1="-0.6756" y1="-5.0744" x2="-0.8552" y2="-5" width="0.2032" layer="21" curve="45.018018"/>
<wire x1="-0.8552" y1="-5" x2="-1.5" y2="-5" width="0.2032" layer="21"/>
<wire x1="-0.25" y1="5.5" x2="-0.25" y2="-5.5" width="0.2032" layer="51"/>
<wire x1="-2" y1="1.25" x2="-2" y2="-1.25" width="0.2032" layer="51"/>
<wire x1="2" y1="1.25" x2="2" y2="-1.25" width="0.2032" layer="51"/>
<pad name="+" x="-2.5" y="0" drill="1.016" diameter="2.159" shape="octagon"/>
<pad name="-" x="2.5" y="0" drill="1.016" diameter="2.159" shape="octagon"/>
<text x="-2.54" y="1.5875" size="1.016" layer="25" ratio="18" rot="R90">&gt;NAME</text>
<text x="1.27" y="-2.2225" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.7" y1="-0.25" x2="-2.1" y2="0.25" layer="51"/>
<rectangle x1="2.1" y1="-1" x2="2.4" y2="0.9" layer="51"/>
<rectangle x1="-2.4" y1="-1" x2="-2.1" y2="0.9" layer="51"/>
<rectangle x1="2.1" y1="-0.25" x2="2.7" y2="0.25" layer="51"/>
</package>
<package name="CE-015X040">
<description>&lt;b&gt;POLCAP RADIAL&lt;/b&gt;&lt;p&gt;
1.5 mm lead spacing, 4 mm diameter, grid 0.0125 inch</description>
<wire x1="-1.905" y1="1.7463" x2="-1.27" y2="1.7463" width="0.254" layer="21"/>
<wire x1="-1.5875" y1="1.4288" x2="-1.5875" y2="2.0638" width="0.254" layer="21"/>
<wire x1="-0.1587" y1="0" x2="-0.6112" y2="0" width="0.2032" layer="51"/>
<wire x1="0.4764" y1="0" x2="0.9288" y2="0" width="0.2032" layer="51"/>
<wire x1="0.4" y1="0" x2="0.6" y2="0.6" width="0.254" layer="51" curve="-37.877434" cap="flat"/>
<wire x1="0.6" y1="-0.6" x2="0.4" y2="0" width="0.254" layer="51" curve="-37.380842" cap="flat"/>
<wire x1="-0.0587" y1="-0.6349" x2="-0.0587" y2="0.635" width="0.254" layer="51"/>
<circle x="0.1588" y="0" radius="1.8581" width="0.2032" layer="21"/>
<pad name="+" x="-0.635" y="0" drill="0.6096" diameter="1.016" shape="long" rot="R90"/>
<pad name="-" x="0.9525" y="0" drill="0.6096" diameter="1.016" shape="long" rot="R90"/>
<text x="2.2225" y="-1.9049" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="2.2225" y="0.635" size="1.016" layer="25" ratio="18">&gt;NAME</text>
</package>
<package name="CE-020X060">
<description>&lt;b&gt;POLCAP RADIAL&lt;/b&gt;&lt;p&gt;
2.0 mm lead spacing, 6 mm diameter, grid 0.0125 inch</description>
<wire x1="-1.4287" y1="2.0638" x2="-0.4762" y2="2.0638" width="0.254" layer="21"/>
<wire x1="-0.9525" y1="2.54" x2="-0.9525" y2="1.5875" width="0.254" layer="21"/>
<wire x1="-0.3175" y1="0" x2="-0.9525" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3176" y1="0" x2="0.9525" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3176" y1="0" x2="0.6351" y2="1.1112" width="0.3048" layer="51" curve="-37.874253" cap="flat"/>
<wire x1="0.6351" y1="-1.1113" x2="0.3176" y2="0" width="0.3048" layer="51" curve="-37.379812" cap="flat"/>
<wire x1="-0.3175" y1="-1.1112" x2="-0.3175" y2="1.1113" width="0.3048" layer="51"/>
<circle x="0" y="0" radius="3.1324" width="0.2032" layer="21"/>
<pad name="+" x="-0.9525" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="-" x="0.9525" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="3.81" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="3.81" y="-3.175" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="CE-025X075">
<description>&lt;b&gt;POLCAP RADIAL&lt;/b&gt;&lt;p&gt;
2.5 mm lead spacing, 7.5 mm diameter, grid 0.0125 inch</description>
<wire x1="-1.7462" y1="2.0638" x2="-0.7937" y2="2.0638" width="0.254" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="1.5875" width="0.254" layer="21"/>
<wire x1="-0.3175" y1="0" x2="-1.27" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3176" y1="0" x2="1.27" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3176" y1="0" x2="0.6351" y2="1.1112" width="0.3048" layer="51" curve="-37.874253" cap="flat"/>
<wire x1="0.6351" y1="-1.1113" x2="0.3176" y2="0" width="0.3048" layer="51" curve="-37.379812" cap="flat"/>
<wire x1="-0.3175" y1="-1.1112" x2="-0.3175" y2="1.1113" width="0.3048" layer="51"/>
<circle x="0" y="0" radius="3.9051" width="0.2032" layer="21"/>
<pad name="+" x="-1.27" y="0" drill="0.8128" diameter="1.778"/>
<pad name="-" x="1.27" y="0" drill="0.8128" diameter="1.778" shape="octagon"/>
<text x="3.81" y="1.905" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="3.81" y="-3.175" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="CE-025X085">
<description>&lt;b&gt;POLCAP RADIAL&lt;/b&gt;&lt;p&gt;
2.5 mm lead spacing, 8.5 mm diameter, grid 0.0125 inch</description>
<wire x1="-1.7462" y1="2.0638" x2="-0.7937" y2="2.0638" width="0.254" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="1.5875" width="0.254" layer="21"/>
<wire x1="-0.3175" y1="0" x2="-1.27" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3176" y1="0" x2="1.27" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3176" y1="0" x2="0.6351" y2="1.1112" width="0.3048" layer="51" curve="-37.874253" cap="flat"/>
<wire x1="0.6351" y1="-1.1113" x2="0.3176" y2="0" width="0.3048" layer="51" curve="-37.379812" cap="flat"/>
<wire x1="-0.3175" y1="-1.1112" x2="-0.3175" y2="1.1113" width="0.3048" layer="51"/>
<circle x="0" y="0" radius="4.6097" width="0.2032" layer="21"/>
<pad name="+" x="-1.27" y="0" drill="0.8128" diameter="1.778"/>
<pad name="-" x="1.27" y="0" drill="0.8128" diameter="1.778" shape="octagon"/>
<text x="4.445" y="2.8575" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="4.445" y="-3.81" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="CEH-035-080X115">
<description>&lt;b&gt;POLCAP RADIAL&lt;/b&gt; - Horizontal&lt;p&gt;
3.5 mm lead spacing, 8 mm diameter, 11.5 mm length, grid 0.00625 inch</description>
<wire x1="-3.905" y1="1.9525" x2="3.9051" y2="1.9525" width="0.2032" layer="21"/>
<wire x1="4.0638" y1="2.1112" x2="4.0638" y2="2.4288" width="0.2032" layer="21"/>
<wire x1="4.0638" y1="2.9052" x2="4.0638" y2="13.7313" width="0.2032" layer="21"/>
<wire x1="3.9051" y1="13.89" x2="-3.905" y2="13.89" width="0.2032" layer="21"/>
<wire x1="-4.0637" y1="13.7313" x2="-4.0637" y2="2.9051" width="0.2032" layer="21"/>
<wire x1="-4.0637" y1="2.4288" x2="-4.0637" y2="2.1112" width="0.2032" layer="21"/>
<wire x1="-4.0638" y1="13.7313" x2="-3.905" y2="13.89" width="0.2032" layer="21" curve="-90.108344" cap="flat"/>
<wire x1="4.0638" y1="13.7313" x2="3.9051" y2="13.8901" width="0.2032" layer="21" curve="89.747993" cap="flat"/>
<wire x1="-3.905" y1="1.9524" x2="-4.0637" y2="2.1112" width="0.2032" layer="21" curve="-89.963931" cap="flat"/>
<wire x1="3.9051" y1="1.9525" x2="4.0639" y2="2.1112" width="0.2032" layer="21" curve="90" cap="flat"/>
<wire x1="-4.0638" y1="2.4287" x2="-4.0638" y2="2.9049" width="0.2032" layer="21" curve="180" cap="flat"/>
<wire x1="4.0639" y1="2.4288" x2="4.0639" y2="2.905" width="0.2032" layer="21" curve="-180" cap="flat"/>
<wire x1="-3.8256" y1="2.6669" x2="3.8257" y2="2.6669" width="0.2032" layer="21"/>
<wire x1="-3.8337" y1="0" x2="-2.8811" y2="0" width="0.254" layer="21"/>
<wire x1="-3.3574" y1="0.4763" x2="-3.3574" y2="-0.4763" width="0.254" layer="21"/>
<pad name="+" x="-1.7463" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="-" x="1.7463" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="5.7625" y="1.905" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<text x="-4.81" y="1.905" size="1.016" layer="25" ratio="18" rot="R90">&gt;NAME</text>
</package>
<package name="CEH-035-080X115/R">
<description>&lt;b&gt;POLCAP RADIAL&lt;/b&gt; - Horizontal Rotated&lt;p&gt;
3.5 mm lead spacing, 8 mm diameter, 11.5 mm length, grid 0.00625 inch</description>
<wire x1="-3.905" y1="1.9525" x2="3.9051" y2="1.9525" width="0.2032" layer="21"/>
<wire x1="4.0638" y1="2.1112" x2="4.0638" y2="2.4288" width="0.2032" layer="21"/>
<wire x1="4.0638" y1="2.9052" x2="4.0638" y2="13.7313" width="0.2032" layer="21"/>
<wire x1="3.9051" y1="13.89" x2="-3.905" y2="13.89" width="0.2032" layer="21"/>
<wire x1="-4.0637" y1="13.7313" x2="-4.0637" y2="2.9051" width="0.2032" layer="21"/>
<wire x1="-4.0637" y1="2.4288" x2="-4.0637" y2="2.1112" width="0.2032" layer="21"/>
<wire x1="-4.0638" y1="13.7313" x2="-3.905" y2="13.89" width="0.2032" layer="21" curve="-90.108344" cap="flat"/>
<wire x1="4.0638" y1="13.7313" x2="3.9051" y2="13.8901" width="0.2032" layer="21" curve="89.747993" cap="flat"/>
<wire x1="-3.905" y1="1.9524" x2="-4.0637" y2="2.1112" width="0.2032" layer="21" curve="-89.963931" cap="flat"/>
<wire x1="3.9051" y1="1.9525" x2="4.0639" y2="2.1112" width="0.2032" layer="21" curve="90" cap="flat"/>
<wire x1="-4.0638" y1="2.4287" x2="-4.0638" y2="2.9049" width="0.2032" layer="21" curve="180" cap="flat"/>
<wire x1="4.0639" y1="2.4288" x2="4.0639" y2="2.905" width="0.2032" layer="21" curve="-180" cap="flat"/>
<wire x1="-3.8256" y1="2.6669" x2="3.8257" y2="2.6669" width="0.2032" layer="21"/>
<wire x1="3.8337" y1="0" x2="2.8811" y2="0" width="0.254" layer="21"/>
<wire x1="3.3574" y1="-0.4763" x2="3.3574" y2="0.4763" width="0.254" layer="21"/>
<pad name="+" x="1.7463" y="0" drill="0.8128" shape="long" rot="R270"/>
<pad name="-" x="-1.7463" y="0" drill="0.8128" shape="long" rot="R270"/>
<text x="5.7625" y="1.905" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<text x="-4.81" y="1.905" size="1.016" layer="25" ratio="18" rot="R90">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="CNP-">
<wire x1="-2.54" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="0.635" y1="1.5875" x2="0.635" y2="0" width="0.508" layer="94"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-1.5875" width="0.508" layer="94"/>
<wire x1="-0.635" y1="1.5875" x2="-0.635" y2="0" width="0.508" layer="94"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-1.5875" width="0.508" layer="94"/>
<text x="-2.794" y="-1.27" size="0.8636" layer="93">1</text>
<text x="2.286" y="-1.27" size="0.8636" layer="93">2</text>
<text x="-3.683" y="2.794" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-4.1275" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="R-">
<wire x1="-2.54" y1="0" x2="-2.2225" y2="0.9525" width="0.2032" layer="94"/>
<wire x1="-2.2225" y1="0.9525" x2="-1.5875" y2="-0.9525" width="0.2032" layer="94"/>
<wire x1="-1.5875" y1="-0.9525" x2="-0.9525" y2="0.9525" width="0.2032" layer="94"/>
<wire x1="-0.9525" y1="0.9525" x2="-0.3175" y2="-0.9525" width="0.2032" layer="94"/>
<wire x1="-0.3175" y1="-0.9525" x2="0.3175" y2="0.9525" width="0.2032" layer="94"/>
<wire x1="0.3175" y1="0.9525" x2="0.9525" y2="-0.9525" width="0.2032" layer="94"/>
<wire x1="0.9525" y1="-0.9525" x2="1.5875" y2="0.9525" width="0.2032" layer="94"/>
<wire x1="1.5875" y1="0.9525" x2="2.2225" y2="-0.9525" width="0.2032" layer="94"/>
<wire x1="2.2225" y1="-0.9525" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-2.54" y="1.5875" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="CP-">
<wire x1="0.635" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="0.635" y1="1.5875" x2="0.635" y2="0" width="0.508" layer="94"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-1.5875" width="0.508" layer="94"/>
<wire x1="-0.381" y1="0" x2="-0.3811" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.3811" y1="0" x2="-2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.365" y1="0" x2="-0.9017" y2="-1.5367" width="0.4064" layer="94" curve="-37.878659" cap="flat"/>
<wire x1="-0.8979" y1="1.5144" x2="-0.3811" y2="0" width="0.4064" layer="94" curve="-37.377473" cap="flat"/>
<text x="-4.953" y="2.667" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<text x="1.27" y="0.635" size="1.4224" layer="94" ratio="12">+</text>
<pin name="-" x="-2.54" y="0" visible="off" length="point" direction="pas"/>
<pin name="+" x="2.54" y="0" visible="off" length="point" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="C_" prefix="C" uservalue="yes">
<description>&lt;b&gt;NON-POLARIZED CAP&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="CNP-" x="2.54" y="0"/>
</gates>
<devices>
<device name="1812" package="C1812">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812K" package="C1812K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1825" package="C1825">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1825K" package="C1825K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2012" package="C2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2220K" package="C2220K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2225K" package="C2225K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3216" package="C3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3225" package="C3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4532" package="C4532">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4564" package="C4564">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C100-140X060" package="C100-140X060">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C120-150X060" package="C120-150X060">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C145-180X070" package="C145-180X070">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C150-190X070" package="C150-190X070">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C170-210X070" package="C170-210X070">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C200-230X080" package="C200-230X080">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C225-260X090" package="C225-260X090">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C275-320X100" package="C275-320X100">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C035-055X025" package="C035-055X025">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C050-075X030" package="C050-075X030">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C075-110X050" package="C075-110X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C085-110X050" package="C085-110X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C275-310X150" package="C275-310X150">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C050-075X040" package="C050-075X040">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C060-090X030" package="C060-090X030">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C050-080X045" package="C050-080X045">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C075-110X060" package="C075-110X060">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C075-110X070" package="C075-110X070">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C050-110X055" package="C050-110X055">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C075-105X035" package="C075-105X035">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C050-075X035" package="C050-075X035">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C050-075X080" package="C050-075X080">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C072-095X030" package="C072-095X030">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C072-095X035" package="C072-095X035">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C080-110X045" package="C080-110X045">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C072-095X060" package="C072-095X060">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C072-095X065" package="C072-095X065">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C072-095X070" package="C072-095X070">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C025-040X018" package="C025-040X018">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C020-035X018" package="C020-035X018">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C025-050X025" package="C025-050X025">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CA-100-024X044" package="CA-100-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CA-050-024X044" package="CA-050-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="C1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C070-095X035" package="C070-095X035">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C050-120X055" package="C050-120X055">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C075-120X055" package="C075-120X055">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C150-200X095" package="C150-200X095">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C050-085X045" package="C050-085X045">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C100-200X100" package="C100-200X100">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CX23P-050" package="CX23PW-050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CX23P-060" package="CX23PW-060">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CX23P-070" package="CX23PW-070">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CX23P-080" package="CX23PW-080">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CX23P-090" package="CX23PW-090">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0201" package="C0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1608" package="C1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1808" package="C1808">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CX23PS-055X120" package="CX23PS-055X120">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C050-075X57" package="C050-075X57">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C075-105X045" package="C075-100X045">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C050-075X018" package="C050-075X018">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R_" prefix="R" uservalue="yes">
<description>&lt;B&gt;RESISTOR&lt;/B&gt;</description>
<gates>
<gate name="G$1" symbol="R-" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="R0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805W" package="R0805W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1005" package="R1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206W" package="R1206W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="R1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210W" package="R1210W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010W" package="R2010W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2012" package="R2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2012W" package="R2012W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512W" package="R2512W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3216" package="R3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3216W" package="R3216W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3225" package="R3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3225W" package="R3225W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5025" package="R5025">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5025W" package="R5025W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="6332" package="R6332">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="6332W" package="R6332W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M0805" package="RM0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M1206" package="RM1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M1406" package="RM1406">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2012" package="RM2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2309" package="RM2309">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3216" package="RM3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3516" package="RM3516">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M5923" package="RM5923">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="X0204/5" package="0204/5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="X0204/7" package="0204/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="X0207/10" package="0207/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="X0207/12" package="0207/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="X0207/15" package="0207/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="X0207/2V" package="0207/2V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="X0207/5V" package="0207/5V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="X0207/7" package="0207/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="X0309/10" package="0309/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="X0309/12" package="0309/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="X0411/12" package="0411/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="X0411/15" package="0411/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="X0411/3V" package="0411V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="X0414/15" package="0414/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="X0414/5V" package="0414V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="X0617/17" package="0617/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="X0617/22" package="0617/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="X0617/5V" package="0617V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="X0922/22" package="0922/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="XP0613/5V" package="P0613V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="XP0613/15" package="P0613/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="XP0817/22" package="P0817/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="XP0817/7V" package="P0817V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102AX" package="RMINI_MELF-0102AX">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="X0922V" package="0922V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102R" package="RMINI_MELF-0102R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102W" package="RMINI_MELF-0102W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0204R" package="RMINI_MELF-0204R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0204W" package="RMINI_MELF-0204W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0207R" package="RMINI_MELF-0207R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0207W" package="RMINI_MELF-0207W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="X0204/2V" package="0204V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="X0309/V" package="0309V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="X0207/10B" package="0207/10B">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="XMF50" package="MF50">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="XMF60" package="MF60">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="XMF55" package="MF55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="XMF65" package="MF65">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="XMF70" package="MF70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="XMF75" package="MF75">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="X0207/10C" package="0207/10C">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="X0309/12B" package="0309/12B">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="X0309/14" package="0309/14">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="X0309/14B" package="0309/14B">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="X0309/15" package="0309/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="X0309/15B" package="0309/15B">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="X0309/20" package="0309/20">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="X0207/2VB" package="0207/2VB">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="X0207/2VC" package="0207/2VC">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0201" package="R0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="X0414/17" package="0414/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CP_" prefix="C" uservalue="yes">
<description>&lt;B&gt;POLARIZED CAP&lt;/B&gt;</description>
<gates>
<gate name="G$1" symbol="CP-" x="0" y="0"/>
</gates>
<devices>
<device name="E-010X030" package="CE-010X030">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="E-020X050" package="CE-020X050">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="E-025X060" package="CE-025X060">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="E-025X065" package="CE-025X065">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="E-035X100" package="CE-035X100">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="E-035X080" package="CE-035X080">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="E-050X100" package="CE-050X100">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="E-050X120" package="CE-050X120">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="E-075X160" package="CE-075X160">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="E-075X180" package="CE-075X180">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="CEA-100X100">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EA-100X250" package="CEA-100X250">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EA-120X250" package="CEA-120X250">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EA-120X310" package="CEA-120X310">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EA-160X250" package="CEA-160X250">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EA-160X310" package="CEA-160X310">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EA-180X400" package="CEA-180X400">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EA-220X400" package="CEA-220X400">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EA-220X500" package="CEA-220X500">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EA-035X070" package="CEA-035X070">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EA-045X100" package="CEA-045X100">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EA-063X100" package="CEA-063X100">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EA-080X160" package="CEA-080X160">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EA-080X200" package="CEA-080X200">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ETS-20" package="CETS-20">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ETS-22" package="CETS-22">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ETS-25" package="CETS-25">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ETS-30" package="CETS-30">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ETS-35" package="CETS-35">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ETS-40" package="CETS-40">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="E-050X125" package="CE-050X125">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="7343-TE-D" package="C7343-TE-D">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EH-015-040X110" package="CEH-015-040X110">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EH-015-040X070" package="CEH-015-040X070">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EH-020-050X110" package="CEH-020-050X110">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EH-020-050X150" package="CEH-020-050X150">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EH-025-060X110" package="CEH-025-060X110">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EH-025-060X150" package="CEH-025-060X150">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EH-050-100X120" package="CEH-050-100X120">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EH-050-100X200" package="CEH-050-100X200">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EH-050-100X120/R" package="CEH-050-100X120/R">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EH-015-040X110/R" package="CEH-015-040X110/R">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EH-015-040X070/R" package="CEH-015-040X070/R">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EH-020-050X110/R" package="CEH-020-050X110/R">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EH-020-050X150/R" package="CEH-020-050X150/R">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EH-025-060X110/R" package="CEH-025-060X110/R">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EH-025-060X150/R" package="CEH-025-060X150/R">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="AA127X350" package="CEAA127X350">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="AA222X510" package="CEAA222X510">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="AA286X640" package="CEAA286X640">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="AA318X790" package="CEAA318X790">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TT025-100" package="CTT-025-100">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TT025-030" package="CTT-025-030">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TT025-040" package="CTT-025-040">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TT025-050" package="CTT-025-050">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TT025-070" package="CTT-025-070">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TT050-100" package="CTT-050-100">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TT050-080" package="CTT-050-080">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3216-TE-A" package="C3216-TE-A">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3258-TE-B" package="C3528-TE-B">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="6032-TE-C" package="C6032-TE-C">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SV-A" package="CSV-A">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SV-B" package="CSV-B">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SV-C" package="CSV-C">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SV-D" package="CSV-D">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SV-E/F" package="CSV-E/F">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SV-G" package="CSV-G">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3216" package="C3216/T">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3216-18R" package="C3216-18R/T">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3216-18W" package="C3216-18W/T">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3528" package="C3528/T">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3528-21R" package="C3528-21R/T">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3528-21W" package="C3528-21W/T">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="6032" package="C6032/T">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="6032-28R" package="C6032-28R/T">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="6032-28W" package="C6032-28W/T">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="7343" package="C7343/T">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="BC139CLL-2R" package="C139CLL-2R">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="BC139CLL-2W" package="C139CLL-2W">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="BC139CLL-3W" package="C139CLL-3W">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="BC140CLH-0810" package="C140CLH-0810">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="BC140CLH-1010" package="C140CLH-1010">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="BC140CLH-1014" package="C140CLH-1014">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="BC150CLZ-0810" package="C150CLZ-0810">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="BC150CLZ-1010" package="C150CLZ-1010">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="BC150CLZ-1014" package="C150CLZ-1014">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="BC153CLV-0405" package="C153CLV-0405">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="BC153CLV-0505" package="C153CLV-0505">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="BC153CLV-0605" package="C153CLV-0605">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="BC153CLV-0807" package="C153CLV-0807">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="BC153CLV-0810" package="C153CLV-0810">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="BC153CLV-1010" package="C153CLV-1010">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="BC153CLV-1012" package="C153CLV-1012">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="BC153CLV-1014" package="C153CLV-1014">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="BC175TMP-0808" package="C175TMP-0808">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="BC175TMP-0810" package="C175TMP-0810">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SV-H" package="CSV-H">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SV-J" package="CSV-J">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SV-K" package="CSV-K">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="GC-SD-H" package="CGOLD-SD-H">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="GC-SD-V" package="CGOLD-SD-V">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="E-015X040" package="CE-015X040">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="E-020X060" package="CE-020X060">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="E-025X075" package="CE-025X075">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="E-025X085" package="CE-025X085">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EH-035-080X115" package="CEH-035-080X115">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EH-035-080X115/R" package="CEH-035-080X115/R">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-PowerIC">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find drivers, regulators, and amplifiers.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="PTC">
<wire x1="-3.81" y1="1.524" x2="3.81" y2="1.524" width="0.2032" layer="21"/>
<wire x1="3.81" y1="1.524" x2="3.81" y2="-1.524" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-1.524" x2="-3.81" y2="-1.524" width="0.2032" layer="21"/>
<wire x1="-3.81" y1="-1.524" x2="-3.81" y2="1.524" width="0.2032" layer="21"/>
<pad name="P$1" x="-2.54" y="0" drill="0.8" diameter="1.8796"/>
<pad name="P$2" x="2.54" y="0" drill="0.8" diameter="1.8796"/>
<text x="-3.81" y="1.705" size="0.4318" layer="25">&gt;Name</text>
<text x="-3.81" y="-2.14" size="0.4318" layer="27">&gt;Value</text>
</package>
<package name="PTC-1206">
<wire x1="-1.5" y1="0.75" x2="1.5" y2="0.75" width="0.127" layer="51"/>
<wire x1="1.5" y1="0.75" x2="1.5" y2="-0.75" width="0.127" layer="51"/>
<wire x1="1.5" y1="-0.75" x2="-1.5" y2="-0.75" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-0.75" x2="-1.5" y2="0.75" width="0.127" layer="51"/>
<wire x1="0.635" y1="-0.762" x2="-0.635" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="0.762" x2="0.635" y2="0.762" width="0.2032" layer="21"/>
<wire x1="-1.143" y1="-1.016" x2="0.254" y2="1.016" width="0.127" layer="51"/>
<wire x1="0.254" y1="1.016" x2="1.143" y2="1.016" width="0.127" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1" dy="1.8" layer="1"/>
<text x="-1.524" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.524" y="-1.651" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="0603">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="0.762" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="PTC-1206-WIDE">
<wire x1="-1.5" y1="0.75" x2="1.5" y2="0.75" width="0.127" layer="51"/>
<wire x1="1.5" y1="0.75" x2="1.5" y2="-0.75" width="0.127" layer="51"/>
<wire x1="1.5" y1="-0.75" x2="-1.5" y2="-0.75" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-0.75" x2="-1.5" y2="0.75" width="0.127" layer="51"/>
<wire x1="0.635" y1="-0.762" x2="-0.635" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="0.762" x2="0.635" y2="0.762" width="0.2032" layer="21"/>
<wire x1="-1.143" y1="-1.016" x2="0.254" y2="1.016" width="0.127" layer="51"/>
<wire x1="0.254" y1="1.016" x2="1.143" y2="1.016" width="0.127" layer="51"/>
<smd name="1" x="-1.654" y="0" dx="1" dy="1.8" layer="1"/>
<smd name="2" x="1.654" y="0" dx="1" dy="1.8" layer="1"/>
<text x="-1.524" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.524" y="-1.651" size="0.4064" layer="27">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="PTC">
<wire x1="5.08" y1="1.27" x2="5.08" y2="-1.27" width="0.254" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="-2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="-2.54" y1="1.27" x2="5.08" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.524" y1="-2.54" x2="3.81" y2="2.54" width="0.254" layer="94"/>
<wire x1="3.81" y1="2.54" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<text x="-2.54" y="3.048" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.302" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short"/>
<pin name="2" x="7.62" y="0" visible="off" length="short" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PTC" prefix="F">
<description>&lt;b&gt;Resettable Fuse PTC&lt;/b&gt;
Resettable Fuse. Spark Fun Electronics SKU : COM-08357</description>
<gates>
<gate name="G$1" symbol="PTC" x="0" y="0"/>
</gates>
<devices>
<device name="PTH" package="PTC">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="PTC-1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-11150"/>
</technology>
</technologies>
</device>
<device name="0603" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-W" package="PTC-1206-WIDE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="THERMISTOR1206" package="PTC-1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-08585" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="general">
<packages>
<package name="ALPS_POT">
<description>RK09 vertical</description>
<wire x1="8" y1="6.2" x2="8" y2="-6.2" width="0.2032" layer="21"/>
<wire x1="8" y1="-6.2" x2="-5.6" y2="-6.2" width="0.2032" layer="21"/>
<wire x1="-5.6" y1="-6.2" x2="-5.6" y2="6.2" width="0.2032" layer="21"/>
<wire x1="-5.6" y1="6.2" x2="8" y2="6.2" width="0.2032" layer="21"/>
<pad name="P$1" x="7" y="-2.5" drill="1" diameter="1.8796"/>
<pad name="P$2" x="7" y="0" drill="1" diameter="1.8796"/>
<pad name="P$3" x="7" y="2.5" drill="1" diameter="1.8796"/>
<pad name="P$4" x="0" y="-4.4" drill="2.2" shape="octagon"/>
<pad name="P$5" x="0" y="4.4" drill="2.2" shape="octagon"/>
<text x="-6.35" y="-5.08" size="1.27" layer="21" ratio="14" rot="R90">&gt;NAME</text>
</package>
<package name="TRIM_POT">
<pad name="2" x="0" y="2.54" drill="1" diameter="1.8796"/>
<pad name="3" x="0" y="-2.54" drill="1" diameter="1.8796"/>
<pad name="1" x="5.08" y="0" drill="1" diameter="1.8796"/>
</package>
<package name="SMA-DIODE">
<description>&lt;B&gt;Diode&lt;/B&gt;&lt;p&gt;
Basic SMA packaged diode. Good for reverse polarization protection. Common part #: MBRA140</description>
<wire x1="-2.3" y1="1" x2="-2.3" y2="1.45" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="1.45" x2="2.3" y2="1.45" width="0.2032" layer="21"/>
<wire x1="2.3" y1="1.45" x2="2.3" y2="1" width="0.2032" layer="21"/>
<wire x1="2.3" y1="-1" x2="2.3" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="2.3" y1="-1.45" x2="-2.3" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.45" x2="-2.3" y2="-1" width="0.2032" layer="21"/>
<wire x1="1" y1="1" x2="1" y2="-1" width="0.2032" layer="21"/>
<smd name="A" x="-2.15" y="0" dx="1.27" dy="1.47" layer="1" rot="R180"/>
<smd name="C" x="2.15" y="0" dx="1.27" dy="1.47" layer="1"/>
<text x="-2.286" y="1.651" size="0.4064" layer="25">&gt;NAME</text>
<text x="0.254" y="1.651" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="DIODE-1N4001">
<wire x1="3.175" y1="1.27" x2="1.905" y2="1.27" width="0.254" layer="21"/>
<wire x1="1.905" y1="1.27" x2="-3.175" y2="1.27" width="0.254" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.254" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.254" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="3.175" y2="-1.27" width="0.254" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="3.175" y2="0" width="0.254" layer="21"/>
<wire x1="3.175" y1="0" x2="3.175" y2="1.27" width="0.254" layer="21"/>
<wire x1="1.905" y1="1.27" x2="1.905" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.81" y2="0" width="0.254" layer="21"/>
<wire x1="3.175" y1="0" x2="3.81" y2="0" width="0.254" layer="21"/>
<pad name="A" x="-5.08" y="0" drill="1" diameter="1.9812"/>
<pad name="C" x="5.08" y="0" drill="1" diameter="1.9812"/>
<text x="-2.921" y="1.651" size="0.6096" layer="25">&gt;Name</text>
<text x="-2.921" y="-0.508" size="1.016" layer="21" ratio="12">&gt;Value</text>
</package>
<package name="SOD-323">
<wire x1="-0.9" y1="0.65" x2="-0.5" y2="0.65" width="0.2032" layer="21"/>
<wire x1="-0.5" y1="0.65" x2="0.9" y2="0.65" width="0.2032" layer="21"/>
<wire x1="-0.9" y1="-0.65" x2="-0.5" y2="-0.65" width="0.2032" layer="21"/>
<wire x1="-0.5" y1="-0.65" x2="0.9" y2="-0.65" width="0.2032" layer="21"/>
<wire x1="-0.5" y1="0.65" x2="-0.5" y2="-0.65" width="0.2032" layer="21"/>
<smd name="1" x="-1.15" y="0" dx="0.63" dy="0.83" layer="1"/>
<smd name="2" x="1.15" y="0" dx="0.63" dy="0.83" layer="1"/>
<text x="-0.889" y="1.016" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="SOT23-3">
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-0.8" y1="0.7" x2="-1.4" y2="0.7" width="0.2032" layer="21"/>
<wire x1="-1.4" y1="0.7" x2="-1.4" y2="-0.1" width="0.2032" layer="21"/>
<wire x1="0.8" y1="0.7" x2="1.4" y2="0.7" width="0.2032" layer="21"/>
<wire x1="1.4" y1="0.7" x2="1.4" y2="-0.1" width="0.2032" layer="21"/>
<smd name="1" x="-0.95" y="-1" dx="0.8" dy="0.9" layer="1"/>
<smd name="2" x="0.95" y="-1" dx="0.8" dy="0.9" layer="1"/>
<smd name="3" x="0" y="1.1" dx="0.8" dy="0.9" layer="1"/>
<text x="-0.8255" y="1.778" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-0.1905" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="DIODE-1N4148">
<wire x1="-2.54" y1="0.762" x2="0" y2="0.762" width="0.2032" layer="21"/>
<wire x1="0" y1="0.762" x2="0" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="0" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="0.254" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<pad name="A" x="-3.81" y="0" drill="0.9" diameter="1.8796"/>
<pad name="C" x="1.27" y="0" drill="0.9" diameter="1.8796"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<wire x1="-0.635" y1="0.635" x2="-0.635" y2="-0.635" width="0.3048" layer="21"/>
</package>
<package name="SMB-DIODE">
<description>&lt;b&gt;Diode&lt;/b&gt;&lt;p&gt;
Basic small signal diode good up to 200mA. SMB footprint. Common part #: BAS16</description>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.2606" y1="1.905" x2="2.2606" y2="1.905" width="0.1016" layer="21"/>
<wire x1="-2.2606" y1="-1.905" x2="2.2606" y2="-1.905" width="0.1016" layer="21"/>
<wire x1="-2.261" y1="-1.905" x2="-2.261" y2="1.905" width="0.1016" layer="51"/>
<wire x1="2.261" y1="-1.905" x2="2.261" y2="1.905" width="0.1016" layer="51"/>
<wire x1="0.643" y1="1" x2="-0.73" y2="0" width="0.2032" layer="21"/>
<wire x1="-0.73" y1="0" x2="0.643" y2="-1" width="0.2032" layer="21"/>
<wire x1="0.643" y1="-1" x2="0.643" y2="1" width="0.2032" layer="21"/>
<wire x1="-0.73" y1="1" x2="-0.73" y2="-1" width="0.2032" layer="21"/>
<smd name="C" x="-2.2" y="0" dx="2.4" dy="2.4" layer="1"/>
<smd name="A" x="2.2" y="0" dx="2.4" dy="2.4" layer="1"/>
<text x="-2.159" y="2.159" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.429" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.794" y1="-1.0922" x2="-2.2606" y2="1.0922" layer="51"/>
<rectangle x1="2.2606" y1="-1.0922" x2="2.794" y2="1.0922" layer="51"/>
</package>
<package name="DIODE-HV">
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.2606" y1="1.905" x2="2.2606" y2="1.905" width="0.1016" layer="21"/>
<wire x1="-2.2606" y1="-1.905" x2="2.2606" y2="-1.905" width="0.1016" layer="21"/>
<wire x1="-2.261" y1="-1.905" x2="-2.261" y2="1.905" width="0.1016" layer="51"/>
<wire x1="2.261" y1="-1.905" x2="2.261" y2="1.905" width="0.1016" layer="51"/>
<wire x1="0.643" y1="1" x2="-0.73" y2="0" width="0.2032" layer="21"/>
<wire x1="-0.73" y1="0" x2="0.643" y2="-1" width="0.2032" layer="21"/>
<wire x1="0.643" y1="-1" x2="0.643" y2="1" width="0.2032" layer="21"/>
<wire x1="-0.73" y1="1" x2="-0.73" y2="-1" width="0.2032" layer="21"/>
<smd name="C" x="-2.454" y="0" dx="2.2" dy="2.4" layer="1"/>
<smd name="A" x="2.454" y="0" dx="2.2" dy="2.4" layer="1"/>
<text x="-2.159" y="2.159" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.429" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.794" y1="-1.0922" x2="-2.2606" y2="1.0922" layer="51"/>
<rectangle x1="2.2606" y1="-1.0922" x2="2.794" y2="1.0922" layer="51"/>
</package>
<package name="SMA-DIODE_ALT">
<wire x1="-2.3" y1="1.3" x2="-2.3" y2="1.45" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="1.45" x2="2.3" y2="1.45" width="0.2032" layer="21"/>
<wire x1="2.3" y1="1.45" x2="2.3" y2="1.3" width="0.2032" layer="21"/>
<wire x1="2.3" y1="-1.3" x2="2.3" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="2.3" y1="-1.45" x2="-2.3" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.45" x2="-2.3" y2="-1.3" width="0.2032" layer="21"/>
<wire x1="0.6" y1="1" x2="0.6" y2="-1" width="0.2032" layer="21"/>
<smd name="A" x="-2" y="0" dx="2" dy="2" layer="1" rot="R180"/>
<smd name="C" x="2" y="0" dx="2" dy="2" layer="1"/>
<text x="-2.286" y="1.651" size="0.4064" layer="25">&gt;NAME</text>
<text x="0.254" y="1.651" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="SMA-DIODE-KIT">
<wire x1="-2.3" y1="1" x2="-2.3" y2="1.45" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="1.45" x2="2.3" y2="1.45" width="0.2032" layer="21"/>
<wire x1="2.3" y1="1.45" x2="2.3" y2="1" width="0.2032" layer="21"/>
<wire x1="2.3" y1="-1" x2="2.3" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="2.3" y1="-1.45" x2="-2.3" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.45" x2="-2.3" y2="-1" width="0.2032" layer="21"/>
<wire x1="1" y1="1" x2="1" y2="-1" width="0.2032" layer="21"/>
<smd name="A" x="-2.4" y="0" dx="1.77" dy="1.47" layer="1" rot="R180"/>
<smd name="C" x="2.4" y="0" dx="1.77" dy="1.47" layer="1"/>
<text x="-2.286" y="1.651" size="0.4064" layer="25">&gt;NAME</text>
<text x="0.254" y="1.651" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="SOD523">
<wire x1="-0.59" y1="0.4" x2="0.59" y2="0.4" width="0.1016" layer="21"/>
<wire x1="0.59" y1="0.4" x2="0.59" y2="-0.4" width="0.1016" layer="51"/>
<wire x1="0.59" y1="-0.4" x2="-0.59" y2="-0.4" width="0.1016" layer="21"/>
<wire x1="-0.59" y1="-0.4" x2="-0.59" y2="0.4" width="0.1016" layer="51"/>
<rectangle x1="-0.75" y1="-0.17" x2="-0.54" y2="0.17" layer="51"/>
<rectangle x1="0.54" y1="-0.17" x2="0.75" y2="0.17" layer="51"/>
<rectangle x1="-0.59" y1="-0.4" x2="-0.3" y2="0.4" layer="51"/>
<smd name="A" x="0.7" y="0" dx="0.7" dy="0.5" layer="1"/>
<smd name="C" x="-0.6" y="0" dx="0.7" dy="0.5" layer="1"/>
<text x="-0.7366" y="0.5588" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.6858" y="-0.9906" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1397" y1="-0.3937" x2="-0.0127" y2="0.381" layer="21"/>
</package>
<package name="SMC">
<description>&lt;b&gt;DIODE&lt;/b&gt;</description>
<wire x1="-3.5606" y1="3.105" x2="3.5606" y2="3.105" width="0.1016" layer="21"/>
<wire x1="-3.5606" y1="-3.105" x2="3.5606" y2="-3.105" width="0.1016" layer="21"/>
<wire x1="-3.5606" y1="-3.105" x2="-3.5606" y2="3.105" width="0.1016" layer="51"/>
<wire x1="3.5606" y1="-3.105" x2="3.5606" y2="3.105" width="0.1016" layer="51"/>
<wire x1="0.543" y1="1" x2="-0.83" y2="0" width="0.2032" layer="21"/>
<wire x1="-0.83" y1="0" x2="0.543" y2="-1" width="0.2032" layer="21"/>
<wire x1="0.543" y1="-1" x2="0.543" y2="1" width="0.2032" layer="21"/>
<smd name="C" x="-3.7" y="0" dx="2.8" dy="3.8" layer="1"/>
<smd name="A" x="3.7" y="0" dx="2.8" dy="3.8" layer="1"/>
<text x="-3.459" y="3.359" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.459" y="-4.629" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-4.094" y1="-1.0922" x2="-3.5606" y2="1.0922" layer="51"/>
<rectangle x1="3.5606" y1="-1.0922" x2="4.094" y2="1.0922" layer="51"/>
<rectangle x1="-2.1" y1="-3.1" x2="-0.85" y2="3.1" layer="21"/>
</package>
<package name="1X06">
<wire x1="11.43" y1="0.635" x2="12.065" y2="1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="1.27" x2="13.335" y2="1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="13.335" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.27" x2="12.065" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="9.525" y2="1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.795" y2="1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.27" x2="11.43" y2="0.635" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.635" x2="10.795" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="9.525" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.97" y1="0.635" x2="13.97" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="MOLEX-1X6">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="13.97" y1="3.048" x2="13.97" y2="-2.54" width="0.127" layer="21"/>
<wire x1="13.97" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="13.97" y1="-2.54" x2="12.7" y2="-2.54" width="0.127" layer="21"/>
<wire x1="12.7" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="12.7" y2="-1.27" width="0.127" layer="21"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.8796"/>
</package>
<package name="MOLEX-1X6-RA">
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="3.175" width="0.127" layer="21"/>
<wire x1="13.97" y1="0.635" x2="13.97" y2="3.175" width="0.127" layer="21"/>
<wire x1="13.97" y1="0.635" x2="-1.27" y2="0.635" width="0.127" layer="21"/>
<wire x1="13.97" y1="3.175" x2="12.7" y2="3.175" width="0.127" layer="21"/>
<wire x1="12.7" y1="3.175" x2="0" y2="3.175" width="0.127" layer="21"/>
<wire x1="0" y1="3.175" x2="-1.27" y2="3.175" width="0.127" layer="21"/>
<wire x1="0" y1="3.175" x2="0" y2="7.62" width="0.127" layer="21"/>
<wire x1="0" y1="7.62" x2="12.7" y2="7.62" width="0.127" layer="21"/>
<wire x1="12.7" y1="7.62" x2="12.7" y2="3.175" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.8796"/>
<text x="-0.889" y="-2.794" size="1.27" layer="25">&gt;NAME</text>
<text x="8.001" y="-2.794" size="1.27" layer="25">&gt;VALUE</text>
</package>
<package name="1X06-SMD">
<wire x1="7.62" y1="1.25" x2="-7.62" y2="1.25" width="0.127" layer="51"/>
<wire x1="-7.62" y1="1.25" x2="-7.62" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-7.62" y1="-1.25" x2="-6.35" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-6.35" y1="-1.25" x2="-3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="6.35" y2="-1.25" width="0.127" layer="51"/>
<wire x1="6.35" y1="-1.25" x2="7.62" y2="-1.25" width="0.127" layer="51"/>
<wire x1="7.62" y1="-1.25" x2="7.62" y2="1.25" width="0.127" layer="51"/>
<wire x1="6.35" y1="-1.25" x2="6.35" y2="-7.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="-7.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="-1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-3.81" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-6.35" y1="-1.25" x2="-6.35" y2="-7.25" width="0.127" layer="51"/>
<smd name="4" x="1.27" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="5" x="3.81" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="6" x="6.35" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="3" x="-1.27" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-3.81" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="1" x="-6.35" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<hole x="-5.08" y="0" drill="1.4"/>
<hole x="5.08" y="0" drill="1.4"/>
</package>
<package name="1X06_LOCK">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<wire x1="-1.27" y1="0.508" x2="-0.635" y2="1.143" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.143" x2="0.635" y2="1.143" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.143" x2="1.27" y2="0.508" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.508" x2="1.905" y2="1.143" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.143" x2="3.175" y2="1.143" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.143" x2="3.81" y2="0.508" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.508" x2="4.445" y2="1.143" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.143" x2="5.715" y2="1.143" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.143" x2="6.35" y2="0.508" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.508" x2="6.985" y2="1.143" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.143" x2="8.255" y2="1.143" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.143" x2="8.89" y2="0.508" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.508" x2="9.525" y2="1.143" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.143" x2="10.795" y2="1.143" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.143" x2="11.43" y2="0.508" width="0.2032" layer="21"/>
<wire x1="11.43" y1="0.508" x2="12.065" y2="1.143" width="0.2032" layer="21"/>
<wire x1="12.065" y1="1.143" x2="13.335" y2="1.143" width="0.2032" layer="21"/>
<wire x1="13.335" y1="1.143" x2="13.97" y2="0.508" width="0.2032" layer="21"/>
<wire x1="13.97" y1="0.508" x2="13.97" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.762" x2="13.335" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.397" x2="12.065" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.397" x2="11.43" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.762" x2="10.795" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.397" x2="9.525" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.397" x2="8.89" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.762" x2="8.255" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.397" x2="6.985" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="6.985" y1="-1.397" x2="6.35" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.762" x2="5.715" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.397" x2="4.445" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.397" x2="3.81" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.762" x2="3.175" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.397" x2="1.905" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.397" x2="1.27" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.762" x2="0.635" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.397" x2="-0.635" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.397" x2="-1.27" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.762" x2="-1.27" y2="0.508" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.508" x2="1.27" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.508" x2="3.81" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.508" x2="6.35" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.508" x2="8.89" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="11.43" y1="0.508" x2="11.43" y2="-0.762" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="-0.254" drill="1.016" diameter="1.8796"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="12.7" y="-0.254" drill="1.016" diameter="1.8796"/>
<text x="-1.27" y="1.778" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-3.302" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
<rectangle x1="7.3279" y1="-0.4191" x2="7.9121" y2="0.1651" layer="51"/>
<rectangle x1="9.8679" y1="-0.4191" x2="10.4521" y2="0.1651" layer="51"/>
<rectangle x1="12.4079" y1="-0.4191" x2="12.9921" y2="0.1651" layer="51"/>
</package>
<package name="1X06_LOCK_LONGPADS">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<wire x1="1.524" y1="-0.127" x2="1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="4.064" y1="-0.127" x2="3.556" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="6.604" y1="-0.127" x2="6.096" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="9.144" y1="-0.127" x2="8.636" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="11.684" y1="-0.127" x2="11.176" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.8636" x2="-0.9906" y2="1.143" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.1176" x2="-0.9906" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.127" x2="13.716" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.127" x2="13.97" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-1.1176" x2="13.6906" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.127" x2="13.97" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="13.97" y1="0.8636" x2="13.6906" y2="1.143" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="6" x="12.7" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<text x="-1.27" y="1.778" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-3.302" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
<rectangle x1="7.3279" y1="-0.4191" x2="7.9121" y2="0.1651" layer="51" rot="R90"/>
<rectangle x1="9.8679" y1="-0.4191" x2="10.4521" y2="0.1651" layer="51"/>
<rectangle x1="12.4079" y1="-0.4191" x2="12.9921" y2="0.1651" layer="51"/>
</package>
<package name="MOLEX-1X6_LOCK">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="13.97" y1="3.048" x2="13.97" y2="-2.54" width="0.127" layer="21"/>
<wire x1="13.97" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="13.97" y1="-2.54" x2="12.7" y2="-2.54" width="0.127" layer="21"/>
<wire x1="12.7" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="12.7" y2="-1.27" width="0.127" layer="21"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="-0.254" drill="1.016" diameter="1.8796"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="12.7" y="-0.254" drill="1.016" diameter="1.8796"/>
<text x="-1.27" y="3.302" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-4.064" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
<rectangle x1="7.3279" y1="-0.4191" x2="7.9121" y2="0.1651" layer="51"/>
<rectangle x1="9.8679" y1="-0.4191" x2="10.4521" y2="0.1651" layer="51"/>
<rectangle x1="12.4079" y1="-0.4191" x2="12.9921" y2="0.1651" layer="51"/>
</package>
<package name="MOLEX-1X6-RA_LOCK">
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="3.175" width="0.127" layer="21"/>
<wire x1="13.97" y1="0.635" x2="13.97" y2="3.175" width="0.127" layer="21"/>
<wire x1="13.97" y1="0.635" x2="-1.27" y2="0.635" width="0.127" layer="21"/>
<wire x1="13.97" y1="3.175" x2="12.7" y2="3.175" width="0.127" layer="21"/>
<wire x1="12.7" y1="3.175" x2="0" y2="3.175" width="0.127" layer="21"/>
<wire x1="0" y1="3.175" x2="-1.27" y2="3.175" width="0.127" layer="21"/>
<wire x1="0" y1="3.175" x2="0" y2="7.62" width="0.127" layer="21"/>
<wire x1="0" y1="7.62" x2="12.7" y2="7.62" width="0.127" layer="21"/>
<wire x1="12.7" y1="7.62" x2="12.7" y2="3.175" width="0.127" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" diameter="1.8796"/>
<pad name="5" x="10.16" y="0.127" drill="1.016" diameter="1.8796"/>
<pad name="6" x="12.7" y="-0.127" drill="1.016" diameter="1.8796"/>
<text x="-0.889" y="-2.794" size="1.27" layer="25">&gt;NAME</text>
<text x="8.001" y="-2.794" size="1.27" layer="25">&gt;VALUE</text>
</package>
<package name="1X06-SIP_LOCK">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<wire x1="11.43" y1="0.635" x2="12.065" y2="1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="1.27" x2="13.335" y2="1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="13.335" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.27" x2="12.065" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="9.525" y2="1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.795" y2="1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.27" x2="11.43" y2="0.635" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.635" x2="10.795" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="9.525" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.97" y1="0.635" x2="13.97" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="-0.254" drill="1.016" diameter="1.8796"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="12.7" y="-0.254" drill="1.016" diameter="1.8796"/>
<text x="-1.27" y="1.524" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-2.921" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
<rectangle x1="7.3279" y1="-0.4191" x2="7.9121" y2="0.1651" layer="51"/>
<rectangle x1="9.8679" y1="-0.4191" x2="10.4521" y2="0.1651" layer="51"/>
<rectangle x1="12.4079" y1="-0.4191" x2="12.9921" y2="0.1651" layer="51"/>
</package>
<package name="1X06_FEMALE_LOCK.010">
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="13.97" y2="1.27" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-1.27" x2="-1.27" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.97" y1="1.27" x2="13.97" y2="-1.27" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.254" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.254" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="-0.254" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0.254" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="12.7" y="-0.254" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3175" y1="-0.1905" x2="0.3175" y2="0.1905" layer="51"/>
<rectangle x1="2.2225" y1="-0.1905" x2="2.8575" y2="0.1905" layer="51"/>
<rectangle x1="4.7625" y1="-0.1905" x2="5.3975" y2="0.1905" layer="51"/>
<rectangle x1="7.3025" y1="-0.1905" x2="7.9375" y2="0.1905" layer="51"/>
<rectangle x1="9.8425" y1="-0.1905" x2="10.4775" y2="0.1905" layer="51"/>
<rectangle x1="12.3825" y1="-0.1905" x2="13.0175" y2="0.1905" layer="51"/>
</package>
<package name="1X06_LONGPADS">
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="0.635" x2="13.97" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<text x="-1.3462" y="2.4638" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X06-SMD_EDGE">
<wire x1="13.97" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="-11.176" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-11.176" x2="13.97" y2="-11.176" width="0.127" layer="51"/>
<wire x1="13.97" y1="-11.176" x2="13.97" y2="-2.54" width="0.127" layer="51"/>
<smd name="4" x="7.62" y="-0.08" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="5" x="10.16" y="-0.08" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="6" x="12.7" y="-0.08" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="3" x="5.08" y="-0.08" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="2" x="2.54" y="-0.08" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="1" x="0" y="-0.08" dx="3" dy="1" layer="1" rot="R90"/>
<text x="0" y="-6.35" size="0.4064" layer="51">thru-hole vertical Female Header</text>
<text x="0" y="-7.62" size="0.4064" layer="51">used as an SMD part </text>
<text x="0" y="-8.89" size="0.4064" layer="51">(placed horizontally, at board's edge)</text>
<rectangle x1="-0.381" y1="-2.4892" x2="0.381" y2="0.2794" layer="51"/>
<rectangle x1="2.159" y1="-2.4892" x2="2.921" y2="0.2794" layer="51"/>
<rectangle x1="4.699" y1="-2.4892" x2="5.461" y2="0.2794" layer="51"/>
<rectangle x1="7.239" y1="-2.4892" x2="8.001" y2="0.2794" layer="51"/>
<rectangle x1="9.779" y1="-2.4892" x2="10.541" y2="0.2794" layer="51"/>
<rectangle x1="12.319" y1="-2.4892" x2="13.081" y2="0.2794" layer="51"/>
</package>
<package name="SCREWTERMINAL-3.5MM-6">
<wire x1="-2.3" y1="3.4" x2="19.76" y2="3.4" width="0.2032" layer="21"/>
<wire x1="19.76" y1="3.4" x2="19.76" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="19.76" y1="-2.8" x2="19.76" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="19.76" y1="-3.6" x2="-2.3" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-3.6" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-2.8" x2="-2.3" y2="3.4" width="0.2032" layer="21"/>
<wire x1="19.76" y1="-2.8" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.35" x2="-2.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-1.35" x2="-2.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-2.35" x2="-2.3" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="19.76" y1="3.15" x2="20.16" y2="3.15" width="0.2032" layer="51"/>
<wire x1="20.16" y1="3.15" x2="20.16" y2="2.15" width="0.2032" layer="51"/>
<wire x1="20.16" y1="2.15" x2="19.76" y2="2.15" width="0.2032" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="3" x="7" y="0" drill="1.2" diameter="2.032"/>
<pad name="4" x="10.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="5" x="14" y="0" drill="1.2" diameter="2.032"/>
<pad name="6" x="17.5" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X06-SMD-FEMALE">
<wire x1="-7.62" y1="4.05" x2="7.62" y2="4.05" width="0.2032" layer="51"/>
<wire x1="7.62" y1="4.05" x2="7.62" y2="-4.05" width="0.2032" layer="51"/>
<wire x1="7.62" y1="-4.05" x2="-7.62" y2="-4.05" width="0.2032" layer="51"/>
<wire x1="-7.62" y1="-4.05" x2="-7.62" y2="4.05" width="0.2032" layer="51"/>
<wire x1="-6.85" y1="-3" x2="-6.85" y2="-2" width="0.2032" layer="21"/>
<wire x1="-6.85" y1="-2" x2="-5.85" y2="-2" width="0.2032" layer="21"/>
<wire x1="-5.85" y1="-2" x2="-5.85" y2="-3" width="0.2032" layer="21"/>
<wire x1="-7.1" y1="4" x2="-7.6" y2="4" width="0.2032" layer="21"/>
<wire x1="-7.6" y1="4" x2="-7.6" y2="-4" width="0.2032" layer="21"/>
<wire x1="-7.6" y1="-4" x2="-7.1" y2="-4" width="0.2032" layer="21"/>
<wire x1="7.1" y1="4" x2="7.6" y2="4" width="0.2032" layer="21"/>
<wire x1="7.6" y1="4" x2="7.6" y2="-4" width="0.2032" layer="21"/>
<wire x1="7.6" y1="-4" x2="7.1" y2="-4" width="0.2032" layer="21"/>
<wire x1="-0.3" y1="4" x2="0.3" y2="4" width="0.2032" layer="21"/>
<wire x1="-2.8" y1="4" x2="-2.2" y2="4" width="0.2032" layer="21"/>
<wire x1="-5.4" y1="4" x2="-4.8" y2="4" width="0.2032" layer="21"/>
<wire x1="2.3" y1="4" x2="2.9" y2="4" width="0.2032" layer="21"/>
<wire x1="4.8" y1="4" x2="5.4" y2="4" width="0.2032" layer="21"/>
<wire x1="4.8" y1="-4" x2="5.4" y2="-4" width="0.2032" layer="21"/>
<wire x1="2.2" y1="-4" x2="2.8" y2="-4" width="0.2032" layer="21"/>
<wire x1="-0.3" y1="-4" x2="0.3" y2="-4" width="0.2032" layer="21"/>
<wire x1="-2.8" y1="-4" x2="-2.2" y2="-4" width="0.2032" layer="21"/>
<wire x1="-5.4" y1="-4" x2="-4.8" y2="-4" width="0.2032" layer="21"/>
<smd name="6" x="6.35" y="-4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="5" x="3.81" y="-4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="4" x="1.27" y="-4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="3" x="-1.27" y="-4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="2" x="-3.81" y="-4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="1" x="-6.35" y="-4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="A1" x="-6.35" y="4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="A2" x="-3.81" y="4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="A3" x="-1.27" y="4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="A4" x="1.27" y="4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="A5" x="3.81" y="4.62" dx="1.02" dy="2.16" layer="1"/>
<smd name="A6" x="6.35" y="4.62" dx="1.02" dy="2.16" layer="1"/>
</package>
<package name="1X06-SMD-FEMALE-V2">
<description>Package for 4UCONN part #19686 *UNPROVEN*</description>
<wire x1="-7.5" y1="0.45" x2="-7.5" y2="-8.05" width="0.127" layer="21"/>
<wire x1="7.5" y1="0.45" x2="-7.5" y2="0.45" width="0.127" layer="21"/>
<wire x1="7.5" y1="-8.05" x2="7.5" y2="0.45" width="0.127" layer="21"/>
<wire x1="-7.5" y1="-8.05" x2="7.5" y2="-8.05" width="0.127" layer="21"/>
<smd name="4" x="-1.27" y="3.425" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="5" x="-3.81" y="3.425" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="6" x="-6.35" y="3.425" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="3" x="1.27" y="3.425" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="2" x="3.81" y="3.425" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="1" x="6.35" y="3.425" dx="1.25" dy="3" layer="1" rot="R180"/>
<text x="7.6" y="-8.3" size="1" layer="27" rot="R180">&gt;Value</text>
<text x="-7.4" y="-9.3" size="1" layer="25">&gt;Name</text>
</package>
<package name="1X06_HOLES_ONLY">
<circle x="0" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="2.54" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="5.08" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="7.62" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="10.16" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="12.7" y="0" radius="0.635" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<hole x="0" y="0" drill="1.4732"/>
<hole x="2.54" y="0" drill="1.4732"/>
<hole x="5.08" y="0" drill="1.4732"/>
<hole x="7.62" y="0" drill="1.4732"/>
<hole x="10.16" y="0" drill="1.4732"/>
<hole x="12.7" y="0" drill="1.4732"/>
</package>
<package name="1X06_SMD_STRAIGHT">
<wire x1="1.37" y1="1.25" x2="-14.07" y2="1.25" width="0.127" layer="51"/>
<wire x1="-14.07" y1="1.25" x2="-14.07" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-14.07" y1="-1.25" x2="1.37" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.37" y1="-1.25" x2="1.37" y2="1.25" width="0.127" layer="51"/>
<wire x1="1.37" y1="1.25" x2="1.37" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-14.07" y1="-1.25" x2="-14.07" y2="1.25" width="0.127" layer="21"/>
<wire x1="0.85" y1="1.25" x2="1.37" y2="1.25" width="0.127" layer="21"/>
<wire x1="-14.07" y1="1.25" x2="-10.883" y2="1.25" width="0.127" layer="21"/>
<wire x1="-13.55" y1="-1.25" x2="-14.07" y2="-1.25" width="0.127" layer="21"/>
<wire x1="1.37" y1="-1.25" x2="-1.817" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-4.377" y1="1.25" x2="-0.703" y2="1.25" width="0.127" layer="21"/>
<wire x1="-9.457" y1="1.25" x2="-5.783" y2="1.25" width="0.127" layer="21"/>
<wire x1="-3.329" y1="-1.25" x2="-6.831" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-8.409" y1="-1.25" x2="-11.911" y2="-1.25" width="0.127" layer="21"/>
<smd name="5" x="-10.16" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="3" x="-5.08" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="1" x="0" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="6" x="-12.7" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="4" x="-7.62" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-2.54" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<text x="-13.97" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-13.97" y="-4.191" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="1X06_SMD_STRAIGHT_ALT">
<wire x1="1.37" y1="1.25" x2="-14.07" y2="1.25" width="0.127" layer="51"/>
<wire x1="-14.07" y1="1.25" x2="-14.07" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-14.07" y1="-1.25" x2="1.37" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.37" y1="-1.25" x2="1.37" y2="1.25" width="0.127" layer="51"/>
<wire x1="-14.07" y1="1.25" x2="-14.07" y2="-1.25" width="0.127" layer="21"/>
<wire x1="1.37" y1="-1.25" x2="1.37" y2="1.25" width="0.127" layer="21"/>
<wire x1="-13.55" y1="1.25" x2="-14.07" y2="1.25" width="0.127" layer="21"/>
<wire x1="1.37" y1="1.25" x2="-1.817" y2="1.25" width="0.127" layer="21"/>
<wire x1="0.85" y1="-1.25" x2="1.37" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-14.07" y1="-1.25" x2="-10.883" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-8.323" y1="1.25" x2="-11.997" y2="1.25" width="0.127" layer="21"/>
<wire x1="-3.243" y1="1.25" x2="-6.917" y2="1.25" width="0.127" layer="21"/>
<wire x1="-9.371" y1="-1.25" x2="-5.869" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-4.291" y1="-1.25" x2="-0.789" y2="-1.25" width="0.127" layer="21"/>
<smd name="5" x="-10.16" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="3" x="-5.08" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1" x="0" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="6" x="-12.7" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="4" x="-7.62" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="2" x="-2.54" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<text x="-13.97" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-13.97" y="-4.191" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="1X06_SMD_STRAIGHT_COMBO">
<wire x1="12.7" y1="1.27" x2="12.7" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="10.16" y1="1.27" x2="10.16" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="7.62" y1="1.27" x2="7.62" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="5.08" y1="1.27" x2="5.08" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="-1.37" y1="-1.25" x2="-1.37" y2="1.25" width="0.1778" layer="21"/>
<wire x1="14.07" y1="1.25" x2="14.07" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="-0.73" y1="-1.25" x2="-1.37" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="14.07" y1="-1.25" x2="13.4" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="13.4" y1="1.25" x2="14.07" y2="1.25" width="0.1778" layer="21"/>
<wire x1="-1.37" y1="1.25" x2="-0.73" y2="1.25" width="0.1778" layer="21"/>
<wire x1="10.949" y1="1.25" x2="11.911" y2="1.25" width="0.1778" layer="21"/>
<wire x1="10.949" y1="-1.29" x2="11.911" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="8.409" y1="1.25" x2="9.371" y2="1.25" width="0.1778" layer="21"/>
<wire x1="8.409" y1="-1.29" x2="9.371" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="5.869" y1="-1.29" x2="6.831" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="5.869" y1="1.25" x2="6.831" y2="1.25" width="0.1778" layer="21"/>
<wire x1="3.329" y1="-1.29" x2="4.291" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="3.329" y1="1.25" x2="4.291" y2="1.25" width="0.1778" layer="21"/>
<wire x1="0.789" y1="-1.29" x2="1.751" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="0.789" y1="1.25" x2="1.751" y2="1.25" width="0.1778" layer="21"/>
<smd name="5" x="10.16" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="3" x="5.08" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1" x="0" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="6" x="12.7" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="4" x="7.62" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="2" x="2.54" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1-2" x="0" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="2-2" x="2.54" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="3-2" x="5.08" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="4-2" x="7.62" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="5-2" x="10.16" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="6-2" x="12.7" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<text x="0" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="-4.191" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="EM406">
<wire x1="0" y1="-2.921" x2="5.08" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-1.778" y1="-0.762" x2="-1.778" y2="0.635" width="0.254" layer="21"/>
<wire x1="5.842" y1="0.635" x2="6.858" y2="0.635" width="0.254" layer="21"/>
<wire x1="6.858" y1="0.635" x2="6.858" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-1.778" y1="0.635" x2="-0.762" y2="0.635" width="0.254" layer="21"/>
<circle x="0" y="1.27" radius="0.1047" width="0.4064" layer="21"/>
<smd name="P$1" x="-1.3" y="-2.225" dx="1.2" dy="1.8" layer="1"/>
<smd name="P$2" x="6.3" y="-2.225" dx="1.2" dy="1.8" layer="1"/>
<smd name="1" x="0" y="0" dx="0.6" dy="1.55" layer="1"/>
<smd name="2" x="1" y="0" dx="0.6" dy="1.55" layer="1"/>
<smd name="3" x="2" y="0" dx="0.6" dy="1.55" layer="1"/>
<smd name="4" x="3" y="0" dx="0.6" dy="1.55" layer="1"/>
<smd name="5" x="4" y="0" dx="0.6" dy="1.55" layer="1"/>
<smd name="6" x="5" y="0" dx="0.6" dy="1.55" layer="1"/>
<text x="1.27" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="1.27" y="-3.81" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="BTA">
<description>British Telecom connector, used for Vernier sensors (analog)</description>
<pad name="1" x="-3.175" y="-3.81" drill="0.8" shape="square"/>
<pad name="3" x="-5.715" y="-3.81" drill="0.8" shape="square"/>
<pad name="5" x="-8.255" y="-3.81" drill="0.8" shape="square"/>
<pad name="2" x="-4.445" y="-7.62" drill="0.8" shape="square"/>
<pad name="4" x="-6.985" y="-7.62" drill="0.8" shape="square"/>
<pad name="6" x="-9.525" y="-7.62" drill="0.8" shape="square"/>
<wire x1="0" y1="0" x2="0" y2="-10.795" width="0.127" layer="21"/>
<wire x1="0" y1="-10.795" x2="0" y2="-14.605" width="0.127" layer="21"/>
<wire x1="0" y1="-14.605" x2="-15.875" y2="-14.605" width="0.127" layer="21"/>
<wire x1="-15.875" y1="-14.605" x2="-15.875" y2="-10.795" width="0.127" layer="21"/>
<wire x1="-15.875" y1="-10.795" x2="-15.875" y2="0" width="0.127" layer="21"/>
<wire x1="-15.875" y1="0" x2="0" y2="0" width="0.127" layer="21"/>
<wire x1="0" y1="-14.605" x2="3.175" y2="-14.605" width="0.127" layer="21"/>
<wire x1="3.175" y1="-14.605" x2="3.175" y2="-12.7" width="0.127" layer="21"/>
<wire x1="3.175" y1="-12.7" x2="1.905" y2="-12.7" width="0.127" layer="21"/>
<wire x1="1.905" y1="-12.7" x2="0" y2="-10.795" width="0.127" layer="21"/>
<wire x1="-15.875" y1="-14.605" x2="-19.05" y2="-14.605" width="0.127" layer="21"/>
<wire x1="-19.05" y1="-14.605" x2="-19.05" y2="-12.7" width="0.127" layer="21"/>
<wire x1="-19.05" y1="-12.7" x2="-17.78" y2="-12.7" width="0.127" layer="21"/>
<wire x1="-17.78" y1="-12.7" x2="-15.875" y2="-10.795" width="0.127" layer="21"/>
<text x="-10.16" y="-6.35" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="BTD">
<description>British Telecom connector, used for Vernier sensors (digital)</description>
<pad name="1" x="-6.35" y="-3.81" drill="0.8" shape="square"/>
<pad name="3" x="-8.89" y="-3.81" drill="0.8" shape="square"/>
<pad name="5" x="-11.43" y="-3.81" drill="0.8" shape="square"/>
<pad name="2" x="-7.62" y="-7.62" drill="0.8" shape="square"/>
<pad name="4" x="-10.16" y="-7.62" drill="0.8" shape="square"/>
<pad name="6" x="-12.7" y="-7.62" drill="0.8" shape="square"/>
<wire x1="0" y1="0" x2="0" y2="-10.795" width="0.127" layer="21"/>
<wire x1="0" y1="-10.795" x2="0" y2="-14.605" width="0.127" layer="21"/>
<wire x1="0" y1="-14.605" x2="-15.875" y2="-14.605" width="0.127" layer="21"/>
<wire x1="-15.875" y1="-14.605" x2="-15.875" y2="-10.795" width="0.127" layer="21"/>
<wire x1="-15.875" y1="-10.795" x2="-15.875" y2="0" width="0.127" layer="21"/>
<wire x1="-15.875" y1="0" x2="0" y2="0" width="0.127" layer="21"/>
<wire x1="0" y1="-14.605" x2="3.175" y2="-14.605" width="0.127" layer="21"/>
<wire x1="3.175" y1="-14.605" x2="3.175" y2="-12.7" width="0.127" layer="21"/>
<wire x1="3.175" y1="-12.7" x2="1.905" y2="-12.7" width="0.127" layer="21"/>
<wire x1="1.905" y1="-12.7" x2="0" y2="-10.795" width="0.127" layer="21"/>
<wire x1="-15.875" y1="-14.605" x2="-19.05" y2="-14.605" width="0.127" layer="21"/>
<wire x1="-19.05" y1="-14.605" x2="-19.05" y2="-12.7" width="0.127" layer="21"/>
<wire x1="-19.05" y1="-12.7" x2="-17.78" y2="-12.7" width="0.127" layer="21"/>
<wire x1="-17.78" y1="-12.7" x2="-15.875" y2="-10.795" width="0.127" layer="21"/>
<text x="-13.335" y="-6.35" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="1X06_SMD_MALE">
<text x="-1" y="3.321" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1" y="-4.591" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-1.27" y1="1.25" x2="-1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="13.97" y2="-1.25" width="0.127" layer="51"/>
<wire x1="13.97" y1="-1.25" x2="13.97" y2="1.25" width="0.127" layer="51"/>
<wire x1="13.97" y1="1.25" x2="-1.27" y2="1.25" width="0.127" layer="51"/>
<circle x="0" y="0" radius="0.64" width="0.127" layer="51"/>
<circle x="2.54" y="0" radius="0.64" width="0.127" layer="51"/>
<circle x="5.08" y="0" radius="0.64" width="0.127" layer="51"/>
<circle x="7.62" y="0" radius="0.64" width="0.127" layer="51"/>
<circle x="10.16" y="0" radius="0.64" width="0.127" layer="51"/>
<circle x="12.7" y="0" radius="0.64" width="0.127" layer="51"/>
<rectangle x1="-0.32" y1="0" x2="0.32" y2="2.75" layer="51"/>
<rectangle x1="4.76" y1="0" x2="5.4" y2="2.75" layer="51"/>
<rectangle x1="9.84" y1="0" x2="10.48" y2="2.75" layer="51"/>
<rectangle x1="2.22" y1="-2.75" x2="2.86" y2="0" layer="51" rot="R180"/>
<rectangle x1="7.3" y1="-2.75" x2="7.94" y2="0" layer="51" rot="R180"/>
<rectangle x1="12.38" y1="-2.75" x2="13.02" y2="0" layer="51" rot="R180"/>
<smd name="1" x="0" y="0" dx="1.02" dy="6" layer="1"/>
<smd name="2" x="2.54" y="0" dx="1.02" dy="6" layer="1"/>
<smd name="3" x="5.08" y="0" dx="1.02" dy="6" layer="1"/>
<smd name="4" x="7.62" y="0" dx="1.02" dy="6" layer="1"/>
<smd name="5" x="10.16" y="0" dx="1.02" dy="6" layer="1"/>
<smd name="6" x="12.7" y="0" dx="1.02" dy="6" layer="1"/>
<wire x1="-1.27" y1="1.25" x2="-1.27" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="-1.27" y1="-1.25" x2="-0.635" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="-1.27" y1="1.25" x2="-0.635" y2="1.25" width="0.1778" layer="21"/>
<wire x1="0.762" y1="1.25" x2="1.778" y2="1.25" width="0.1778" layer="21"/>
<wire x1="3.302" y1="1.25" x2="4.318" y2="1.25" width="0.1778" layer="21"/>
<wire x1="5.842" y1="1.25" x2="6.858" y2="1.25" width="0.1778" layer="21"/>
<wire x1="8.382" y1="1.25" x2="9.398" y2="1.25" width="0.1778" layer="21"/>
<wire x1="10.922" y1="1.25" x2="11.938" y2="1.25" width="0.1778" layer="21"/>
<wire x1="1.778" y1="-1.25" x2="0.762" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="4.318" y1="-1.25" x2="3.302" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="6.858" y1="-1.25" x2="5.842" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="9.398" y1="-1.25" x2="8.382" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="11.938" y1="-1.25" x2="10.922" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="13.97" y1="-1.25" x2="13.97" y2="1.25" width="0.1778" layer="21"/>
<wire x1="13.97" y1="-1.25" x2="13.335" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="13.97" y1="1.25" x2="13.335" y2="1.25" width="0.1778" layer="21"/>
</package>
<package name="EVQPJ">
<description>&lt;B&gt;Panasonic EVQ-PJC tact switch&lt;/B&gt;</description>
<wire x1="1.29" y1="1.115" x2="5.21" y2="1.115" width="0.127" layer="21"/>
<wire x1="5.21" y1="1.115" x2="5.21" y2="-1.115" width="0.127" layer="21"/>
<wire x1="5.21" y1="-1.115" x2="1.29" y2="-1.115" width="0.127" layer="21"/>
<wire x1="1.29" y1="-1.115" x2="1.29" y2="1.115" width="0.127" layer="21"/>
<pad name="A" x="0" y="0" drill="1.3" diameter="2.1844" shape="octagon"/>
<pad name="B" x="6.5" y="0" drill="1.3" diameter="2.1844" shape="octagon"/>
<text x="-1" y="2" size="1.27" layer="25">&gt;NAME</text>
<text x="-1" y="-3.5" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="2.2225" y1="0.635" x2="2.2225" y2="-0.635" width="0.127" layer="21"/>
<wire x1="2.2225" y1="-0.635" x2="4.1275" y2="-0.635" width="0.127" layer="21"/>
<wire x1="4.1275" y1="-0.635" x2="4.1275" y2="0.635" width="0.127" layer="21"/>
<wire x1="4.1275" y1="0.635" x2="2.2225" y2="0.635" width="0.127" layer="21"/>
</package>
<package name="6MMTACTTILE_RECT_SMD">
<smd name="P$1" x="-4.6" y="0" dx="1.6" dy="2.19" layer="1" rot="R90"/>
<smd name="P$2" x="4.6" y="0" dx="1.6" dy="2.19" layer="1" rot="R90"/>
<wire x1="-3" y1="1.75" x2="3" y2="1.75" width="0.127" layer="21"/>
<wire x1="3" y1="1.75" x2="3" y2="-1.75" width="0.127" layer="21"/>
<wire x1="-3" y1="1.75" x2="-3" y2="-1.75" width="0.127" layer="21"/>
<wire x1="-3" y1="-1.75" x2="3" y2="-1.75" width="0.127" layer="21"/>
<wire x1="-1.5" y1="0.76" x2="1.5" y2="0.76" width="0.127" layer="21"/>
<wire x1="1.5" y1="0.76" x2="1.5" y2="-0.76" width="0.127" layer="21"/>
<wire x1="1.5" y1="-0.76" x2="-1.5" y2="-0.76" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-0.76" x2="-1.5" y2="0.76" width="0.127" layer="21"/>
<text x="-3" y="2.5" size="1.27" layer="25">&gt;NAME</text>
<text x="-3" y="-3.5" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="TACT_2PIN">
<pad name="1" x="0" y="0" drill="1" diameter="1.778"/>
<pad name="2" x="5" y="0" drill="1" diameter="1.778"/>
<circle x="2.5" y="0" radius="1.270628125" width="0.127" layer="21"/>
<circle x="2.5" y="0" radius="3" width="0.127" layer="51"/>
<text x="0" y="-3" size="1.27" layer="27">&gt;NAME</text>
</package>
<package name="SKHH-SIDE">
<pad name="1" x="-2.25" y="2.66" drill="1" diameter="1.778"/>
<pad name="2" x="2.25" y="2.55" drill="1" diameter="1.778"/>
<pad name="P$1" x="-3.5" y="5.05" drill="1.3" diameter="2.1844"/>
<pad name="P$2" x="3.5" y="5.05" drill="1.3" diameter="2.1844"/>
<wire x1="-3.75" y1="0" x2="-1.5" y2="0" width="0.127" layer="21"/>
<wire x1="-1.5" y1="0" x2="1.5" y2="0" width="0.127" layer="21"/>
<wire x1="1.5" y1="0" x2="3.75" y2="0" width="0.127" layer="21"/>
<wire x1="3.7" y1="6.5" x2="-3.7" y2="6.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="0" x2="-1.5" y2="-1" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-1" x2="1.5" y2="-1" width="0.127" layer="51"/>
<wire x1="1.5" y1="-1" x2="1.5" y2="0" width="0.127" layer="51"/>
<wire x1="-3.75" y1="3.5" x2="-3.75" y2="0" width="0.127" layer="21"/>
<wire x1="3.75" y1="0" x2="3.75" y2="3.5" width="0.127" layer="21"/>
<text x="-3.175" y="6.985" size="1.27" layer="25" ratio="14">&gt;NAME</text>
</package>
<package name="TACTILE_SWITCH_SMD">
<wire x1="-1.54" y1="-2.54" x2="-2.54" y2="-1.54" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-1.24" x2="-2.54" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="1.54" x2="-1.54" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-1.54" y1="2.54" x2="1.54" y2="2.54" width="0.2032" layer="21"/>
<wire x1="1.54" y1="2.54" x2="2.54" y2="1.54" width="0.2032" layer="51"/>
<wire x1="2.54" y1="1.24" x2="2.54" y2="-1.24" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.54" x2="1.54" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="1.54" y1="-2.54" x2="-1.54" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="1.905" y2="0.445" width="0.127" layer="51"/>
<wire x1="1.905" y1="0.445" x2="2.16" y2="-0.01" width="0.127" layer="51"/>
<wire x1="1.905" y1="-0.23" x2="1.905" y2="-1.115" width="0.127" layer="51"/>
<circle x="0" y="0" radius="1.27" width="0.2032" layer="21"/>
<smd name="1" x="-2.54" y="1.905" dx="0.762" dy="1.524" layer="1" rot="R90"/>
<smd name="2" x="2.54" y="1.905" dx="0.762" dy="1.524" layer="1" rot="R90"/>
<smd name="3" x="-2.54" y="-1.905" dx="0.762" dy="1.524" layer="1" rot="R90"/>
<smd name="4" x="2.54" y="-1.905" dx="0.762" dy="1.524" layer="1" rot="R90"/>
<text x="-0.889" y="1.778" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.889" y="-2.032" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="TACTILE-PTH">
<description>&lt;b&gt;OMRON SWITCH&lt;/b&gt;</description>
<wire x1="3.048" y1="1.016" x2="3.048" y2="2.54" width="0.2032" layer="51"/>
<wire x1="3.048" y1="2.54" x2="2.54" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.54" y1="-3.048" x2="3.048" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="3.048" y1="-2.54" x2="3.048" y2="-1.016" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="3.048" x2="-3.048" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-3.048" y1="2.54" x2="-3.048" y2="1.016" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-3.048" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-3.048" y1="-2.54" x2="-3.048" y2="-1.016" width="0.2032" layer="51"/>
<wire x1="2.54" y1="-3.048" x2="2.159" y2="-3.048" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-2.159" y2="-3.048" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="3.048" x2="-2.159" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.54" y1="3.048" x2="2.159" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.159" y1="3.048" x2="-2.159" y2="3.048" width="0.2032" layer="21"/>
<wire x1="-2.159" y1="-3.048" x2="2.159" y2="-3.048" width="0.2032" layer="21"/>
<wire x1="3.048" y1="0.998" x2="3.048" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="-3.048" y1="1.028" x2="-3.048" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="0.508" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-0.508" x2="-2.54" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="0.508" x2="-2.159" y2="-0.381" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.778" width="0.2032" layer="21"/>
<pad name="1" x="-3.2512" y="2.2606" drill="1.016" diameter="1.8796"/>
<pad name="2" x="3.2512" y="2.2606" drill="1.016" diameter="1.8796"/>
<pad name="3" x="-3.2512" y="-2.2606" drill="1.016" diameter="1.8796"/>
<pad name="4" x="3.2512" y="-2.2606" drill="1.016" diameter="1.8796"/>
<text x="-2.54" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
</package>
<package name="KSA_SEALED_TAC_SWITCH">
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.127" layer="21"/>
<wire x1="-5.08" y1="3.81" x2="5.08" y2="3.81" width="0.127" layer="21"/>
<wire x1="5.08" y1="3.81" x2="5.08" y2="-3.81" width="0.127" layer="21"/>
<wire x1="5.08" y1="-3.81" x2="-5.08" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-3.81" x2="-5.08" y2="3.81" width="0.127" layer="21"/>
<pad name="P$1" x="-3.81" y="2.54" drill="1" shape="square"/>
<pad name="P$2" x="3.81" y="2.54" drill="1" shape="square"/>
<pad name="P$3" x="-3.81" y="-2.54" drill="1" shape="square"/>
<pad name="P$4" x="3.81" y="-2.54" drill="1" shape="square"/>
</package>
<package name="TACT_SW_12MM">
<wire x1="5" y1="-1.3" x2="5" y2="-0.7" width="0.2032" layer="51"/>
<wire x1="5" y1="-0.7" x2="4.5" y2="-0.2" width="0.2032" layer="51"/>
<wire x1="5" y1="0.2" x2="5" y2="1" width="0.2032" layer="51"/>
<wire x1="-6" y1="4" x2="-6" y2="5" width="0.2032" layer="21"/>
<wire x1="-5" y1="6" x2="5" y2="6" width="0.2032" layer="21"/>
<wire x1="6" y1="5" x2="6" y2="4" width="0.2032" layer="21"/>
<wire x1="6" y1="1" x2="6" y2="-1" width="0.2032" layer="21"/>
<wire x1="6" y1="-4" x2="6" y2="-5" width="0.2032" layer="21"/>
<wire x1="5" y1="-6" x2="-5" y2="-6" width="0.2032" layer="21"/>
<wire x1="-6" y1="-5" x2="-6" y2="-4" width="0.2032" layer="21"/>
<wire x1="-6" y1="-1" x2="-6" y2="1" width="0.2032" layer="21"/>
<wire x1="-6" y1="5" x2="-5" y2="6" width="0.2032" layer="21" curve="-90"/>
<wire x1="5" y1="6" x2="6" y2="5" width="0.2032" layer="21" curve="-90"/>
<wire x1="6" y1="-5" x2="5" y2="-6" width="0.2032" layer="21" curve="-90"/>
<wire x1="-5" y1="-6" x2="-6" y2="-5" width="0.2032" layer="21" curve="-90"/>
<circle x="0" y="0" radius="3.5" width="0.2032" layer="21"/>
<circle x="-4.5" y="4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="4.5" y="4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="4.5" y="-4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="-4.5" y="-4.5" radius="0.3" width="0.7" layer="21"/>
<pad name="4" x="-6.25" y="2.5" drill="1.2" diameter="2.159"/>
<pad name="2" x="-6.25" y="-2.5" drill="1.2" diameter="2.159"/>
<pad name="1" x="6.25" y="-2.5" drill="1.2" diameter="2.159"/>
<pad name="3" x="6.25" y="2.5" drill="1.2" diameter="2.159"/>
</package>
<package name="LED5MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, round</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796" shape="square"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED3MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
3 mm, round</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="21" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="21" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="21" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="21" curve="60.255215" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822" cap="flat"/>
<wire x1="1.5748" y1="1.2954" x2="1.5748" y2="0.7874" width="0.254" layer="21"/>
<wire x1="1.5748" y1="-1.2954" x2="1.5748" y2="-0.8382" width="0.254" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796"/>
<text x="1.905" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.905" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<wire x1="-3.81" y1="0" x2="-2.54" y2="0" width="0.3048" layer="21"/>
<wire x1="-3.175" y1="0.635" x2="-3.175" y2="-0.635" width="0.3048" layer="21"/>
</package>
<package name="LED-1206">
<wire x1="-1" y1="1" x2="-2.4" y2="1" width="0.2032" layer="21"/>
<wire x1="-2.4" y1="1" x2="-2.4" y2="-1" width="0.2032" layer="21"/>
<wire x1="-2.4" y1="-1" x2="-1" y2="-1" width="0.2032" layer="21"/>
<wire x1="1" y1="1" x2="2.4" y2="1" width="0.2032" layer="21"/>
<wire x1="2.4" y1="1" x2="2.4" y2="-1" width="0.2032" layer="21"/>
<wire x1="2.4" y1="-1" x2="1" y2="-1" width="0.2032" layer="21"/>
<wire x1="0.3" y1="0.7" x2="0.3" y2="0" width="0.2032" layer="21"/>
<wire x1="0.3" y1="0" x2="0.3" y2="-0.7" width="0.2032" layer="21"/>
<wire x1="0.3" y1="0" x2="-0.3" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.3" y1="0.6" x2="-0.3" y2="-0.6" width="0.2032" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="0" width="0.2032" layer="21"/>
<smd name="A" x="-1.5" y="0" dx="1.2" dy="1.4" layer="1"/>
<smd name="C" x="1.5" y="0" dx="1.2" dy="1.4" layer="1"/>
<text x="-0.889" y="1.397" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.778" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="LED-0603">
<wire x1="0.46" y1="0.17" x2="0" y2="0.17" width="0.2032" layer="21"/>
<wire x1="-0.46" y1="0.17" x2="0" y2="0.17" width="0.2032" layer="21"/>
<wire x1="0" y1="0.17" x2="0.2338" y2="-0.14" width="0.2032" layer="21"/>
<wire x1="-0.0254" y1="0.1546" x2="-0.2184" y2="-0.14" width="0.2032" layer="21"/>
<smd name="C" x="0" y="0.75" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0" y="-0.75" dx="0.8" dy="0.8" layer="1"/>
<text x="-0.6985" y="-0.889" size="0.4064" layer="25" rot="R90">&gt;NAME</text>
<text x="1.0795" y="-1.016" size="0.4064" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="LED10MM">
<wire x1="-5" y1="-2" x2="-5" y2="2" width="0.2032" layer="21" curve="316.862624"/>
<wire x1="-5" y1="2" x2="-5" y2="-2" width="0.2032" layer="21"/>
<pad name="A" x="2.54" y="0" drill="2.4" diameter="3.7"/>
<pad name="C" x="-2.54" y="0" drill="2.4" diameter="3.7" shape="square"/>
<text x="2.159" y="2.54" size="1.016" layer="51" ratio="15">L</text>
<text x="-2.921" y="2.54" size="1.016" layer="51" ratio="15">S</text>
</package>
<package name="FKIT-LED-1206">
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="-0.75" x2="-1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="0.75" x2="1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="-0.5" x2="0.55" y2="-0.5" width="0.1016" layer="21" curve="95.452622"/>
<wire x1="-0.55" y1="0.5" x2="-0.55" y2="-0.5" width="0.1016" layer="51" curve="84.547378"/>
<wire x1="0.55" y1="0.5" x2="-0.55" y2="0.5" width="0.1016" layer="21" curve="95.452622"/>
<wire x1="0.55" y1="-0.5" x2="0.55" y2="0.5" width="0.1016" layer="51" curve="84.547378"/>
<smd name="A" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="C" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1" y1="-0.1" x2="0.1" y2="0.1" layer="21"/>
<rectangle x1="0.45" y1="-0.7" x2="0.8" y2="-0.45" layer="51"/>
<rectangle x1="0.8" y1="-0.7" x2="0.9" y2="0.5" layer="51"/>
<rectangle x1="0.8" y1="0.55" x2="0.9" y2="0.7" layer="51"/>
<rectangle x1="-0.9" y1="-0.7" x2="-0.8" y2="0.5" layer="51"/>
<rectangle x1="-0.9" y1="0.55" x2="-0.8" y2="0.7" layer="51"/>
<rectangle x1="0.45" y1="-0.7" x2="0.6" y2="-0.45" layer="21"/>
</package>
<package name="LED3MM-NS">
<description>&lt;h3&gt;LED 3MM - No Silk&lt;/h3&gt;
3 mm, round</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="51" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="51" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="51" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="51" curve="60.255215" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822" cap="flat"/>
<wire x1="1.5748" y1="1.2954" x2="1.5748" y2="0.7874" width="0.254" layer="51"/>
<wire x1="1.5748" y1="-1.2954" x2="1.5748" y2="-0.8382" width="0.254" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128"/>
<pad name="K" x="1.27" y="0" drill="0.8128"/>
<text x="1.905" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.905" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED5MM-KIT">
<description>&lt;h3&gt;LED5MM-KIT&lt;/h3&gt;
5MM Through-hole LED&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796" stop="no"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796" stop="no"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<polygon width="0.127" layer="30">
<vertex x="-1.2675" y="-0.9525" curve="-90"/>
<vertex x="-2.2224" y="-0.0228" curve="-90.011749"/>
<vertex x="-1.27" y="0.9526" curve="-90"/>
<vertex x="-0.32" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1.27" y="-0.4445" curve="-90.012891"/>
<vertex x="-1.7145" y="-0.0203" curve="-90"/>
<vertex x="-1.27" y="0.447" curve="-90"/>
<vertex x="-0.8281" y="-0.0101" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.2725" y="-0.9525" curve="-90"/>
<vertex x="0.3176" y="-0.0228" curve="-90.011749"/>
<vertex x="1.27" y="0.9526" curve="-90"/>
<vertex x="2.22" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1.27" y="-0.4445" curve="-90.012891"/>
<vertex x="0.8255" y="-0.0203" curve="-90"/>
<vertex x="1.27" y="0.447" curve="-90"/>
<vertex x="1.7119" y="-0.0101" curve="-90.012967"/>
</polygon>
</package>
<package name="LED-TLP">
<pad name="ANODE" x="-1.27" y="0" drill="0.8" diameter="1.4224" shape="square"/>
<pad name="CATHODE" x="1.27" y="0" drill="0.8" diameter="1.4224"/>
<wire x1="-2.3" y1="0.9" x2="2.3" y2="0.9" width="0.127" layer="21"/>
<wire x1="2.3" y1="0.9" x2="2.3" y2="-0.9" width="0.127" layer="21"/>
<wire x1="2.3" y1="-0.9" x2="1.6" y2="-0.9" width="0.127" layer="21"/>
<wire x1="1.6" y1="-0.9" x2="-1.6" y2="-0.9" width="0.127" layer="21"/>
<wire x1="-1.6" y1="-0.9" x2="-2.3" y2="-0.9" width="0.127" layer="21"/>
<wire x1="-2.3" y1="-0.9" x2="-2.3" y2="0.9" width="0.127" layer="21"/>
<wire x1="-0.5" y1="-2" x2="-1.6" y2="-0.9" width="0.127" layer="51" curve="-90"/>
<wire x1="-0.5" y1="-2" x2="0.5" y2="-2" width="0.127" layer="51"/>
<wire x1="0.5" y1="-2" x2="1.6" y2="-0.9" width="0.127" layer="51" curve="90"/>
<text x="-2.286" y="1.27" size="0.8128" layer="25" ratio="15">&gt;NAME</text>
</package>
<package name="TACT_SW_12MM_HOLE">
<wire x1="5" y1="-1.3" x2="5" y2="-0.7" width="0.2032" layer="51"/>
<wire x1="5" y1="-0.7" x2="4.5" y2="-0.2" width="0.2032" layer="51"/>
<wire x1="5" y1="0.2" x2="5" y2="1" width="0.2032" layer="51"/>
<wire x1="-6" y1="4" x2="-6" y2="5" width="0.2032" layer="21"/>
<wire x1="-5" y1="6" x2="5" y2="6" width="0.2032" layer="21"/>
<wire x1="6" y1="5" x2="6" y2="4" width="0.2032" layer="21"/>
<wire x1="6" y1="1" x2="6" y2="-1" width="0.2032" layer="21"/>
<wire x1="6" y1="-4" x2="6" y2="-5" width="0.2032" layer="21"/>
<wire x1="5" y1="-6" x2="-5" y2="-6" width="0.2032" layer="21"/>
<wire x1="-6" y1="-5" x2="-6" y2="-4" width="0.2032" layer="21"/>
<wire x1="-6" y1="-1" x2="-6" y2="1" width="0.2032" layer="21"/>
<wire x1="-6" y1="5" x2="-5" y2="6" width="0.2032" layer="21" curve="-90"/>
<wire x1="5" y1="6" x2="6" y2="5" width="0.2032" layer="21" curve="-90"/>
<wire x1="6" y1="-5" x2="5" y2="-6" width="0.2032" layer="21" curve="-90"/>
<wire x1="-5" y1="-6" x2="-6" y2="-5" width="0.2032" layer="21" curve="-90"/>
<circle x="0" y="0" radius="3.5" width="0.2032" layer="21"/>
<circle x="-4.5" y="4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="4.5" y="4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="4.5" y="-4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="-4.5" y="-4.5" radius="0.3" width="0.7" layer="21"/>
<pad name="4" x="-6.25" y="2.5" drill="1.2" diameter="2.159"/>
<pad name="2" x="-6.25" y="-2.5" drill="1.2" diameter="2.159"/>
<pad name="1" x="6.25" y="-2.5" drill="1.2" diameter="2.159"/>
<pad name="3" x="6.25" y="2.5" drill="1.2" diameter="2.159"/>
<hole x="0" y="4.5" drill="1.8"/>
<hole x="0" y="-4.5" drill="1.8"/>
</package>
<package name="LED_2X5">
<pad name="C" x="-1.27" y="0" drill="0.8" shape="square"/>
<pad name="A" x="1.27" y="0" drill="0.8"/>
<wire x1="-2.5" y1="1" x2="2.5" y2="1" width="0.127" layer="21"/>
<wire x1="2.5" y1="1" x2="2.5" y2="-1" width="0.127" layer="21"/>
<wire x1="2.5" y1="-1" x2="-2.5" y2="-1" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-1" x2="-2.5" y2="1" width="0.127" layer="21"/>
<text x="-2.54" y="1.27" size="1.27" layer="21" ratio="14">&gt;NAME</text>
</package>
<package name="1X04">
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="MOLEX-1X4">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="8.89" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="8.89" y1="-2.54" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<wire x1="7.62" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="7.62" y2="-1.27" width="0.127" layer="21"/>
<wire x1="7.62" y1="-1.27" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
</package>
<package name="SCREWTERMINAL-3.5MM-4">
<wire x1="-2.3" y1="3.4" x2="12.8" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.8" y1="3.4" x2="12.8" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-2.8" x2="12.8" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-3.6" x2="-2.3" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-3.6" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-2.8" x2="-2.3" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-2.8" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.35" x2="-2.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-1.35" x2="-2.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-2.35" x2="-2.3" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="12.8" y1="3.15" x2="13.2" y2="3.15" width="0.2032" layer="51"/>
<wire x1="13.2" y1="3.15" x2="13.2" y2="2.15" width="0.2032" layer="51"/>
<wire x1="13.2" y1="2.15" x2="12.8" y2="2.15" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="3.5" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="7" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="10.5" y="0" radius="0.425" width="0.001" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="3" x="7" y="0" drill="1.2" diameter="2.032"/>
<pad name="4" x="10.5" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X04-1.27MM">
<wire x1="-0.381" y1="-0.889" x2="0.381" y2="-0.889" width="0.127" layer="21"/>
<wire x1="0.381" y1="-0.889" x2="0.635" y2="-0.635" width="0.127" layer="21"/>
<wire x1="0.635" y1="-0.635" x2="0.889" y2="-0.889" width="0.127" layer="21"/>
<wire x1="0.889" y1="-0.889" x2="1.651" y2="-0.889" width="0.127" layer="21"/>
<wire x1="1.651" y1="-0.889" x2="1.905" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.905" y1="-0.635" x2="2.159" y2="-0.889" width="0.127" layer="21"/>
<wire x1="2.159" y1="-0.889" x2="2.921" y2="-0.889" width="0.127" layer="21"/>
<wire x1="2.921" y1="-0.889" x2="3.175" y2="-0.635" width="0.127" layer="21"/>
<wire x1="3.175" y1="-0.635" x2="3.429" y2="-0.889" width="0.127" layer="21"/>
<wire x1="3.429" y1="-0.889" x2="4.191" y2="-0.889" width="0.127" layer="21"/>
<wire x1="4.191" y1="0.889" x2="3.429" y2="0.889" width="0.127" layer="21"/>
<wire x1="3.429" y1="0.889" x2="3.175" y2="0.635" width="0.127" layer="21"/>
<wire x1="3.175" y1="0.635" x2="2.921" y2="0.889" width="0.127" layer="21"/>
<wire x1="2.921" y1="0.889" x2="2.159" y2="0.889" width="0.127" layer="21"/>
<wire x1="2.159" y1="0.889" x2="1.905" y2="0.635" width="0.127" layer="21"/>
<wire x1="1.905" y1="0.635" x2="1.651" y2="0.889" width="0.127" layer="21"/>
<wire x1="1.651" y1="0.889" x2="0.889" y2="0.889" width="0.127" layer="21"/>
<wire x1="0.889" y1="0.889" x2="0.635" y2="0.635" width="0.127" layer="21"/>
<wire x1="0.635" y1="0.635" x2="0.381" y2="0.889" width="0.127" layer="21"/>
<wire x1="0.381" y1="0.889" x2="-0.381" y2="0.889" width="0.127" layer="21"/>
<wire x1="-0.381" y1="0.889" x2="-0.889" y2="0.381" width="0.127" layer="21"/>
<wire x1="-0.889" y1="-0.381" x2="-0.381" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-0.889" y1="0.381" x2="-0.889" y2="-0.381" width="0.127" layer="21"/>
<wire x1="4.191" y1="0.889" x2="4.699" y2="0.381" width="0.127" layer="21"/>
<wire x1="4.699" y1="0.381" x2="4.699" y2="-0.381" width="0.127" layer="21"/>
<wire x1="4.699" y1="-0.381" x2="4.191" y2="-0.889" width="0.127" layer="21"/>
<pad name="4" x="3.81" y="0" drill="0.508" diameter="1"/>
<pad name="3" x="2.54" y="0" drill="0.508" diameter="1"/>
<pad name="2" x="1.27" y="0" drill="0.508" diameter="1"/>
<pad name="1" x="0" y="0" drill="0.508" diameter="1"/>
<text x="-0.508" y="1.27" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.508" y="-1.651" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X04_LOCK">
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" shape="square" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X04_LOCK_LONGPADS">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<wire x1="1.524" y1="-0.127" x2="1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="4.064" y1="-0.127" x2="3.556" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="6.604" y1="-0.127" x2="6.096" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.8636" x2="-0.9906" y2="1.143" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.1176" x2="-0.9906" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.127" x2="8.636" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.127" x2="8.89" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-1.1176" x2="8.6106" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.127" x2="8.89" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.8636" x2="8.6106" y2="1.143" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<text x="-1.27" y="1.778" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-3.302" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
<rectangle x1="7.3279" y1="-0.4191" x2="7.9121" y2="0.1651" layer="51" rot="R90"/>
</package>
<package name="MOLEX-1X4_LOCK">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="8.89" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="8.89" y1="-2.54" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<wire x1="7.62" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="7.62" y2="-1.27" width="0.127" layer="21"/>
<wire x1="7.62" y1="-1.27" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" diameter="1.8796"/>
</package>
<package name="1X04-SMD">
<wire x1="5.08" y1="1.25" x2="-5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="1.25" x2="-5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="-1.25" x2="-3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="5.08" y1="-1.25" x2="5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="-7.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="-1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-3.81" y2="-7.25" width="0.127" layer="51"/>
<smd name="4" x="3.81" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="3" x="1.27" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-1.27" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="1" x="-3.81" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<hole x="-2.54" y="0" drill="1.4"/>
<hole x="2.54" y="0" drill="1.4"/>
</package>
<package name="1X04_LONGPADS">
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<text x="-1.3462" y="2.4638" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X04_NO_SILK">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="JST-4-PTH">
<pad name="1" x="-3" y="0" drill="0.7" diameter="1.6"/>
<pad name="2" x="-1" y="0" drill="0.7" diameter="1.6"/>
<pad name="3" x="1" y="0" drill="0.7" diameter="1.6"/>
<pad name="4" x="3" y="0" drill="0.7" diameter="1.6"/>
<text x="-2.27" y="5.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-2.27" y="4" size="0.4064" layer="27">&gt;Value</text>
<text x="-3.4" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
<text x="0.7" y="0.9" size="0.8" layer="51">S</text>
<text x="2.7" y="0.9" size="0.8" layer="51">S</text>
<wire x1="-4.95" y1="-1.6" x2="-4.95" y2="6" width="0.2032" layer="21"/>
<wire x1="-4.95" y1="6" x2="4.95" y2="6" width="0.2032" layer="21"/>
<wire x1="4.95" y1="6" x2="4.95" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-4.95" y1="-1.6" x2="-4.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="4.95" y1="-1.6" x2="4.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-4.3" y1="-1.6" x2="-4.3" y2="0" width="0.2032" layer="21"/>
<wire x1="4.3" y1="-1.6" x2="4.3" y2="0" width="0.2032" layer="21"/>
</package>
<package name="SCREWTERMINAL-3.5MM-4_LOCK">
<wire x1="-2.3" y1="3.4" x2="12.8" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.8" y1="3.4" x2="12.8" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-2.8" x2="12.8" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-3.6" x2="-2.3" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-3.6" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-2.8" x2="-2.3" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-2.8" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.35" x2="-2.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-1.35" x2="-2.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-2.35" x2="-2.3" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="12.8" y1="3.15" x2="13.2" y2="3.15" width="0.2032" layer="51"/>
<wire x1="13.2" y1="3.15" x2="13.2" y2="2.15" width="0.2032" layer="51"/>
<wire x1="13.2" y1="2.15" x2="12.8" y2="2.15" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="3.5" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="7" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="10.5" y="0" radius="0.425" width="0.001" layer="51"/>
<pad name="1" x="-0.1778" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.6778" y="0" drill="1.2" diameter="2.032"/>
<pad name="3" x="6.8222" y="0" drill="1.2" diameter="2.032"/>
<pad name="4" x="10.6778" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X04-1MM-RA">
<wire x1="-1.5" y1="-4.6" x2="1.5" y2="-4.6" width="0.254" layer="21"/>
<wire x1="-3" y1="-2" x2="-3" y2="-0.35" width="0.254" layer="21"/>
<wire x1="2.25" y1="-0.35" x2="3" y2="-0.35" width="0.254" layer="21"/>
<wire x1="3" y1="-0.35" x2="3" y2="-2" width="0.254" layer="21"/>
<wire x1="-3" y1="-0.35" x2="-2.25" y2="-0.35" width="0.254" layer="21"/>
<circle x="-2.5" y="0.3" radius="0.1414" width="0.4" layer="21"/>
<smd name="NC2" x="-2.8" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="NC1" x="2.8" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="1" x="-1.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="2" x="-0.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="3" x="0.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="4" x="1.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<text x="-1.73" y="1.73" size="0.4064" layer="25" rot="R180">&gt;NAME</text>
<text x="3.46" y="1.73" size="0.4064" layer="27" rot="R180">&gt;VALUE</text>
</package>
<package name="1X04_SMD_STRAIGHT_COMBO">
<wire x1="7.62" y1="1.27" x2="7.62" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="5.08" y1="1.27" x2="5.08" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="-1.37" y1="-1.25" x2="-1.37" y2="1.25" width="0.1778" layer="21"/>
<wire x1="8.99" y1="1.25" x2="8.99" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="-0.73" y1="-1.25" x2="-1.37" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="8.99" y1="-1.25" x2="8.32" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="8.32" y1="1.25" x2="8.99" y2="1.25" width="0.1778" layer="21"/>
<wire x1="-1.37" y1="1.25" x2="-0.73" y2="1.25" width="0.1778" layer="21"/>
<wire x1="5.869" y1="-1.29" x2="6.831" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="5.869" y1="1.25" x2="6.831" y2="1.25" width="0.1778" layer="21"/>
<wire x1="3.329" y1="-1.29" x2="4.291" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="3.329" y1="1.25" x2="4.291" y2="1.25" width="0.1778" layer="21"/>
<wire x1="0.789" y1="-1.29" x2="1.751" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="0.789" y1="1.25" x2="1.751" y2="1.25" width="0.1778" layer="21"/>
<smd name="3" x="5.08" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1" x="0" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="4" x="7.62" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="2" x="2.54" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1-2" x="0" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="2-2" x="2.54" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="3-2" x="5.08" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="4-2" x="7.62" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<text x="0" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="-4.191" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="1X04-SMD_LONG">
<wire x1="5.08" y1="1.25" x2="-5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="1.25" x2="-5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="-1.25" x2="-3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="5.08" y1="-1.25" x2="5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="-7.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="-1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-3.81" y2="-7.25" width="0.127" layer="51"/>
<smd name="4" x="3.81" y="5.5" dx="4" dy="1" layer="1" rot="R90"/>
<smd name="3" x="1.27" y="5.5" dx="4" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-1.27" y="5.5" dx="4" dy="1" layer="1" rot="R90"/>
<smd name="1" x="-3.81" y="5.5" dx="4" dy="1" layer="1" rot="R90"/>
<hole x="-2.54" y="0" drill="1.4"/>
<hole x="2.54" y="0" drill="1.4"/>
</package>
<package name="JST-4-PTH-VERT">
<wire x1="-4.95" y1="-2.25" x2="-4.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-4.95" y1="2.25" x2="4.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="4.95" y1="-2.25" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-2.25" x2="-4.95" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="1" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.75" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="-1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="4.95" y1="2.25" x2="4.95" y2="-2.25" width="0.2032" layer="21"/>
<pad name="1" x="-3" y="-0.55" drill="0.7" diameter="1.6"/>
<pad name="2" x="-1" y="-0.55" drill="0.7" diameter="1.6"/>
<pad name="3" x="1" y="-0.55" drill="0.7" diameter="1.6"/>
<pad name="4" x="3" y="-0.55" drill="0.7" diameter="1.6"/>
<text x="-3" y="3" size="0.4064" layer="25">&gt;Name</text>
<text x="1" y="3" size="0.4064" layer="27">&gt;Value</text>
<text x="-1.4" y="0.75" size="1.27" layer="51">+</text>
<text x="0.6" y="0.75" size="1.27" layer="51">-</text>
<text x="2.7" y="0.95" size="0.8" layer="51">Y</text>
<text x="-3.3" y="0.95" size="0.8" layer="51">B</text>
</package>
<package name="1X04_SMD_RA_FEMALE">
<wire x1="-5.205" y1="4.25" x2="-5.205" y2="-4.25" width="0.1778" layer="21"/>
<wire x1="5.205" y1="4.25" x2="-5.205" y2="4.25" width="0.1778" layer="21"/>
<wire x1="5.205" y1="-4.25" x2="5.205" y2="4.25" width="0.1778" layer="21"/>
<wire x1="-5.205" y1="-4.25" x2="5.205" y2="-4.25" width="0.1778" layer="21"/>
<rectangle x1="-1.59" y1="6.8" x2="-0.95" y2="7.65" layer="51"/>
<rectangle x1="0.95" y1="6.8" x2="1.59" y2="7.65" layer="51"/>
<rectangle x1="-4.13" y1="6.8" x2="-3.49" y2="7.65" layer="51"/>
<smd name="3" x="1.27" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="2" x="-1.27" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="1" x="-3.81" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<text x="-4.425" y="2.775" size="1" layer="27">&gt;Value</text>
<text x="-4.225" y="-3.395" size="1" layer="25">&gt;Name</text>
<rectangle x1="3.49" y1="6.8" x2="4.13" y2="7.65" layer="51"/>
<smd name="4" x="3.81" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
</package>
<package name="1X04-1.5MM_JST">
<pad name="4" x="4.5" y="0" drill="0.7"/>
<pad name="3" x="3" y="0" drill="0.7"/>
<pad name="2" x="1.5" y="0" drill="0.7"/>
<pad name="1" x="0" y="0" drill="0.7"/>
<text x="-0.508" y="1.27" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.508" y="-1.651" size="0.4064" layer="27">&gt;VALUE</text>
<wire x1="-1.5" y1="2.2" x2="6" y2="2.2" width="0.3048" layer="21"/>
<wire x1="6" y1="2.2" x2="6" y2="-1.5" width="0.3048" layer="21"/>
<wire x1="6" y1="-1.5" x2="4.5" y2="-1.5" width="0.3048" layer="21"/>
<wire x1="4.5" y1="-1.5" x2="4.5" y2="-1" width="0.3048" layer="21"/>
<wire x1="4.5" y1="-1" x2="0" y2="-1" width="0.3048" layer="21"/>
<wire x1="0" y1="-1" x2="0" y2="-1.5" width="0.3048" layer="21"/>
<wire x1="0" y1="-1.5" x2="-1.5" y2="-1.5" width="0.3048" layer="21"/>
<wire x1="-1.5" y1="-1.5" x2="-1.5" y2="2.2" width="0.3048" layer="21"/>
</package>
<package name="1X04_NO_SILK_ALL_ROUND">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="POT">
<wire x1="0" y1="-5.08" x2="0" y2="-4.572" width="0.1524" layer="94"/>
<wire x1="0" y1="-4.572" x2="-1.016" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-1.016" y1="-3.81" x2="1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="-1.016" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-1.016" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.016" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.27" x2="1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="-1.016" y2="3.81" width="0.254" layer="94"/>
<wire x1="-1.016" y1="3.81" x2="0" y2="4.572" width="0.254" layer="94"/>
<wire x1="0" y1="4.572" x2="0" y2="5.08" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="1.27" width="0.2032" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="1.27" y2="0" width="0.2032" layer="94"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.2032" layer="94"/>
<wire x1="2.032" y1="-4.699" x2="2.032" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="2.032" y1="-2.159" x2="2.667" y2="-3.429" width="0.1524" layer="94"/>
<wire x1="2.667" y1="-3.429" x2="1.397" y2="-3.429" width="0.1524" layer="94"/>
<wire x1="1.397" y1="-3.429" x2="2.032" y2="-2.159" width="0.1524" layer="94"/>
<text x="5.08" y="-10.16" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="-2.54" y="-10.16" size="1.778" layer="95" rot="R90">&gt;Value</text>
<pin name="A" x="0" y="-7.62" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="E" x="0" y="7.62" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="S" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="DIODE">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<text x="2.54" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.3114" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="M06">
<wire x1="1.27" y1="-7.62" x2="-5.08" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="0" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="0" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-5.08" y1="10.16" x2="-5.08" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="1.27" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="10.16" x2="1.27" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="0" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="7.62" x2="0" y2="7.62" width="0.6096" layer="94"/>
<text x="-5.08" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-5.08" y="10.922" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="5.08" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="5.08" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="5.08" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="5.08" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="5.08" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="5.08" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="SPST">
<wire x1="-3.175" y1="0" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="3.175" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="0" y2="0.635" width="0.254" layer="94"/>
<wire x1="0" y1="0.635" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<text x="-3.937" y="2.794" size="1.778" layer="95">&gt;NAME</text>
<text x="-4.699" y="-3.556" size="1.778" layer="96">&gt;VALUE</text>
<pin name="P" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="S" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="SWITCH-MOMENTARY">
<wire x1="1.905" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="1.905" y1="4.445" x2="1.905" y2="3.175" width="0.254" layer="94"/>
<wire x1="-1.905" y1="4.445" x2="-1.905" y2="3.175" width="0.254" layer="94"/>
<wire x1="1.905" y1="4.445" x2="0" y2="4.445" width="0.254" layer="94"/>
<wire x1="0" y1="4.445" x2="-1.905" y2="4.445" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="4.445" x2="0" y2="3.175" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="1.905" y2="1.27" width="0.254" layer="94"/>
<circle x="-2.54" y="0" radius="0.127" width="0.4064" layer="94"/>
<circle x="2.54" y="0" radius="0.127" width="0.4064" layer="94"/>
<text x="-2.54" y="6.35" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-6.35" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="2"/>
<pin name="3" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="-5.08" y="-2.54" visible="pad" length="short" direction="pas" swaplevel="2"/>
</symbol>
<symbol name="LED">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
<symbol name="M04">
<wire x1="1.27" y1="-5.08" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="0" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="0" y2="5.08" width="0.6096" layer="94"/>
<text x="-5.08" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-5.08" y="8.382" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="5.08" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="5.08" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="5.08" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="5.08" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="POT" prefix="POT" uservalue="yes">
<gates>
<gate name="G$1" symbol="POT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ALPS_POT">
<connects>
<connect gate="G$1" pin="A" pad="P$3"/>
<connect gate="G$1" pin="E" pad="P$1"/>
<connect gate="G$1" pin="S" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TRIM" package="TRIM_POT">
<connects>
<connect gate="G$1" pin="A" pad="3"/>
<connect gate="G$1" pin="E" pad="2"/>
<connect gate="G$1" pin="S" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE" prefix="D" uservalue="yes">
<description>&lt;b&gt;Diode&lt;/b&gt;
These are standard reverse protection diodes and small signal diodes. SMA package can handle up to about 1A. SOD-323 can handle about 200mA. What the SOD-323 package when ordering, there are some mfgs out there that are 5-pin packages.</description>
<gates>
<gate name="G$1" symbol="DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="SMA" package="SMA-DIODE">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH" package="DIODE-1N4001">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD" package="SOD-323">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="C" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT23" package="SOT23-3">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1N4148" package="DIODE-1N4148">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMB" package="SMB-DIODE">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="HV" package="DIODE-HV">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMA-ALT" package="SMA-DIODE_ALT">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMA-KIT" package="SMA-DIODE-KIT">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD523" package="SOD523">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMC/DO-214AB" package="SMC">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="M06" prefix="JP" uservalue="yes">
<description>&lt;b&gt;Header 6&lt;/b&gt;&lt;br&gt;
Standard 6-pin 0.1" header. Use with straight break away headers (SKU : PRT-00116), right angle break away headers (PRT-00553), swiss pins (PRT-00743), machine pins (PRT-00117), and female headers (PRT-00115). Molex polarized connector foot print use with SKU : PRT-08094 with associated crimp pins and housings.&lt;p&gt;



NOTES ON THE VARIANTS LOCK and LOCK_LONGPADS...
This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place. You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers. Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<gates>
<gate name="G$1" symbol="M06" x="-2.54" y="0"/>
</gates>
<devices>
<device name="SIP" package="1X06">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR" package="MOLEX-1X6">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RA" package="MOLEX-1X6-RA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="1X06-SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK" package="1X06_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X06_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR_LOCK" package="MOLEX-1X6_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RA_LOCK" package="MOLEX-1X6-RA_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SIP_LOCK" package="1X06-SIP_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="FEMALE_LOCK" package="1X06_FEMALE_LOCK.010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LONGPADS" package="1X06_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1X06-SMD_EDGE_FEMALE" package="1X06-SMD_EDGE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM-6" package="SCREWTERMINAL-3.5MM-6">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMDF" package="1X06-SMD-FEMALE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-FEMALE-V2" package="1X06-SMD-FEMALE-V2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POGOPIN_HOLES_ONLY" package="1X06_HOLES_ONLY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-STRAIGHT" package="1X06_SMD_STRAIGHT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-STRAIGHT-ALT" package="1X06_SMD_STRAIGHT_ALT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-STRAIGHT-COMBO" package="1X06_SMD_STRAIGHT_COMBO">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EM406" package="EM406">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VERNIER-ANALOG" package="BTA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VERNIER-DIGITAL" package="BTD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD_MALE" package="1X06_SMD_MALE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-11293"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SPST" prefix="S">
<description>&lt;b&gt;SPST SWITCH&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="SPST" x="0" y="0"/>
</gates>
<devices>
<device name="_TACT_RECT2" package="EVQPJ">
<connects>
<connect gate="G$1" pin="P" pad="A"/>
<connect gate="G$1" pin="S" pad="B"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_TACT_RECT_SMD" package="6MMTACTTILE_RECT_SMD">
<connects>
<connect gate="G$1" pin="P" pad="P$1"/>
<connect gate="G$1" pin="S" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_TAKT_ROUND_2PIN" package="TACT_2PIN">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_SIDEPUSH" package="SKHH-SIDE">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TAC_SWITCH" prefix="SW" uservalue="yes">
<description>&lt;b&gt;Momentary Switch&lt;/b&gt;&lt;br&gt;
Button commonly used for reset or general input.&lt;br&gt;
Spark Fun Electronics SKU : COM-00097&lt;br&gt;
SMT- SWCH-08247</description>
<gates>
<gate name="S" symbol="SWITCH-MOMENTARY" x="0" y="0"/>
</gates>
<devices>
<device name="SMD" package="TACTILE_SWITCH_SMD">
<connects>
<connect gate="S" pin="1" pad="1"/>
<connect gate="S" pin="2" pad="2"/>
<connect gate="S" pin="3" pad="3"/>
<connect gate="S" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-08247"/>
</technology>
</technologies>
</device>
<device name="PTH" package="TACTILE-PTH">
<connects>
<connect gate="S" pin="1" pad="1"/>
<connect gate="S" pin="2" pad="2"/>
<connect gate="S" pin="3" pad="3"/>
<connect gate="S" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KSA_SEALED" package="KSA_SEALED_TAC_SWITCH">
<connects>
<connect gate="S" pin="1" pad="P$1"/>
<connect gate="S" pin="2" pad="P$2"/>
<connect gate="S" pin="3" pad="P$3"/>
<connect gate="S" pin="4" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_12MM" package="TACT_SW_12MM">
<connects>
<connect gate="S" pin="1" pad="1"/>
<connect gate="S" pin="2" pad="2"/>
<connect gate="S" pin="3" pad="3"/>
<connect gate="S" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_12MM_H" package="TACT_SW_12MM_HOLE">
<connects>
<connect gate="S" pin="1" pad="1"/>
<connect gate="S" pin="2" pad="2"/>
<connect gate="S" pin="3" pad="3"/>
<connect gate="S" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED" prefix="LED" uservalue="yes">
<description>&lt;b&gt;LEDs&lt;/b&gt;
Standard schematic elements and footprints for 5mm, 3mm, 1206, and 0603 sized LEDs. 5mm - Spark Fun Electronics SKU : COM-00529 (and others)</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="5MM" package="LED5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3MM" package="LED3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="LED-1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="LED-0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="10MM" package="LED10MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-FKIT-1206" package="FKIT-LED-1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3MM-NO_SILK" package="LED3MM-NS">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM-KIT" package="LED5MM-KIT">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="LED-TLP">
<connects>
<connect gate="G$1" pin="A" pad="ANODE"/>
<connect gate="G$1" pin="C" pad="CATHODE"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_2X5" package="LED_2X5">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="M04" prefix="JP" uservalue="yes">
<description>&lt;b&gt;Header 4&lt;/b&gt;
Standard 4-pin 0.1" header. Use with straight break away headers (SKU : PRT-00116), right angle break away headers (PRT-00553), swiss pins (PRT-00743), machine pins (PRT-00117), and female headers (PRT-00115). Molex polarized connector foot print use with SKU : PRT-08231 with associated crimp pins and housings. 1MM SMD Version SKU: PRT-10208</description>
<gates>
<gate name="G$1" symbol="M04" x="-2.54" y="0"/>
</gates>
<devices>
<device name="PTH" package="1X04">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR" package="MOLEX-1X4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SCREW" package="SCREWTERMINAL-3.5MM-4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1.27MM" package="1X04-1.27MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK" package="1X04_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X04_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR_LOCK" package="MOLEX-1X4_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="1X04-SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LONGPADS" package="1X04_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1X04_NO_SILK" package="1X04_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH" package="JST-4-PTH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="SKU" value="PRT-09916" constant="no"/>
</technology>
</technologies>
</device>
<device name="SCREW_LOCK" package="SCREWTERMINAL-3.5MM-4_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD2" package="1X04-1MM-RA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD_STRAIGHT_COMBO" package="1X04_SMD_STRAIGHT_COMBO">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08511"/>
<attribute name="VALUE" value="1X04_SMD_STRAIGHT_COMBO"/>
</technology>
</technologies>
</device>
<device name="SMD_LONG" package="1X04-SMD_LONG">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-VERT" package="JST-4-PTH-VERT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD_RA_FEMALE" package="1X04_SMD_RA_FEMALE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST" package="1X04-1.5MM_JST">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="NO_SILK_ALL_ROUND" package="1X04_NO_SILK_ALL_ROUND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="dm9_microcontroller">
<packages>
<package name="DIL28-3">
<description>Standard 28-pin DIP package.&lt;br&gt;
IC needs to have legs bent before insertion.</description>
<wire x1="-17.78" y1="-1.27" x2="-17.78" y2="-3.048" width="0.2032" layer="21"/>
<wire x1="-17.78" y1="-1.27" x2="-17.78" y2="1.27" width="0.2032" layer="21" curve="180"/>
<wire x1="17.78" y1="-3.048" x2="17.78" y2="3.048" width="0.2032" layer="21"/>
<wire x1="-17.78" y1="3.048" x2="-17.78" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-17.78" y1="3.048" x2="-17.526" y2="3.048" width="0.2032" layer="21"/>
<wire x1="-17.78" y1="-3.048" x2="-17.526" y2="-3.048" width="0.2032" layer="21"/>
<wire x1="17.78" y1="-3.048" x2="17.526" y2="-3.048" width="0.2032" layer="21"/>
<wire x1="17.78" y1="3.048" x2="17.526" y2="3.048" width="0.2032" layer="21"/>
<pad name="1" x="-16.51" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-13.97" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="-11.43" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="-8.89" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="-6.35" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="-3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="-1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="9" x="3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="10" x="6.35" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="11" x="8.89" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="12" x="11.43" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="13" x="13.97" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="14" x="16.51" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="15" x="16.51" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="16" x="13.97" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="17" x="11.43" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="18" x="8.89" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="19" x="6.35" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="20" x="3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="21" x="1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="22" x="-1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="23" x="-3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="24" x="-6.35" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="25" x="-8.89" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="26" x="-11.43" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="27" x="-13.97" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="28" x="-16.51" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<text x="-15.24" y="0.635" size="1.27" layer="25" ratio="14">&gt;NAME</text>
<text x="-15.24" y="-1.905" size="1.27" layer="27" ratio="14">&gt;VALUE</text>
</package>
<package name="SSOP28DB">
<description>&lt;b&gt;Small Shrink Outline Package&lt;/b&gt;</description>
<wire x1="-5.038" y1="2.763" x2="5.038" y2="2.763" width="0.1524" layer="27"/>
<wire x1="5.038" y1="2.763" x2="5.038" y2="-2.763" width="0.1524" layer="27"/>
<wire x1="5.038" y1="-2.763" x2="-5.038" y2="-2.763" width="0.1524" layer="27"/>
<wire x1="-5.038" y1="-2.763" x2="-5.038" y2="2.763" width="0.1524" layer="27"/>
<smd name="28" x="-4.225" y="3.7322" dx="0.348" dy="1.524" layer="1"/>
<smd name="27" x="-3.575" y="3.7322" dx="0.348" dy="1.524" layer="1"/>
<smd name="26" x="-2.925" y="3.7322" dx="0.348" dy="1.524" layer="1"/>
<smd name="25" x="-2.275" y="3.7322" dx="0.348" dy="1.524" layer="1"/>
<smd name="24" x="-1.625" y="3.7322" dx="0.348" dy="1.524" layer="1"/>
<smd name="23" x="-0.975" y="3.7322" dx="0.348" dy="1.524" layer="1"/>
<smd name="22" x="-0.325" y="3.7322" dx="0.348" dy="1.524" layer="1"/>
<smd name="20" x="0.975" y="3.7322" dx="0.348" dy="1.524" layer="1"/>
<smd name="21" x="0.325" y="3.7322" dx="0.348" dy="1.524" layer="1"/>
<smd name="19" x="1.625" y="3.7322" dx="0.348" dy="1.524" layer="1"/>
<smd name="18" x="2.275" y="3.7322" dx="0.348" dy="1.524" layer="1"/>
<smd name="17" x="2.925" y="3.7322" dx="0.348" dy="1.524" layer="1"/>
<smd name="16" x="3.575" y="3.7322" dx="0.348" dy="1.524" layer="1"/>
<smd name="15" x="4.225" y="3.7322" dx="0.348" dy="1.524" layer="1"/>
<smd name="1" x="-4.225" y="-3.7322" dx="0.348" dy="1.524" layer="1"/>
<smd name="2" x="-3.575" y="-3.7322" dx="0.348" dy="1.524" layer="1"/>
<smd name="3" x="-2.925" y="-3.7322" dx="0.348" dy="1.524" layer="1"/>
<smd name="4" x="-2.275" y="-3.7322" dx="0.348" dy="1.524" layer="1"/>
<smd name="5" x="-1.625" y="-3.7322" dx="0.348" dy="1.524" layer="1"/>
<smd name="6" x="-0.975" y="-3.7322" dx="0.348" dy="1.524" layer="1"/>
<smd name="7" x="-0.325" y="-3.7322" dx="0.348" dy="1.524" layer="1"/>
<smd name="8" x="0.325" y="-3.7322" dx="0.348" dy="1.524" layer="1"/>
<smd name="9" x="0.975" y="-3.7322" dx="0.348" dy="1.524" layer="1"/>
<smd name="10" x="1.625" y="-3.7322" dx="0.348" dy="1.524" layer="1"/>
<smd name="11" x="2.275" y="-3.7322" dx="0.348" dy="1.524" layer="1"/>
<smd name="12" x="2.925" y="-3.7322" dx="0.348" dy="1.524" layer="1"/>
<smd name="13" x="3.575" y="-3.7322" dx="0.348" dy="1.524" layer="1"/>
<smd name="14" x="4.225" y="-3.7322" dx="0.348" dy="1.524" layer="1"/>
<text x="-5.715" y="-2.76" size="1.27" layer="25" ratio="12" rot="R90">&gt;NAME</text>
<text x="-4.03" y="-0.635" size="1.27" layer="27" ratio="12">&gt;VALUE</text>
<rectangle x1="-4.3875" y1="2.9656" x2="-4.0625" y2="3.9" layer="51"/>
<rectangle x1="-4.3875" y1="-3.9" x2="-4.0625" y2="-2.9656" layer="51"/>
<rectangle x1="-3.7375" y1="-3.9" x2="-3.4125" y2="-2.9656" layer="51"/>
<rectangle x1="-3.0875" y1="-3.9" x2="-2.7625" y2="-2.9656" layer="51"/>
<rectangle x1="-3.7375" y1="2.9656" x2="-3.4125" y2="3.9" layer="51"/>
<rectangle x1="-3.0875" y1="2.9656" x2="-2.7625" y2="3.9" layer="51"/>
<rectangle x1="-2.4375" y1="2.9656" x2="-2.1125" y2="3.9" layer="51"/>
<rectangle x1="-1.7875" y1="2.9656" x2="-1.4625" y2="3.9" layer="51"/>
<rectangle x1="-1.1375" y1="2.9656" x2="-0.8125" y2="3.9" layer="51"/>
<rectangle x1="-0.4875" y1="2.9656" x2="-0.1625" y2="3.9" layer="51"/>
<rectangle x1="0.1625" y1="2.9656" x2="0.4875" y2="3.9" layer="51"/>
<rectangle x1="0.8125" y1="2.9656" x2="1.1375" y2="3.9" layer="51"/>
<rectangle x1="1.4625" y1="2.9656" x2="1.7875" y2="3.9" layer="51"/>
<rectangle x1="2.1125" y1="2.9656" x2="2.4375" y2="3.9" layer="51"/>
<rectangle x1="2.7625" y1="2.9656" x2="3.0875" y2="3.9" layer="51"/>
<rectangle x1="3.4125" y1="2.9656" x2="3.7375" y2="3.9" layer="51"/>
<rectangle x1="4.0625" y1="2.9656" x2="4.3875" y2="3.9" layer="51"/>
<rectangle x1="-2.4375" y1="-3.9" x2="-2.1125" y2="-2.9656" layer="51"/>
<rectangle x1="-1.7875" y1="-3.9" x2="-1.4625" y2="-2.9656" layer="51"/>
<rectangle x1="-1.1375" y1="-3.9" x2="-0.8125" y2="-2.9656" layer="51"/>
<rectangle x1="-0.4875" y1="-3.9" x2="-0.1625" y2="-2.9656" layer="51"/>
<rectangle x1="0.1625" y1="-3.9" x2="0.4875" y2="-2.9656" layer="51"/>
<rectangle x1="0.8125" y1="-3.9" x2="1.1375" y2="-2.9656" layer="51"/>
<rectangle x1="1.4625" y1="-3.9" x2="1.7875" y2="-2.9656" layer="51"/>
<rectangle x1="2.1125" y1="-3.9" x2="2.4375" y2="-2.9656" layer="51"/>
<rectangle x1="2.7625" y1="-3.9" x2="3.0875" y2="-2.9656" layer="51"/>
<rectangle x1="3.4125" y1="-3.9" x2="3.7375" y2="-2.9656" layer="51"/>
<rectangle x1="4.0625" y1="-3.9" x2="4.3875" y2="-2.9656" layer="51"/>
<circle x="-5.23875" y="-3.65125" radius="0.3175" width="0.1524" layer="21"/>
</package>
<package name="SO28W">
<description>&lt;b&gt;SMALL OUTLINE INTEGRATED CIRCUIT&lt;/b&gt;&lt;p&gt;
wide body 7.5 mm/JEDEC MO-119AB</description>
<wire x1="-9.395" y1="6.5" x2="9.395" y2="6.5" width="0.1998" layer="39"/>
<wire x1="9.395" y1="-6.5" x2="-9.395" y2="-6.5" width="0.1998" layer="39"/>
<wire x1="-9.395" y1="-6.5" x2="-9.395" y2="6.5" width="0.1998" layer="39"/>
<wire x1="9.05" y1="-3.7" x2="-9.05" y2="-3.7" width="0.2032" layer="51"/>
<wire x1="-9.05" y1="-3.7" x2="-9.05" y2="3.7" width="0.2032" layer="51"/>
<wire x1="-9.05" y1="3.7" x2="9.05" y2="3.7" width="0.2032" layer="51"/>
<wire x1="9.05" y1="3.7" x2="9.05" y2="-3.7" width="0.2032" layer="51"/>
<wire x1="9.395" y1="6.5" x2="9.395" y2="-6.5" width="0.1998" layer="39"/>
<smd name="2" x="-6.985" y="-4.9" dx="0.6" dy="2.8" layer="1"/>
<smd name="13" x="6.985" y="-4.9" dx="0.6" dy="2.8" layer="1"/>
<smd name="1" x="-8.255" y="-4.9" dx="0.6" dy="2.8" layer="1"/>
<smd name="3" x="-5.715" y="-4.9" dx="0.6" dy="2.8" layer="1"/>
<smd name="4" x="-4.445" y="-4.9" dx="0.6" dy="2.8" layer="1"/>
<smd name="14" x="8.255" y="-4.9" dx="0.6" dy="2.8" layer="1"/>
<smd name="12" x="5.715" y="-4.9" dx="0.6" dy="2.8" layer="1"/>
<smd name="11" x="4.445" y="-4.9" dx="0.6" dy="2.8" layer="1"/>
<smd name="6" x="-1.905" y="-4.9" dx="0.6" dy="2.8" layer="1"/>
<smd name="9" x="1.905" y="-4.9" dx="0.6" dy="2.8" layer="1"/>
<smd name="5" x="-3.175" y="-4.9" dx="0.6" dy="2.8" layer="1"/>
<smd name="7" x="-0.635" y="-4.9" dx="0.6" dy="2.8" layer="1"/>
<smd name="10" x="3.175" y="-4.9" dx="0.6" dy="2.8" layer="1"/>
<smd name="8" x="0.635" y="-4.9" dx="0.6" dy="2.8" layer="1"/>
<smd name="15" x="8.255" y="4.9" dx="0.6" dy="2.8" layer="1"/>
<smd name="16" x="6.985" y="4.9" dx="0.6" dy="2.8" layer="1"/>
<smd name="17" x="5.715" y="4.9" dx="0.6" dy="2.8" layer="1"/>
<smd name="18" x="4.445" y="4.9" dx="0.6" dy="2.8" layer="1"/>
<smd name="19" x="3.175" y="4.9" dx="0.6" dy="2.8" layer="1"/>
<smd name="20" x="1.905" y="4.9" dx="0.6" dy="2.8" layer="1"/>
<smd name="21" x="0.635" y="4.9" dx="0.6" dy="2.8" layer="1"/>
<smd name="22" x="-0.635" y="4.9" dx="0.6" dy="2.8" layer="1"/>
<smd name="23" x="-1.905" y="4.9" dx="0.6" dy="2.8" layer="1"/>
<smd name="24" x="-3.175" y="4.9" dx="0.6" dy="2.8" layer="1"/>
<smd name="25" x="-4.445" y="4.9" dx="0.6" dy="2.8" layer="1"/>
<smd name="26" x="-5.715" y="4.9" dx="0.6" dy="2.8" layer="1"/>
<smd name="27" x="-6.985" y="4.9" dx="0.6" dy="2.8" layer="1"/>
<smd name="28" x="-8.255" y="4.9" dx="0.6" dy="2.8" layer="1"/>
<text x="-8.255" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-8.255" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-8.5001" y1="-5.32" x2="-8.0099" y2="-3.8001" layer="51"/>
<rectangle x1="-7.2301" y1="-5.32" x2="-6.7399" y2="-3.8001" layer="51"/>
<rectangle x1="-5.9601" y1="-5.32" x2="-5.4699" y2="-3.8001" layer="51"/>
<rectangle x1="-4.6901" y1="-5.32" x2="-4.1999" y2="-3.8001" layer="51"/>
<rectangle x1="-3.4201" y1="-5.32" x2="-2.9299" y2="-3.8001" layer="51"/>
<rectangle x1="-2.1501" y1="-5.32" x2="-1.6599" y2="-3.8001" layer="51"/>
<rectangle x1="-0.8801" y1="-5.32" x2="-0.3899" y2="-3.8001" layer="51"/>
<rectangle x1="0.3899" y1="-5.32" x2="0.8801" y2="-3.8001" layer="51"/>
<rectangle x1="1.6599" y1="-5.32" x2="2.1501" y2="-3.8001" layer="51"/>
<rectangle x1="2.9299" y1="-5.32" x2="3.4201" y2="-3.8001" layer="51"/>
<rectangle x1="4.1999" y1="-5.32" x2="4.6901" y2="-3.8001" layer="51"/>
<rectangle x1="5.4699" y1="-5.32" x2="5.9601" y2="-3.8001" layer="51"/>
<rectangle x1="6.7399" y1="-5.32" x2="7.2301" y2="-3.8001" layer="51"/>
<rectangle x1="8.0099" y1="-5.32" x2="8.5001" y2="-3.8001" layer="51"/>
<rectangle x1="8.0099" y1="3.8001" x2="8.5001" y2="5.32" layer="51"/>
<rectangle x1="6.7399" y1="3.8001" x2="7.2301" y2="5.32" layer="51"/>
<rectangle x1="5.4699" y1="3.8001" x2="5.9601" y2="5.32" layer="51"/>
<rectangle x1="4.1999" y1="3.8001" x2="4.6901" y2="5.32" layer="51"/>
<rectangle x1="2.9299" y1="3.8001" x2="3.4201" y2="5.32" layer="51"/>
<rectangle x1="1.6599" y1="3.8001" x2="2.1501" y2="5.32" layer="51"/>
<rectangle x1="0.3899" y1="3.8001" x2="0.8801" y2="5.32" layer="51"/>
<rectangle x1="-0.8801" y1="3.8001" x2="-0.3899" y2="5.32" layer="51"/>
<rectangle x1="-2.1501" y1="3.8001" x2="-1.6599" y2="5.32" layer="51"/>
<rectangle x1="-3.4201" y1="3.8001" x2="-2.9299" y2="5.32" layer="51"/>
<rectangle x1="-4.6901" y1="3.8001" x2="-4.1999" y2="5.32" layer="51"/>
<rectangle x1="-5.9601" y1="3.8001" x2="-5.4699" y2="5.32" layer="51"/>
<rectangle x1="-7.2301" y1="3.8001" x2="-6.7399" y2="5.32" layer="51"/>
<rectangle x1="-8.5001" y1="3.8001" x2="-8.0099" y2="5.32" layer="51"/>
<wire x1="-8.7" y1="3" x2="-8.7" y2="-3" width="0.127" layer="21"/>
<wire x1="-8.7" y1="-3" x2="8.7" y2="-3" width="0.127" layer="21"/>
<wire x1="8.7" y1="-3" x2="8.7" y2="3" width="0.127" layer="21"/>
<wire x1="8.7" y1="3" x2="-8.7" y2="3" width="0.127" layer="21"/>
<circle x="-10.5" y="-5.1" radius="0.3" width="0.6" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="PIC18F25K50">
<pin name="MCLR/VPP/RE3" x="-30.48" y="27.94" length="middle" direction="in"/>
<pin name="RA0/AN0" x="33.02" y="27.94" length="middle" rot="R180"/>
<pin name="RA1/AN1" x="33.02" y="25.4" length="middle" rot="R180"/>
<pin name="RA2/AN2/VREF-/DACOUT" x="33.02" y="22.86" length="middle" rot="R180"/>
<pin name="RA3/AN3/VREF+" x="33.02" y="20.32" length="middle" rot="R180"/>
<pin name="RA4" x="33.02" y="17.78" length="middle" rot="R180"/>
<pin name="RA5/AN4/_SS" x="33.02" y="15.24" length="middle" rot="R180"/>
<pin name="RA6/OSC2/CLKO" x="33.02" y="12.7" length="middle" rot="R180"/>
<pin name="RA7/OSC1/CLKI" x="33.02" y="10.16" length="middle" rot="R180"/>
<pin name="VSS@8" x="-30.48" y="-25.4" length="middle" direction="pwr"/>
<pin name="RC0/IOCC0" x="33.02" y="-17.78" length="middle" rot="R180"/>
<pin name="RC1/IOCC1" x="33.02" y="-20.32" length="middle" rot="R180"/>
<pin name="RC2/AN14/IOCC2" x="33.02" y="-22.86" length="middle" rot="R180"/>
<pin name="VUSB3V3" x="-30.48" y="-15.24" length="middle"/>
<pin name="D-/IOCC4" x="-30.48" y="7.62" length="middle"/>
<pin name="D+/IOCC5" x="-30.48" y="15.24" length="middle"/>
<pin name="RC6/TX/CK/IOCC6" x="33.02" y="-25.4" length="middle" rot="R180"/>
<pin name="RC7/RX/DT/SDO/IOCC7" x="33.02" y="-27.94" length="middle" rot="R180"/>
<pin name="VDD" x="-30.48" y="-5.08" length="middle" direction="pwr"/>
<pin name="VSS@19" x="-30.48" y="-27.94" length="middle" direction="pwr"/>
<pin name="RB0/AN12/SDI/SDA/INT0" x="33.02" y="5.08" length="middle" rot="R180"/>
<pin name="RB1/AN10/SCK/SCL/INT1" x="33.02" y="2.54" length="middle" rot="R180"/>
<pin name="RB2/AN8/INT2" x="33.02" y="0" length="middle" rot="R180"/>
<pin name="RB3/AN9/SDO" x="33.02" y="-2.54" length="middle" rot="R180"/>
<pin name="RB4/AN11/IOCB4" x="33.02" y="-5.08" length="middle" rot="R180"/>
<pin name="RB5/AN13/IOCB5" x="33.02" y="-7.62" length="middle" rot="R180"/>
<pin name="RB6/IOCB6/PGC" x="33.02" y="-10.16" length="middle" rot="R180"/>
<pin name="RB7/IOCB7/PGD" x="33.02" y="-12.7" length="middle" rot="R180"/>
<wire x1="-25.4" y1="30.48" x2="-25.4" y2="-30.48" width="0.254" layer="94"/>
<wire x1="-25.4" y1="-30.48" x2="27.94" y2="-30.48" width="0.254" layer="94"/>
<wire x1="27.94" y1="-30.48" x2="27.94" y2="30.48" width="0.254" layer="94"/>
<wire x1="27.94" y1="30.48" x2="-25.4" y2="30.48" width="0.254" layer="94"/>
<text x="-25.4" y="30.988" size="1.778" layer="95">&gt;NAME</text>
<text x="-25.4" y="-33.02" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="PIC18F2X50" prefix="IC">
<gates>
<gate name="IC" symbol="PIC18F25K50" x="0" y="0"/>
</gates>
<devices>
<device name="-E/SP" package="DIL28-3">
<connects>
<connect gate="IC" pin="D+/IOCC5" pad="16"/>
<connect gate="IC" pin="D-/IOCC4" pad="15"/>
<connect gate="IC" pin="MCLR/VPP/RE3" pad="1"/>
<connect gate="IC" pin="RA0/AN0" pad="2"/>
<connect gate="IC" pin="RA1/AN1" pad="3"/>
<connect gate="IC" pin="RA2/AN2/VREF-/DACOUT" pad="4"/>
<connect gate="IC" pin="RA3/AN3/VREF+" pad="5"/>
<connect gate="IC" pin="RA4" pad="6"/>
<connect gate="IC" pin="RA5/AN4/_SS" pad="7"/>
<connect gate="IC" pin="RA6/OSC2/CLKO" pad="10"/>
<connect gate="IC" pin="RA7/OSC1/CLKI" pad="9"/>
<connect gate="IC" pin="RB0/AN12/SDI/SDA/INT0" pad="21"/>
<connect gate="IC" pin="RB1/AN10/SCK/SCL/INT1" pad="22"/>
<connect gate="IC" pin="RB2/AN8/INT2" pad="23"/>
<connect gate="IC" pin="RB3/AN9/SDO" pad="24"/>
<connect gate="IC" pin="RB4/AN11/IOCB4" pad="25"/>
<connect gate="IC" pin="RB5/AN13/IOCB5" pad="26"/>
<connect gate="IC" pin="RB6/IOCB6/PGC" pad="27"/>
<connect gate="IC" pin="RB7/IOCB7/PGD" pad="28"/>
<connect gate="IC" pin="RC0/IOCC0" pad="11"/>
<connect gate="IC" pin="RC1/IOCC1" pad="12"/>
<connect gate="IC" pin="RC2/AN14/IOCC2" pad="13"/>
<connect gate="IC" pin="RC6/TX/CK/IOCC6" pad="17"/>
<connect gate="IC" pin="RC7/RX/DT/SDO/IOCC7" pad="18"/>
<connect gate="IC" pin="VDD" pad="20"/>
<connect gate="IC" pin="VSS@19" pad="19"/>
<connect gate="IC" pin="VSS@8" pad="8"/>
<connect gate="IC" pin="VUSB3V3" pad="14"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-E/SS" package="SSOP28DB">
<connects>
<connect gate="IC" pin="D+/IOCC5" pad="16"/>
<connect gate="IC" pin="D-/IOCC4" pad="15"/>
<connect gate="IC" pin="MCLR/VPP/RE3" pad="1"/>
<connect gate="IC" pin="RA0/AN0" pad="2"/>
<connect gate="IC" pin="RA1/AN1" pad="3"/>
<connect gate="IC" pin="RA2/AN2/VREF-/DACOUT" pad="4"/>
<connect gate="IC" pin="RA3/AN3/VREF+" pad="5"/>
<connect gate="IC" pin="RA4" pad="6"/>
<connect gate="IC" pin="RA5/AN4/_SS" pad="7"/>
<connect gate="IC" pin="RA6/OSC2/CLKO" pad="10"/>
<connect gate="IC" pin="RA7/OSC1/CLKI" pad="9"/>
<connect gate="IC" pin="RB0/AN12/SDI/SDA/INT0" pad="21"/>
<connect gate="IC" pin="RB1/AN10/SCK/SCL/INT1" pad="22"/>
<connect gate="IC" pin="RB2/AN8/INT2" pad="23"/>
<connect gate="IC" pin="RB3/AN9/SDO" pad="24"/>
<connect gate="IC" pin="RB4/AN11/IOCB4" pad="25"/>
<connect gate="IC" pin="RB5/AN13/IOCB5" pad="26"/>
<connect gate="IC" pin="RB6/IOCB6/PGC" pad="27"/>
<connect gate="IC" pin="RB7/IOCB7/PGD" pad="28"/>
<connect gate="IC" pin="RC0/IOCC0" pad="11"/>
<connect gate="IC" pin="RC1/IOCC1" pad="12"/>
<connect gate="IC" pin="RC2/AN14/IOCC2" pad="13"/>
<connect gate="IC" pin="RC6/TX/CK/IOCC6" pad="17"/>
<connect gate="IC" pin="RC7/RX/DT/SDO/IOCC7" pad="18"/>
<connect gate="IC" pin="VDD" pad="20"/>
<connect gate="IC" pin="VSS@19" pad="19"/>
<connect gate="IC" pin="VSS@8" pad="8"/>
<connect gate="IC" pin="VUSB3V3" pad="14"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-E/SO" package="SO28W">
<connects>
<connect gate="IC" pin="D+/IOCC5" pad="16"/>
<connect gate="IC" pin="D-/IOCC4" pad="15"/>
<connect gate="IC" pin="MCLR/VPP/RE3" pad="1"/>
<connect gate="IC" pin="RA0/AN0" pad="2"/>
<connect gate="IC" pin="RA1/AN1" pad="3"/>
<connect gate="IC" pin="RA2/AN2/VREF-/DACOUT" pad="4"/>
<connect gate="IC" pin="RA3/AN3/VREF+" pad="5"/>
<connect gate="IC" pin="RA4" pad="6"/>
<connect gate="IC" pin="RA5/AN4/_SS" pad="7"/>
<connect gate="IC" pin="RA6/OSC2/CLKO" pad="10"/>
<connect gate="IC" pin="RA7/OSC1/CLKI" pad="9"/>
<connect gate="IC" pin="RB0/AN12/SDI/SDA/INT0" pad="21"/>
<connect gate="IC" pin="RB1/AN10/SCK/SCL/INT1" pad="22"/>
<connect gate="IC" pin="RB2/AN8/INT2" pad="23"/>
<connect gate="IC" pin="RB3/AN9/SDO" pad="24"/>
<connect gate="IC" pin="RB4/AN11/IOCB4" pad="25"/>
<connect gate="IC" pin="RB5/AN13/IOCB5" pad="26"/>
<connect gate="IC" pin="RB6/IOCB6/PGC" pad="27"/>
<connect gate="IC" pin="RB7/IOCB7/PGD" pad="28"/>
<connect gate="IC" pin="RC0/IOCC0" pad="11"/>
<connect gate="IC" pin="RC1/IOCC1" pad="12"/>
<connect gate="IC" pin="RC2/AN14/IOCC2" pad="13"/>
<connect gate="IC" pin="RC6/TX/CK/IOCC6" pad="17"/>
<connect gate="IC" pin="RC7/RX/DT/SDO/IOCC7" pad="18"/>
<connect gate="IC" pin="VDD" pad="20"/>
<connect gate="IC" pin="VSS@19" pad="19"/>
<connect gate="IC" pin="VSS@8" pad="8"/>
<connect gate="IC" pin="VUSB3V3" pad="14"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="DINA4_L">
<frame x1="0" y1="0" x2="264.16" y2="180.34" columns="4" rows="4" layer="94" border-left="no" border-top="no" border-right="no" border-bottom="no"/>
</symbol>
<symbol name="DOCFIELD">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="87.63" y2="15.24" width="0.1016" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="22.86" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="15.24" width="0.1016" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94" font="vector">Date:</text>
<text x="12.7" y="1.27" size="2.54" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="1.27" size="2.54" layer="94" font="vector">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="88.9" y="11.43" size="2.54" layer="94" font="vector">REV:</text>
<text x="1.27" y="19.05" size="2.54" layer="94" font="vector">TITLE:</text>
<text x="1.27" y="11.43" size="2.54" layer="94" font="vector">Document Number:</text>
<text x="17.78" y="19.05" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="DINA4_L" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with extra doc field</description>
<gates>
<gate name="G$1" symbol="DINA4_L" x="0" y="0"/>
<gate name="G$2" symbol="DOCFIELD" x="162.56" y="0" addlevel="must"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="dp_devices">
<description>Dangerous Prototypes Standard PCB sizes
http://dangerousprototypes.com</description>
<packages>
<package name="CC-BY-SA">
<rectangle x1="0.0191" y1="-0.0191" x2="15.2972" y2="0.0191" layer="21"/>
<rectangle x1="-0.0191" y1="0.0191" x2="15.3353" y2="0.0572" layer="21"/>
<rectangle x1="-0.0191" y1="0.0572" x2="15.3353" y2="0.0953" layer="21"/>
<rectangle x1="-0.0191" y1="0.0953" x2="15.3353" y2="0.1334" layer="21"/>
<rectangle x1="-0.0191" y1="0.1334" x2="15.3353" y2="0.1715" layer="21"/>
<rectangle x1="-0.0191" y1="0.1715" x2="15.3353" y2="0.2096" layer="21"/>
<rectangle x1="-0.0191" y1="0.2096" x2="15.3353" y2="0.2477" layer="21"/>
<rectangle x1="-0.0191" y1="0.2477" x2="15.3353" y2="0.2858" layer="21"/>
<rectangle x1="-0.0191" y1="0.2858" x2="11.1443" y2="0.3239" layer="21"/>
<rectangle x1="11.2205" y1="0.2858" x2="15.3353" y2="0.3239" layer="21"/>
<rectangle x1="-0.0191" y1="0.3239" x2="7.2581" y2="0.362" layer="21"/>
<rectangle x1="7.7915" y1="0.3239" x2="8.3249" y2="0.362" layer="21"/>
<rectangle x1="8.5154" y1="0.3239" x2="10.9919" y2="0.362" layer="21"/>
<rectangle x1="11.3729" y1="0.3239" x2="11.5634" y2="0.362" layer="21"/>
<rectangle x1="11.7539" y1="0.3239" x2="12.173" y2="0.362" layer="21"/>
<rectangle x1="12.3635" y1="0.3239" x2="15.3353" y2="0.362" layer="21"/>
<rectangle x1="-0.0191" y1="0.362" x2="7.2581" y2="0.4001" layer="21"/>
<rectangle x1="7.8677" y1="0.362" x2="8.3249" y2="0.4001" layer="21"/>
<rectangle x1="8.5154" y1="0.362" x2="10.9157" y2="0.4001" layer="21"/>
<rectangle x1="11.4491" y1="0.362" x2="11.5634" y2="0.4001" layer="21"/>
<rectangle x1="11.7539" y1="0.362" x2="12.173" y2="0.4001" layer="21"/>
<rectangle x1="12.3635" y1="0.362" x2="15.3353" y2="0.4001" layer="21"/>
<rectangle x1="-0.0191" y1="0.4001" x2="2.6099" y2="0.4382" layer="21"/>
<rectangle x1="2.7242" y1="0.4001" x2="7.2581" y2="0.4382" layer="21"/>
<rectangle x1="7.9058" y1="0.4001" x2="8.3249" y2="0.4382" layer="21"/>
<rectangle x1="8.5154" y1="0.4001" x2="10.8776" y2="0.4382" layer="21"/>
<rectangle x1="11.4872" y1="0.4001" x2="11.6015" y2="0.4382" layer="21"/>
<rectangle x1="11.792" y1="0.4001" x2="12.173" y2="0.4382" layer="21"/>
<rectangle x1="12.3635" y1="0.4001" x2="15.3353" y2="0.4382" layer="21"/>
<rectangle x1="-0.0191" y1="0.4382" x2="2.2289" y2="0.4763" layer="21"/>
<rectangle x1="3.0671" y1="0.4382" x2="7.2581" y2="0.4763" layer="21"/>
<rectangle x1="7.9439" y1="0.4382" x2="8.3249" y2="0.4763" layer="21"/>
<rectangle x1="8.5154" y1="0.4382" x2="10.8776" y2="0.4763" layer="21"/>
<rectangle x1="11.1062" y1="0.4382" x2="11.2586" y2="0.4763" layer="21"/>
<rectangle x1="11.5253" y1="0.4382" x2="11.6015" y2="0.4763" layer="21"/>
<rectangle x1="11.792" y1="0.4382" x2="12.1349" y2="0.4763" layer="21"/>
<rectangle x1="12.3254" y1="0.4382" x2="15.3353" y2="0.4763" layer="21"/>
<rectangle x1="-0.0191" y1="0.4763" x2="2.0384" y2="0.5144" layer="21"/>
<rectangle x1="3.2576" y1="0.4763" x2="7.2581" y2="0.5144" layer="21"/>
<rectangle x1="7.4486" y1="0.4763" x2="7.7534" y2="0.5144" layer="21"/>
<rectangle x1="7.9439" y1="0.4763" x2="8.3249" y2="0.5144" layer="21"/>
<rectangle x1="8.5154" y1="0.4763" x2="10.8395" y2="0.5144" layer="21"/>
<rectangle x1="11.03" y1="0.4763" x2="11.3348" y2="0.5144" layer="21"/>
<rectangle x1="11.5253" y1="0.4763" x2="11.6396" y2="0.5144" layer="21"/>
<rectangle x1="11.8301" y1="0.4763" x2="12.1349" y2="0.5144" layer="21"/>
<rectangle x1="12.3254" y1="0.4763" x2="15.3353" y2="0.5144" layer="21"/>
<rectangle x1="-0.0191" y1="0.5144" x2="1.9241" y2="0.5525" layer="21"/>
<rectangle x1="3.3719" y1="0.5144" x2="7.2581" y2="0.5525" layer="21"/>
<rectangle x1="7.4486" y1="0.5144" x2="7.7915" y2="0.5525" layer="21"/>
<rectangle x1="7.982" y1="0.5144" x2="8.3249" y2="0.5525" layer="21"/>
<rectangle x1="8.5154" y1="0.5144" x2="10.8395" y2="0.5525" layer="21"/>
<rectangle x1="11.03" y1="0.5144" x2="11.3348" y2="0.5525" layer="21"/>
<rectangle x1="11.5253" y1="0.5144" x2="11.6396" y2="0.5525" layer="21"/>
<rectangle x1="12.2873" y1="0.5144" x2="15.3353" y2="0.5525" layer="21"/>
<rectangle x1="-0.0191" y1="0.5525" x2="1.8479" y2="0.5906" layer="21"/>
<rectangle x1="3.4862" y1="0.5525" x2="7.2581" y2="0.5906" layer="21"/>
<rectangle x1="7.4486" y1="0.5525" x2="7.7915" y2="0.5906" layer="21"/>
<rectangle x1="7.982" y1="0.5525" x2="8.3249" y2="0.5906" layer="21"/>
<rectangle x1="8.5154" y1="0.5525" x2="10.8395" y2="0.5906" layer="21"/>
<rectangle x1="10.9919" y1="0.5525" x2="11.3348" y2="0.5906" layer="21"/>
<rectangle x1="11.5253" y1="0.5525" x2="11.6396" y2="0.5906" layer="21"/>
<rectangle x1="12.2873" y1="0.5525" x2="15.3353" y2="0.5906" layer="21"/>
<rectangle x1="-0.0191" y1="0.5906" x2="1.7336" y2="0.6287" layer="21"/>
<rectangle x1="3.5624" y1="0.5906" x2="7.2581" y2="0.6287" layer="21"/>
<rectangle x1="7.4486" y1="0.5906" x2="7.7915" y2="0.6287" layer="21"/>
<rectangle x1="7.982" y1="0.5906" x2="8.3249" y2="0.6287" layer="21"/>
<rectangle x1="8.5154" y1="0.5906" x2="11.3348" y2="0.6287" layer="21"/>
<rectangle x1="11.5253" y1="0.5906" x2="11.6777" y2="0.6287" layer="21"/>
<rectangle x1="12.2873" y1="0.5906" x2="15.3353" y2="0.6287" layer="21"/>
<rectangle x1="-0.0191" y1="0.6287" x2="1.6574" y2="0.6668" layer="21"/>
<rectangle x1="3.6386" y1="0.6287" x2="7.2581" y2="0.6668" layer="21"/>
<rectangle x1="7.4486" y1="0.6287" x2="7.7534" y2="0.6668" layer="21"/>
<rectangle x1="7.9439" y1="0.6287" x2="8.3249" y2="0.6668" layer="21"/>
<rectangle x1="8.5154" y1="0.6287" x2="11.2586" y2="0.6668" layer="21"/>
<rectangle x1="11.5253" y1="0.6287" x2="11.6777" y2="0.6668" layer="21"/>
<rectangle x1="11.8682" y1="0.6287" x2="12.0587" y2="0.6668" layer="21"/>
<rectangle x1="12.2492" y1="0.6287" x2="15.3353" y2="0.6668" layer="21"/>
<rectangle x1="-0.0191" y1="0.6668" x2="1.5812" y2="0.7049" layer="21"/>
<rectangle x1="3.7148" y1="0.6668" x2="7.2581" y2="0.7049" layer="21"/>
<rectangle x1="7.4486" y1="0.6668" x2="7.6772" y2="0.7049" layer="21"/>
<rectangle x1="7.9439" y1="0.6668" x2="8.2868" y2="0.7049" layer="21"/>
<rectangle x1="8.5154" y1="0.6668" x2="11.1062" y2="0.7049" layer="21"/>
<rectangle x1="11.4872" y1="0.6668" x2="11.6777" y2="0.7049" layer="21"/>
<rectangle x1="11.8682" y1="0.6668" x2="12.0587" y2="0.7049" layer="21"/>
<rectangle x1="12.2492" y1="0.6668" x2="15.3353" y2="0.7049" layer="21"/>
<rectangle x1="-0.0191" y1="0.7049" x2="1.505" y2="0.743" layer="21"/>
<rectangle x1="3.791" y1="0.7049" x2="7.2581" y2="0.743" layer="21"/>
<rectangle x1="7.9058" y1="0.7049" x2="8.2487" y2="0.743" layer="21"/>
<rectangle x1="8.5535" y1="0.7049" x2="10.9919" y2="0.743" layer="21"/>
<rectangle x1="11.4872" y1="0.7049" x2="11.7158" y2="0.743" layer="21"/>
<rectangle x1="11.9063" y1="0.7049" x2="12.0587" y2="0.743" layer="21"/>
<rectangle x1="12.2492" y1="0.7049" x2="15.3353" y2="0.743" layer="21"/>
<rectangle x1="-0.0191" y1="0.743" x2="1.4669" y2="0.7811" layer="21"/>
<rectangle x1="3.8672" y1="0.743" x2="7.2581" y2="0.7811" layer="21"/>
<rectangle x1="7.8296" y1="0.743" x2="8.2487" y2="0.7811" layer="21"/>
<rectangle x1="8.5535" y1="0.743" x2="10.9157" y2="0.7811" layer="21"/>
<rectangle x1="11.411" y1="0.743" x2="11.7158" y2="0.7811" layer="21"/>
<rectangle x1="11.9063" y1="0.743" x2="12.0206" y2="0.7811" layer="21"/>
<rectangle x1="12.2111" y1="0.743" x2="15.3353" y2="0.7811" layer="21"/>
<rectangle x1="-0.0191" y1="0.7811" x2="1.3907" y2="0.8192" layer="21"/>
<rectangle x1="3.9053" y1="0.7811" x2="7.2581" y2="0.8192" layer="21"/>
<rectangle x1="7.8296" y1="0.7811" x2="8.2106" y2="0.8192" layer="21"/>
<rectangle x1="8.5916" y1="0.7811" x2="10.9157" y2="0.8192" layer="21"/>
<rectangle x1="11.3348" y1="0.7811" x2="11.7539" y2="0.8192" layer="21"/>
<rectangle x1="11.9063" y1="0.7811" x2="12.0206" y2="0.8192" layer="21"/>
<rectangle x1="12.2111" y1="0.7811" x2="15.3353" y2="0.8192" layer="21"/>
<rectangle x1="-0.0191" y1="0.8192" x2="1.3526" y2="0.8573" layer="21"/>
<rectangle x1="3.9815" y1="0.8192" x2="7.2581" y2="0.8573" layer="21"/>
<rectangle x1="7.4486" y1="0.8192" x2="7.6772" y2="0.8573" layer="21"/>
<rectangle x1="7.9058" y1="0.8192" x2="8.2106" y2="0.8573" layer="21"/>
<rectangle x1="8.6297" y1="0.8192" x2="10.8776" y2="0.8573" layer="21"/>
<rectangle x1="11.1824" y1="0.8192" x2="11.7539" y2="0.8573" layer="21"/>
<rectangle x1="11.9444" y1="0.8192" x2="12.0206" y2="0.8573" layer="21"/>
<rectangle x1="12.173" y1="0.8192" x2="15.3353" y2="0.8573" layer="21"/>
<rectangle x1="-0.0191" y1="0.8573" x2="1.2764" y2="0.8954" layer="21"/>
<rectangle x1="2.267" y1="0.8573" x2="3.029" y2="0.8954" layer="21"/>
<rectangle x1="4.0196" y1="0.8573" x2="7.2581" y2="0.8954" layer="21"/>
<rectangle x1="7.4486" y1="0.8573" x2="7.7534" y2="0.8954" layer="21"/>
<rectangle x1="7.9058" y1="0.8573" x2="8.1725" y2="0.8954" layer="21"/>
<rectangle x1="8.363" y1="0.8573" x2="8.4392" y2="0.8954" layer="21"/>
<rectangle x1="8.6297" y1="0.8573" x2="10.8776" y2="0.8954" layer="21"/>
<rectangle x1="11.0681" y1="0.8573" x2="11.7539" y2="0.8954" layer="21"/>
<rectangle x1="11.9444" y1="0.8573" x2="11.9825" y2="0.8954" layer="21"/>
<rectangle x1="12.173" y1="0.8573" x2="15.3353" y2="0.8954" layer="21"/>
<rectangle x1="-0.0191" y1="0.8954" x2="1.2383" y2="0.9335" layer="21"/>
<rectangle x1="2.1146" y1="0.8954" x2="3.2195" y2="0.9335" layer="21"/>
<rectangle x1="4.0577" y1="0.8954" x2="7.2581" y2="0.9335" layer="21"/>
<rectangle x1="7.4486" y1="0.8954" x2="7.7534" y2="0.9335" layer="21"/>
<rectangle x1="7.9058" y1="0.8954" x2="8.1344" y2="0.9335" layer="21"/>
<rectangle x1="8.363" y1="0.8954" x2="8.4773" y2="0.9335" layer="21"/>
<rectangle x1="8.6678" y1="0.8954" x2="10.8395" y2="0.9335" layer="21"/>
<rectangle x1="11.03" y1="0.8954" x2="11.3348" y2="0.9335" layer="21"/>
<rectangle x1="11.4872" y1="0.8954" x2="11.792" y2="0.9335" layer="21"/>
<rectangle x1="11.9444" y1="0.8954" x2="11.9825" y2="0.9335" layer="21"/>
<rectangle x1="12.173" y1="0.8954" x2="15.3353" y2="0.9335" layer="21"/>
<rectangle x1="-0.0191" y1="0.9335" x2="1.2002" y2="0.9716" layer="21"/>
<rectangle x1="2.0003" y1="0.9335" x2="3.3338" y2="0.9716" layer="21"/>
<rectangle x1="4.0958" y1="0.9335" x2="7.2581" y2="0.9716" layer="21"/>
<rectangle x1="7.4486" y1="0.9335" x2="7.7534" y2="0.9716" layer="21"/>
<rectangle x1="7.9439" y1="0.9335" x2="8.1344" y2="0.9716" layer="21"/>
<rectangle x1="8.3249" y1="0.9335" x2="8.4773" y2="0.9716" layer="21"/>
<rectangle x1="8.6678" y1="0.9335" x2="10.8395" y2="0.9716" layer="21"/>
<rectangle x1="11.03" y1="0.9335" x2="11.3348" y2="0.9716" layer="21"/>
<rectangle x1="11.4872" y1="0.9335" x2="11.792" y2="0.9716" layer="21"/>
<rectangle x1="12.1349" y1="0.9335" x2="15.3353" y2="0.9716" layer="21"/>
<rectangle x1="-0.0191" y1="0.9716" x2="1.1621" y2="1.0097" layer="21"/>
<rectangle x1="1.886" y1="0.9716" x2="3.41" y2="1.0097" layer="21"/>
<rectangle x1="4.172" y1="0.9716" x2="7.2581" y2="1.0097" layer="21"/>
<rectangle x1="7.4486" y1="0.9716" x2="7.7153" y2="1.0097" layer="21"/>
<rectangle x1="7.9058" y1="0.9716" x2="8.0963" y2="1.0097" layer="21"/>
<rectangle x1="8.3249" y1="0.9716" x2="8.5154" y2="1.0097" layer="21"/>
<rectangle x1="8.7059" y1="0.9716" x2="10.8776" y2="1.0097" layer="21"/>
<rectangle x1="11.03" y1="0.9716" x2="11.2967" y2="1.0097" layer="21"/>
<rectangle x1="11.4872" y1="0.9716" x2="11.8301" y2="1.0097" layer="21"/>
<rectangle x1="12.1349" y1="0.9716" x2="15.3353" y2="1.0097" layer="21"/>
<rectangle x1="-0.0191" y1="1.0097" x2="1.124" y2="1.0478" layer="21"/>
<rectangle x1="1.8098" y1="1.0097" x2="3.5243" y2="1.0478" layer="21"/>
<rectangle x1="4.2101" y1="1.0097" x2="7.2581" y2="1.0478" layer="21"/>
<rectangle x1="7.9058" y1="1.0097" x2="8.0963" y2="1.0478" layer="21"/>
<rectangle x1="8.2868" y1="1.0097" x2="8.5154" y2="1.0478" layer="21"/>
<rectangle x1="8.744" y1="1.0097" x2="10.8776" y2="1.0478" layer="21"/>
<rectangle x1="11.1062" y1="1.0097" x2="11.2205" y2="1.0478" layer="21"/>
<rectangle x1="11.4872" y1="1.0097" x2="11.8301" y2="1.0478" layer="21"/>
<rectangle x1="12.1349" y1="1.0097" x2="15.3353" y2="1.0478" layer="21"/>
<rectangle x1="-0.0191" y1="1.0478" x2="1.0478" y2="1.0859" layer="21"/>
<rectangle x1="1.7336" y1="1.0478" x2="3.6005" y2="1.0859" layer="21"/>
<rectangle x1="4.2482" y1="1.0478" x2="7.2581" y2="1.0859" layer="21"/>
<rectangle x1="7.9058" y1="1.0478" x2="8.0582" y2="1.0859" layer="21"/>
<rectangle x1="8.2868" y1="1.0478" x2="8.5535" y2="1.0859" layer="21"/>
<rectangle x1="8.744" y1="1.0478" x2="10.9157" y2="1.0859" layer="21"/>
<rectangle x1="11.4491" y1="1.0478" x2="11.8301" y2="1.0859" layer="21"/>
<rectangle x1="12.0968" y1="1.0478" x2="15.3353" y2="1.0859" layer="21"/>
<rectangle x1="-0.0191" y1="1.0859" x2="1.0097" y2="1.124" layer="21"/>
<rectangle x1="1.6574" y1="1.0859" x2="3.6386" y2="1.124" layer="21"/>
<rectangle x1="4.2863" y1="1.0859" x2="7.2581" y2="1.124" layer="21"/>
<rectangle x1="7.8677" y1="1.0859" x2="8.0201" y2="1.124" layer="21"/>
<rectangle x1="8.2487" y1="1.0859" x2="8.5535" y2="1.124" layer="21"/>
<rectangle x1="8.7821" y1="1.0859" x2="10.9538" y2="1.124" layer="21"/>
<rectangle x1="11.411" y1="1.0859" x2="11.8682" y2="1.124" layer="21"/>
<rectangle x1="12.0968" y1="1.0859" x2="15.3353" y2="1.124" layer="21"/>
<rectangle x1="-0.0191" y1="1.124" x2="1.0097" y2="1.1621" layer="21"/>
<rectangle x1="1.6193" y1="1.124" x2="3.7148" y2="1.1621" layer="21"/>
<rectangle x1="4.3244" y1="1.124" x2="7.2581" y2="1.1621" layer="21"/>
<rectangle x1="7.7915" y1="1.124" x2="8.0201" y2="1.1621" layer="21"/>
<rectangle x1="8.2106" y1="1.124" x2="8.5916" y2="1.1621" layer="21"/>
<rectangle x1="8.8202" y1="1.124" x2="10.9919" y2="1.1621" layer="21"/>
<rectangle x1="11.3729" y1="1.124" x2="11.8682" y2="1.1621" layer="21"/>
<rectangle x1="12.0587" y1="1.124" x2="15.3353" y2="1.1621" layer="21"/>
<rectangle x1="-0.0191" y1="1.1621" x2="0.9716" y2="1.2002" layer="21"/>
<rectangle x1="1.5431" y1="1.1621" x2="3.791" y2="1.2002" layer="21"/>
<rectangle x1="4.3625" y1="1.1621" x2="11.1443" y2="1.2002" layer="21"/>
<rectangle x1="11.2205" y1="1.1621" x2="15.3353" y2="1.2002" layer="21"/>
<rectangle x1="-0.0191" y1="1.2002" x2="0.9335" y2="1.2383" layer="21"/>
<rectangle x1="1.505" y1="1.2002" x2="2.6099" y2="1.2383" layer="21"/>
<rectangle x1="2.7242" y1="1.2002" x2="3.8291" y2="1.2383" layer="21"/>
<rectangle x1="4.3625" y1="1.2002" x2="15.3353" y2="1.2383" layer="21"/>
<rectangle x1="-0.0191" y1="1.2383" x2="0.8954" y2="1.2764" layer="21"/>
<rectangle x1="1.4669" y1="1.2383" x2="2.3051" y2="1.2764" layer="21"/>
<rectangle x1="3.029" y1="1.2383" x2="3.8672" y2="1.2764" layer="21"/>
<rectangle x1="4.4006" y1="1.2383" x2="15.3353" y2="1.2764" layer="21"/>
<rectangle x1="-0.0191" y1="1.2764" x2="0.8573" y2="1.3145" layer="21"/>
<rectangle x1="1.3907" y1="1.2764" x2="2.1527" y2="1.3145" layer="21"/>
<rectangle x1="3.1433" y1="1.2764" x2="3.9053" y2="1.3145" layer="21"/>
<rectangle x1="4.4387" y1="1.2764" x2="15.3353" y2="1.3145" layer="21"/>
<rectangle x1="-0.0191" y1="1.3145" x2="0.8192" y2="1.3526" layer="21"/>
<rectangle x1="1.3526" y1="1.3145" x2="2.0384" y2="1.3526" layer="21"/>
<rectangle x1="3.2576" y1="1.3145" x2="3.9434" y2="1.3526" layer="21"/>
<rectangle x1="4.4768" y1="1.3145" x2="15.3353" y2="1.3526" layer="21"/>
<rectangle x1="-0.0191" y1="1.3526" x2="0.8192" y2="1.3907" layer="21"/>
<rectangle x1="1.3145" y1="1.3526" x2="1.9622" y2="1.3907" layer="21"/>
<rectangle x1="3.3338" y1="1.3526" x2="4.0196" y2="1.3907" layer="21"/>
<rectangle x1="4.5149" y1="1.3526" x2="15.3353" y2="1.3907" layer="21"/>
<rectangle x1="-0.0191" y1="1.3907" x2="0.7811" y2="1.4288" layer="21"/>
<rectangle x1="1.2764" y1="1.3907" x2="1.886" y2="1.4288" layer="21"/>
<rectangle x1="3.41" y1="1.3907" x2="4.0577" y2="1.4288" layer="21"/>
<rectangle x1="4.5149" y1="1.3907" x2="15.3353" y2="1.4288" layer="21"/>
<rectangle x1="-0.0191" y1="1.4288" x2="0.743" y2="1.4669" layer="21"/>
<rectangle x1="1.2383" y1="1.4288" x2="1.8098" y2="1.4669" layer="21"/>
<rectangle x1="3.4862" y1="1.4288" x2="4.0958" y2="1.4669" layer="21"/>
<rectangle x1="4.553" y1="1.4288" x2="15.3353" y2="1.4669" layer="21"/>
<rectangle x1="-0.0191" y1="1.4669" x2="0.743" y2="1.505" layer="21"/>
<rectangle x1="1.2002" y1="1.4669" x2="1.7717" y2="1.505" layer="21"/>
<rectangle x1="3.5624" y1="1.4669" x2="4.1339" y2="1.505" layer="21"/>
<rectangle x1="4.553" y1="1.4669" x2="15.3353" y2="1.505" layer="21"/>
<rectangle x1="-0.0191" y1="1.505" x2="0.7049" y2="1.5431" layer="21"/>
<rectangle x1="1.1621" y1="1.505" x2="1.6955" y2="1.5431" layer="21"/>
<rectangle x1="3.6005" y1="1.505" x2="4.172" y2="1.5431" layer="21"/>
<rectangle x1="4.5911" y1="1.505" x2="15.3353" y2="1.5431" layer="21"/>
<rectangle x1="-0.0191" y1="1.5431" x2="0.6668" y2="1.5812" layer="21"/>
<rectangle x1="1.124" y1="1.5431" x2="1.6574" y2="1.5812" layer="21"/>
<rectangle x1="3.6386" y1="1.5431" x2="4.2101" y2="1.5812" layer="21"/>
<rectangle x1="4.6292" y1="1.5431" x2="15.3353" y2="1.5812" layer="21"/>
<rectangle x1="-0.0191" y1="1.5812" x2="0.1334" y2="1.6193" layer="21"/>
<rectangle x1="1.0859" y1="1.5812" x2="1.6193" y2="1.6193" layer="21"/>
<rectangle x1="3.7148" y1="1.5812" x2="4.2482" y2="1.6193" layer="21"/>
<rectangle x1="15.1829" y1="1.5812" x2="15.3353" y2="1.6193" layer="21"/>
<rectangle x1="-0.0191" y1="1.6193" x2="0.1334" y2="1.6574" layer="21"/>
<rectangle x1="1.0859" y1="1.6193" x2="1.5812" y2="1.6574" layer="21"/>
<rectangle x1="3.7529" y1="1.6193" x2="4.2482" y2="1.6574" layer="21"/>
<rectangle x1="15.1829" y1="1.6193" x2="15.3353" y2="1.6574" layer="21"/>
<rectangle x1="-0.0191" y1="1.6574" x2="0.1334" y2="1.6955" layer="21"/>
<rectangle x1="1.0478" y1="1.6574" x2="1.5431" y2="1.6955" layer="21"/>
<rectangle x1="3.791" y1="1.6574" x2="4.2863" y2="1.6955" layer="21"/>
<rectangle x1="15.1829" y1="1.6574" x2="15.3353" y2="1.6955" layer="21"/>
<rectangle x1="-0.0191" y1="1.6955" x2="0.1334" y2="1.7336" layer="21"/>
<rectangle x1="1.0097" y1="1.6955" x2="1.505" y2="1.7336" layer="21"/>
<rectangle x1="3.8291" y1="1.6955" x2="4.3244" y2="1.7336" layer="21"/>
<rectangle x1="15.1829" y1="1.6955" x2="15.3353" y2="1.7336" layer="21"/>
<rectangle x1="-0.0191" y1="1.7336" x2="0.1334" y2="1.7717" layer="21"/>
<rectangle x1="0.9716" y1="1.7336" x2="1.4669" y2="1.7717" layer="21"/>
<rectangle x1="3.8672" y1="1.7336" x2="4.3625" y2="1.7717" layer="21"/>
<rectangle x1="15.1829" y1="1.7336" x2="15.3353" y2="1.7717" layer="21"/>
<rectangle x1="-0.0191" y1="1.7717" x2="0.1334" y2="1.8098" layer="21"/>
<rectangle x1="0.9335" y1="1.7717" x2="1.4288" y2="1.8098" layer="21"/>
<rectangle x1="3.9053" y1="1.7717" x2="4.3625" y2="1.8098" layer="21"/>
<rectangle x1="15.1829" y1="1.7717" x2="15.3353" y2="1.8098" layer="21"/>
<rectangle x1="-0.0191" y1="1.8098" x2="0.1334" y2="1.8479" layer="21"/>
<rectangle x1="0.9335" y1="1.8098" x2="1.3907" y2="1.8479" layer="21"/>
<rectangle x1="3.9434" y1="1.8098" x2="4.4006" y2="1.8479" layer="21"/>
<rectangle x1="15.1829" y1="1.8098" x2="15.3353" y2="1.8479" layer="21"/>
<rectangle x1="-0.0191" y1="1.8479" x2="0.1334" y2="1.886" layer="21"/>
<rectangle x1="0.8954" y1="1.8479" x2="1.3526" y2="1.886" layer="21"/>
<rectangle x1="3.9815" y1="1.8479" x2="4.4006" y2="1.886" layer="21"/>
<rectangle x1="15.1829" y1="1.8479" x2="15.3353" y2="1.886" layer="21"/>
<rectangle x1="-0.0191" y1="1.886" x2="0.1334" y2="1.9241" layer="21"/>
<rectangle x1="0.8954" y1="1.886" x2="1.3145" y2="1.9241" layer="21"/>
<rectangle x1="3.9815" y1="1.886" x2="4.4387" y2="1.9241" layer="21"/>
<rectangle x1="15.1829" y1="1.886" x2="15.3353" y2="1.9241" layer="21"/>
<rectangle x1="-0.0191" y1="1.9241" x2="0.1334" y2="1.9622" layer="21"/>
<rectangle x1="0.8573" y1="1.9241" x2="1.3145" y2="1.9622" layer="21"/>
<rectangle x1="4.0196" y1="1.9241" x2="4.4387" y2="1.9622" layer="21"/>
<rectangle x1="15.1829" y1="1.9241" x2="15.3353" y2="1.9622" layer="21"/>
<rectangle x1="-0.0191" y1="1.9622" x2="0.1334" y2="2.0003" layer="21"/>
<rectangle x1="0.8573" y1="1.9622" x2="1.2764" y2="2.0003" layer="21"/>
<rectangle x1="4.0577" y1="1.9622" x2="4.4768" y2="2.0003" layer="21"/>
<rectangle x1="7.7534" y1="1.9622" x2="8.4011" y2="2.0003" layer="21"/>
<rectangle x1="11.411" y1="1.9622" x2="12.0587" y2="2.0003" layer="21"/>
<rectangle x1="15.1829" y1="1.9622" x2="15.3353" y2="2.0003" layer="21"/>
<rectangle x1="-0.0191" y1="2.0003" x2="0.1334" y2="2.0384" layer="21"/>
<rectangle x1="0.8192" y1="2.0003" x2="1.2383" y2="2.0384" layer="21"/>
<rectangle x1="4.0577" y1="2.0003" x2="4.4768" y2="2.0384" layer="21"/>
<rectangle x1="7.6391" y1="2.0003" x2="8.5535" y2="2.0384" layer="21"/>
<rectangle x1="11.2586" y1="2.0003" x2="12.173" y2="2.0384" layer="21"/>
<rectangle x1="15.1829" y1="2.0003" x2="15.3353" y2="2.0384" layer="21"/>
<rectangle x1="-0.0191" y1="2.0384" x2="0.1334" y2="2.0765" layer="21"/>
<rectangle x1="0.8192" y1="2.0384" x2="1.2383" y2="2.0765" layer="21"/>
<rectangle x1="4.0958" y1="2.0384" x2="4.5149" y2="2.0765" layer="21"/>
<rectangle x1="7.5248" y1="2.0384" x2="8.6678" y2="2.0765" layer="21"/>
<rectangle x1="11.1443" y1="2.0384" x2="12.2873" y2="2.0765" layer="21"/>
<rectangle x1="15.1829" y1="2.0384" x2="15.3353" y2="2.0765" layer="21"/>
<rectangle x1="-0.0191" y1="2.0765" x2="0.1334" y2="2.1146" layer="21"/>
<rectangle x1="0.7811" y1="2.0765" x2="1.2002" y2="2.1146" layer="21"/>
<rectangle x1="4.0958" y1="2.0765" x2="4.5149" y2="2.1146" layer="21"/>
<rectangle x1="7.4486" y1="2.0765" x2="8.744" y2="2.1146" layer="21"/>
<rectangle x1="11.0681" y1="2.0765" x2="12.3635" y2="2.1146" layer="21"/>
<rectangle x1="15.1829" y1="2.0765" x2="15.3353" y2="2.1146" layer="21"/>
<rectangle x1="-0.0191" y1="2.1146" x2="0.1334" y2="2.1527" layer="21"/>
<rectangle x1="0.7811" y1="2.1146" x2="1.2002" y2="2.1527" layer="21"/>
<rectangle x1="4.1339" y1="2.1146" x2="4.553" y2="2.1527" layer="21"/>
<rectangle x1="7.3724" y1="2.1146" x2="8.8202" y2="2.1527" layer="21"/>
<rectangle x1="11.03" y1="2.1146" x2="12.4397" y2="2.1527" layer="21"/>
<rectangle x1="15.1829" y1="2.1146" x2="15.3353" y2="2.1527" layer="21"/>
<rectangle x1="-0.0191" y1="2.1527" x2="0.1334" y2="2.1908" layer="21"/>
<rectangle x1="0.7811" y1="2.1527" x2="1.1621" y2="2.1908" layer="21"/>
<rectangle x1="4.1339" y1="2.1527" x2="4.553" y2="2.1908" layer="21"/>
<rectangle x1="7.2962" y1="2.1527" x2="8.8583" y2="2.1908" layer="21"/>
<rectangle x1="10.9538" y1="2.1527" x2="12.5159" y2="2.1908" layer="21"/>
<rectangle x1="15.1829" y1="2.1527" x2="15.3353" y2="2.1908" layer="21"/>
<rectangle x1="-0.0191" y1="2.1908" x2="0.1334" y2="2.2289" layer="21"/>
<rectangle x1="0.743" y1="2.1908" x2="1.1621" y2="2.2289" layer="21"/>
<rectangle x1="4.172" y1="2.1908" x2="4.553" y2="2.2289" layer="21"/>
<rectangle x1="7.2581" y1="2.1908" x2="8.9345" y2="2.2289" layer="21"/>
<rectangle x1="10.8776" y1="2.1908" x2="12.554" y2="2.2289" layer="21"/>
<rectangle x1="15.1829" y1="2.1908" x2="15.3353" y2="2.2289" layer="21"/>
<rectangle x1="-0.0191" y1="2.2289" x2="0.1334" y2="2.267" layer="21"/>
<rectangle x1="0.743" y1="2.2289" x2="1.124" y2="2.267" layer="21"/>
<rectangle x1="1.9622" y1="2.2289" x2="2.267" y2="2.267" layer="21"/>
<rectangle x1="3.1052" y1="2.2289" x2="3.41" y2="2.267" layer="21"/>
<rectangle x1="4.172" y1="2.2289" x2="4.5911" y2="2.267" layer="21"/>
<rectangle x1="7.22" y1="2.2289" x2="7.8296" y2="2.267" layer="21"/>
<rectangle x1="8.3249" y1="2.2289" x2="8.9726" y2="2.267" layer="21"/>
<rectangle x1="10.8395" y1="2.2289" x2="11.4872" y2="2.267" layer="21"/>
<rectangle x1="11.9825" y1="2.2289" x2="12.6302" y2="2.267" layer="21"/>
<rectangle x1="15.1829" y1="2.2289" x2="15.3353" y2="2.267" layer="21"/>
<rectangle x1="-0.0191" y1="2.267" x2="0.1334" y2="2.3051" layer="21"/>
<rectangle x1="0.743" y1="2.267" x2="1.124" y2="2.3051" layer="21"/>
<rectangle x1="1.8479" y1="2.267" x2="2.3813" y2="2.3051" layer="21"/>
<rectangle x1="2.9909" y1="2.267" x2="3.5243" y2="2.3051" layer="21"/>
<rectangle x1="4.2101" y1="2.267" x2="4.5911" y2="2.3051" layer="21"/>
<rectangle x1="7.1438" y1="2.267" x2="7.7153" y2="2.3051" layer="21"/>
<rectangle x1="8.4392" y1="2.267" x2="9.0107" y2="2.3051" layer="21"/>
<rectangle x1="10.8014" y1="2.267" x2="11.3729" y2="2.3051" layer="21"/>
<rectangle x1="12.0968" y1="2.267" x2="12.6683" y2="2.3051" layer="21"/>
<rectangle x1="15.1829" y1="2.267" x2="15.3353" y2="2.3051" layer="21"/>
<rectangle x1="-0.0191" y1="2.3051" x2="0.1334" y2="2.3432" layer="21"/>
<rectangle x1="0.7049" y1="2.3051" x2="1.124" y2="2.3432" layer="21"/>
<rectangle x1="1.7717" y1="2.3051" x2="2.4575" y2="2.3432" layer="21"/>
<rectangle x1="2.9147" y1="2.3051" x2="3.6005" y2="2.3432" layer="21"/>
<rectangle x1="4.2101" y1="2.3051" x2="4.5911" y2="2.3432" layer="21"/>
<rectangle x1="7.1057" y1="2.3051" x2="7.601" y2="2.3432" layer="21"/>
<rectangle x1="8.5535" y1="2.3051" x2="9.0869" y2="2.3432" layer="21"/>
<rectangle x1="10.7633" y1="2.3051" x2="11.2586" y2="2.3432" layer="21"/>
<rectangle x1="12.2111" y1="2.3051" x2="12.7064" y2="2.3432" layer="21"/>
<rectangle x1="15.1829" y1="2.3051" x2="15.3353" y2="2.3432" layer="21"/>
<rectangle x1="-0.0191" y1="2.3432" x2="0.1334" y2="2.3813" layer="21"/>
<rectangle x1="0.7049" y1="2.3432" x2="1.0859" y2="2.3813" layer="21"/>
<rectangle x1="1.7336" y1="2.3432" x2="2.4956" y2="2.3813" layer="21"/>
<rectangle x1="2.8766" y1="2.3432" x2="3.6386" y2="2.3813" layer="21"/>
<rectangle x1="4.2101" y1="2.3432" x2="4.5911" y2="2.3813" layer="21"/>
<rectangle x1="7.0676" y1="2.3432" x2="7.5248" y2="2.3813" layer="21"/>
<rectangle x1="8.6297" y1="2.3432" x2="9.125" y2="2.3813" layer="21"/>
<rectangle x1="10.6871" y1="2.3432" x2="11.1824" y2="2.3813" layer="21"/>
<rectangle x1="12.2873" y1="2.3432" x2="12.7445" y2="2.3813" layer="21"/>
<rectangle x1="15.1829" y1="2.3432" x2="15.3353" y2="2.3813" layer="21"/>
<rectangle x1="-0.0191" y1="2.3813" x2="0.1334" y2="2.4194" layer="21"/>
<rectangle x1="0.7049" y1="2.3813" x2="1.0859" y2="2.4194" layer="21"/>
<rectangle x1="1.6574" y1="2.3813" x2="2.5337" y2="2.4194" layer="21"/>
<rectangle x1="2.8385" y1="2.3813" x2="3.6767" y2="2.4194" layer="21"/>
<rectangle x1="4.2482" y1="2.3813" x2="4.6292" y2="2.4194" layer="21"/>
<rectangle x1="7.0295" y1="2.3813" x2="7.4486" y2="2.4194" layer="21"/>
<rectangle x1="8.7059" y1="2.3813" x2="9.1631" y2="2.4194" layer="21"/>
<rectangle x1="10.649" y1="2.3813" x2="11.1062" y2="2.4194" layer="21"/>
<rectangle x1="12.3254" y1="2.3813" x2="12.7826" y2="2.4194" layer="21"/>
<rectangle x1="15.1829" y1="2.3813" x2="15.3353" y2="2.4194" layer="21"/>
<rectangle x1="-0.0191" y1="2.4194" x2="0.1334" y2="2.4575" layer="21"/>
<rectangle x1="0.7049" y1="2.4194" x2="1.0859" y2="2.4575" layer="21"/>
<rectangle x1="1.6574" y1="2.4194" x2="2.5718" y2="2.4575" layer="21"/>
<rectangle x1="2.8004" y1="2.4194" x2="3.7148" y2="2.4575" layer="21"/>
<rectangle x1="4.2482" y1="2.4194" x2="4.6292" y2="2.4575" layer="21"/>
<rectangle x1="6.9914" y1="2.4194" x2="7.4105" y2="2.4575" layer="21"/>
<rectangle x1="8.7821" y1="2.4194" x2="9.2012" y2="2.4575" layer="21"/>
<rectangle x1="10.6109" y1="2.4194" x2="11.03" y2="2.4575" layer="21"/>
<rectangle x1="12.4016" y1="2.4194" x2="12.8207" y2="2.4575" layer="21"/>
<rectangle x1="15.1829" y1="2.4194" x2="15.3353" y2="2.4575" layer="21"/>
<rectangle x1="-0.0191" y1="2.4575" x2="0.1334" y2="2.4956" layer="21"/>
<rectangle x1="0.6668" y1="2.4575" x2="1.0478" y2="2.4956" layer="21"/>
<rectangle x1="1.6193" y1="2.4575" x2="2.6099" y2="2.4956" layer="21"/>
<rectangle x1="2.7623" y1="2.4575" x2="3.7529" y2="2.4956" layer="21"/>
<rectangle x1="4.2482" y1="2.4575" x2="4.6292" y2="2.4956" layer="21"/>
<rectangle x1="6.9533" y1="2.4575" x2="7.3724" y2="2.4956" layer="21"/>
<rectangle x1="8.8202" y1="2.4575" x2="9.2393" y2="2.4956" layer="21"/>
<rectangle x1="10.6109" y1="2.4575" x2="10.9919" y2="2.4956" layer="21"/>
<rectangle x1="12.4397" y1="2.4575" x2="12.8588" y2="2.4956" layer="21"/>
<rectangle x1="15.1829" y1="2.4575" x2="15.3353" y2="2.4956" layer="21"/>
<rectangle x1="-0.0191" y1="2.4956" x2="0.1334" y2="2.5337" layer="21"/>
<rectangle x1="0.6668" y1="2.4956" x2="1.0478" y2="2.5337" layer="21"/>
<rectangle x1="1.5812" y1="2.4956" x2="2.0384" y2="2.5337" layer="21"/>
<rectangle x1="2.2289" y1="2.4956" x2="2.6099" y2="2.5337" layer="21"/>
<rectangle x1="2.7623" y1="2.4956" x2="3.1814" y2="2.5337" layer="21"/>
<rectangle x1="3.41" y1="2.4956" x2="3.791" y2="2.5337" layer="21"/>
<rectangle x1="4.2482" y1="2.4956" x2="4.6292" y2="2.5337" layer="21"/>
<rectangle x1="6.9152" y1="2.4956" x2="7.2962" y2="2.5337" layer="21"/>
<rectangle x1="7.8677" y1="2.4956" x2="8.3249" y2="2.5337" layer="21"/>
<rectangle x1="8.8583" y1="2.4956" x2="9.2393" y2="2.5337" layer="21"/>
<rectangle x1="10.5728" y1="2.4956" x2="10.9538" y2="2.5337" layer="21"/>
<rectangle x1="12.5159" y1="2.4956" x2="12.8969" y2="2.5337" layer="21"/>
<rectangle x1="15.1829" y1="2.4956" x2="15.3353" y2="2.5337" layer="21"/>
<rectangle x1="-0.0191" y1="2.5337" x2="0.1334" y2="2.5718" layer="21"/>
<rectangle x1="0.6668" y1="2.5337" x2="1.0478" y2="2.5718" layer="21"/>
<rectangle x1="1.5812" y1="2.5337" x2="2.0003" y2="2.5718" layer="21"/>
<rectangle x1="2.3051" y1="2.5337" x2="2.6099" y2="2.5718" layer="21"/>
<rectangle x1="2.7242" y1="2.5337" x2="3.1433" y2="2.5718" layer="21"/>
<rectangle x1="3.4481" y1="2.5337" x2="3.7529" y2="2.5718" layer="21"/>
<rectangle x1="4.2863" y1="2.5337" x2="4.6292" y2="2.5718" layer="21"/>
<rectangle x1="6.9152" y1="2.5337" x2="7.2581" y2="2.5718" layer="21"/>
<rectangle x1="7.8677" y1="2.5337" x2="8.3249" y2="2.5718" layer="21"/>
<rectangle x1="8.8964" y1="2.5337" x2="9.2774" y2="2.5718" layer="21"/>
<rectangle x1="10.5347" y1="2.5337" x2="10.9157" y2="2.5718" layer="21"/>
<rectangle x1="12.554" y1="2.5337" x2="12.935" y2="2.5718" layer="21"/>
<rectangle x1="15.1829" y1="2.5337" x2="15.3353" y2="2.5718" layer="21"/>
<rectangle x1="-0.0191" y1="2.5718" x2="0.1334" y2="2.6099" layer="21"/>
<rectangle x1="0.6668" y1="2.5718" x2="1.0478" y2="2.6099" layer="21"/>
<rectangle x1="1.5431" y1="2.5718" x2="1.9241" y2="2.6099" layer="21"/>
<rectangle x1="2.3432" y1="2.5718" x2="2.5337" y2="2.6099" layer="21"/>
<rectangle x1="2.7242" y1="2.5718" x2="3.1052" y2="2.6099" layer="21"/>
<rectangle x1="3.4862" y1="2.5718" x2="3.6767" y2="2.6099" layer="21"/>
<rectangle x1="4.2863" y1="2.5718" x2="4.6292" y2="2.6099" layer="21"/>
<rectangle x1="6.8771" y1="2.5718" x2="7.22" y2="2.6099" layer="21"/>
<rectangle x1="7.8677" y1="2.5718" x2="8.3249" y2="2.6099" layer="21"/>
<rectangle x1="8.9726" y1="2.5718" x2="9.3155" y2="2.6099" layer="21"/>
<rectangle x1="10.4966" y1="2.5718" x2="10.8776" y2="2.6099" layer="21"/>
<rectangle x1="12.5921" y1="2.5718" x2="12.9731" y2="2.6099" layer="21"/>
<rectangle x1="15.1829" y1="2.5718" x2="15.3353" y2="2.6099" layer="21"/>
<rectangle x1="-0.0191" y1="2.6099" x2="0.1334" y2="2.648" layer="21"/>
<rectangle x1="0.6668" y1="2.6099" x2="1.0478" y2="2.648" layer="21"/>
<rectangle x1="1.5431" y1="2.6099" x2="1.9241" y2="2.648" layer="21"/>
<rectangle x1="2.3813" y1="2.6099" x2="2.4575" y2="2.648" layer="21"/>
<rectangle x1="2.6861" y1="2.6099" x2="3.0671" y2="2.648" layer="21"/>
<rectangle x1="3.5243" y1="2.6099" x2="3.6005" y2="2.648" layer="21"/>
<rectangle x1="4.2863" y1="2.6099" x2="4.6292" y2="2.648" layer="21"/>
<rectangle x1="6.839" y1="2.6099" x2="7.1819" y2="2.648" layer="21"/>
<rectangle x1="7.8677" y1="2.6099" x2="8.3249" y2="2.648" layer="21"/>
<rectangle x1="8.9726" y1="2.6099" x2="9.3536" y2="2.648" layer="21"/>
<rectangle x1="10.4585" y1="2.6099" x2="10.8395" y2="2.648" layer="21"/>
<rectangle x1="11.6777" y1="2.6099" x2="11.7539" y2="2.648" layer="21"/>
<rectangle x1="12.6302" y1="2.6099" x2="12.9731" y2="2.648" layer="21"/>
<rectangle x1="15.1829" y1="2.6099" x2="15.3353" y2="2.648" layer="21"/>
<rectangle x1="-0.0191" y1="2.648" x2="0.1334" y2="2.6861" layer="21"/>
<rectangle x1="0.6668" y1="2.648" x2="1.0478" y2="2.6861" layer="21"/>
<rectangle x1="1.5431" y1="2.648" x2="1.9241" y2="2.6861" layer="21"/>
<rectangle x1="2.6861" y1="2.648" x2="3.0671" y2="2.6861" layer="21"/>
<rectangle x1="4.2863" y1="2.648" x2="4.6292" y2="2.6861" layer="21"/>
<rectangle x1="6.8009" y1="2.648" x2="7.1438" y2="2.6861" layer="21"/>
<rectangle x1="7.8677" y1="2.648" x2="8.3249" y2="2.6861" layer="21"/>
<rectangle x1="9.0107" y1="2.648" x2="9.3536" y2="2.6861" layer="21"/>
<rectangle x1="10.4585" y1="2.648" x2="10.8014" y2="2.6861" layer="21"/>
<rectangle x1="11.4491" y1="2.648" x2="11.9825" y2="2.6861" layer="21"/>
<rectangle x1="12.6683" y1="2.648" x2="13.0112" y2="2.6861" layer="21"/>
<rectangle x1="15.1829" y1="2.648" x2="15.3353" y2="2.6861" layer="21"/>
<rectangle x1="-0.0191" y1="2.6861" x2="0.1334" y2="2.7242" layer="21"/>
<rectangle x1="0.6668" y1="2.6861" x2="1.0478" y2="2.7242" layer="21"/>
<rectangle x1="1.5431" y1="2.6861" x2="1.886" y2="2.7242" layer="21"/>
<rectangle x1="2.6861" y1="2.6861" x2="3.029" y2="2.7242" layer="21"/>
<rectangle x1="4.2863" y1="2.6861" x2="4.6292" y2="2.7242" layer="21"/>
<rectangle x1="6.8009" y1="2.6861" x2="7.1438" y2="2.7242" layer="21"/>
<rectangle x1="7.8677" y1="2.6861" x2="8.3249" y2="2.7242" layer="21"/>
<rectangle x1="9.0488" y1="2.6861" x2="9.3917" y2="2.7242" layer="21"/>
<rectangle x1="10.4204" y1="2.6861" x2="10.7633" y2="2.7242" layer="21"/>
<rectangle x1="11.3729" y1="2.6861" x2="12.0587" y2="2.7242" layer="21"/>
<rectangle x1="12.7064" y1="2.6861" x2="13.0112" y2="2.7242" layer="21"/>
<rectangle x1="15.1829" y1="2.6861" x2="15.3353" y2="2.7242" layer="21"/>
<rectangle x1="-0.0191" y1="2.7242" x2="0.1334" y2="2.7623" layer="21"/>
<rectangle x1="0.6668" y1="2.7242" x2="1.0097" y2="2.7623" layer="21"/>
<rectangle x1="1.5431" y1="2.7242" x2="1.886" y2="2.7623" layer="21"/>
<rectangle x1="2.6861" y1="2.7242" x2="3.029" y2="2.7623" layer="21"/>
<rectangle x1="4.2863" y1="2.7242" x2="4.6673" y2="2.7623" layer="21"/>
<rectangle x1="6.7628" y1="2.7242" x2="7.1057" y2="2.7623" layer="21"/>
<rectangle x1="7.8677" y1="2.7242" x2="8.3249" y2="2.7623" layer="21"/>
<rectangle x1="9.0869" y1="2.7242" x2="9.3917" y2="2.7623" layer="21"/>
<rectangle x1="10.4204" y1="2.7242" x2="10.7252" y2="2.7623" layer="21"/>
<rectangle x1="11.3348" y1="2.7242" x2="12.1349" y2="2.7623" layer="21"/>
<rectangle x1="12.7064" y1="2.7242" x2="13.0493" y2="2.7623" layer="21"/>
<rectangle x1="15.1829" y1="2.7242" x2="15.3353" y2="2.7623" layer="21"/>
<rectangle x1="-0.0191" y1="2.7623" x2="0.1334" y2="2.8004" layer="21"/>
<rectangle x1="0.6668" y1="2.7623" x2="1.0097" y2="2.8004" layer="21"/>
<rectangle x1="1.505" y1="2.7623" x2="1.886" y2="2.8004" layer="21"/>
<rectangle x1="2.6861" y1="2.7623" x2="3.029" y2="2.8004" layer="21"/>
<rectangle x1="4.2863" y1="2.7623" x2="4.6673" y2="2.8004" layer="21"/>
<rectangle x1="6.7628" y1="2.7623" x2="7.0676" y2="2.8004" layer="21"/>
<rectangle x1="7.8677" y1="2.7623" x2="8.3249" y2="2.8004" layer="21"/>
<rectangle x1="9.125" y1="2.7623" x2="9.4298" y2="2.8004" layer="21"/>
<rectangle x1="10.3823" y1="2.7623" x2="10.6871" y2="2.8004" layer="21"/>
<rectangle x1="11.2586" y1="2.7623" x2="12.173" y2="2.8004" layer="21"/>
<rectangle x1="12.7445" y1="2.7623" x2="13.0493" y2="2.8004" layer="21"/>
<rectangle x1="15.1829" y1="2.7623" x2="15.3353" y2="2.8004" layer="21"/>
<rectangle x1="-0.0191" y1="2.8004" x2="0.1334" y2="2.8385" layer="21"/>
<rectangle x1="0.6668" y1="2.8004" x2="1.0097" y2="2.8385" layer="21"/>
<rectangle x1="1.505" y1="2.8004" x2="1.886" y2="2.8385" layer="21"/>
<rectangle x1="2.6861" y1="2.8004" x2="3.029" y2="2.8385" layer="21"/>
<rectangle x1="4.2863" y1="2.8004" x2="4.6673" y2="2.8385" layer="21"/>
<rectangle x1="6.7247" y1="2.8004" x2="7.0295" y2="2.8385" layer="21"/>
<rectangle x1="7.8677" y1="2.8004" x2="8.3249" y2="2.8385" layer="21"/>
<rectangle x1="9.125" y1="2.8004" x2="9.4679" y2="2.8385" layer="21"/>
<rectangle x1="10.3823" y1="2.8004" x2="10.6871" y2="2.8385" layer="21"/>
<rectangle x1="11.2205" y1="2.8004" x2="12.2111" y2="2.8385" layer="21"/>
<rectangle x1="12.7826" y1="2.8004" x2="13.0874" y2="2.8385" layer="21"/>
<rectangle x1="15.1829" y1="2.8004" x2="15.3353" y2="2.8385" layer="21"/>
<rectangle x1="-0.0191" y1="2.8385" x2="0.1334" y2="2.8766" layer="21"/>
<rectangle x1="0.6287" y1="2.8385" x2="1.0097" y2="2.8766" layer="21"/>
<rectangle x1="1.505" y1="2.8385" x2="1.886" y2="2.8766" layer="21"/>
<rectangle x1="2.6861" y1="2.8385" x2="3.029" y2="2.8766" layer="21"/>
<rectangle x1="4.2863" y1="2.8385" x2="4.6673" y2="2.8766" layer="21"/>
<rectangle x1="6.7247" y1="2.8385" x2="7.0295" y2="2.8766" layer="21"/>
<rectangle x1="7.8677" y1="2.8385" x2="8.3249" y2="2.8766" layer="21"/>
<rectangle x1="9.1631" y1="2.8385" x2="9.4679" y2="2.8766" layer="21"/>
<rectangle x1="10.3442" y1="2.8385" x2="10.649" y2="2.8766" layer="21"/>
<rectangle x1="11.1824" y1="2.8385" x2="12.2492" y2="2.8766" layer="21"/>
<rectangle x1="12.7826" y1="2.8385" x2="13.0874" y2="2.8766" layer="21"/>
<rectangle x1="15.1829" y1="2.8385" x2="15.3353" y2="2.8766" layer="21"/>
<rectangle x1="-0.0191" y1="2.8766" x2="0.1334" y2="2.9147" layer="21"/>
<rectangle x1="0.6668" y1="2.8766" x2="1.0097" y2="2.9147" layer="21"/>
<rectangle x1="1.505" y1="2.8766" x2="1.886" y2="2.9147" layer="21"/>
<rectangle x1="2.6861" y1="2.8766" x2="3.029" y2="2.9147" layer="21"/>
<rectangle x1="4.2863" y1="2.8766" x2="4.6673" y2="2.9147" layer="21"/>
<rectangle x1="6.6866" y1="2.8766" x2="6.9914" y2="2.9147" layer="21"/>
<rectangle x1="7.8677" y1="2.8766" x2="8.3249" y2="2.9147" layer="21"/>
<rectangle x1="9.1631" y1="2.8766" x2="9.4679" y2="2.9147" layer="21"/>
<rectangle x1="10.3442" y1="2.8766" x2="10.649" y2="2.9147" layer="21"/>
<rectangle x1="11.1824" y1="2.8766" x2="12.2873" y2="2.9147" layer="21"/>
<rectangle x1="12.8207" y1="2.8766" x2="13.1255" y2="2.9147" layer="21"/>
<rectangle x1="15.1829" y1="2.8766" x2="15.3353" y2="2.9147" layer="21"/>
<rectangle x1="-0.0191" y1="2.9147" x2="0.1334" y2="2.9528" layer="21"/>
<rectangle x1="0.6668" y1="2.9147" x2="1.0097" y2="2.9528" layer="21"/>
<rectangle x1="1.505" y1="2.9147" x2="1.886" y2="2.9528" layer="21"/>
<rectangle x1="2.6861" y1="2.9147" x2="3.029" y2="2.9528" layer="21"/>
<rectangle x1="4.2863" y1="2.9147" x2="4.6673" y2="2.9528" layer="21"/>
<rectangle x1="6.6866" y1="2.9147" x2="6.9914" y2="2.9528" layer="21"/>
<rectangle x1="7.8677" y1="2.9147" x2="8.3249" y2="2.9528" layer="21"/>
<rectangle x1="9.2012" y1="2.9147" x2="9.506" y2="2.9528" layer="21"/>
<rectangle x1="10.3061" y1="2.9147" x2="10.6109" y2="2.9528" layer="21"/>
<rectangle x1="11.1443" y1="2.9147" x2="12.3254" y2="2.9528" layer="21"/>
<rectangle x1="12.8207" y1="2.9147" x2="13.1255" y2="2.9528" layer="21"/>
<rectangle x1="15.1829" y1="2.9147" x2="15.3353" y2="2.9528" layer="21"/>
<rectangle x1="-0.0191" y1="2.9528" x2="0.1334" y2="2.9909" layer="21"/>
<rectangle x1="0.6668" y1="2.9528" x2="1.0097" y2="2.9909" layer="21"/>
<rectangle x1="1.5431" y1="2.9528" x2="1.886" y2="2.9909" layer="21"/>
<rectangle x1="2.6861" y1="2.9528" x2="3.029" y2="2.9909" layer="21"/>
<rectangle x1="4.2863" y1="2.9528" x2="4.6673" y2="2.9909" layer="21"/>
<rectangle x1="6.6866" y1="2.9528" x2="6.9533" y2="2.9909" layer="21"/>
<rectangle x1="7.8677" y1="2.9528" x2="8.3249" y2="2.9909" layer="21"/>
<rectangle x1="9.2012" y1="2.9528" x2="9.506" y2="2.9909" layer="21"/>
<rectangle x1="10.3061" y1="2.9528" x2="10.6109" y2="2.9909" layer="21"/>
<rectangle x1="11.1062" y1="2.9528" x2="11.5634" y2="2.9909" layer="21"/>
<rectangle x1="11.9063" y1="2.9528" x2="12.3635" y2="2.9909" layer="21"/>
<rectangle x1="12.8588" y1="2.9528" x2="13.1255" y2="2.9909" layer="21"/>
<rectangle x1="15.1829" y1="2.9528" x2="15.3353" y2="2.9909" layer="21"/>
<rectangle x1="-0.0191" y1="2.9909" x2="0.1334" y2="3.029" layer="21"/>
<rectangle x1="0.6668" y1="2.9909" x2="1.0478" y2="3.029" layer="21"/>
<rectangle x1="1.5431" y1="2.9909" x2="1.886" y2="3.029" layer="21"/>
<rectangle x1="2.6861" y1="2.9909" x2="3.029" y2="3.029" layer="21"/>
<rectangle x1="4.2863" y1="2.9909" x2="4.6292" y2="3.029" layer="21"/>
<rectangle x1="6.6485" y1="2.9909" x2="6.9533" y2="3.029" layer="21"/>
<rectangle x1="7.8677" y1="2.9909" x2="8.3249" y2="3.029" layer="21"/>
<rectangle x1="9.2393" y1="2.9909" x2="9.506" y2="3.029" layer="21"/>
<rectangle x1="10.3061" y1="2.9909" x2="10.5728" y2="3.029" layer="21"/>
<rectangle x1="11.1062" y1="2.9909" x2="11.5253" y2="3.029" layer="21"/>
<rectangle x1="11.9444" y1="2.9909" x2="12.3635" y2="3.029" layer="21"/>
<rectangle x1="12.8588" y1="2.9909" x2="13.1636" y2="3.029" layer="21"/>
<rectangle x1="15.1829" y1="2.9909" x2="15.3353" y2="3.029" layer="21"/>
<rectangle x1="-0.0191" y1="3.029" x2="0.1334" y2="3.0671" layer="21"/>
<rectangle x1="0.6668" y1="3.029" x2="1.0478" y2="3.0671" layer="21"/>
<rectangle x1="1.5431" y1="3.029" x2="1.9241" y2="3.0671" layer="21"/>
<rectangle x1="2.6861" y1="3.029" x2="3.0671" y2="3.0671" layer="21"/>
<rectangle x1="4.2863" y1="3.029" x2="4.6292" y2="3.0671" layer="21"/>
<rectangle x1="6.6485" y1="3.029" x2="6.9533" y2="3.0671" layer="21"/>
<rectangle x1="7.8677" y1="3.029" x2="8.3249" y2="3.0671" layer="21"/>
<rectangle x1="9.2393" y1="3.029" x2="9.5441" y2="3.0671" layer="21"/>
<rectangle x1="10.268" y1="3.029" x2="10.5728" y2="3.0671" layer="21"/>
<rectangle x1="11.1062" y1="3.029" x2="11.4872" y2="3.0671" layer="21"/>
<rectangle x1="11.9825" y1="3.029" x2="12.4016" y2="3.0671" layer="21"/>
<rectangle x1="12.8588" y1="3.029" x2="13.1636" y2="3.0671" layer="21"/>
<rectangle x1="15.1829" y1="3.029" x2="15.3353" y2="3.0671" layer="21"/>
<rectangle x1="-0.0191" y1="3.0671" x2="0.1334" y2="3.1052" layer="21"/>
<rectangle x1="0.6668" y1="3.0671" x2="1.0478" y2="3.1052" layer="21"/>
<rectangle x1="1.5431" y1="3.0671" x2="1.9241" y2="3.1052" layer="21"/>
<rectangle x1="2.3432" y1="3.0671" x2="2.4575" y2="3.1052" layer="21"/>
<rectangle x1="2.7242" y1="3.0671" x2="3.0671" y2="3.1052" layer="21"/>
<rectangle x1="3.4862" y1="3.0671" x2="3.6005" y2="3.1052" layer="21"/>
<rectangle x1="4.2863" y1="3.0671" x2="4.6292" y2="3.1052" layer="21"/>
<rectangle x1="6.6485" y1="3.0671" x2="6.9152" y2="3.1052" layer="21"/>
<rectangle x1="7.8677" y1="3.0671" x2="8.3249" y2="3.1052" layer="21"/>
<rectangle x1="9.2393" y1="3.0671" x2="9.5441" y2="3.1052" layer="21"/>
<rectangle x1="10.268" y1="3.0671" x2="10.5728" y2="3.1052" layer="21"/>
<rectangle x1="11.0681" y1="3.0671" x2="11.4491" y2="3.1052" layer="21"/>
<rectangle x1="12.0206" y1="3.0671" x2="12.4016" y2="3.1052" layer="21"/>
<rectangle x1="12.8969" y1="3.0671" x2="13.1636" y2="3.1052" layer="21"/>
<rectangle x1="15.1829" y1="3.0671" x2="15.3353" y2="3.1052" layer="21"/>
<rectangle x1="-0.0191" y1="3.1052" x2="0.1334" y2="3.1433" layer="21"/>
<rectangle x1="0.6668" y1="3.1052" x2="1.0478" y2="3.1433" layer="21"/>
<rectangle x1="1.5812" y1="3.1052" x2="1.9622" y2="3.1433" layer="21"/>
<rectangle x1="2.3051" y1="3.1052" x2="2.5337" y2="3.1433" layer="21"/>
<rectangle x1="2.7242" y1="3.1052" x2="3.1052" y2="3.1433" layer="21"/>
<rectangle x1="3.4481" y1="3.1052" x2="3.6767" y2="3.1433" layer="21"/>
<rectangle x1="4.2863" y1="3.1052" x2="4.6292" y2="3.1433" layer="21"/>
<rectangle x1="6.6104" y1="3.1052" x2="6.9152" y2="3.1433" layer="21"/>
<rectangle x1="7.8677" y1="3.1052" x2="8.3249" y2="3.1433" layer="21"/>
<rectangle x1="9.2774" y1="3.1052" x2="9.5441" y2="3.1433" layer="21"/>
<rectangle x1="10.268" y1="3.1052" x2="10.5347" y2="3.1433" layer="21"/>
<rectangle x1="11.0681" y1="3.1052" x2="11.4491" y2="3.1433" layer="21"/>
<rectangle x1="12.0206" y1="3.1052" x2="12.4397" y2="3.1433" layer="21"/>
<rectangle x1="12.8969" y1="3.1052" x2="13.2017" y2="3.1433" layer="21"/>
<rectangle x1="15.1829" y1="3.1052" x2="15.3353" y2="3.1433" layer="21"/>
<rectangle x1="-0.0191" y1="3.1433" x2="0.1334" y2="3.1814" layer="21"/>
<rectangle x1="0.6668" y1="3.1433" x2="1.0478" y2="3.1814" layer="21"/>
<rectangle x1="1.5812" y1="3.1433" x2="2.0003" y2="3.1814" layer="21"/>
<rectangle x1="2.267" y1="3.1433" x2="2.6099" y2="3.1814" layer="21"/>
<rectangle x1="2.7623" y1="3.1433" x2="3.1433" y2="3.1814" layer="21"/>
<rectangle x1="3.41" y1="3.1433" x2="3.7529" y2="3.1814" layer="21"/>
<rectangle x1="4.2482" y1="3.1433" x2="4.6292" y2="3.1814" layer="21"/>
<rectangle x1="6.6104" y1="3.1433" x2="6.9152" y2="3.1814" layer="21"/>
<rectangle x1="7.8677" y1="3.1433" x2="8.3249" y2="3.1814" layer="21"/>
<rectangle x1="9.2774" y1="3.1433" x2="9.5441" y2="3.1814" layer="21"/>
<rectangle x1="10.268" y1="3.1433" x2="10.5347" y2="3.1814" layer="21"/>
<rectangle x1="11.0681" y1="3.1433" x2="11.4491" y2="3.1814" layer="21"/>
<rectangle x1="12.0587" y1="3.1433" x2="12.4397" y2="3.1814" layer="21"/>
<rectangle x1="12.8969" y1="3.1433" x2="13.2017" y2="3.1814" layer="21"/>
<rectangle x1="15.1829" y1="3.1433" x2="15.3353" y2="3.1814" layer="21"/>
<rectangle x1="-0.0191" y1="3.1814" x2="0.1334" y2="3.2195" layer="21"/>
<rectangle x1="0.6668" y1="3.1814" x2="1.0478" y2="3.2195" layer="21"/>
<rectangle x1="1.6193" y1="3.1814" x2="2.1146" y2="3.2195" layer="21"/>
<rectangle x1="2.1908" y1="3.1814" x2="2.6099" y2="3.2195" layer="21"/>
<rectangle x1="2.7623" y1="3.1814" x2="3.2576" y2="3.2195" layer="21"/>
<rectangle x1="3.3338" y1="3.1814" x2="3.7529" y2="3.2195" layer="21"/>
<rectangle x1="4.2482" y1="3.1814" x2="4.6292" y2="3.2195" layer="21"/>
<rectangle x1="6.6104" y1="3.1814" x2="6.8771" y2="3.2195" layer="21"/>
<rectangle x1="7.7153" y1="3.1814" x2="8.4773" y2="3.2195" layer="21"/>
<rectangle x1="9.2774" y1="3.1814" x2="9.5441" y2="3.2195" layer="21"/>
<rectangle x1="10.268" y1="3.1814" x2="10.5347" y2="3.2195" layer="21"/>
<rectangle x1="11.0681" y1="3.1814" x2="11.411" y2="3.2195" layer="21"/>
<rectangle x1="12.0587" y1="3.1814" x2="12.4397" y2="3.2195" layer="21"/>
<rectangle x1="12.935" y1="3.1814" x2="13.2017" y2="3.2195" layer="21"/>
<rectangle x1="15.1829" y1="3.1814" x2="15.3353" y2="3.2195" layer="21"/>
<rectangle x1="-0.0191" y1="3.2195" x2="0.1334" y2="3.2576" layer="21"/>
<rectangle x1="0.6668" y1="3.2195" x2="1.0478" y2="3.2576" layer="21"/>
<rectangle x1="1.6193" y1="3.2195" x2="2.5718" y2="3.2576" layer="21"/>
<rectangle x1="2.8004" y1="3.2195" x2="3.7529" y2="3.2576" layer="21"/>
<rectangle x1="4.2482" y1="3.2195" x2="4.6292" y2="3.2576" layer="21"/>
<rectangle x1="6.6104" y1="3.2195" x2="6.8771" y2="3.2576" layer="21"/>
<rectangle x1="7.6772" y1="3.2195" x2="8.4773" y2="3.2576" layer="21"/>
<rectangle x1="9.2774" y1="3.2195" x2="9.5441" y2="3.2576" layer="21"/>
<rectangle x1="10.2299" y1="3.2195" x2="10.5347" y2="3.2576" layer="21"/>
<rectangle x1="12.0587" y1="3.2195" x2="12.4397" y2="3.2576" layer="21"/>
<rectangle x1="12.935" y1="3.2195" x2="13.2017" y2="3.2576" layer="21"/>
<rectangle x1="15.1829" y1="3.2195" x2="15.3353" y2="3.2576" layer="21"/>
<rectangle x1="-0.0191" y1="3.2576" x2="0.1334" y2="3.2957" layer="21"/>
<rectangle x1="0.7049" y1="3.2576" x2="1.0859" y2="3.2957" layer="21"/>
<rectangle x1="1.6574" y1="3.2576" x2="2.5718" y2="3.2957" layer="21"/>
<rectangle x1="2.8004" y1="3.2576" x2="3.7148" y2="3.2957" layer="21"/>
<rectangle x1="4.2482" y1="3.2576" x2="4.6292" y2="3.2957" layer="21"/>
<rectangle x1="6.6104" y1="3.2576" x2="6.8771" y2="3.2957" layer="21"/>
<rectangle x1="7.6772" y1="3.2576" x2="8.4773" y2="3.2957" layer="21"/>
<rectangle x1="9.2774" y1="3.2576" x2="9.5822" y2="3.2957" layer="21"/>
<rectangle x1="10.2299" y1="3.2576" x2="10.5347" y2="3.2957" layer="21"/>
<rectangle x1="12.0968" y1="3.2576" x2="12.4778" y2="3.2957" layer="21"/>
<rectangle x1="12.935" y1="3.2576" x2="13.2017" y2="3.2957" layer="21"/>
<rectangle x1="15.1829" y1="3.2576" x2="15.3353" y2="3.2957" layer="21"/>
<rectangle x1="-0.0191" y1="3.2957" x2="0.1334" y2="3.3338" layer="21"/>
<rectangle x1="0.7049" y1="3.2957" x2="1.0859" y2="3.3338" layer="21"/>
<rectangle x1="1.6955" y1="3.2957" x2="2.5337" y2="3.3338" layer="21"/>
<rectangle x1="2.8385" y1="3.2957" x2="3.6767" y2="3.3338" layer="21"/>
<rectangle x1="4.2101" y1="3.2957" x2="4.6292" y2="3.3338" layer="21"/>
<rectangle x1="6.6104" y1="3.2957" x2="6.8771" y2="3.3338" layer="21"/>
<rectangle x1="7.6772" y1="3.2957" x2="8.4773" y2="3.3338" layer="21"/>
<rectangle x1="9.2774" y1="3.2957" x2="9.5822" y2="3.3338" layer="21"/>
<rectangle x1="10.2299" y1="3.2957" x2="10.5347" y2="3.3338" layer="21"/>
<rectangle x1="12.0968" y1="3.2957" x2="12.4778" y2="3.3338" layer="21"/>
<rectangle x1="12.935" y1="3.2957" x2="13.2017" y2="3.3338" layer="21"/>
<rectangle x1="15.1829" y1="3.2957" x2="15.3353" y2="3.3338" layer="21"/>
<rectangle x1="-0.0191" y1="3.3338" x2="0.1334" y2="3.3719" layer="21"/>
<rectangle x1="0.7049" y1="3.3338" x2="1.0859" y2="3.3719" layer="21"/>
<rectangle x1="1.7336" y1="3.3338" x2="2.4956" y2="3.3719" layer="21"/>
<rectangle x1="2.9147" y1="3.3338" x2="3.6386" y2="3.3719" layer="21"/>
<rectangle x1="4.2101" y1="3.3338" x2="4.5911" y2="3.3719" layer="21"/>
<rectangle x1="6.6104" y1="3.3338" x2="6.8771" y2="3.3719" layer="21"/>
<rectangle x1="7.6772" y1="3.3338" x2="8.4773" y2="3.3719" layer="21"/>
<rectangle x1="9.2774" y1="3.3338" x2="9.5822" y2="3.3719" layer="21"/>
<rectangle x1="10.2299" y1="3.3338" x2="10.4966" y2="3.3719" layer="21"/>
<rectangle x1="12.0968" y1="3.3338" x2="12.4778" y2="3.3719" layer="21"/>
<rectangle x1="12.935" y1="3.3338" x2="13.2017" y2="3.3719" layer="21"/>
<rectangle x1="15.1829" y1="3.3338" x2="15.3353" y2="3.3719" layer="21"/>
<rectangle x1="-0.0191" y1="3.3719" x2="0.1334" y2="3.41" layer="21"/>
<rectangle x1="0.7049" y1="3.3719" x2="1.124" y2="3.41" layer="21"/>
<rectangle x1="1.8098" y1="3.3719" x2="2.4194" y2="3.41" layer="21"/>
<rectangle x1="2.9528" y1="3.3719" x2="3.6005" y2="3.41" layer="21"/>
<rectangle x1="4.2101" y1="3.3719" x2="4.5911" y2="3.41" layer="21"/>
<rectangle x1="6.6104" y1="3.3719" x2="6.8771" y2="3.41" layer="21"/>
<rectangle x1="7.6772" y1="3.3719" x2="8.4773" y2="3.41" layer="21"/>
<rectangle x1="9.3155" y1="3.3719" x2="9.5822" y2="3.41" layer="21"/>
<rectangle x1="10.2299" y1="3.3719" x2="10.4966" y2="3.41" layer="21"/>
<rectangle x1="11.1824" y1="3.3719" x2="11.2967" y2="3.41" layer="21"/>
<rectangle x1="12.0968" y1="3.3719" x2="12.4778" y2="3.41" layer="21"/>
<rectangle x1="12.935" y1="3.3719" x2="13.2017" y2="3.41" layer="21"/>
<rectangle x1="15.1829" y1="3.3719" x2="15.3353" y2="3.41" layer="21"/>
<rectangle x1="-0.0191" y1="3.41" x2="0.1334" y2="3.4481" layer="21"/>
<rectangle x1="0.743" y1="3.41" x2="1.124" y2="3.4481" layer="21"/>
<rectangle x1="1.886" y1="3.41" x2="2.3432" y2="3.4481" layer="21"/>
<rectangle x1="3.029" y1="3.41" x2="3.4862" y2="3.4481" layer="21"/>
<rectangle x1="4.172" y1="3.41" x2="4.5911" y2="3.4481" layer="21"/>
<rectangle x1="6.6104" y1="3.41" x2="6.8771" y2="3.4481" layer="21"/>
<rectangle x1="7.6772" y1="3.41" x2="8.4773" y2="3.4481" layer="21"/>
<rectangle x1="9.3155" y1="3.41" x2="9.5822" y2="3.4481" layer="21"/>
<rectangle x1="10.2299" y1="3.41" x2="10.4966" y2="3.4481" layer="21"/>
<rectangle x1="11.1443" y1="3.41" x2="11.3348" y2="3.4481" layer="21"/>
<rectangle x1="12.0968" y1="3.41" x2="12.4778" y2="3.4481" layer="21"/>
<rectangle x1="12.935" y1="3.41" x2="13.2017" y2="3.4481" layer="21"/>
<rectangle x1="15.1829" y1="3.41" x2="15.3353" y2="3.4481" layer="21"/>
<rectangle x1="-0.0191" y1="3.4481" x2="0.1334" y2="3.4862" layer="21"/>
<rectangle x1="0.743" y1="3.4481" x2="1.124" y2="3.4862" layer="21"/>
<rectangle x1="2.0765" y1="3.4481" x2="2.1527" y2="3.4862" layer="21"/>
<rectangle x1="3.2195" y1="3.4481" x2="3.2957" y2="3.4862" layer="21"/>
<rectangle x1="4.172" y1="3.4481" x2="4.553" y2="3.4862" layer="21"/>
<rectangle x1="6.6104" y1="3.4481" x2="6.8771" y2="3.4862" layer="21"/>
<rectangle x1="7.6772" y1="3.4481" x2="8.4773" y2="3.4862" layer="21"/>
<rectangle x1="9.3155" y1="3.4481" x2="9.5822" y2="3.4862" layer="21"/>
<rectangle x1="10.2299" y1="3.4481" x2="10.4966" y2="3.4862" layer="21"/>
<rectangle x1="11.1062" y1="3.4481" x2="11.3729" y2="3.4862" layer="21"/>
<rectangle x1="12.0968" y1="3.4481" x2="12.4778" y2="3.4862" layer="21"/>
<rectangle x1="12.935" y1="3.4481" x2="13.2017" y2="3.4862" layer="21"/>
<rectangle x1="15.1829" y1="3.4481" x2="15.3353" y2="3.4862" layer="21"/>
<rectangle x1="-0.0191" y1="3.4862" x2="0.1334" y2="3.5243" layer="21"/>
<rectangle x1="0.743" y1="3.4862" x2="1.1621" y2="3.5243" layer="21"/>
<rectangle x1="4.172" y1="3.4862" x2="4.553" y2="3.5243" layer="21"/>
<rectangle x1="6.6104" y1="3.4862" x2="6.8771" y2="3.5243" layer="21"/>
<rectangle x1="7.6772" y1="3.4862" x2="8.4773" y2="3.5243" layer="21"/>
<rectangle x1="9.2774" y1="3.4862" x2="9.5822" y2="3.5243" layer="21"/>
<rectangle x1="10.2299" y1="3.4862" x2="10.4966" y2="3.5243" layer="21"/>
<rectangle x1="11.0681" y1="3.4862" x2="11.411" y2="3.5243" layer="21"/>
<rectangle x1="12.0968" y1="3.4862" x2="12.4778" y2="3.5243" layer="21"/>
<rectangle x1="12.935" y1="3.4862" x2="13.2017" y2="3.5243" layer="21"/>
<rectangle x1="15.1829" y1="3.4862" x2="15.3353" y2="3.5243" layer="21"/>
<rectangle x1="-0.0191" y1="3.5243" x2="0.1334" y2="3.5624" layer="21"/>
<rectangle x1="0.7811" y1="3.5243" x2="1.1621" y2="3.5624" layer="21"/>
<rectangle x1="4.1339" y1="3.5243" x2="4.553" y2="3.5624" layer="21"/>
<rectangle x1="6.6104" y1="3.5243" x2="6.8771" y2="3.5624" layer="21"/>
<rectangle x1="7.6772" y1="3.5243" x2="8.4773" y2="3.5624" layer="21"/>
<rectangle x1="9.2774" y1="3.5243" x2="9.5822" y2="3.5624" layer="21"/>
<rectangle x1="10.2299" y1="3.5243" x2="10.4966" y2="3.5624" layer="21"/>
<rectangle x1="11.03" y1="3.5243" x2="11.4491" y2="3.5624" layer="21"/>
<rectangle x1="12.0968" y1="3.5243" x2="12.4778" y2="3.5624" layer="21"/>
<rectangle x1="12.935" y1="3.5243" x2="13.2017" y2="3.5624" layer="21"/>
<rectangle x1="15.1829" y1="3.5243" x2="15.3353" y2="3.5624" layer="21"/>
<rectangle x1="-0.0191" y1="3.5624" x2="0.1334" y2="3.6005" layer="21"/>
<rectangle x1="0.7811" y1="3.5624" x2="1.2002" y2="3.6005" layer="21"/>
<rectangle x1="4.1339" y1="3.5624" x2="4.5149" y2="3.6005" layer="21"/>
<rectangle x1="6.6104" y1="3.5624" x2="6.8771" y2="3.6005" layer="21"/>
<rectangle x1="7.6772" y1="3.5624" x2="8.4773" y2="3.6005" layer="21"/>
<rectangle x1="9.2774" y1="3.5624" x2="9.5822" y2="3.6005" layer="21"/>
<rectangle x1="10.2299" y1="3.5624" x2="10.5347" y2="3.6005" layer="21"/>
<rectangle x1="10.9919" y1="3.5624" x2="11.4872" y2="3.6005" layer="21"/>
<rectangle x1="12.0968" y1="3.5624" x2="12.4778" y2="3.6005" layer="21"/>
<rectangle x1="12.935" y1="3.5624" x2="13.2017" y2="3.6005" layer="21"/>
<rectangle x1="15.1829" y1="3.5624" x2="15.3353" y2="3.6005" layer="21"/>
<rectangle x1="-0.0191" y1="3.6005" x2="0.1334" y2="3.6386" layer="21"/>
<rectangle x1="0.7811" y1="3.6005" x2="1.2002" y2="3.6386" layer="21"/>
<rectangle x1="4.0958" y1="3.6005" x2="4.5149" y2="3.6386" layer="21"/>
<rectangle x1="6.6104" y1="3.6005" x2="6.8771" y2="3.6386" layer="21"/>
<rectangle x1="7.6772" y1="3.6005" x2="8.4773" y2="3.6386" layer="21"/>
<rectangle x1="9.2774" y1="3.6005" x2="9.5441" y2="3.6386" layer="21"/>
<rectangle x1="10.2299" y1="3.6005" x2="10.5347" y2="3.6386" layer="21"/>
<rectangle x1="10.9538" y1="3.6005" x2="11.5253" y2="3.6386" layer="21"/>
<rectangle x1="12.0968" y1="3.6005" x2="12.4397" y2="3.6386" layer="21"/>
<rectangle x1="12.935" y1="3.6005" x2="13.2017" y2="3.6386" layer="21"/>
<rectangle x1="15.1829" y1="3.6005" x2="15.3353" y2="3.6386" layer="21"/>
<rectangle x1="-0.0191" y1="3.6386" x2="0.1334" y2="3.6767" layer="21"/>
<rectangle x1="0.8192" y1="3.6386" x2="1.2383" y2="3.6767" layer="21"/>
<rectangle x1="4.0958" y1="3.6386" x2="4.5149" y2="3.6767" layer="21"/>
<rectangle x1="6.6104" y1="3.6386" x2="6.8771" y2="3.6767" layer="21"/>
<rectangle x1="7.6772" y1="3.6386" x2="8.4773" y2="3.6767" layer="21"/>
<rectangle x1="9.2774" y1="3.6386" x2="9.5441" y2="3.6767" layer="21"/>
<rectangle x1="10.268" y1="3.6386" x2="10.5347" y2="3.6767" layer="21"/>
<rectangle x1="11.0681" y1="3.6386" x2="11.4491" y2="3.6767" layer="21"/>
<rectangle x1="12.0587" y1="3.6386" x2="12.4397" y2="3.6767" layer="21"/>
<rectangle x1="12.935" y1="3.6386" x2="13.2017" y2="3.6767" layer="21"/>
<rectangle x1="15.1829" y1="3.6386" x2="15.3353" y2="3.6767" layer="21"/>
<rectangle x1="-0.0191" y1="3.6767" x2="0.1334" y2="3.7148" layer="21"/>
<rectangle x1="0.8192" y1="3.6767" x2="1.2764" y2="3.7148" layer="21"/>
<rectangle x1="4.0577" y1="3.6767" x2="4.4768" y2="3.7148" layer="21"/>
<rectangle x1="6.6104" y1="3.6767" x2="6.9152" y2="3.7148" layer="21"/>
<rectangle x1="7.6772" y1="3.6767" x2="8.4773" y2="3.7148" layer="21"/>
<rectangle x1="9.2774" y1="3.6767" x2="9.5441" y2="3.7148" layer="21"/>
<rectangle x1="10.268" y1="3.6767" x2="10.5347" y2="3.7148" layer="21"/>
<rectangle x1="11.0681" y1="3.6767" x2="11.4491" y2="3.7148" layer="21"/>
<rectangle x1="12.0587" y1="3.6767" x2="12.4397" y2="3.7148" layer="21"/>
<rectangle x1="12.8969" y1="3.6767" x2="13.2017" y2="3.7148" layer="21"/>
<rectangle x1="15.1829" y1="3.6767" x2="15.3353" y2="3.7148" layer="21"/>
<rectangle x1="-0.0191" y1="3.7148" x2="0.1334" y2="3.7529" layer="21"/>
<rectangle x1="0.8573" y1="3.7148" x2="1.2764" y2="3.7529" layer="21"/>
<rectangle x1="4.0196" y1="3.7148" x2="4.4768" y2="3.7529" layer="21"/>
<rectangle x1="6.6104" y1="3.7148" x2="6.9152" y2="3.7529" layer="21"/>
<rectangle x1="7.6772" y1="3.7148" x2="8.4773" y2="3.7529" layer="21"/>
<rectangle x1="9.2774" y1="3.7148" x2="9.5441" y2="3.7529" layer="21"/>
<rectangle x1="10.268" y1="3.7148" x2="10.5347" y2="3.7529" layer="21"/>
<rectangle x1="11.0681" y1="3.7148" x2="11.4491" y2="3.7529" layer="21"/>
<rectangle x1="12.0206" y1="3.7148" x2="12.4397" y2="3.7529" layer="21"/>
<rectangle x1="12.8969" y1="3.7148" x2="13.2017" y2="3.7529" layer="21"/>
<rectangle x1="15.1829" y1="3.7148" x2="15.3353" y2="3.7529" layer="21"/>
<rectangle x1="-0.0191" y1="3.7529" x2="0.1334" y2="3.791" layer="21"/>
<rectangle x1="0.8573" y1="3.7529" x2="1.3145" y2="3.791" layer="21"/>
<rectangle x1="4.0196" y1="3.7529" x2="4.4387" y2="3.791" layer="21"/>
<rectangle x1="6.6485" y1="3.7529" x2="6.9152" y2="3.791" layer="21"/>
<rectangle x1="7.6772" y1="3.7529" x2="8.4773" y2="3.791" layer="21"/>
<rectangle x1="9.2393" y1="3.7529" x2="9.5441" y2="3.791" layer="21"/>
<rectangle x1="10.268" y1="3.7529" x2="10.5728" y2="3.791" layer="21"/>
<rectangle x1="11.1062" y1="3.7529" x2="11.4491" y2="3.791" layer="21"/>
<rectangle x1="12.0206" y1="3.7529" x2="12.4016" y2="3.791" layer="21"/>
<rectangle x1="12.8969" y1="3.7529" x2="13.1636" y2="3.791" layer="21"/>
<rectangle x1="15.1829" y1="3.7529" x2="15.3353" y2="3.791" layer="21"/>
<rectangle x1="-0.0191" y1="3.791" x2="0.1334" y2="3.8291" layer="21"/>
<rectangle x1="0.8954" y1="3.791" x2="1.3526" y2="3.8291" layer="21"/>
<rectangle x1="3.9815" y1="3.791" x2="4.4387" y2="3.8291" layer="21"/>
<rectangle x1="6.6485" y1="3.791" x2="6.9533" y2="3.8291" layer="21"/>
<rectangle x1="7.6772" y1="3.791" x2="8.4773" y2="3.8291" layer="21"/>
<rectangle x1="9.2393" y1="3.791" x2="9.5441" y2="3.8291" layer="21"/>
<rectangle x1="10.268" y1="3.791" x2="10.5728" y2="3.8291" layer="21"/>
<rectangle x1="11.1062" y1="3.791" x2="11.4872" y2="3.8291" layer="21"/>
<rectangle x1="11.9825" y1="3.791" x2="12.4016" y2="3.8291" layer="21"/>
<rectangle x1="12.8588" y1="3.791" x2="13.1636" y2="3.8291" layer="21"/>
<rectangle x1="15.1829" y1="3.791" x2="15.3353" y2="3.8291" layer="21"/>
<rectangle x1="-0.0191" y1="3.8291" x2="0.1334" y2="3.8672" layer="21"/>
<rectangle x1="0.8954" y1="3.8291" x2="1.3526" y2="3.8672" layer="21"/>
<rectangle x1="3.9434" y1="3.8291" x2="4.4006" y2="3.8672" layer="21"/>
<rectangle x1="6.6485" y1="3.8291" x2="6.9533" y2="3.8672" layer="21"/>
<rectangle x1="7.7153" y1="3.8291" x2="8.4773" y2="3.8672" layer="21"/>
<rectangle x1="9.2393" y1="3.8291" x2="9.506" y2="3.8672" layer="21"/>
<rectangle x1="10.3061" y1="3.8291" x2="10.5728" y2="3.8672" layer="21"/>
<rectangle x1="11.1062" y1="3.8291" x2="11.5253" y2="3.8672" layer="21"/>
<rectangle x1="11.9444" y1="3.8291" x2="12.3635" y2="3.8672" layer="21"/>
<rectangle x1="12.8588" y1="3.8291" x2="13.1636" y2="3.8672" layer="21"/>
<rectangle x1="15.1829" y1="3.8291" x2="15.3353" y2="3.8672" layer="21"/>
<rectangle x1="-0.0191" y1="3.8672" x2="0.1334" y2="3.9053" layer="21"/>
<rectangle x1="0.9335" y1="3.8672" x2="1.3907" y2="3.9053" layer="21"/>
<rectangle x1="3.9053" y1="3.8672" x2="4.3625" y2="3.9053" layer="21"/>
<rectangle x1="6.6485" y1="3.8672" x2="6.9533" y2="3.9053" layer="21"/>
<rectangle x1="7.7534" y1="3.8672" x2="8.4392" y2="3.9053" layer="21"/>
<rectangle x1="9.2012" y1="3.8672" x2="9.506" y2="3.9053" layer="21"/>
<rectangle x1="10.3061" y1="3.8672" x2="10.6109" y2="3.9053" layer="21"/>
<rectangle x1="11.1443" y1="3.8672" x2="11.6015" y2="3.9053" layer="21"/>
<rectangle x1="11.8682" y1="3.8672" x2="12.3635" y2="3.9053" layer="21"/>
<rectangle x1="12.8588" y1="3.8672" x2="13.1255" y2="3.9053" layer="21"/>
<rectangle x1="15.1829" y1="3.8672" x2="15.3353" y2="3.9053" layer="21"/>
<rectangle x1="-0.0191" y1="3.9053" x2="0.1334" y2="3.9434" layer="21"/>
<rectangle x1="0.9716" y1="3.9053" x2="1.4288" y2="3.9434" layer="21"/>
<rectangle x1="3.9053" y1="3.9053" x2="4.3625" y2="3.9434" layer="21"/>
<rectangle x1="6.6866" y1="3.9053" x2="6.9914" y2="3.9434" layer="21"/>
<rectangle x1="9.2012" y1="3.9053" x2="9.506" y2="3.9434" layer="21"/>
<rectangle x1="10.3061" y1="3.9053" x2="10.6109" y2="3.9434" layer="21"/>
<rectangle x1="11.1443" y1="3.9053" x2="12.3254" y2="3.9434" layer="21"/>
<rectangle x1="12.8207" y1="3.9053" x2="13.1255" y2="3.9434" layer="21"/>
<rectangle x1="15.1829" y1="3.9053" x2="15.3353" y2="3.9434" layer="21"/>
<rectangle x1="-0.0191" y1="3.9434" x2="0.1334" y2="3.9815" layer="21"/>
<rectangle x1="0.9716" y1="3.9434" x2="1.4669" y2="3.9815" layer="21"/>
<rectangle x1="3.8672" y1="3.9434" x2="4.3244" y2="3.9815" layer="21"/>
<rectangle x1="6.6866" y1="3.9434" x2="6.9914" y2="3.9815" layer="21"/>
<rectangle x1="9.2012" y1="3.9434" x2="9.4679" y2="3.9815" layer="21"/>
<rectangle x1="10.3442" y1="3.9434" x2="10.649" y2="3.9815" layer="21"/>
<rectangle x1="11.1824" y1="3.9434" x2="12.2873" y2="3.9815" layer="21"/>
<rectangle x1="12.8207" y1="3.9434" x2="13.1255" y2="3.9815" layer="21"/>
<rectangle x1="15.1829" y1="3.9434" x2="15.3353" y2="3.9815" layer="21"/>
<rectangle x1="-0.0191" y1="3.9815" x2="0.1334" y2="4.0196" layer="21"/>
<rectangle x1="1.0097" y1="3.9815" x2="1.505" y2="4.0196" layer="21"/>
<rectangle x1="3.8291" y1="3.9815" x2="4.3244" y2="4.0196" layer="21"/>
<rectangle x1="6.6866" y1="3.9815" x2="7.0295" y2="4.0196" layer="21"/>
<rectangle x1="7.982" y1="3.9815" x2="8.1725" y2="4.0196" layer="21"/>
<rectangle x1="9.1631" y1="3.9815" x2="9.4679" y2="4.0196" layer="21"/>
<rectangle x1="10.3442" y1="3.9815" x2="10.649" y2="4.0196" layer="21"/>
<rectangle x1="11.2205" y1="3.9815" x2="12.2492" y2="4.0196" layer="21"/>
<rectangle x1="12.7826" y1="3.9815" x2="13.1255" y2="4.0196" layer="21"/>
<rectangle x1="15.1829" y1="3.9815" x2="15.3353" y2="4.0196" layer="21"/>
<rectangle x1="-0.0191" y1="4.0196" x2="0.1334" y2="4.0577" layer="21"/>
<rectangle x1="1.0478" y1="4.0196" x2="1.5431" y2="4.0577" layer="21"/>
<rectangle x1="3.791" y1="4.0196" x2="4.2863" y2="4.0577" layer="21"/>
<rectangle x1="6.7247" y1="4.0196" x2="7.0295" y2="4.0577" layer="21"/>
<rectangle x1="7.9439" y1="4.0196" x2="8.2106" y2="4.0577" layer="21"/>
<rectangle x1="9.125" y1="4.0196" x2="9.4679" y2="4.0577" layer="21"/>
<rectangle x1="10.3442" y1="4.0196" x2="10.6871" y2="4.0577" layer="21"/>
<rectangle x1="11.2586" y1="4.0196" x2="12.2111" y2="4.0577" layer="21"/>
<rectangle x1="12.7826" y1="4.0196" x2="13.0874" y2="4.0577" layer="21"/>
<rectangle x1="15.1829" y1="4.0196" x2="15.3353" y2="4.0577" layer="21"/>
<rectangle x1="-0.0191" y1="4.0577" x2="0.1334" y2="4.0958" layer="21"/>
<rectangle x1="1.0859" y1="4.0577" x2="1.5812" y2="4.0958" layer="21"/>
<rectangle x1="3.7529" y1="4.0577" x2="4.2482" y2="4.0958" layer="21"/>
<rectangle x1="6.7247" y1="4.0577" x2="7.0676" y2="4.0958" layer="21"/>
<rectangle x1="7.9058" y1="4.0577" x2="8.2487" y2="4.0958" layer="21"/>
<rectangle x1="9.125" y1="4.0577" x2="9.4298" y2="4.0958" layer="21"/>
<rectangle x1="10.3823" y1="4.0577" x2="10.6871" y2="4.0958" layer="21"/>
<rectangle x1="11.2967" y1="4.0577" x2="12.173" y2="4.0958" layer="21"/>
<rectangle x1="12.7445" y1="4.0577" x2="13.0874" y2="4.0958" layer="21"/>
<rectangle x1="15.1829" y1="4.0577" x2="15.3353" y2="4.0958" layer="21"/>
<rectangle x1="-0.0191" y1="4.0958" x2="0.1334" y2="4.1339" layer="21"/>
<rectangle x1="1.124" y1="4.0958" x2="1.6193" y2="4.1339" layer="21"/>
<rectangle x1="3.6767" y1="4.0958" x2="4.2101" y2="4.1339" layer="21"/>
<rectangle x1="6.7628" y1="4.0958" x2="7.0676" y2="4.1339" layer="21"/>
<rectangle x1="7.9058" y1="4.0958" x2="8.2868" y2="4.1339" layer="21"/>
<rectangle x1="9.0869" y1="4.0958" x2="9.4298" y2="4.1339" layer="21"/>
<rectangle x1="10.3823" y1="4.0958" x2="10.7252" y2="4.1339" layer="21"/>
<rectangle x1="11.3348" y1="4.0958" x2="12.0968" y2="4.1339" layer="21"/>
<rectangle x1="12.7445" y1="4.0958" x2="13.0493" y2="4.1339" layer="21"/>
<rectangle x1="15.1829" y1="4.0958" x2="15.3353" y2="4.1339" layer="21"/>
<rectangle x1="-0.0191" y1="4.1339" x2="0.1334" y2="4.172" layer="21"/>
<rectangle x1="1.124" y1="4.1339" x2="1.6574" y2="4.172" layer="21"/>
<rectangle x1="3.6386" y1="4.1339" x2="4.172" y2="4.172" layer="21"/>
<rectangle x1="6.8009" y1="4.1339" x2="7.1057" y2="4.172" layer="21"/>
<rectangle x1="7.8677" y1="4.1339" x2="8.2868" y2="4.172" layer="21"/>
<rectangle x1="9.0488" y1="4.1339" x2="9.3917" y2="4.172" layer="21"/>
<rectangle x1="10.4204" y1="4.1339" x2="10.7633" y2="4.172" layer="21"/>
<rectangle x1="11.411" y1="4.1339" x2="12.0587" y2="4.172" layer="21"/>
<rectangle x1="12.7064" y1="4.1339" x2="13.0493" y2="4.172" layer="21"/>
<rectangle x1="15.1829" y1="4.1339" x2="15.3353" y2="4.172" layer="21"/>
<rectangle x1="-0.0191" y1="4.172" x2="0.1334" y2="4.2101" layer="21"/>
<rectangle x1="1.1621" y1="4.172" x2="1.7336" y2="4.2101" layer="21"/>
<rectangle x1="3.6005" y1="4.172" x2="4.172" y2="4.2101" layer="21"/>
<rectangle x1="6.8009" y1="4.172" x2="7.1438" y2="4.2101" layer="21"/>
<rectangle x1="7.8677" y1="4.172" x2="8.2868" y2="4.2101" layer="21"/>
<rectangle x1="9.0488" y1="4.172" x2="9.3917" y2="4.2101" layer="21"/>
<rectangle x1="10.4585" y1="4.172" x2="10.7633" y2="4.2101" layer="21"/>
<rectangle x1="11.5253" y1="4.172" x2="11.9063" y2="4.2101" layer="21"/>
<rectangle x1="12.6683" y1="4.172" x2="13.0112" y2="4.2101" layer="21"/>
<rectangle x1="15.1829" y1="4.172" x2="15.3353" y2="4.2101" layer="21"/>
<rectangle x1="-0.0191" y1="4.2101" x2="0.1334" y2="4.2482" layer="21"/>
<rectangle x1="1.2002" y1="4.2101" x2="1.7717" y2="4.2482" layer="21"/>
<rectangle x1="3.5624" y1="4.2101" x2="4.1339" y2="4.2482" layer="21"/>
<rectangle x1="6.839" y1="4.2101" x2="7.1819" y2="4.2482" layer="21"/>
<rectangle x1="7.9058" y1="4.2101" x2="8.2868" y2="4.2482" layer="21"/>
<rectangle x1="9.0107" y1="4.2101" x2="9.3536" y2="4.2482" layer="21"/>
<rectangle x1="10.4585" y1="4.2101" x2="10.8014" y2="4.2482" layer="21"/>
<rectangle x1="12.6302" y1="4.2101" x2="12.9731" y2="4.2482" layer="21"/>
<rectangle x1="15.1829" y1="4.2101" x2="15.3353" y2="4.2482" layer="21"/>
<rectangle x1="-0.0191" y1="4.2482" x2="0.1334" y2="4.2863" layer="21"/>
<rectangle x1="1.2383" y1="4.2482" x2="1.8479" y2="4.2863" layer="21"/>
<rectangle x1="3.4862" y1="4.2482" x2="4.0958" y2="4.2863" layer="21"/>
<rectangle x1="6.839" y1="4.2482" x2="7.22" y2="4.2863" layer="21"/>
<rectangle x1="7.9058" y1="4.2482" x2="8.2868" y2="4.2863" layer="21"/>
<rectangle x1="8.9726" y1="4.2482" x2="9.3155" y2="4.2863" layer="21"/>
<rectangle x1="10.4966" y1="4.2482" x2="10.8395" y2="4.2863" layer="21"/>
<rectangle x1="12.5921" y1="4.2482" x2="12.9731" y2="4.2863" layer="21"/>
<rectangle x1="15.1829" y1="4.2482" x2="15.3353" y2="4.2863" layer="21"/>
<rectangle x1="-0.0191" y1="4.2863" x2="0.1334" y2="4.3244" layer="21"/>
<rectangle x1="1.2764" y1="4.2863" x2="1.9241" y2="4.3244" layer="21"/>
<rectangle x1="3.41" y1="4.2863" x2="4.0577" y2="4.3244" layer="21"/>
<rectangle x1="6.8771" y1="4.2863" x2="7.2581" y2="4.3244" layer="21"/>
<rectangle x1="7.9058" y1="4.2863" x2="8.2487" y2="4.3244" layer="21"/>
<rectangle x1="8.9345" y1="4.2863" x2="9.3155" y2="4.3244" layer="21"/>
<rectangle x1="10.4966" y1="4.2863" x2="10.8776" y2="4.3244" layer="21"/>
<rectangle x1="12.554" y1="4.2863" x2="12.935" y2="4.3244" layer="21"/>
<rectangle x1="15.1829" y1="4.2863" x2="15.3353" y2="4.3244" layer="21"/>
<rectangle x1="-0.0191" y1="4.3244" x2="0.1334" y2="4.3625" layer="21"/>
<rectangle x1="1.3145" y1="4.3244" x2="1.9622" y2="4.3625" layer="21"/>
<rectangle x1="3.3338" y1="4.3244" x2="4.0196" y2="4.3625" layer="21"/>
<rectangle x1="6.9152" y1="4.3244" x2="7.2962" y2="4.3625" layer="21"/>
<rectangle x1="7.9439" y1="4.3244" x2="8.2106" y2="4.3625" layer="21"/>
<rectangle x1="8.8964" y1="4.3244" x2="9.2774" y2="4.3625" layer="21"/>
<rectangle x1="10.5347" y1="4.3244" x2="10.9157" y2="4.3625" layer="21"/>
<rectangle x1="12.5159" y1="4.3244" x2="12.8969" y2="4.3625" layer="21"/>
<rectangle x1="15.1829" y1="4.3244" x2="15.3353" y2="4.3625" layer="21"/>
<rectangle x1="-0.0191" y1="4.3625" x2="0.1334" y2="4.4006" layer="21"/>
<rectangle x1="1.3526" y1="4.3625" x2="2.0765" y2="4.4006" layer="21"/>
<rectangle x1="3.2576" y1="4.3625" x2="3.9434" y2="4.4006" layer="21"/>
<rectangle x1="6.9533" y1="4.3625" x2="7.3343" y2="4.4006" layer="21"/>
<rectangle x1="8.0201" y1="4.3625" x2="8.1344" y2="4.4006" layer="21"/>
<rectangle x1="8.8583" y1="4.3625" x2="9.2393" y2="4.4006" layer="21"/>
<rectangle x1="10.5728" y1="4.3625" x2="10.9538" y2="4.4006" layer="21"/>
<rectangle x1="12.4778" y1="4.3625" x2="12.8588" y2="4.4006" layer="21"/>
<rectangle x1="15.1829" y1="4.3625" x2="15.3353" y2="4.4006" layer="21"/>
<rectangle x1="-0.0191" y1="4.4006" x2="0.1334" y2="4.4387" layer="21"/>
<rectangle x1="1.4288" y1="4.4006" x2="2.1908" y2="4.4387" layer="21"/>
<rectangle x1="3.1052" y1="4.4006" x2="3.9053" y2="4.4387" layer="21"/>
<rectangle x1="6.9914" y1="4.4006" x2="7.3724" y2="4.4387" layer="21"/>
<rectangle x1="8.7821" y1="4.4006" x2="9.2012" y2="4.4387" layer="21"/>
<rectangle x1="10.6109" y1="4.4006" x2="11.03" y2="4.4387" layer="21"/>
<rectangle x1="12.4397" y1="4.4006" x2="12.8588" y2="4.4387" layer="21"/>
<rectangle x1="15.1829" y1="4.4006" x2="15.3353" y2="4.4387" layer="21"/>
<rectangle x1="-0.0191" y1="4.4387" x2="0.1334" y2="4.4768" layer="21"/>
<rectangle x1="1.4669" y1="4.4387" x2="2.3051" y2="4.4768" layer="21"/>
<rectangle x1="2.9909" y1="4.4387" x2="3.8672" y2="4.4768" layer="21"/>
<rectangle x1="6.9914" y1="4.4387" x2="7.4105" y2="4.4768" layer="21"/>
<rectangle x1="8.744" y1="4.4387" x2="9.1631" y2="4.4768" layer="21"/>
<rectangle x1="10.649" y1="4.4387" x2="11.0681" y2="4.4768" layer="21"/>
<rectangle x1="12.4016" y1="4.4387" x2="12.8207" y2="4.4768" layer="21"/>
<rectangle x1="15.1829" y1="4.4387" x2="15.3353" y2="4.4768" layer="21"/>
<rectangle x1="-0.0191" y1="4.4768" x2="0.1334" y2="4.5149" layer="21"/>
<rectangle x1="1.505" y1="4.4768" x2="3.8291" y2="4.5149" layer="21"/>
<rectangle x1="7.0295" y1="4.4768" x2="7.4867" y2="4.5149" layer="21"/>
<rectangle x1="8.6678" y1="4.4768" x2="9.125" y2="4.5149" layer="21"/>
<rectangle x1="10.6871" y1="4.4768" x2="11.1443" y2="4.5149" layer="21"/>
<rectangle x1="12.3254" y1="4.4768" x2="12.7826" y2="4.5149" layer="21"/>
<rectangle x1="15.1829" y1="4.4768" x2="15.3353" y2="4.5149" layer="21"/>
<rectangle x1="-0.0191" y1="4.5149" x2="0.1334" y2="4.553" layer="21"/>
<rectangle x1="1.5431" y1="4.5149" x2="3.7529" y2="4.553" layer="21"/>
<rectangle x1="7.0676" y1="4.5149" x2="7.5629" y2="4.553" layer="21"/>
<rectangle x1="8.6297" y1="4.5149" x2="9.0869" y2="4.553" layer="21"/>
<rectangle x1="10.7252" y1="4.5149" x2="11.2205" y2="4.553" layer="21"/>
<rectangle x1="12.2492" y1="4.5149" x2="12.7445" y2="4.553" layer="21"/>
<rectangle x1="15.1829" y1="4.5149" x2="15.3353" y2="4.553" layer="21"/>
<rectangle x1="-0.0191" y1="4.553" x2="0.1334" y2="4.5911" layer="21"/>
<rectangle x1="1.6193" y1="4.553" x2="3.7148" y2="4.5911" layer="21"/>
<rectangle x1="7.1438" y1="4.553" x2="7.6391" y2="4.5911" layer="21"/>
<rectangle x1="8.5154" y1="4.553" x2="9.0488" y2="4.5911" layer="21"/>
<rectangle x1="10.7633" y1="4.553" x2="11.2967" y2="4.5911" layer="21"/>
<rectangle x1="12.173" y1="4.553" x2="12.6683" y2="4.5911" layer="21"/>
<rectangle x1="15.1829" y1="4.553" x2="15.3353" y2="4.5911" layer="21"/>
<rectangle x1="-0.0191" y1="4.5911" x2="0.1334" y2="4.6292" layer="21"/>
<rectangle x1="1.6574" y1="4.5911" x2="3.6386" y2="4.6292" layer="21"/>
<rectangle x1="7.1819" y1="4.5911" x2="7.7915" y2="4.6292" layer="21"/>
<rectangle x1="8.4011" y1="4.5911" x2="9.0107" y2="4.6292" layer="21"/>
<rectangle x1="10.8014" y1="4.5911" x2="11.411" y2="4.6292" layer="21"/>
<rectangle x1="12.0206" y1="4.5911" x2="12.6302" y2="4.6292" layer="21"/>
<rectangle x1="15.1829" y1="4.5911" x2="15.3353" y2="4.6292" layer="21"/>
<rectangle x1="-0.0191" y1="4.6292" x2="0.1334" y2="4.6673" layer="21"/>
<rectangle x1="1.7336" y1="4.6292" x2="3.5624" y2="4.6673" layer="21"/>
<rectangle x1="7.22" y1="4.6292" x2="8.0201" y2="4.6673" layer="21"/>
<rectangle x1="8.1344" y1="4.6292" x2="8.9726" y2="4.6673" layer="21"/>
<rectangle x1="10.8395" y1="4.6292" x2="11.6777" y2="4.6673" layer="21"/>
<rectangle x1="11.792" y1="4.6292" x2="12.5921" y2="4.6673" layer="21"/>
<rectangle x1="15.1829" y1="4.6292" x2="15.3353" y2="4.6673" layer="21"/>
<rectangle x1="-0.0191" y1="4.6673" x2="0.1334" y2="4.7054" layer="21"/>
<rectangle x1="1.8098" y1="4.6673" x2="3.4862" y2="4.7054" layer="21"/>
<rectangle x1="7.2581" y1="4.6673" x2="8.8964" y2="4.7054" layer="21"/>
<rectangle x1="10.9157" y1="4.6673" x2="12.554" y2="4.7054" layer="21"/>
<rectangle x1="15.1829" y1="4.6673" x2="15.3353" y2="4.7054" layer="21"/>
<rectangle x1="-0.0191" y1="4.7054" x2="0.1334" y2="4.7435" layer="21"/>
<rectangle x1="1.9241" y1="4.7054" x2="3.41" y2="4.7435" layer="21"/>
<rectangle x1="7.3343" y1="4.7054" x2="8.8583" y2="4.7435" layer="21"/>
<rectangle x1="10.9538" y1="4.7054" x2="12.4778" y2="4.7435" layer="21"/>
<rectangle x1="15.1829" y1="4.7054" x2="15.3353" y2="4.7435" layer="21"/>
<rectangle x1="-0.0191" y1="4.7435" x2="0.1334" y2="4.7816" layer="21"/>
<rectangle x1="2.0384" y1="4.7435" x2="3.2957" y2="4.7816" layer="21"/>
<rectangle x1="7.4105" y1="4.7435" x2="8.7821" y2="4.7816" layer="21"/>
<rectangle x1="11.03" y1="4.7435" x2="12.4016" y2="4.7816" layer="21"/>
<rectangle x1="15.1829" y1="4.7435" x2="15.3353" y2="4.7816" layer="21"/>
<rectangle x1="-0.0191" y1="4.7816" x2="0.1334" y2="4.8197" layer="21"/>
<rectangle x1="2.1527" y1="4.7816" x2="3.1433" y2="4.8197" layer="21"/>
<rectangle x1="7.4867" y1="4.7816" x2="8.7059" y2="4.8197" layer="21"/>
<rectangle x1="11.1062" y1="4.7816" x2="12.3254" y2="4.8197" layer="21"/>
<rectangle x1="15.1829" y1="4.7816" x2="15.3353" y2="4.8197" layer="21"/>
<rectangle x1="-0.0191" y1="4.8197" x2="0.1334" y2="4.8578" layer="21"/>
<rectangle x1="2.4194" y1="4.8197" x2="2.8766" y2="4.8578" layer="21"/>
<rectangle x1="7.5629" y1="4.8197" x2="8.5916" y2="4.8578" layer="21"/>
<rectangle x1="11.2205" y1="4.8197" x2="12.2492" y2="4.8578" layer="21"/>
<rectangle x1="15.1829" y1="4.8197" x2="15.3353" y2="4.8578" layer="21"/>
<rectangle x1="-0.0191" y1="4.8578" x2="0.1334" y2="4.8959" layer="21"/>
<rectangle x1="7.7153" y1="4.8578" x2="8.4773" y2="4.8959" layer="21"/>
<rectangle x1="11.3348" y1="4.8578" x2="12.0968" y2="4.8959" layer="21"/>
<rectangle x1="15.1829" y1="4.8578" x2="15.3353" y2="4.8959" layer="21"/>
<rectangle x1="-0.0191" y1="4.8959" x2="0.1334" y2="4.934" layer="21"/>
<rectangle x1="7.9439" y1="4.8959" x2="8.2487" y2="4.934" layer="21"/>
<rectangle x1="11.5634" y1="4.8959" x2="11.8682" y2="4.934" layer="21"/>
<rectangle x1="15.1829" y1="4.8959" x2="15.3353" y2="4.934" layer="21"/>
<rectangle x1="-0.0191" y1="4.934" x2="0.1334" y2="4.9721" layer="21"/>
<rectangle x1="15.1829" y1="4.934" x2="15.3353" y2="4.9721" layer="21"/>
<rectangle x1="-0.0191" y1="4.9721" x2="0.1334" y2="5.0102" layer="21"/>
<rectangle x1="15.1829" y1="4.9721" x2="15.3353" y2="5.0102" layer="21"/>
<rectangle x1="-0.0191" y1="5.0102" x2="0.1334" y2="5.0483" layer="21"/>
<rectangle x1="15.1829" y1="5.0102" x2="15.3353" y2="5.0483" layer="21"/>
<rectangle x1="-0.0191" y1="5.0483" x2="0.1334" y2="5.0864" layer="21"/>
<rectangle x1="15.1829" y1="5.0483" x2="15.3353" y2="5.0864" layer="21"/>
<rectangle x1="-0.0191" y1="5.0864" x2="0.1334" y2="5.1245" layer="21"/>
<rectangle x1="15.1829" y1="5.0864" x2="15.3353" y2="5.1245" layer="21"/>
<rectangle x1="-0.0191" y1="5.1245" x2="0.1334" y2="5.1626" layer="21"/>
<rectangle x1="15.1829" y1="5.1245" x2="15.3353" y2="5.1626" layer="21"/>
<rectangle x1="0.0191" y1="5.1626" x2="0.1715" y2="5.2007" layer="21"/>
<rectangle x1="15.1448" y1="5.1626" x2="15.2972" y2="5.2007" layer="21"/>
<rectangle x1="0.0191" y1="5.2007" x2="15.2972" y2="5.2388" layer="21"/>
<rectangle x1="0.0572" y1="5.2388" x2="15.2591" y2="5.2769" layer="21"/>
<rectangle x1="0.0953" y1="5.2769" x2="15.221" y2="5.315" layer="21"/>
<rectangle x1="0.1715" y1="5.315" x2="15.1448" y2="5.3531" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="LOGO_CC_BY_SA">
<wire x1="-10.16" y1="3.81" x2="-11.43" y2="2.54" width="0.254" layer="94" curve="90"/>
<wire x1="-11.43" y1="2.54" x2="-11.43" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-11.43" y1="-2.54" x2="-10.16" y2="-3.81" width="0.254" layer="94" curve="90"/>
<wire x1="-10.16" y1="-3.81" x2="10.16" y2="-3.81" width="0.254" layer="94"/>
<wire x1="10.16" y1="-3.81" x2="11.43" y2="-2.54" width="0.254" layer="94" curve="90"/>
<wire x1="11.43" y1="-2.54" x2="11.43" y2="2.54" width="0.254" layer="94"/>
<wire x1="11.43" y1="2.54" x2="10.16" y2="3.81" width="0.254" layer="94" curve="90"/>
<wire x1="10.16" y1="3.81" x2="-10.16" y2="3.81" width="0.254" layer="94"/>
<circle x="-10.16" y="2.54" radius="0.449" width="0.1524" layer="94"/>
<circle x="-10.16" y="-2.54" radius="0.449" width="0.1524" layer="94"/>
<circle x="10.16" y="-2.54" radius="0.449" width="0.1524" layer="94"/>
<circle x="10.16" y="2.54" radius="0.449" width="0.1524" layer="94"/>
<text x="-9.017" y="-1.016" size="2.54" layer="94">CC-BY-SA</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="LOGO_CC-BY-SA" prefix="LOGO">
<gates>
<gate name="LOGO" symbol="LOGO_CC_BY_SA" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CC-BY-SA">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ohw-logo">
<packages>
<package name="OSHW_FILLX1000_NOTEXT">
<wire x1="12.1158" y1="7.1628" x2="9.7028" y2="1.3462" width="0.2032" layer="21"/>
<wire x1="9.7028" y1="1.3462" x2="8.382" y2="2.1082" width="0.2032" layer="21"/>
<wire x1="8.382" y1="2.1082" x2="5.334" y2="0.0254" width="0.2032" layer="21"/>
<wire x1="5.334" y1="0.0254" x2="2.9972" y2="2.3622" width="0.2032" layer="21"/>
<wire x1="2.9972" y1="2.3622" x2="5.1308" y2="5.5118" width="0.2032" layer="21"/>
<wire x1="5.1308" y1="5.5118" x2="3.81" y2="8.8392" width="0.2032" layer="21" curve="-19.434368"/>
<wire x1="3.81" y1="8.8392" x2="0.0254" y2="9.5504" width="0.2032" layer="21"/>
<wire x1="0.0254" y1="9.5504" x2="0.0254" y2="12.8778" width="0.2032" layer="21"/>
<wire x1="0.0254" y1="12.8778" x2="3.8862" y2="13.589" width="0.2032" layer="21"/>
<wire x1="3.8862" y1="13.589" x2="5.2578" y2="16.7386" width="0.2032" layer="21" curve="-18.899701"/>
<wire x1="5.2578" y1="16.7386" x2="2.9972" y2="20.0406" width="0.2032" layer="21"/>
<wire x1="2.9972" y1="20.0406" x2="5.334" y2="22.3774" width="0.2032" layer="21"/>
<wire x1="5.334" y1="22.3774" x2="8.6614" y2="20.1168" width="0.2032" layer="21"/>
<wire x1="8.6614" y1="20.1168" x2="11.7602" y2="21.3614" width="0.2032" layer="21" curve="-18.029416"/>
<wire x1="11.7602" y1="21.3614" x2="12.5222" y2="25.3746" width="0.2032" layer="21"/>
<wire x1="12.5222" y1="25.3746" x2="15.8242" y2="25.3746" width="0.2032" layer="21"/>
<wire x1="15.8242" y1="25.3746" x2="16.5608" y2="21.3868" width="0.2032" layer="21"/>
<wire x1="16.5608" y1="21.3868" x2="19.6596" y2="20.1168" width="0.2032" layer="21" curve="-18.239355"/>
<wire x1="19.6596" y1="20.1168" x2="23.0124" y2="22.3774" width="0.2032" layer="21"/>
<wire x1="23.0124" y1="22.3774" x2="25.3492" y2="20.0406" width="0.2032" layer="21"/>
<wire x1="25.3492" y1="20.0406" x2="23.114" y2="16.764" width="0.2032" layer="21"/>
<wire x1="23.114" y1="16.764" x2="24.4856" y2="13.589" width="0.2032" layer="21" curve="-18.8999"/>
<wire x1="24.4856" y1="13.589" x2="28.321" y2="12.8778" width="0.2032" layer="21"/>
<wire x1="28.321" y1="12.8778" x2="28.321" y2="9.5504" width="0.2032" layer="21"/>
<wire x1="28.321" y1="9.5504" x2="24.5364" y2="8.8392" width="0.2032" layer="21"/>
<wire x1="24.5364" y1="8.8392" x2="23.2156" y2="5.5118" width="0.2032" layer="21" curve="-19.212623"/>
<wire x1="23.2156" y1="5.5118" x2="25.3492" y2="2.3622" width="0.2032" layer="21"/>
<wire x1="25.3492" y1="2.3622" x2="23.0124" y2="0.0254" width="0.2032" layer="21"/>
<wire x1="23.0124" y1="0.0254" x2="19.939" y2="2.1336" width="0.2032" layer="21"/>
<wire x1="19.939" y1="2.1336" x2="18.6182" y2="1.3716" width="0.2032" layer="21"/>
<wire x1="18.6182" y1="1.3716" x2="16.2052" y2="7.1628" width="0.2032" layer="21"/>
<wire x1="12.1158" y1="7.1628" x2="16.2052" y2="7.1374" width="0.2032" layer="21" curve="-304.652273"/>
<polygon width="0.1524" layer="21">
<vertex x="9.652" y="1.4732"/>
<vertex x="8.3566" y="2.2098"/>
<vertex x="5.334" y="0.1016"/>
<vertex x="3.0988" y="2.3876"/>
<vertex x="5.1816" y="5.5118"/>
<vertex x="3.7338" y="8.9408"/>
<vertex x="0.0508" y="9.6266"/>
<vertex x="0.1016" y="12.8524"/>
<vertex x="3.8608" y="13.5636"/>
<vertex x="5.334" y="16.891"/>
<vertex x="3.0988" y="20.066"/>
<vertex x="5.334" y="22.3012"/>
<vertex x="8.6106" y="20.0914"/>
<vertex x="11.811" y="21.3868"/>
<vertex x="12.5476" y="25.3238"/>
<vertex x="15.8242" y="25.2984"/>
<vertex x="16.5354" y="21.4376"/>
<vertex x="19.685" y="20.0914"/>
<vertex x="23.0124" y="22.3012"/>
<vertex x="25.273" y="20.0406"/>
<vertex x="23.0378" y="16.7386"/>
<vertex x="24.3586" y="13.8938"/>
<vertex x="24.4602" y="13.5128"/>
<vertex x="28.2194" y="12.827"/>
<vertex x="28.2448" y="9.6012"/>
<vertex x="24.511" y="8.9154"/>
<vertex x="23.1902" y="5.4356"/>
<vertex x="25.273" y="2.3622"/>
<vertex x="23.0124" y="0.1016"/>
<vertex x="19.9136" y="2.2606"/>
<vertex x="18.6182" y="1.4478"/>
<vertex x="16.2814" y="7.1374"/>
<vertex x="17.7292" y="8.2042"/>
<vertex x="18.6944" y="10.3124"/>
<vertex x="18.4912" y="12.5984"/>
<vertex x="16.9926" y="14.6812"/>
<vertex x="16.9672" y="14.6558"/>
<vertex x="14.9606" y="15.5448"/>
<vertex x="13.4366" y="15.5702"/>
<vertex x="13.4366" y="15.5194"/>
<vertex x="11.43" y="14.6304"/>
<vertex x="9.9822" y="12.8016"/>
<vertex x="9.6774" y="10.4394"/>
<vertex x="10.5156" y="8.382"/>
<vertex x="11.9634" y="7.112"/>
</polygon>
</package>
<package name="OSHW_FILLX100_NOTEXT">
<wire x1="1.2192" y1="0.7112" x2="0.9652" y2="0.127" width="0.1524" layer="21"/>
<wire x1="0.9652" y1="0.127" x2="0.8382" y2="0.2032" width="0.1524" layer="21"/>
<wire x1="0.8382" y1="0.2032" x2="0.5334" y2="0" width="0.1524" layer="21"/>
<wire x1="0.5334" y1="0" x2="0.3048" y2="0.2286" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="0.2286" x2="0.508" y2="0.5588" width="0.1524" layer="21"/>
<wire x1="0.508" y1="0.5588" x2="0.381" y2="0.889" width="0.1524" layer="21"/>
<wire x1="0.381" y1="0.889" x2="0" y2="0.9652" width="0.1524" layer="21"/>
<wire x1="0" y1="0.9652" x2="0" y2="1.2954" width="0.1524" layer="21"/>
<wire x1="0" y1="1.2954" x2="0.381" y2="1.3462" width="0.1524" layer="21"/>
<wire x1="0.381" y1="1.3462" x2="0.5334" y2="1.6764" width="0.1524" layer="21"/>
<wire x1="0.5334" y1="1.6764" x2="0.3048" y2="2.0066" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="2.0066" x2="0.5334" y2="2.2352" width="0.1524" layer="21"/>
<wire x1="0.5334" y1="2.2352" x2="0.8636" y2="2.0066" width="0.1524" layer="21"/>
<wire x1="0.8636" y1="2.0066" x2="1.1684" y2="2.1336" width="0.1524" layer="21"/>
<wire x1="1.1684" y1="2.1336" x2="1.2446" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.2446" y1="2.54" x2="1.5748" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.5748" y1="2.54" x2="1.651" y2="2.1336" width="0.1524" layer="21"/>
<wire x1="1.651" y1="2.1336" x2="1.9558" y2="2.0066" width="0.1524" layer="21"/>
<wire x1="1.9558" y1="2.0066" x2="2.3114" y2="2.2352" width="0.1524" layer="21"/>
<wire x1="2.3114" y1="2.2352" x2="2.54" y2="2.0066" width="0.1524" layer="21"/>
<wire x1="2.54" y1="2.0066" x2="2.3114" y2="1.6764" width="0.1524" layer="21"/>
<wire x1="2.3114" y1="1.6764" x2="2.4384" y2="1.3716" width="0.1524" layer="21"/>
<wire x1="2.4384" y1="1.3716" x2="2.8194" y2="1.2954" width="0.1524" layer="21"/>
<wire x1="2.8194" y1="1.2954" x2="2.8194" y2="0.9652" width="0.1524" layer="21"/>
<wire x1="2.8194" y1="0.9652" x2="2.4638" y2="0.889" width="0.1524" layer="21"/>
<wire x1="2.4638" y1="0.889" x2="2.3114" y2="0.5588" width="0.1524" layer="21"/>
<wire x1="2.3114" y1="0.5588" x2="2.54" y2="0.2286" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.2286" x2="2.3114" y2="0" width="0.1524" layer="21"/>
<wire x1="2.3114" y1="0" x2="2.0066" y2="0.2032" width="0.1524" layer="21"/>
<wire x1="2.0066" y1="0.2032" x2="1.8542" y2="0.127" width="0.1524" layer="21"/>
<wire x1="1.8542" y1="0.127" x2="1.6256" y2="0.7112" width="0.1524" layer="21"/>
<wire x1="1.2192" y1="0.7112" x2="1.6256" y2="0.7112" width="0.1524" layer="21" curve="-306.869898"/>
<polygon width="0.1524" layer="21">
<vertex x="0.889" y="0.254"/>
<vertex x="0.508" y="0.127"/>
<vertex x="0.381" y="0.254"/>
<vertex x="0.635" y="0.635"/>
<vertex x="0.381" y="1.016"/>
<vertex x="0.127" y="1.016"/>
<vertex x="0.127" y="1.27"/>
<vertex x="0.508" y="1.27"/>
<vertex x="0.635" y="1.651"/>
<vertex x="0.381" y="2.032"/>
<vertex x="0.508" y="2.159"/>
<vertex x="0.889" y="1.905"/>
<vertex x="1.27" y="2.159"/>
<vertex x="1.27" y="2.413"/>
<vertex x="1.524" y="2.413"/>
<vertex x="1.651" y="2.032"/>
<vertex x="2.032" y="1.905"/>
<vertex x="2.286" y="2.159"/>
<vertex x="2.413" y="2.032"/>
<vertex x="2.286" y="1.651"/>
<vertex x="2.413" y="1.27"/>
<vertex x="2.794" y="1.27"/>
<vertex x="2.667" y="1.016"/>
<vertex x="2.286" y="0.889"/>
<vertex x="2.286" y="0.508"/>
<vertex x="2.413" y="0.254"/>
<vertex x="2.286" y="0.127"/>
<vertex x="2.032" y="0.254"/>
<vertex x="1.905" y="0.254"/>
<vertex x="1.778" y="0.635"/>
<vertex x="2.032" y="1.143"/>
<vertex x="1.778" y="1.524"/>
<vertex x="1.397" y="1.651"/>
<vertex x="1.016" y="1.524"/>
<vertex x="0.889" y="1.143"/>
<vertex x="1.016" y="0.762"/>
<vertex x="1.143" y="0.635"/>
</polygon>
</package>
<package name="OSHW_FILLX150_NOTEXT">
<wire x1="1.8288" y1="1.0668" x2="1.4478" y2="0.2032" width="0.1524" layer="21"/>
<wire x1="1.4478" y1="0.2032" x2="1.27" y2="0.3048" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.3048" x2="0.8128" y2="0" width="0.1524" layer="21"/>
<wire x1="0.8128" y1="0" x2="0.4572" y2="0.3556" width="0.1524" layer="21"/>
<wire x1="0.4572" y1="0.3556" x2="0.762" y2="0.8128" width="0.1524" layer="21"/>
<wire x1="0.5588" y1="1.3208" x2="0" y2="1.4224" width="0.1524" layer="21"/>
<wire x1="0" y1="1.4224" x2="0" y2="1.9304" width="0.1524" layer="21"/>
<wire x1="0" y1="1.9304" x2="0.5842" y2="2.032" width="0.1524" layer="21"/>
<wire x1="0.7874" y1="2.5146" x2="0.4572" y2="2.9972" width="0.1524" layer="21"/>
<wire x1="0.4572" y1="2.9972" x2="0.8128" y2="3.3528" width="0.1524" layer="21"/>
<wire x1="0.8128" y1="3.3528" x2="1.2954" y2="3.0226" width="0.1524" layer="21"/>
<wire x1="1.7526" y1="3.2004" x2="1.8796" y2="3.81" width="0.1524" layer="21"/>
<wire x1="1.8796" y1="3.81" x2="2.3622" y2="3.81" width="0.1524" layer="21"/>
<wire x1="2.3622" y1="3.81" x2="2.4892" y2="3.2004" width="0.1524" layer="21"/>
<wire x1="2.9464" y1="3.0226" x2="3.4544" y2="3.3528" width="0.1524" layer="21"/>
<wire x1="3.4544" y1="3.3528" x2="3.81" y2="2.9972" width="0.1524" layer="21"/>
<wire x1="3.81" y1="2.9972" x2="3.4544" y2="2.5146" width="0.1524" layer="21"/>
<wire x1="3.683" y1="2.032" x2="4.2418" y2="1.9304" width="0.1524" layer="21"/>
<wire x1="4.2418" y1="1.9304" x2="4.2418" y2="1.4224" width="0.1524" layer="21"/>
<wire x1="4.2418" y1="1.4224" x2="3.683" y2="1.3208" width="0.1524" layer="21"/>
<wire x1="3.4798" y1="0.8128" x2="3.81" y2="0.3556" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.3556" x2="3.4544" y2="0" width="0.1524" layer="21"/>
<wire x1="3.4544" y1="0" x2="2.9972" y2="0.3048" width="0.1524" layer="21"/>
<wire x1="2.9972" y1="0.3048" x2="2.794" y2="0.2032" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0.2032" x2="2.4384" y2="1.0668" width="0.1524" layer="21"/>
<wire x1="1.8288" y1="1.0668" x2="2.4384" y2="1.0668" width="0.1524" layer="21" curve="-307.809152"/>
<wire x1="0.7681" y1="0.825" x2="0.5654" y2="1.2802" width="0.1524" layer="21" curve="-18.384503"/>
<wire x1="0.5796" y1="2.0376" x2="0.7859" y2="2.5141" width="0.1524" layer="21" curve="-18.74246"/>
<wire x1="1.3015" y1="3.0119" x2="1.7496" y2="3.1933" width="0.1524" layer="21" curve="-17.515118"/>
<wire x1="2.4892" y1="3.204" x2="2.9444" y2="3.0226" width="0.1524" layer="21" curve="-17.788236"/>
<wire x1="3.4671" y1="2.5105" x2="3.6876" y2="2.034" width="0.1524" layer="21" curve="-19.769635"/>
<wire x1="3.6805" y1="1.3228" x2="3.4778" y2="0.8179" width="0.1524" layer="21" curve="-19.717465"/>
<polygon width="0.1524" layer="21">
<vertex x="1.397" y="0.381"/>
<vertex x="1.27" y="0.381"/>
<vertex x="0.889" y="0.127"/>
<vertex x="0.635" y="0.381"/>
<vertex x="0.889" y="0.762"/>
<vertex x="0.635" y="1.397"/>
<vertex x="0.127" y="1.524"/>
<vertex x="0.127" y="1.905"/>
<vertex x="0.635" y="1.905"/>
<vertex x="0.889" y="2.54"/>
<vertex x="0.508" y="3.048"/>
<vertex x="0.889" y="3.302"/>
<vertex x="1.27" y="2.921"/>
<vertex x="1.778" y="3.175"/>
<vertex x="1.905" y="3.683"/>
<vertex x="2.286" y="3.683"/>
<vertex x="2.413" y="3.175"/>
<vertex x="2.921" y="2.921"/>
<vertex x="3.429" y="3.302"/>
<vertex x="3.683" y="3.048"/>
<vertex x="3.429" y="2.54"/>
<vertex x="3.683" y="1.905"/>
<vertex x="4.191" y="1.905"/>
<vertex x="4.191" y="1.524"/>
<vertex x="3.556" y="1.397"/>
<vertex x="3.429" y="0.762"/>
<vertex x="3.683" y="0.381"/>
<vertex x="3.429" y="0.127"/>
<vertex x="3.048" y="0.381"/>
<vertex x="2.794" y="0.254"/>
<vertex x="2.54" y="1.016"/>
<vertex x="2.921" y="1.524"/>
<vertex x="2.794" y="2.159"/>
<vertex x="2.286" y="2.413"/>
<vertex x="1.778" y="2.413"/>
<vertex x="1.397" y="2.032"/>
<vertex x="1.397" y="1.27"/>
<vertex x="1.778" y="1.016"/>
</polygon>
</package>
<package name="OSHW_FILLX200_NOTEXT">
<wire x1="2.4384" y1="1.4224" x2="1.9304" y2="0.2794" width="0.1524" layer="21"/>
<wire x1="1.9304" y1="0.2794" x2="1.6764" y2="0.4318" width="0.1524" layer="21"/>
<wire x1="1.6764" y1="0.4318" x2="1.0668" y2="0" width="0.1524" layer="21"/>
<wire x1="1.0668" y1="0" x2="0.6096" y2="0.4826" width="0.1524" layer="21"/>
<wire x1="0.6096" y1="0.4826" x2="1.016" y2="1.0922" width="0.1524" layer="21"/>
<wire x1="0.762" y1="1.778" x2="0" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0" y2="2.5654" width="0.1524" layer="21"/>
<wire x1="0" y1="2.5654" x2="0.762" y2="2.7178" width="0.1524" layer="21"/>
<wire x1="1.0414" y1="3.3528" x2="0.5842" y2="4.0132" width="0.1524" layer="21"/>
<wire x1="0.5842" y1="4.0132" x2="1.0668" y2="4.4704" width="0.1524" layer="21"/>
<wire x1="1.0668" y1="4.4704" x2="1.7272" y2="4.0132" width="0.1524" layer="21"/>
<wire x1="2.3622" y1="4.2672" x2="2.4892" y2="5.08" width="0.1524" layer="21"/>
<wire x1="2.4892" y1="5.08" x2="3.175" y2="5.08" width="0.1524" layer="21"/>
<wire x1="3.175" y1="5.08" x2="3.302" y2="4.2672" width="0.1524" layer="21"/>
<wire x1="3.937" y1="4.0132" x2="4.5974" y2="4.4704" width="0.1524" layer="21"/>
<wire x1="4.5974" y1="4.4704" x2="5.08" y2="4.0132" width="0.1524" layer="21"/>
<wire x1="5.08" y1="4.0132" x2="4.6228" y2="3.3528" width="0.1524" layer="21"/>
<wire x1="4.9022" y1="2.7178" x2="5.6642" y2="2.5654" width="0.1524" layer="21"/>
<wire x1="5.6642" y1="2.5654" x2="5.6642" y2="1.905" width="0.1524" layer="21"/>
<wire x1="5.6642" y1="1.905" x2="4.9022" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.6482" y1="1.0922" x2="5.08" y2="0.4826" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.4826" x2="4.5974" y2="0" width="0.1524" layer="21"/>
<wire x1="4.5974" y1="0" x2="3.9878" y2="0.4318" width="0.1524" layer="21"/>
<wire x1="3.9878" y1="0.4318" x2="3.7338" y2="0.2794" width="0.1524" layer="21"/>
<wire x1="3.7338" y1="0.2794" x2="3.2512" y2="1.4224" width="0.1524" layer="21"/>
<wire x1="2.4384" y1="1.4478" x2="3.2512" y2="1.4478" width="0.1524" layer="21" curve="-308.267286"/>
<wire x1="1.0245" y1="1.1004" x2="0.7637" y2="1.774" width="0.1524" layer="21" curve="-19.673848"/>
<wire x1="0.7779" y1="2.7179" x2="1.0435" y2="3.3392" width="0.1524" layer="21" curve="-18.371859"/>
<wire x1="1.736" y1="4.0223" x2="2.3432" y2="4.2689" width="0.1524" layer="21" curve="-17.83212"/>
<wire x1="3.3155" y1="4.2736" x2="3.9464" y2="4.0175" width="0.1524" layer="21" curve="-18.449318"/>
<wire x1="4.6199" y1="3.3487" x2="4.8997" y2="2.7179" width="0.1524" layer="21" curve="-19.303177"/>
<wire x1="4.9045" y1="1.7692" x2="4.6484" y2="1.0909" width="0.1524" layer="21" curve="-20.344696"/>
<polygon width="0.1524" layer="21">
<vertex x="1.651" y="0.508"/>
<vertex x="1.143" y="0.127"/>
<vertex x="0.762" y="0.508"/>
<vertex x="1.143" y="1.143"/>
<vertex x="0.889" y="1.778"/>
<vertex x="0.127" y="2.032"/>
<vertex x="0.127" y="1.905"/>
<vertex x="0.127" y="2.54"/>
<vertex x="0.889" y="2.667"/>
<vertex x="1.143" y="3.429"/>
<vertex x="0.635" y="3.937"/>
<vertex x="1.016" y="4.318"/>
<vertex x="1.778" y="3.937"/>
<vertex x="2.54" y="4.318"/>
<vertex x="2.413" y="4.318"/>
<vertex x="2.54" y="4.953"/>
<vertex x="3.175" y="4.953"/>
<vertex x="3.175" y="4.191"/>
<vertex x="3.937" y="3.937"/>
<vertex x="4.572" y="4.318"/>
<vertex x="4.953" y="4.064"/>
<vertex x="4.572" y="3.302"/>
<vertex x="4.826" y="2.667"/>
<vertex x="5.588" y="2.54"/>
<vertex x="5.588" y="1.905"/>
<vertex x="4.826" y="1.905"/>
<vertex x="4.572" y="1.016"/>
<vertex x="4.953" y="0.508"/>
<vertex x="4.572" y="0.127"/>
<vertex x="4.064" y="0.508"/>
<vertex x="3.81" y="0.381"/>
<vertex x="3.302" y="1.397"/>
<vertex x="3.81" y="1.905"/>
<vertex x="3.81" y="2.54"/>
<vertex x="3.429" y="3.175"/>
<vertex x="2.794" y="3.302"/>
<vertex x="2.032" y="3.048"/>
<vertex x="2.159" y="3.048"/>
<vertex x="1.778" y="2.413"/>
<vertex x="2.032" y="1.651"/>
<vertex x="2.286" y="1.397"/>
<vertex x="1.905" y="0.381"/>
</polygon>
</package>
<package name="OSHW_FILLX350_NOTEXT">
<wire x1="4.2418" y1="2.5146" x2="3.4036" y2="0.4826" width="0.2032" layer="21"/>
<wire x1="3.4036" y1="0.4826" x2="2.9464" y2="0.7366" width="0.2032" layer="21"/>
<wire x1="2.9464" y1="0.7366" x2="1.8796" y2="0" width="0.2032" layer="21"/>
<wire x1="1.8796" y1="0" x2="1.0414" y2="0.8382" width="0.2032" layer="21"/>
<wire x1="1.0414" y1="0.8382" x2="1.8034" y2="1.9304" width="0.2032" layer="21"/>
<wire x1="1.3208" y1="3.0988" x2="0" y2="3.3528" width="0.2032" layer="21"/>
<wire x1="0" y1="3.3528" x2="0" y2="4.4958" width="0.2032" layer="21"/>
<wire x1="0" y1="4.4958" x2="1.3462" y2="4.7498" width="0.2032" layer="21"/>
<wire x1="1.8288" y1="5.8674" x2="1.0414" y2="7.0104" width="0.2032" layer="21"/>
<wire x1="1.0414" y1="7.0104" x2="1.8796" y2="7.8232" width="0.2032" layer="21"/>
<wire x1="1.8796" y1="7.8232" x2="3.0226" y2="7.0358" width="0.2032" layer="21"/>
<wire x1="4.1148" y1="7.493" x2="4.3688" y2="8.8646" width="0.2032" layer="21"/>
<wire x1="4.3688" y1="8.8646" x2="5.5372" y2="8.8646" width="0.2032" layer="21"/>
<wire x1="5.5372" y1="8.8646" x2="5.7912" y2="7.493" width="0.2032" layer="21"/>
<wire x1="6.8834" y1="7.0358" x2="8.0518" y2="7.8232" width="0.2032" layer="21"/>
<wire x1="8.0518" y1="7.8232" x2="8.8646" y2="7.0104" width="0.2032" layer="21"/>
<wire x1="8.8646" y1="7.0104" x2="8.0772" y2="5.8674" width="0.2032" layer="21"/>
<wire x1="8.5598" y1="4.7498" x2="9.906" y2="4.4958" width="0.2032" layer="21"/>
<wire x1="9.906" y1="4.4958" x2="9.906" y2="3.3528" width="0.2032" layer="21"/>
<wire x1="9.906" y1="3.3528" x2="8.5852" y2="3.0988" width="0.2032" layer="21"/>
<wire x1="8.128" y1="1.9304" x2="8.8646" y2="0.8382" width="0.2032" layer="21"/>
<wire x1="8.8646" y1="0.8382" x2="8.0518" y2="0.0254" width="0.2032" layer="21"/>
<wire x1="8.0518" y1="0.0254" x2="6.985" y2="0.7366" width="0.2032" layer="21"/>
<wire x1="6.985" y1="0.7366" x2="6.5278" y2="0.4826" width="0.2032" layer="21"/>
<wire x1="6.5278" y1="0.4826" x2="5.6896" y2="2.5146" width="0.2032" layer="21"/>
<wire x1="1.8034" y1="1.9304" x2="1.3462" y2="3.0988" width="0.2032" layer="21" curve="-19.550442"/>
<wire x1="1.3462" y1="4.7498" x2="1.8288" y2="5.8674" width="0.2032" layer="21" curve="-18.924644"/>
<wire x1="3.0226" y1="7.0358" x2="4.1148" y2="7.4676" width="0.2032" layer="21" curve="-18.163939"/>
<wire x1="5.7912" y1="7.4676" x2="6.858" y2="7.0358" width="0.2032" layer="21" curve="-17.87913"/>
<wire x1="8.0772" y1="5.8674" x2="8.5598" y2="4.7498" width="0.2032" layer="21" curve="-18.854515"/>
<wire x1="8.5852" y1="3.0988" x2="8.128" y2="1.9304" width="0.2032" layer="21" curve="-19.550442"/>
<wire x1="4.2418" y1="2.5146" x2="5.6896" y2="2.5146" width="0.2032" layer="21" curve="-303.462974"/>
<polygon width="0.1524" layer="21">
<vertex x="3.429" y="0.5842"/>
<vertex x="2.921" y="0.8636"/>
<vertex x="1.8796" y="0.1016"/>
<vertex x="1.1176" y="0.8382"/>
<vertex x="1.8796" y="1.9304"/>
<vertex x="1.3716" y="3.1496"/>
<vertex x="0.0254" y="3.4036"/>
<vertex x="0.0508" y="4.4704"/>
<vertex x="1.4224" y="4.7244"/>
<vertex x="1.905" y="5.9182"/>
<vertex x="1.1684" y="7.0358"/>
<vertex x="1.905" y="7.6962"/>
<vertex x="2.9972" y="6.9342"/>
<vertex x="4.1656" y="7.4168"/>
<vertex x="4.445" y="8.7884"/>
<vertex x="5.4864" y="8.8138"/>
<vertex x="5.715" y="7.4168"/>
<vertex x="6.9088" y="6.985"/>
<vertex x="8.0518" y="7.7216"/>
<vertex x="8.763" y="6.985"/>
<vertex x="7.9756" y="5.8674"/>
<vertex x="8.5344" y="4.6736"/>
<vertex x="9.8552" y="4.4196"/>
<vertex x="9.8298" y="3.3782"/>
<vertex x="8.509" y="3.1496"/>
<vertex x="8.0772" y="1.905"/>
<vertex x="8.7884" y="0.8128"/>
<vertex x="8.0264" y="0.1016"/>
<vertex x="6.985" y="0.7874"/>
<vertex x="6.5532" y="0.5842"/>
<vertex x="5.7658" y="2.5146"/>
<vertex x="6.5278" y="3.2766"/>
<vertex x="6.5024" y="4.572"/>
<vertex x="5.5118" y="5.461"/>
<vertex x="4.318" y="5.4356"/>
<vertex x="3.4544" y="4.5466"/>
<vertex x="3.429" y="3.2258"/>
<vertex x="4.1402" y="2.4638"/>
</polygon>
</package>
<package name="OSHW_FILLX500_NOTEXT">
<wire x1="6.0452" y1="3.5814" x2="4.8514" y2="0.6858" width="0.2032" layer="21"/>
<wire x1="4.8514" y1="0.6858" x2="4.191" y2="1.0668" width="0.2032" layer="21"/>
<wire x1="4.191" y1="1.0668" x2="2.667" y2="0.0254" width="0.2032" layer="21"/>
<wire x1="2.667" y1="0.0254" x2="1.4986" y2="1.1938" width="0.2032" layer="21"/>
<wire x1="1.4986" y1="1.1938" x2="2.5654" y2="2.7686" width="0.2032" layer="21"/>
<wire x1="2.5654" y1="2.7686" x2="1.905" y2="4.4196" width="0.2032" layer="21" curve="-19.514051"/>
<wire x1="1.905" y1="4.4196" x2="0" y2="4.7752" width="0.2032" layer="21"/>
<wire x1="0" y1="4.7752" x2="0" y2="6.4262" width="0.2032" layer="21"/>
<wire x1="0" y1="6.4262" x2="1.9304" y2="6.8072" width="0.2032" layer="21"/>
<wire x1="1.9304" y1="6.8072" x2="2.6162" y2="8.382" width="0.2032" layer="21" curve="-18.62956"/>
<wire x1="2.6162" y1="8.382" x2="1.4986" y2="10.0076" width="0.2032" layer="21"/>
<wire x1="1.4986" y1="10.0076" x2="2.667" y2="11.176" width="0.2032" layer="21"/>
<wire x1="2.667" y1="11.176" x2="4.3434" y2="10.0584" width="0.2032" layer="21"/>
<wire x1="4.3434" y1="10.0584" x2="5.8674" y2="10.668" width="0.2032" layer="21" curve="-17.898916"/>
<wire x1="5.8674" y1="10.6934" x2="6.2484" y2="12.6746" width="0.2032" layer="21"/>
<wire x1="6.2484" y1="12.6746" x2="7.8994" y2="12.6746" width="0.2032" layer="21"/>
<wire x1="7.8994" y1="12.6746" x2="8.2804" y2="10.6934" width="0.2032" layer="21"/>
<wire x1="8.2804" y1="10.6934" x2="9.8298" y2="10.0584" width="0.2032" layer="21" curve="-18.098861"/>
<wire x1="9.8298" y1="10.0584" x2="11.5062" y2="11.2014" width="0.2032" layer="21"/>
<wire x1="11.5062" y1="11.2014" x2="12.6746" y2="10.0076" width="0.2032" layer="21"/>
<wire x1="12.6746" y1="10.0076" x2="11.557" y2="8.382" width="0.2032" layer="21"/>
<wire x1="11.557" y1="8.382" x2="12.2428" y2="6.8072" width="0.2032" layer="21" curve="-18.775953"/>
<wire x1="12.2428" y1="6.8072" x2="14.1732" y2="6.4262" width="0.2032" layer="21"/>
<wire x1="14.1732" y1="6.4262" x2="14.1732" y2="4.7752" width="0.2032" layer="21"/>
<wire x1="14.1732" y1="4.7752" x2="12.2682" y2="4.4196" width="0.2032" layer="21"/>
<wire x1="12.2682" y1="4.4196" x2="11.6078" y2="2.7432" width="0.2032" layer="21" curve="-19.70616"/>
<wire x1="11.6078" y1="2.7432" x2="12.6746" y2="1.1684" width="0.2032" layer="21"/>
<wire x1="12.6746" y1="1.1684" x2="11.5062" y2="0.0254" width="0.2032" layer="21"/>
<wire x1="11.5062" y1="0.0254" x2="9.9822" y2="1.0668" width="0.2032" layer="21"/>
<wire x1="9.9822" y1="1.0668" x2="9.3218" y2="0.6858" width="0.2032" layer="21"/>
<wire x1="9.3218" y1="0.6858" x2="8.1026" y2="3.5814" width="0.2032" layer="21"/>
<wire x1="6.0452" y1="3.5814" x2="8.1026" y2="3.6068" width="0.2032" layer="21" curve="-305.419449"/>
<polygon width="0.1524" layer="21">
<vertex x="4.826" y="0.7874"/>
<vertex x="4.191" y="1.2192"/>
<vertex x="2.6416" y="0.127"/>
<vertex x="1.5748" y="1.1938"/>
<vertex x="2.667" y="2.7686"/>
<vertex x="1.905" y="4.5212"/>
<vertex x="-0.0254" y="4.8514"/>
<vertex x="0.0508" y="6.4262"/>
<vertex x="1.9812" y="6.731"/>
<vertex x="2.667" y="8.4328"/>
<vertex x="1.6256" y="10.033"/>
<vertex x="2.667" y="11.0744"/>
<vertex x="4.3688" y="9.9822"/>
<vertex x="5.9182" y="10.6426"/>
<vertex x="6.2992" y="12.5984"/>
<vertex x="7.874" y="12.573"/>
<vertex x="8.2296" y="10.6172"/>
<vertex x="9.8552" y="9.9822"/>
<vertex x="11.5316" y="11.1252"/>
<vertex x="12.5984" y="9.9822"/>
<vertex x="11.5062" y="8.382"/>
<vertex x="12.2428" y="6.7056"/>
<vertex x="14.1732" y="6.35"/>
<vertex x="14.1224" y="4.826"/>
<vertex x="12.1158" y="4.4704"/>
<vertex x="11.5824" y="2.5908"/>
<vertex x="12.573" y="1.1938"/>
<vertex x="11.5062" y="0.127"/>
<vertex x="9.9568" y="1.1684"/>
<vertex x="9.3726" y="0.8128"/>
<vertex x="8.2042" y="3.6068"/>
<vertex x="9.1948" y="4.5466"/>
<vertex x="9.3726" y="5.9944"/>
<vertex x="8.6868" y="7.366"/>
<vertex x="7.1374" y="7.9502"/>
<vertex x="5.6642" y="7.5438"/>
<vertex x="4.826" y="6.4516"/>
<vertex x="4.7244" y="5.1816"/>
<vertex x="5.2832" y="3.9624"/>
<vertex x="5.9436" y="3.5052"/>
</polygon>
</package>
<package name="OSHW_FILLX750_NOTEXT">
<wire x1="9.0932" y1="5.3848" x2="7.2898" y2="1.016" width="0.2032" layer="21"/>
<wire x1="7.2898" y1="1.016" x2="6.2992" y2="1.5748" width="0.2032" layer="21"/>
<wire x1="6.2992" y1="1.5748" x2="4.0132" y2="0.0254" width="0.2032" layer="21"/>
<wire x1="4.0132" y1="0.0254" x2="2.2352" y2="1.778" width="0.2032" layer="21"/>
<wire x1="2.2352" y1="1.778" x2="3.8354" y2="4.1402" width="0.2032" layer="21"/>
<wire x1="3.8354" y1="4.1402" x2="2.8448" y2="6.6294" width="0.2032" layer="21" curve="-19.510952"/>
<wire x1="2.8448" y1="6.6294" x2="0" y2="7.1628" width="0.2032" layer="21"/>
<wire x1="0" y1="7.1628" x2="0" y2="9.652" width="0.2032" layer="21"/>
<wire x1="0" y1="9.652" x2="2.921" y2="10.2108" width="0.2032" layer="21"/>
<wire x1="2.921" y1="10.2108" x2="3.9116" y2="12.573" width="0.2032" layer="21" curve="-18.759575"/>
<wire x1="3.9116" y1="12.573" x2="2.2352" y2="15.0368" width="0.2032" layer="21"/>
<wire x1="2.2352" y1="15.0368" x2="4.0132" y2="16.7894" width="0.2032" layer="21"/>
<wire x1="4.0132" y1="16.7894" x2="6.5024" y2="15.0876" width="0.2032" layer="21"/>
<wire x1="6.5024" y1="15.0876" x2="8.8392" y2="16.0528" width="0.2032" layer="21" curve="-18.180554"/>
<wire x1="8.8392" y1="16.0528" x2="9.3726" y2="19.0246" width="0.2032" layer="21"/>
<wire x1="9.3726" y1="19.0246" x2="11.8872" y2="19.0246" width="0.2032" layer="21"/>
<wire x1="11.8872" y1="19.0246" x2="12.4206" y2="16.0274" width="0.2032" layer="21"/>
<wire x1="12.4206" y1="16.0274" x2="14.7574" y2="15.0876" width="0.2032" layer="21" curve="-18.180554"/>
<wire x1="14.7574" y1="15.0876" x2="17.2466" y2="16.7894" width="0.2032" layer="21"/>
<wire x1="17.2466" y1="16.7894" x2="18.9992" y2="15.0368" width="0.2032" layer="21"/>
<wire x1="18.9992" y1="15.0368" x2="17.3228" y2="12.5476" width="0.2032" layer="21"/>
<wire x1="17.3228" y1="12.5476" x2="18.3388" y2="10.1854" width="0.2032" layer="21" curve="-18.56522"/>
<wire x1="18.3388" y1="10.1854" x2="21.2344" y2="9.652" width="0.2032" layer="21"/>
<wire x1="21.2344" y1="9.652" x2="21.209" y2="7.1628" width="0.2032" layer="21"/>
<wire x1="21.209" y1="7.1628" x2="18.3896" y2="6.6294" width="0.2032" layer="21"/>
<wire x1="18.3896" y1="6.6294" x2="17.4244" y2="4.1402" width="0.2032" layer="21" curve="-19.377573"/>
<wire x1="17.4244" y1="4.1402" x2="18.9992" y2="1.778" width="0.2032" layer="21"/>
<wire x1="18.9992" y1="1.778" x2="17.2466" y2="0.0254" width="0.2032" layer="21"/>
<wire x1="17.2466" y1="0.0254" x2="14.9606" y2="1.6002" width="0.2032" layer="21"/>
<wire x1="14.9606" y1="1.6002" x2="13.97" y2="1.016" width="0.2032" layer="21"/>
<wire x1="13.97" y1="1.016" x2="12.1666" y2="5.3848" width="0.2032" layer="21"/>
<wire x1="9.0932" y1="5.3848" x2="12.1666" y2="5.3848" width="0.2032" layer="21" curve="-304.090059"/>
<polygon width="0.1524" layer="21">
<vertex x="7.2898" y="1.1176"/>
<vertex x="6.2484" y="1.7018"/>
<vertex x="4.0132" y="0.0508"/>
<vertex x="2.286" y="1.8288"/>
<vertex x="3.937" y="4.1656"/>
<vertex x="2.7686" y="6.7564"/>
<vertex x="0.1016" y="7.2644"/>
<vertex x="0.0762" y="9.5758"/>
<vertex x="2.9718" y="10.1854"/>
<vertex x="3.9878" y="12.7"/>
<vertex x="2.3368" y="15.0368"/>
<vertex x="4.0132" y="16.7386"/>
<vertex x="6.477" y="15.0114"/>
<vertex x="8.9154" y="16.0528"/>
<vertex x="9.4488" y="18.9484"/>
<vertex x="11.7856" y="18.9484"/>
<vertex x="12.3698" y="15.9258"/>
<vertex x="14.8082" y="15.0368"/>
<vertex x="17.2212" y="16.6624"/>
<vertex x="18.923" y="15.0368"/>
<vertex x="17.2212" y="12.5476"/>
<vertex x="18.3642" y="10.0838"/>
<vertex x="21.1582" y="9.5758"/>
<vertex x="21.1328" y="7.2136"/>
<vertex x="18.3896" y="6.7056"/>
<vertex x="17.3228" y="4.064"/>
<vertex x="18.8722" y="1.778"/>
<vertex x="17.2466" y="0.1524"/>
<vertex x="14.9352" y="1.7272"/>
<vertex x="14.0208" y="1.1684"/>
<vertex x="12.319" y="5.3594"/>
<vertex x="13.716" y="6.731"/>
<vertex x="14.0208" y="8.7884"/>
<vertex x="13.3604" y="10.3632"/>
<vertex x="11.9126" y="11.4808"/>
<vertex x="10.287" y="11.6586"/>
<vertex x="8.8646" y="11.2522"/>
<vertex x="7.8232" y="10.2616"/>
<vertex x="7.2136" y="8.763"/>
<vertex x="7.4676" y="6.9342"/>
<vertex x="8.1788" y="5.9182"/>
<vertex x="8.9916" y="5.3086"/>
</polygon>
</package>
<package name="OSHW_FILLX70_NOTEXT">
<wire x1="0.8382" y1="0.508" x2="0.6858" y2="0.1016" width="0.1524" layer="21"/>
<wire x1="0.6858" y1="0.1016" x2="0.5842" y2="0.1524" width="0.1524" layer="21"/>
<wire x1="0.5842" y1="0.1524" x2="0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="0.381" y1="0" x2="0.2032" y2="0.1524" width="0.1524" layer="21"/>
<wire x1="0.2032" y1="0.1524" x2="0.3556" y2="0.381" width="0.1524" layer="21"/>
<wire x1="0.3556" y1="0.381" x2="0.254" y2="0.6096" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.6096" x2="0" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="0" y1="0.6604" x2="0" y2="0.889" width="0.1524" layer="21"/>
<wire x1="0" y1="0.889" x2="0.2794" y2="0.9398" width="0.1524" layer="21"/>
<wire x1="0.2794" y1="0.9398" x2="0.3556" y2="1.1684" width="0.1524" layer="21"/>
<wire x1="0.3556" y1="1.1684" x2="0.2032" y2="1.397" width="0.1524" layer="21"/>
<wire x1="0.2032" y1="1.397" x2="0.381" y2="1.5748" width="0.1524" layer="21"/>
<wire x1="0.381" y1="1.5748" x2="0.6096" y2="1.397" width="0.1524" layer="21"/>
<wire x1="0.6096" y1="1.397" x2="0.8128" y2="1.4986" width="0.1524" layer="21"/>
<wire x1="0.8128" y1="1.4986" x2="0.889" y2="1.778" width="0.1524" layer="21"/>
<wire x1="0.889" y1="1.778" x2="1.1176" y2="1.778" width="0.1524" layer="21"/>
<wire x1="1.1176" y1="1.778" x2="1.1684" y2="1.4986" width="0.1524" layer="21"/>
<wire x1="1.1684" y1="1.4986" x2="1.3716" y2="1.397" width="0.1524" layer="21"/>
<wire x1="1.3716" y1="1.397" x2="1.6002" y2="1.5748" width="0.1524" layer="21"/>
<wire x1="1.6002" y1="1.5748" x2="1.778" y2="1.397" width="0.1524" layer="21"/>
<wire x1="1.778" y1="1.397" x2="1.6256" y2="1.1684" width="0.1524" layer="21"/>
<wire x1="1.6256" y1="1.1684" x2="1.7272" y2="0.9398" width="0.1524" layer="21"/>
<wire x1="1.7272" y1="0.9398" x2="1.9812" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.9812" y1="0.889" x2="1.9812" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="1.9812" y1="0.6604" x2="1.7272" y2="0.6096" width="0.1524" layer="21"/>
<wire x1="1.7272" y1="0.6096" x2="1.6256" y2="0.381" width="0.1524" layer="21"/>
<wire x1="1.6256" y1="0.381" x2="1.778" y2="0.1524" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.1524" x2="1.6002" y2="0" width="0.1524" layer="21"/>
<wire x1="1.6002" y1="0" x2="1.397" y2="0.1524" width="0.1524" layer="21"/>
<wire x1="1.397" y1="0.1524" x2="1.2954" y2="0.1016" width="0.1524" layer="21"/>
<wire x1="1.2954" y1="0.1016" x2="1.143" y2="0.508" width="0.1524" layer="21"/>
<wire x1="0.8382" y1="0.508" x2="1.143" y2="0.508" width="0.1524" layer="21" curve="-300.510237"/>
<polygon width="0.1524" layer="21">
<vertex x="0.635" y="0.2032"/>
<vertex x="0.4064" y="0.0762"/>
<vertex x="0.2794" y="0.1524"/>
<vertex x="0.4318" y="0.4064"/>
<vertex x="0.3048" y="0.6858"/>
<vertex x="0.0508" y="0.7366"/>
<vertex x="0.0508" y="0.8128"/>
<vertex x="0.3048" y="0.9144"/>
<vertex x="0.4064" y="1.1938"/>
<vertex x="0.2794" y="1.4224"/>
<vertex x="0.381" y="1.4986"/>
<vertex x="0.5842" y="1.3462"/>
<vertex x="0.8636" y="1.4986"/>
<vertex x="0.9398" y="1.7272"/>
<vertex x="1.0668" y="1.7272"/>
<vertex x="1.143" y="1.4478"/>
<vertex x="1.3716" y="1.3462"/>
<vertex x="1.5748" y="1.4986"/>
<vertex x="1.7018" y="1.397"/>
<vertex x="1.5748" y="1.1938"/>
<vertex x="1.7018" y="0.889"/>
<vertex x="1.905" y="0.8382"/>
<vertex x="1.9304" y="0.7112"/>
<vertex x="1.7018" y="0.6604"/>
<vertex x="1.5748" y="0.381"/>
<vertex x="1.6764" y="0.1778"/>
<vertex x="1.5748" y="0.1016"/>
<vertex x="1.3462" y="0.2286"/>
<vertex x="1.27" y="0.4826"/>
<vertex x="1.4224" y="0.7366"/>
<vertex x="1.3208" y="1.016"/>
<vertex x="1.0414" y="1.1684"/>
<vertex x="0.7112" y="1.0668"/>
<vertex x="0.6096" y="0.7366"/>
<vertex x="0.7112" y="0.4572"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="OSHW_LOGO">
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="-5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<text x="-2.54" y="0.254" size="1.27" layer="94">OSHW</text>
<text x="-2.413" y="-1.524" size="1.27" layer="94">LOGO</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="OSHW_LOGO_FILL" prefix="LOGO">
<gates>
<gate name="G$1" symbol="OSHW_LOGO" x="0" y="0"/>
</gates>
<devices>
<device name="X1000-NT" package="OSHW_FILLX1000_NOTEXT">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="X0100-NT" package="OSHW_FILLX100_NOTEXT">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="X0150-NT" package="OSHW_FILLX150_NOTEXT">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="X0200-NT" package="OSHW_FILLX200_NOTEXT">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="X0350-NT" package="OSHW_FILLX350_NOTEXT">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="X0500-NT" package="OSHW_FILLX500_NOTEXT">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="X0750-NT" package="OSHW_FILLX750_NOTEXT">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="X0070-NT" package="OSHW_FILLX70_NOTEXT">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
<class number="1" name="Vcc" width="0" drill="0">
</class>
<class number="2" name="GND" width="0" drill="0">
</class>
</classes>
<parts>
<part name="GND2" library="SparkFun" deviceset="GND" device=""/>
<part name="U$6" library="SparkFun" deviceset="5V" device=""/>
<part name="U$9" library="SparkFun" deviceset="5V" device=""/>
<part name="U$11" library="SparkFun" deviceset="5V" device=""/>
<part name="USB1" library="akizuki" deviceset="USB-MINI-B-TH" device=""/>
<part name="GND4" library="SparkFun" deviceset="GND" device=""/>
<part name="GND5" library="SparkFun" deviceset="GND" device=""/>
<part name="C1" library="rc-master" deviceset="C_" device="C025-040X018" value="0.1u"/>
<part name="C3" library="rc-master" deviceset="C_" device="C050-075X018" value="0.47uF"/>
<part name="F1" library="SparkFun-PowerIC" deviceset="PTC" device="PTH"/>
<part name="POT1" library="general" deviceset="POT" device=""/>
<part name="POT2" library="general" deviceset="POT" device=""/>
<part name="POT3" library="general" deviceset="POT" device=""/>
<part name="U$1" library="SparkFun" deviceset="5V" device=""/>
<part name="GND1" library="SparkFun" deviceset="GND" device=""/>
<part name="POT4" library="general" deviceset="POT" device=""/>
<part name="IC1" library="dm9_microcontroller" deviceset="PIC18F2X50" device="-E/SP"/>
<part name="R2" library="rc-master" deviceset="R_" device="X0204/5" value="1k"/>
<part name="R3" library="rc-master" deviceset="R_" device="X0204/5" value="1k"/>
<part name="R4" library="rc-master" deviceset="R_" device="X0204/5" value="1k"/>
<part name="R5" library="rc-master" deviceset="R_" device="X0204/5" value="1k"/>
<part name="GND3" library="SparkFun" deviceset="GND" device=""/>
<part name="U$2" library="SparkFun" deviceset="5V" device=""/>
<part name="R1" library="rc-master" deviceset="R_" device="X0204/5" value="10k"/>
<part name="GND7" library="SparkFun" deviceset="GND" device=""/>
<part name="GND8" library="SparkFun" deviceset="GND" device=""/>
<part name="C2" library="rc-master" deviceset="CP_" device="E-020X050" value="22uF"/>
<part name="U$4" library="SparkFun" deviceset="5V" device=""/>
<part name="D1" library="general" deviceset="DIODE" device="1N4148" value="1N4148"/>
<part name="D2" library="general" deviceset="DIODE" device="1N4148" value="1N4148"/>
<part name="D3" library="general" deviceset="DIODE" device="1N4148" value="1N4148"/>
<part name="D4" library="general" deviceset="DIODE" device="1N4148" value="1N4148"/>
<part name="SW2" library="general" deviceset="TAC_SWITCH" device="_12MM"/>
<part name="SW3" library="general" deviceset="TAC_SWITCH" device="_12MM"/>
<part name="SW4" library="general" deviceset="TAC_SWITCH" device="_12MM"/>
<part name="SW5" library="general" deviceset="TAC_SWITCH" device="_12MM"/>
<part name="FRAME1" library="frames" deviceset="DINA4_L" device=""/>
<part name="SW1" library="general" deviceset="SPST" device="_SIDEPUSH"/>
<part name="LED1" library="general" deviceset="LED" device="_2X5"/>
<part name="LED2" library="general" deviceset="LED" device="_2X5"/>
<part name="LED3" library="general" deviceset="LED" device="_2X5"/>
<part name="LED4" library="general" deviceset="LED" device="_2X5"/>
<part name="JP1" library="general" deviceset="M04" device="LONGPADS"/>
<part name="JP2" library="general" deviceset="M04" device="LONGPADS"/>
<part name="JP3" library="general" deviceset="M06" device="SIP"/>
<part name="LOGO2" library="dp_devices" deviceset="LOGO_CC-BY-SA" device=""/>
<part name="LOGO1" library="ohw-logo" deviceset="OSHW_LOGO_FILL" device="X0200-NT"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="GND2" gate="1" x="-116.84" y="81.28"/>
<instance part="U$6" gate="G$1" x="-104.14" y="142.24"/>
<instance part="U$9" gate="G$1" x="-116.84" y="157.48" rot="MR0"/>
<instance part="U$11" gate="G$1" x="-116.84" y="99.06"/>
<instance part="USB1" gate="G$1" x="-134.62" y="127"/>
<instance part="GND4" gate="1" x="-116.84" y="111.76" rot="MR0"/>
<instance part="GND5" gate="1" x="-104.14" y="111.76"/>
<instance part="C1" gate="G$1" x="-116.84" y="91.44" rot="R90"/>
<instance part="C3" gate="G$1" x="-104.14" y="121.92" rot="R90"/>
<instance part="F1" gate="G$1" x="-116.84" y="147.32" rot="R90"/>
<instance part="POT1" gate="G$1" x="43.18" y="144.78" rot="R180"/>
<instance part="POT2" gate="G$1" x="58.42" y="144.78" rot="R180"/>
<instance part="POT3" gate="G$1" x="73.66" y="144.78" rot="R180"/>
<instance part="U$1" gate="G$1" x="43.18" y="157.48"/>
<instance part="GND1" gate="1" x="43.18" y="129.54"/>
<instance part="POT4" gate="G$1" x="88.9" y="144.78" rot="R180"/>
<instance part="IC1" gate="IC" x="-66.04" y="142.24"/>
<instance part="R2" gate="G$1" x="88.9" y="104.14"/>
<instance part="R3" gate="G$1" x="88.9" y="93.98"/>
<instance part="R4" gate="G$1" x="88.9" y="83.82"/>
<instance part="R5" gate="G$1" x="88.9" y="73.66"/>
<instance part="GND3" gate="1" x="-134.62" y="165.1" rot="MR0"/>
<instance part="U$2" gate="G$1" x="-111.76" y="185.42" rot="MR0"/>
<instance part="R1" gate="G$1" x="-111.76" y="177.8" rot="R90"/>
<instance part="GND7" gate="1" x="-2.54" y="121.92"/>
<instance part="GND8" gate="1" x="109.22" y="60.96"/>
<instance part="C2" gate="G$1" x="-106.68" y="91.44" rot="R90"/>
<instance part="U$4" gate="G$1" x="-2.54" y="144.78"/>
<instance part="D1" gate="G$1" x="35.56" y="104.14" rot="MR180"/>
<instance part="D2" gate="G$1" x="35.56" y="91.44" rot="MR180"/>
<instance part="D3" gate="G$1" x="35.56" y="78.74" rot="MR180"/>
<instance part="D4" gate="G$1" x="35.56" y="66.04" rot="MR180"/>
<instance part="SW2" gate="S" x="22.86" y="104.14"/>
<instance part="SW3" gate="S" x="22.86" y="91.44"/>
<instance part="SW4" gate="S" x="22.86" y="78.74"/>
<instance part="SW5" gate="S" x="22.86" y="66.04"/>
<instance part="FRAME1" gate="G$1" x="-147.32" y="20.32"/>
<instance part="FRAME1" gate="G$2" x="15.24" y="20.32"/>
<instance part="SW1" gate="G$1" x="-124.46" y="170.18"/>
<instance part="LED1" gate="G$1" x="99.06" y="104.14" rot="R90"/>
<instance part="LED2" gate="G$1" x="99.06" y="93.98" rot="R90"/>
<instance part="LED3" gate="G$1" x="99.06" y="83.82" rot="R90"/>
<instance part="LED4" gate="G$1" x="99.06" y="73.66" rot="R90"/>
<instance part="JP1" gate="G$1" x="-55.88" y="91.44" rot="R180"/>
<instance part="JP2" gate="G$1" x="-55.88" y="73.66" rot="R180"/>
<instance part="JP3" gate="G$1" x="7.62" y="134.62" rot="R180"/>
<instance part="LOGO2" gate="LOGO" x="0" y="30.48"/>
<instance part="LOGO1" gate="G$1" x="-25.4" y="30.48"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="2">
<segment>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="-116.84" y1="88.9" x2="-116.84" y2="83.82" width="0.1524" layer="91"/>
<wire x1="-116.84" y1="83.82" x2="-106.68" y2="83.82" width="0.1524" layer="91"/>
<wire x1="-106.68" y1="83.82" x2="-106.68" y2="88.9" width="0.1524" layer="91"/>
<junction x="-116.84" y="83.82"/>
<pinref part="C1" gate="G$1" pin="1"/>
<pinref part="C2" gate="G$1" pin="-"/>
</segment>
<segment>
<pinref part="USB1" gate="G$1" pin="GND"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="-121.92" y1="129.54" x2="-116.84" y2="129.54" width="0.1524" layer="91"/>
<wire x1="-116.84" y1="129.54" x2="-116.84" y2="114.3" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="-104.14" y1="119.38" x2="-104.14" y2="116.84" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="1"/>
<pinref part="IC1" gate="IC" pin="VSS@8"/>
<wire x1="-104.14" y1="116.84" x2="-104.14" y2="114.3" width="0.1524" layer="91"/>
<wire x1="-96.52" y1="116.84" x2="-104.14" y2="116.84" width="0.1524" layer="91"/>
<junction x="-104.14" y="116.84"/>
<pinref part="IC1" gate="IC" pin="VSS@19"/>
<wire x1="-96.52" y1="114.3" x2="-104.14" y2="114.3" width="0.1524" layer="91"/>
<junction x="-104.14" y="114.3"/>
</segment>
<segment>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="43.18" y1="134.62" x2="43.18" y2="132.08" width="0.1524" layer="91"/>
<wire x1="43.18" y1="134.62" x2="58.42" y2="134.62" width="0.1524" layer="91"/>
<pinref part="POT1" gate="G$1" pin="E"/>
<wire x1="58.42" y1="134.62" x2="73.66" y2="134.62" width="0.1524" layer="91"/>
<wire x1="73.66" y1="134.62" x2="88.9" y2="134.62" width="0.1524" layer="91"/>
<wire x1="43.18" y1="137.16" x2="43.18" y2="134.62" width="0.1524" layer="91"/>
<junction x="43.18" y="134.62"/>
<pinref part="POT2" gate="G$1" pin="E"/>
<wire x1="58.42" y1="137.16" x2="58.42" y2="134.62" width="0.1524" layer="91"/>
<junction x="58.42" y="134.62"/>
<pinref part="POT3" gate="G$1" pin="E"/>
<wire x1="73.66" y1="137.16" x2="73.66" y2="134.62" width="0.1524" layer="91"/>
<junction x="73.66" y="134.62"/>
<pinref part="POT4" gate="G$1" pin="E"/>
<wire x1="88.9" y1="137.16" x2="88.9" y2="134.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="-129.54" y1="170.18" x2="-134.62" y2="170.18" width="0.1524" layer="91"/>
<wire x1="-134.62" y1="170.18" x2="-134.62" y2="167.64" width="0.1524" layer="91"/>
<pinref part="SW1" gate="G$1" pin="P"/>
</segment>
<segment>
<wire x1="2.54" y1="134.62" x2="-2.54" y2="134.62" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="134.62" x2="-2.54" y2="124.46" width="0.1524" layer="91"/>
<pinref part="GND7" gate="1" pin="GND"/>
<pinref part="JP3" gate="G$1" pin="3"/>
</segment>
<segment>
<wire x1="104.14" y1="104.14" x2="109.22" y2="104.14" width="0.1524" layer="91"/>
<wire x1="109.22" y1="104.14" x2="109.22" y2="93.98" width="0.1524" layer="91"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="109.22" y1="93.98" x2="109.22" y2="83.82" width="0.1524" layer="91"/>
<wire x1="109.22" y1="83.82" x2="109.22" y2="73.66" width="0.1524" layer="91"/>
<wire x1="109.22" y1="73.66" x2="109.22" y2="63.5" width="0.1524" layer="91"/>
<wire x1="109.22" y1="93.98" x2="104.14" y2="93.98" width="0.1524" layer="91"/>
<junction x="109.22" y="93.98"/>
<wire x1="109.22" y1="83.82" x2="104.14" y2="83.82" width="0.1524" layer="91"/>
<junction x="109.22" y="83.82"/>
<wire x1="109.22" y1="73.66" x2="104.14" y2="73.66" width="0.1524" layer="91"/>
<junction x="109.22" y="73.66"/>
<pinref part="LED1" gate="G$1" pin="C"/>
<pinref part="LED2" gate="G$1" pin="C"/>
<pinref part="LED3" gate="G$1" pin="C"/>
<pinref part="LED4" gate="G$1" pin="C"/>
</segment>
</net>
<net name="5V" class="2">
<segment>
<wire x1="-106.68" y1="99.06" x2="-106.68" y2="93.98" width="0.1524" layer="91"/>
<wire x1="-116.84" y1="99.06" x2="-106.68" y2="99.06" width="0.1524" layer="91"/>
<junction x="-116.84" y="99.06"/>
<pinref part="U$11" gate="G$1" pin="5V"/>
<wire x1="-116.84" y1="99.06" x2="-116.84" y2="93.98" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="2"/>
<pinref part="C2" gate="G$1" pin="+"/>
</segment>
<segment>
<pinref part="U$9" gate="G$1" pin="5V"/>
<wire x1="-116.84" y1="157.48" x2="-116.84" y2="154.94" width="0.1524" layer="91"/>
<pinref part="F1" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="5V"/>
<wire x1="43.18" y1="157.48" x2="43.18" y2="154.94" width="0.1524" layer="91"/>
<wire x1="43.18" y1="154.94" x2="58.42" y2="154.94" width="0.1524" layer="91"/>
<pinref part="POT1" gate="G$1" pin="A"/>
<wire x1="58.42" y1="154.94" x2="73.66" y2="154.94" width="0.1524" layer="91"/>
<wire x1="73.66" y1="154.94" x2="88.9" y2="154.94" width="0.1524" layer="91"/>
<wire x1="43.18" y1="154.94" x2="43.18" y2="152.4" width="0.1524" layer="91"/>
<junction x="43.18" y="154.94"/>
<pinref part="POT2" gate="G$1" pin="A"/>
<wire x1="58.42" y1="152.4" x2="58.42" y2="154.94" width="0.1524" layer="91"/>
<junction x="58.42" y="154.94"/>
<pinref part="POT3" gate="G$1" pin="A"/>
<wire x1="73.66" y1="152.4" x2="73.66" y2="154.94" width="0.1524" layer="91"/>
<junction x="73.66" y="154.94"/>
<pinref part="POT4" gate="G$1" pin="A"/>
<wire x1="88.9" y1="152.4" x2="88.9" y2="154.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC1" gate="IC" pin="VDD"/>
<pinref part="U$6" gate="G$1" pin="5V"/>
<wire x1="-96.52" y1="137.16" x2="-104.14" y2="137.16" width="0.1524" layer="91"/>
<wire x1="-104.14" y1="137.16" x2="-104.14" y2="142.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<pinref part="U$2" gate="G$1" pin="5V"/>
<wire x1="-111.76" y1="185.42" x2="-111.76" y2="182.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="5V"/>
<wire x1="2.54" y1="137.16" x2="-2.54" y2="137.16" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="137.16" x2="-2.54" y2="144.78" width="0.1524" layer="91"/>
<pinref part="JP3" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="USB1" gate="G$1" pin="VBUS"/>
<wire x1="-121.92" y1="139.7" x2="-116.84" y2="139.7" width="0.1524" layer="91"/>
<wire x1="-116.84" y1="139.7" x2="-116.84" y2="142.24" width="0.1524" layer="91"/>
<pinref part="F1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<wire x1="-104.14" y1="127" x2="-104.14" y2="124.46" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="2"/>
<pinref part="IC1" gate="IC" pin="VUSB3V3"/>
<wire x1="-104.14" y1="127" x2="-96.52" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="USB1" gate="G$1" pin="D+"/>
<wire x1="-121.92" y1="134.62" x2="-109.22" y2="134.62" width="0.1524" layer="91"/>
<wire x1="-109.22" y1="134.62" x2="-109.22" y2="157.48" width="0.1524" layer="91"/>
<pinref part="IC1" gate="IC" pin="D+/IOCC5"/>
<wire x1="-109.22" y1="157.48" x2="-96.52" y2="157.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="USB1" gate="G$1" pin="D-"/>
<wire x1="-121.92" y1="137.16" x2="-111.76" y2="137.16" width="0.1524" layer="91"/>
<wire x1="-111.76" y1="137.16" x2="-111.76" y2="149.86" width="0.1524" layer="91"/>
<pinref part="IC1" gate="IC" pin="D-/IOCC4"/>
<wire x1="-111.76" y1="149.86" x2="-96.52" y2="149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="93.98" y1="104.14" x2="96.52" y2="104.14" width="0.1524" layer="91"/>
<pinref part="LED1" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="93.98" y1="93.98" x2="96.52" y2="93.98" width="0.1524" layer="91"/>
<pinref part="LED2" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="93.98" y1="83.82" x2="96.52" y2="83.82" width="0.1524" layer="91"/>
<pinref part="LED3" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="96.52" y1="73.66" x2="93.98" y2="73.66" width="0.1524" layer="91"/>
<pinref part="LED4" gate="G$1" pin="A"/>
</segment>
</net>
<net name="MCLR" class="0">
<segment>
<pinref part="IC1" gate="IC" pin="MCLR/VPP/RE3"/>
<wire x1="-96.52" y1="170.18" x2="-111.76" y2="170.18" width="0.1524" layer="91"/>
<wire x1="-111.76" y1="170.18" x2="-119.38" y2="170.18" width="0.1524" layer="91"/>
<wire x1="-111.76" y1="172.72" x2="-111.76" y2="170.18" width="0.1524" layer="91"/>
<junction x="-111.76" y="170.18"/>
<pinref part="R1" gate="G$1" pin="1"/>
<label x="-106.68" y="170.18" size="1.778" layer="95"/>
<pinref part="SW1" gate="G$1" pin="S"/>
</segment>
<segment>
<wire x1="2.54" y1="139.7" x2="-10.16" y2="139.7" width="0.1524" layer="91"/>
<label x="-10.16" y="139.7" size="1.778" layer="95"/>
<pinref part="JP3" gate="G$1" pin="1"/>
</segment>
</net>
<net name="POT1" class="0">
<segment>
<pinref part="POT1" gate="G$1" pin="S"/>
<wire x1="38.1" y1="144.78" x2="30.48" y2="144.78" width="0.1524" layer="91"/>
<label x="30.48" y="144.78" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-33.02" y1="144.78" x2="-15.24" y2="144.78" width="0.1524" layer="91"/>
<label x="-25.4" y="144.78" size="1.778" layer="95"/>
<pinref part="IC1" gate="IC" pin="RB1/AN10/SCK/SCL/INT1"/>
</segment>
</net>
<net name="POT2" class="0">
<segment>
<pinref part="POT2" gate="G$1" pin="S"/>
<wire x1="53.34" y1="144.78" x2="45.72" y2="144.78" width="0.1524" layer="91"/>
<label x="45.72" y="144.78" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-33.02" y1="147.32" x2="-15.24" y2="147.32" width="0.1524" layer="91"/>
<label x="-25.4" y="147.32" size="1.778" layer="95"/>
<pinref part="IC1" gate="IC" pin="RB0/AN12/SDI/SDA/INT0"/>
</segment>
</net>
<net name="POT3" class="0">
<segment>
<pinref part="POT3" gate="G$1" pin="S"/>
<wire x1="68.58" y1="144.78" x2="60.96" y2="144.78" width="0.1524" layer="91"/>
<label x="60.96" y="144.78" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC1" gate="IC" pin="RA5/AN4/_SS"/>
<wire x1="-33.02" y1="157.48" x2="-15.24" y2="157.48" width="0.1524" layer="91"/>
<label x="-25.4" y="157.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="POT4" class="0">
<segment>
<pinref part="POT4" gate="G$1" pin="S"/>
<wire x1="83.82" y1="144.78" x2="76.2" y2="144.78" width="0.1524" layer="91"/>
<label x="76.2" y="144.78" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC1" gate="IC" pin="RC2/AN14/IOCC2"/>
<wire x1="-33.02" y1="119.38" x2="-17.78" y2="119.38" width="0.1524" layer="91"/>
<label x="-25.4" y="119.38" size="1.778" layer="95"/>
</segment>
</net>
<net name="SW_ROW1" class="0">
<segment>
<wire x1="12.7" y1="66.04" x2="12.7" y2="78.74" width="0.1524" layer="91"/>
<wire x1="12.7" y1="78.74" x2="12.7" y2="91.44" width="0.1524" layer="91"/>
<wire x1="12.7" y1="91.44" x2="12.7" y2="104.14" width="0.1524" layer="91"/>
<junction x="12.7" y="91.44"/>
<junction x="12.7" y="78.74"/>
<wire x1="12.7" y1="104.14" x2="-5.08" y2="104.14" width="0.1524" layer="91"/>
<junction x="12.7" y="104.14"/>
<label x="-5.08" y="104.14" size="1.778" layer="95"/>
<pinref part="SW2" gate="S" pin="1"/>
<wire x1="17.78" y1="104.14" x2="12.7" y2="104.14" width="0.1524" layer="91"/>
<pinref part="SW2" gate="S" pin="2"/>
<wire x1="17.78" y1="101.6" x2="17.78" y2="104.14" width="0.1524" layer="91"/>
<junction x="17.78" y="104.14"/>
<pinref part="SW3" gate="S" pin="1"/>
<pinref part="SW3" gate="S" pin="2"/>
<wire x1="17.78" y1="91.44" x2="17.78" y2="88.9" width="0.1524" layer="91"/>
<wire x1="17.78" y1="91.44" x2="12.7" y2="91.44" width="0.1524" layer="91"/>
<junction x="17.78" y="91.44"/>
<pinref part="SW4" gate="S" pin="1"/>
<wire x1="12.7" y1="78.74" x2="17.78" y2="78.74" width="0.1524" layer="91"/>
<pinref part="SW4" gate="S" pin="2"/>
<wire x1="17.78" y1="76.2" x2="17.78" y2="78.74" width="0.1524" layer="91"/>
<junction x="17.78" y="78.74"/>
<pinref part="SW5" gate="S" pin="1"/>
<wire x1="12.7" y1="66.04" x2="17.78" y2="66.04" width="0.1524" layer="91"/>
<pinref part="SW5" gate="S" pin="2"/>
<wire x1="17.78" y1="66.04" x2="17.78" y2="63.5" width="0.1524" layer="91"/>
<junction x="17.78" y="66.04"/>
</segment>
<segment>
<label x="-25.4" y="170.18" size="1.778" layer="95"/>
<wire x1="-33.02" y1="170.18" x2="-15.24" y2="170.18" width="0.1524" layer="91"/>
<pinref part="IC1" gate="IC" pin="RA0/AN0"/>
</segment>
</net>
<net name="SW_ROW5" class="0">
<segment>
<wire x1="-60.96" y1="68.58" x2="-78.74" y2="68.58" width="0.1524" layer="91"/>
<label x="-78.74" y="68.58" size="1.778" layer="95"/>
<pinref part="JP2" gate="G$1" pin="4"/>
</segment>
<segment>
<label x="-25.4" y="160.02" size="1.778" layer="95"/>
<wire x1="-33.02" y1="160.02" x2="-15.24" y2="160.02" width="0.1524" layer="91"/>
<pinref part="IC1" gate="IC" pin="RA4"/>
</segment>
</net>
<net name="SW_ROW2" class="0">
<segment>
<wire x1="-60.96" y1="76.2" x2="-78.74" y2="76.2" width="0.1524" layer="91"/>
<label x="-78.74" y="76.2" size="1.778" layer="95"/>
<pinref part="JP2" gate="G$1" pin="1"/>
</segment>
<segment>
<label x="-25.4" y="167.64" size="1.778" layer="95"/>
<wire x1="-33.02" y1="167.64" x2="-15.24" y2="167.64" width="0.1524" layer="91"/>
<pinref part="IC1" gate="IC" pin="RA1/AN1"/>
</segment>
</net>
<net name="LED1" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="83.82" y1="104.14" x2="73.66" y2="104.14" width="0.1524" layer="91"/>
<label x="73.66" y="104.14" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC1" gate="IC" pin="RC6/TX/CK/IOCC6"/>
<wire x1="-33.02" y1="116.84" x2="-17.78" y2="116.84" width="0.1524" layer="91"/>
<label x="-25.4" y="116.84" size="1.778" layer="95"/>
</segment>
</net>
<net name="LED2" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="83.82" y1="93.98" x2="73.66" y2="93.98" width="0.1524" layer="91"/>
<label x="73.66" y="93.98" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC1" gate="IC" pin="RC7/RX/DT/SDO/IOCC7"/>
<wire x1="-17.78" y1="114.3" x2="-33.02" y2="114.3" width="0.1524" layer="91"/>
<label x="-25.4" y="114.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="LED3" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="83.82" y1="83.82" x2="73.66" y2="83.82" width="0.1524" layer="91"/>
<label x="73.66" y="83.82" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC1" gate="IC" pin="RC0/IOCC0"/>
<wire x1="-33.02" y1="124.46" x2="-17.78" y2="124.46" width="0.1524" layer="91"/>
<label x="-25.4" y="124.46" size="1.778" layer="95"/>
</segment>
</net>
<net name="LED4" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="73.66" y1="73.66" x2="83.82" y2="73.66" width="0.1524" layer="91"/>
<label x="73.66" y="73.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC1" gate="IC" pin="RC1/IOCC1"/>
<wire x1="-33.02" y1="121.92" x2="-17.78" y2="121.92" width="0.1524" layer="91"/>
<label x="-25.4" y="121.92" size="1.778" layer="95"/>
</segment>
</net>
<net name="SW_ROW3" class="0">
<segment>
<wire x1="-60.96" y1="73.66" x2="-78.74" y2="73.66" width="0.1524" layer="91"/>
<label x="-78.74" y="73.66" size="1.778" layer="95"/>
<pinref part="JP2" gate="G$1" pin="2"/>
</segment>
<segment>
<label x="-25.4" y="165.1" size="1.778" layer="95"/>
<wire x1="-33.02" y1="165.1" x2="-15.24" y2="165.1" width="0.1524" layer="91"/>
<pinref part="IC1" gate="IC" pin="RA2/AN2/VREF-/DACOUT"/>
</segment>
</net>
<net name="SW_ROW4" class="0">
<segment>
<wire x1="-60.96" y1="71.12" x2="-78.74" y2="71.12" width="0.1524" layer="91"/>
<label x="-78.74" y="71.12" size="1.778" layer="95"/>
<pinref part="JP2" gate="G$1" pin="3"/>
</segment>
<segment>
<label x="-25.4" y="162.56" size="1.778" layer="95"/>
<wire x1="-33.02" y1="162.56" x2="-15.24" y2="162.56" width="0.1524" layer="91"/>
<pinref part="IC1" gate="IC" pin="RA3/AN3/VREF+"/>
</segment>
</net>
<net name="SW_IN1" class="0">
<segment>
<wire x1="-60.96" y1="93.98" x2="-78.74" y2="93.98" width="0.1524" layer="91"/>
<label x="-78.74" y="93.98" size="1.778" layer="95"/>
<pinref part="JP1" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="D1" gate="G$1" pin="C"/>
<wire x1="60.96" y1="104.14" x2="38.1" y2="104.14" width="0.1524" layer="91"/>
<label x="60.96" y="104.14" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="IC1" gate="IC" pin="RB2/AN8/INT2"/>
<wire x1="-33.02" y1="142.24" x2="-15.24" y2="142.24" width="0.1524" layer="91"/>
<label x="-25.4" y="142.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="SW_IN2" class="0">
<segment>
<wire x1="-60.96" y1="91.44" x2="-78.74" y2="91.44" width="0.1524" layer="91"/>
<label x="-78.74" y="91.44" size="1.778" layer="95"/>
<pinref part="JP1" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="60.96" y1="91.44" x2="38.1" y2="91.44" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="C"/>
<label x="60.96" y="91.44" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="IC1" gate="IC" pin="RB4/AN11/IOCB4"/>
<wire x1="-33.02" y1="137.16" x2="-17.78" y2="137.16" width="0.1524" layer="91"/>
<label x="-25.4" y="137.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="SW_IN3" class="0">
<segment>
<wire x1="-78.74" y1="88.9" x2="-60.96" y2="88.9" width="0.1524" layer="91"/>
<label x="-78.74" y="88.9" size="1.778" layer="95"/>
<pinref part="JP1" gate="G$1" pin="3"/>
</segment>
<segment>
<pinref part="D3" gate="G$1" pin="C"/>
<wire x1="60.96" y1="78.74" x2="38.1" y2="78.74" width="0.1524" layer="91"/>
<label x="60.96" y="78.74" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="IC1" gate="IC" pin="RB6/IOCB6/PGC"/>
<wire x1="-33.02" y1="132.08" x2="-12.7" y2="132.08" width="0.1524" layer="91"/>
<label x="-25.4" y="132.08" size="1.778" layer="95"/>
<wire x1="2.54" y1="129.54" x2="-10.16" y2="129.54" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="129.54" x2="-12.7" y2="132.08" width="0.1524" layer="91"/>
<pinref part="JP3" gate="G$1" pin="5"/>
</segment>
</net>
<net name="SW_IN4" class="0">
<segment>
<wire x1="-60.96" y1="86.36" x2="-78.74" y2="86.36" width="0.1524" layer="91"/>
<label x="-78.74" y="86.36" size="1.778" layer="95"/>
<pinref part="JP1" gate="G$1" pin="4"/>
</segment>
<segment>
<wire x1="38.1" y1="66.04" x2="60.96" y2="66.04" width="0.1524" layer="91"/>
<pinref part="D4" gate="G$1" pin="C"/>
<label x="60.96" y="66.04" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="IC1" gate="IC" pin="RB7/IOCB7/PGD"/>
<wire x1="-33.02" y1="129.54" x2="-12.7" y2="129.54" width="0.1524" layer="91"/>
<label x="-25.4" y="129.54" size="1.778" layer="95"/>
<wire x1="-12.7" y1="129.54" x2="-10.16" y2="132.08" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="132.08" x2="2.54" y2="132.08" width="0.1524" layer="91"/>
<pinref part="JP3" gate="G$1" pin="4"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="A"/>
<wire x1="33.02" y1="78.74" x2="27.94" y2="78.74" width="0.1524" layer="91"/>
<pinref part="SW4" gate="S" pin="3"/>
<pinref part="SW4" gate="S" pin="4"/>
<wire x1="27.94" y1="76.2" x2="27.94" y2="78.74" width="0.1524" layer="91"/>
<junction x="27.94" y="78.74"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="D4" gate="G$1" pin="A"/>
<wire x1="33.02" y1="66.04" x2="27.94" y2="66.04" width="0.1524" layer="91"/>
<pinref part="SW5" gate="S" pin="3"/>
<pinref part="SW5" gate="S" pin="4"/>
<wire x1="27.94" y1="66.04" x2="27.94" y2="63.5" width="0.1524" layer="91"/>
<junction x="27.94" y="66.04"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="33.02" y1="91.44" x2="27.94" y2="91.44" width="0.1524" layer="91"/>
<pinref part="SW3" gate="S" pin="3"/>
<pinref part="SW3" gate="S" pin="4"/>
<wire x1="27.94" y1="88.9" x2="27.94" y2="91.44" width="0.1524" layer="91"/>
<junction x="27.94" y="91.44"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="A"/>
<wire x1="33.02" y1="104.14" x2="27.94" y2="104.14" width="0.1524" layer="91"/>
<pinref part="SW2" gate="S" pin="3"/>
<pinref part="SW2" gate="S" pin="4"/>
<wire x1="27.94" y1="101.6" x2="27.94" y2="104.14" width="0.1524" layer="91"/>
<junction x="27.94" y="104.14"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
