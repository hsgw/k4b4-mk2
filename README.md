# k4b4 mk2 - Tiny USB-MIDI Controller Kit#

* Tiny USB-MIDI controller with 4pots, 4buttons and 4LEDs.
* PIC18F25K50
* With USB-HID bootloader based on USB-HID bootloader in [Microchip Libraries for Application](http://www.microchip.com/pagehandler/en-us/devtools/mla/home.html)
* Open Source Hardware


## License ##

Schematic and board data : [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).

USB library and Bootloader : [Microchip Libraries for Application](http://www.microchip.com/pagehandler/en-us/devtools/mla/home.html).


##Copyright##
(c)2015, Takuya Urakawam,Dm9 Records

(c)2015, Microchip Technology Inc. All rights reserved.